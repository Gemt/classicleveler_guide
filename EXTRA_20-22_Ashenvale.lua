-- Author      : G3m7
-- Create Date : 5/5/2019 2:32:24 PM


CLGuide_Ashenvale = {
	Title="20-22 Ashenvale",
	Pinboard = {},
	Steps = {
        {Text="Turn inn The Tower of Althalaxx", Dt={q="The Tower of Althalaxx"}, point={x=2619,y=3872,m=1440},
            PinAdd="If already 20, or extremely close, do the 2 quests here now"},
        {Text="Accept The Tower of Althalaxx", At="The Tower of Althalaxx", point={x=2619,y=3872,m=1440}},
        
        {Text="Run to Astranaar", Proximity=40, point={x=3379,y=4802,m=1440}},
        {Text="Discover the FP", Proximity=15, point={x=3441,y=4799,m=1440}},
        {Text="Accept The Zoram Strand", At="The Zoram Strand", point={x=3467,y=4883,m=1440}},
        {Text="Accept On Guard in Stonetalon", At="On Guard in Stonetalon", point={x=3491,y=4979,m=1440}},
        {Text="Accept Journey to Stonetalon Peak", At="Journey to Stonetalon Peak", point={x=3576,y=4910,m=1440}},
        {Text="Accept Culling the Threat", At="Culling the Threat", point={x=3663,y=4961,m=1440}},
        {Text="Accept Raene's Cleansing", At="Raene's Cleansing", point={x=3663,y=4961,m=1440}},
        
        {Text="Set HS in Astranaar", SetHs="Innkeeper Kimlya", point={x=3698,y=4923,m=1440}}, 
        {Text="Fly to Auberdine", Taxi="Auberdine, Darkshore", PinAdd="Good time to start fixing bank char on second acc etc", point={x=3441,y=4798,m=1440}},
        {Text="Turn inn Beached Sea Creature", Dt={q="Beached Sea Creature"}, point={x=3662,y=4559,m=1439}},
        {Text="Turn inn Beached Sea Turtle", Dt={q="Beached Sea Turtle"}, point={x=3662,y=4559,m=1439}},
        {Text="Turn inn Beached Sea Turtle", Dt={q="Beached Sea Turtle"}, point={x=3662,y=4559,m=1439}},

        {Text="==== RESTOCK On Longjaw Mud Snapper ====", Class="Hunter", BuyItem={Npc="Laird", Item="Longjaw Mud Snapper", Count=20}, point={x=3674,y=4429,m=1439}},
        {Text="Deliver The Absent Minded Prospector", Dt={q="The Absent Minded Prospector"}, point={x=3745,y=4186,m=1439}},
        {Text="Accept The Absent Minded Prospector", At="The Absent Minded Prospector", point={x=3745,y=4186,m=1439}},
        {Text="Deliver Escape Through Force (new belt)", Dt={q="Escape Through Force", Item="Steadfast Cinch", Use=1}, point={x=3937,y=4348,m=1439}},
        {Text="Accept Trek To Ashenvale from mob that comes running into the house", At="Trek to Ashenvale"},

        {Text="Fly to Rut'Theran Village", Taxi="Rut'Theran Village, Teldrassil", point={x=3634,y=4558,m=1439}},
        
        -- darnassus
        {Text="Enter Darnassus", Zone="Darnassus", point={x=5599,y=8966,m=1438}},
        {Text="Bank any cloth you may have", PutInBank={"Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth", "Book: The Powers Below"}, point={x=4003,y=4236,m=1457}},
        {Text="Deliver The Absent Minded Prospector", Dt={q="The Absent Minded Prospector", Item="Windfelt Gloves", Use=1}, point={x=3127,y=8444,m=1457}},
        {Text="Accept The Absent Minded Prospector", At="The Absent Minded Prospector", point={x=3127,y=8444,m=1457}},
        {Text="Buy Heavy Recurve Bow from Bow Merchant (south of warriors terrace, third house left)", 
            Class="Hunter", BuyItem={Npc="Landria", Item="Heavy Recurve Bow", Count=1}, point={x=6326,y=6620,m=1457}, PinAdd="Can train staves again now if forgot earlier"},
        {Text="=== RESTOCK Sharp Arrow (if buying bow) ===", Class="Hunter", BuyItem={Npc="Landria", Item="Sharp Arrow", Count=1600}, point={x=6326,y=6620,m=1457}},
        {Text="Train Hunter skills", TrainSkill=1, point={x=4033,y=880,m=1457}},
        {Text="Train Pet skills (up the ramp)", TrainSkill=1, PinAdd="Remember to put beast talents"},

        -- darnassus => ashenvale
        {Text="Hearthstone to Astranaar", UseItem="Hearthstone", Zone="Ashenvale"},
        {Text="Deliver Trek to Ashenvale", Dt={q="Trek to Ashenvale"}, point={x=3662,y=4958,m=1440}},
        
        {Text="Deliver Raene's Cleansing", Dt={q="Raene's Cleansing"}, point={x=2031,y=4235,m=1440}},
        {Text="Accept Raene's Cleansing", At="Raene's Cleansing", point={x=2031,y=4235,m=1440}},
        {Text="Complete Raene's Cleansing (Grind murlocs until Glowing Gem drops. Careful with oracles (heal full))", Ct="Raene's Cleansing"},
        {Text="(Misthowler) Accept The Ancient Statuette", At="The Ancient Statuette", point={x=1479,y=3129,m=1440}},
        {Text="Loot Ancient Statuette", Ct="The Ancient Statuette", point={x=1419,y=2064}, PinAdd="Need 20 Wrathtail Heads here. Casters are squishy"},
        {Text="Grind until lvl 20 before going to deliver (quest gives 1150 xp)", Lvl={lvl=20}, PinAdd="Remember lvl 20 bow if got"},
        {Text="Deliver The Ancient Statuette", Dt={q="The Ancient Statuette"}, point={x=1479,y=3129,m=1440}},
        {Text="Accept Ruuzel (Req lvl 20)", At="Ruuzel", point={x=1479,y=3129,m=1440}},
        {Text="Kill Ruuzel. Careful with pulls. Priests heal, Myrmidons are tanky. Finish Zoram Strand", Mct={"Ruuzel","The Zoram Strand"}, point={x=783,y=1377,m=1440}},
        {Text="Deliver Ruuzel. Make sure to finish 20 Wrathtail Heads on way back", Dt={q="Ruuzel", Item="Robes of Antiquity", Use=1}, point={x=1479,y=3129,m=1440}},

        {Text="If not already done mastra quests, run to Maestra's Post, Accept Bathran's Hair", At="Bathran's Hair", point={x=2642,y=3854,m=1440}},
        {Text="Grind cultist mobs until Glowing Soul Gem Drops", Ct="The Tower of Althalaxx", point={x=3038,y=3181,m=1440}},
        {Text="Finish Bathran's Hair to the north (bags on ground)", Ct="Bathran's Hair", point={x=3114,y=2470,m=1440}},
        {Text="Deliver Bathran's Hair", Dt={q="Bathran's Hair"}, point={x=2643,y=3860,m=1440}},
        {Text="Accept Orendil's Cure (may need to wait for RP)", At="Orendil's Cure", point={x=2642,y=3854,m=1440}},
        {Text="Deliver The Tower of Althalaxx (skip followup)", Dt={q="The Tower of Althalaxx"}, point={x=2617,y=3870,m=1440}},
        {Text="Accept The Tower of Althalaxx", At="The Tower of Althalaxx", point={x=2619,y=3872,m=1440}},

        
        {Text="Deliver The Zoram Strand", Dt={q="The Zoram Strand"}, point={x=3467,y=4883,m=1440}},
        {Text="Accept Pridewings of Stonetalon", At="Pridewings of Stonetalon", point={x=3467,y=4883,m=1440}},
        
        --{Text="Heartstone to Darkshore", Zone="Darkshore", UseItem="Hearthstone"},
        --{Text="=== RESTOCK Sharp Arrow ===", BuyItem={Npc="Haljan Oakheart", Item="Sharp Arrow", Count=1600}, point={x=3487,y=5087,m=1440}},
        
        {Text="Repair", point={x=3579,y=5210,m=1440}},
        {Text="Deliver Raene's Cleansing", Dt={q="Raene's Cleansing"}, point={x=3663,y=4958,m=1440}, PinAdd="Delete Teronis' Journal from bags"},
        {Text="Accept Raene's Cleansing", At="Raene's Cleansing", point={x=3663,y=4958,m=1440}},
        {Text="Accept An Aggressive Defense", At="An Aggressive Defense", point={x=3663,y=4958,m=1440}},
        
        {Text="Deliver Orendil's Cure", Dt={q="Orendil's Cure"}, point={x=3733,y=5182,m=1440}},
        {Text="Accept Elune's Tear (few seconds of RP)", At="Elune's Tear", point={x=3733,y=5182,m=1440}},
        

        -- run to stonetalon
        {Text="Run into Stonetalon Mountains", Proximity=60, Zone="Stonetalon Mountains", point={x=4301,y=7139,m=1440}},
        {Text="Run through Windshear Crag, past goblin quest", Proximity=50, point={x=5962,y=6958,m=1442}},
        {Text="Deliver On Guard in Stonetalon to Nelf", Dt={q="On Guard in Stonetalon"}, point={x=5991,y=6684,m=1442}},
        {Text="Accept On Guard in Stonetalon from Nelf", At="On Guard in Stonetalon", point={x=5991,y=6684,m=1442}},
        {Text="Deliver On Guard in Stonetalon to Gnome", Dt={q="On Guard in Stonetalon"}, point={x=5951,y=6715,m=1442}},
        {Text="Accept A Gnome's Respite from the Gnome", At="A Gnome's Respite", point={x=5951,y=6715,m=1442}},
        {Text="Accept Super Reaper 6000 at the Goblin hut", At="Super Reaper 6000", point={x=5899,y=6260,m=1442}},
        {Text="Complete the two quests in the crag. Blueprints only from Operators", Mct={"Super Reaper 6000", "A Gnome's Respite"}, point={x=6260,y=5402,m=1442},
            PinAdd="Consider Gerenzo's Orders in cave. Not tested on beta"},

        {Text="Deliver Super Reaper 6000 (skip followup)", Dt={q="Super Reaper 6000"}, point={x=5899,y=6260,m=1442}},
        {Text="Deliver A Gnome's Respite", Dt={q="A Gnome's Respite"}, point={x=5951,y=6715,m=1442}},
        {Text="Accept An Old Colleague", At="An Old Colleague", point={x=5951,y=6715,m=1442}},
        {Text="Accept A Scroll from Mauren", At="A Scroll from Mauren", point={x=5951,y=6715,m=1442}},
        {Text="Run north towards xroads to Charred Vale",Proximity=75, point={x=5146,y=4857,m=1442}},
        {Text="Complete Pridewings of Stonetalon, Wyverns in area NE", Ct="Pridewings of Stonetalon",
            PinAdd="Look out for the lvl 25 rare mob (pridewing patriarch), drops 5agi cloak. Easily killed if full mana/hp"},
        {Text="Run north towards Stonetalon Peak",Proximity=75, point={x=4286,y=1632,m=1442}},
        {Text="Deliver Journey to Stonetalon Peak (skip followup)", Dt={q="Journey to Stonetalon Peak"}, point={x=3711,y=811,m=1442}},
        {Text="DISCOVER FP, then HS Astranaar", UseItem="Hearthstone", Zone="Ashenvale", Taxi="Astranaar, Ashenvale", point={x=3643,y=718,m=1442}},
        
        {Text="Turn inn Pridewings of Stonetalon", Dt={q="Pridewings of Stonetalon"}, point={x=3467,y=4883,m=1440}},

        {Text="Stable pet", Proximity=10, point={x=3651,y=5036,m=1440}},
        --{Text="Make sure to have Elune's Tear (followup from Orendil's Cure)", Proximity=15, point={x=3733,y=5182,m=1440}},
        {Text="Tame an Ashenvale Bear lvl 21-22", Proximity=50, point={x=4422,y=5498,m=1440}},
        {Text="Run north towards Iris Lake", Proximity=50, point={x=4342,y=4585,m=1440}},
        {Text="Complete Elune's Tear (white thing on ground on the isle)", Ct="Elune's Tear", point={x=4623,y=4592,m=1440}},
        {Text="Grind to Dal Bloodclaw. Make sure new Claw is learned", Ct="Culling the Threat", point={x=3773,y=3473,m=1440}},
        {Text="Run back to Astranaar, deliver Culling the Threat", Dt={q="Culling the Threat"}, point={x=3662,y=4958,m=1440}},
        
        {Text="Abandon bear, get pet back from stable master", Proximity=10, point={x=3649,y=5034,m=1440}},
        {Text="Deliver Elune's Tear", Dt={q="Elune's Tear"}, point={x=3734,y=5180,m=1440}},
        {Text="Accept The Ruins of Stardust (few sec of RP)", At="The Ruins of Stardust", point={x=3734,y=5180,m=1440}},
        {Text="Complete The Ruins of Stardust (careful with pulls)", Ct="The Ruins of Stardust", point={x=3434,y=6484,m=1440},
            PinAdd="Train Claw r3 on cat"},

 
        {Text="Run east and Accept Elemental Bracers", At="Elemental Bracers", point={x=4981,y=6722,m=1440}},
        {Text="Loot 5 Intact Elemental Bracer in the lake", Item={Name="Intact Elemental Bracer", Count=5}},
        {Text="Use Divining Scroll to complete Elemental Bracer", Ct="Elemental Bracers", UseItem="Divining Scroll"},
        {Text="Deliver Elemental Bracers (skip followup)", Dt={q="Elemental Bracers"}, point={x=4981,y=6722,m=1440}},
        {Text="Train Hunter skills lvl 22 if reached", TrainSkill=1, point={x=5013,y=6795,m=1440}},
        {Text="=== RESTOCK Sharp Arrow === (Look for 30 bow if lots of gold)", Class="Hunter", BuyItem={Npc="Bhaldaran Ravenshade", Item="Sharp Arrow", Count=1600}, point={x=6326,y=6620,m=1440}},
        {Text="Complete An Aggressive Defense to the north", Ct="An Aggressive Defense", point={x=5069,y=6122,m=1440}},
        {Text="Deliver Raene's Cleansing to the north (skip followup)", Dt={q="Raene's Cleansing"}, point={x=5354,y=4621,m=1440}},


        {Text="Run to Astranaar, Turn inn The Ruins of Stardust (skip followup)", Dt={q="The Ruins of Stardust"}, point={x=3734,y=5180,m=1440},
            PinAdd="Delete Teronis' Journal"},
        {Text="Turn An Aggressive Defense", Dt={q="An Aggressive Defense"}, point={x=3661,y=4959,m=1440}},

        {Text="Complete The Tower of Althalaxx (S-SW of Astranaar)", Ct="The Tower of Althalaxx", point={x=2837,y=6022,m=1440},
            PinAdd="Let pet tank, make sure it has SR trained. Put down freezing trap"},
        {Text="Run to Maestra's Post, turn in Tower of Althalaxx (skip followup)", 
            Dt={q="The Tower of Althalaxx", Item="Staff of the Purifier", Use=1}, point={x=2617,y=3870,m=1440}},

        {Text="Hearthstone/run to astranaar", UseItem="Hearthstone"},
        {Text="Fly to Auberdine", Taxi="Auberdine, Darkshore", point={x=3441,y=4798,m=1440}},
        {Text="RESTOCK On Longjaw Mud Snapper (then boat)", Class="Hunter", BuyItem={Npc="Laird", Item="Longjaw Mud Snapper", Count=20}, point={x=3674,y=4429,m=1439}},
        {Text="Go to section 23-23 Ashenvale-Redridge", NextGuide="23-23 Ashenvale-Redridge"}
        -- boat to EK
    }
}

-- Author      : G3m7
-- Create Date : 6/23/2019 2:42:26 PM

CLGuide_SouthshoreRun = {
    Title="EXTRA Southshore run",
    Pinboard = {},
    Steps = {
        {Text="Accept Fall of Dun Modr (dead guy outside keep)", At="Fall of Dun Modr", point={x=1085,y=5590,m=1437}},
        {Text="Run to Dun'Modr (Arathi border), turn inn Fall of Dun Modr", Dt={q="Fall of Dun Modr"}, point={x=4980,y=1826,m=1437}},
        {Text="Accept A Grim Task", At="A Grim Task", point={x=4980,y=1826,m=1437}},
        {Text="Accept The Thandol Span (on the right)", At="The Thandol Span", point={x=4995,y=1821,m=1437}},

        {Text="Kill Balgaras for A Grim Task. Nuke pet first, then kite Balgaras back to delivery (can be in any camp)", Ct="A Grim Task", point={x=6247,y=2843,m=1437}},
        {Text="Deliver A Grim Task (Make sure to switch correct ring)", Dt={q="A Grim Task", Item="Tranquil Ring", Use=1}, point={x=4980,y=1826,m=1437}},

        {Text="On Bridge, enter right door, get to bottom floor and turnin The Thandol Span. Use pet to tank mobs", 
            Dt={q="The Thandol Span"}, point={x=5128,y=795,m=1437}},
        {Text="Accept The Thandol Span pt2", At="The Thandol Span", point={x=5128,y=795,m=1437}},
        {Text="Deliver The Thandol Span back at previous place", Dt={q="The Thandol Span"}, point={x=4995,y=1821,m=1437}},
        {Text="Accept The Thandol Span pt3", At="The Thandol Span", point={x=4995,y=1821,m=1437}},

        -- todo: test if enough time to accept run to southshore quest before completing the thandol span
        -- todo: verify the coordinates in the next 5 or so steps. GOt messed up a bit
        {Text="Run accross the bridge then right, then Destroy Cache of Explosives for The Thandol Span (Careful with shadowcasters)", Ct="The Thandol Span", point={x=4874,y=8806,m=1417}},
        {Text="Run back and Deliver The Thandol Span back at previous place", Dt={q="The Thandol Span", Item="Dwarven Guard Cloak", Use=1}, point={x=4995,y=1821,m=1437}},
        {Text="Accept Plea To The Alliance", At="Plea To The Alliance", point={x=4995,y=1821,m=1437}},
        
        -- This step only works reliably with some runspeed increase. Hunter improved aspect is reliable
        {Text="Run back to Arathi across bridge, Go on broken bridge to left, jump over on pillar and accept MacKreel's Moonshine", 
            At="MacKreel's Moonshine", point={x=4324,y=9264,m=1417}, Class="Hunter"},

        {Text="Get up again by jumping in water, swim EAST, then run to Refuge Pointe. Accept the item quest", Proximity=20, point={x=5244,y=9133,m=1417}},
        {Text="Run to Refuge Pointe (Arathi). Deliver Plea to the alliance", Dt={q="Plea To The Alliance"}, point={x=4584,y=4756,m=1417}},
        {Text="Accept Northfold Manor", At="Northfold Manor", point={x=4584,y=4756,m=1417}},
        
        {Text="Discover FP", Proximity=20, point={x=4576,y=4612,m=1417}},
        {Text="Run West on road to Hillsbrad Foothills", Zone="Hillsbrad Foothills", point={x=2012,y=2942,m=1417},
            PinAdd="If First Aid: Stormgarde Keep, at the first intersection go right and hug the wall around over the bridge"},
        {Text="Enter the Inn, Deliver MacKreel's Moonshine in the Cellar", Dt={q="MacKreel's Moonshine"}, point={x=5215,y=5868,m=1424}, Class="Hunter"},
    }
}
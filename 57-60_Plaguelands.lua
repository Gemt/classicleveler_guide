-- Author      : G3m7
-- Create Date : 6/28/2019 3:26:49 PM

CLGuide_WesternPlaguelands = {
    Title="57-60 Blasted Lands -> Plaguelands",
    Pinboard = {},
    Steps = {
        --{Text="If not able to group (3++), skip to \"===Fly to Stormwind\=== step"},

        --{Text="Follow the road north towards ZG, deliver The Formation of Felbane", Dt={q="The Formation of Felbane"}, point={x=5062,y=2048,m=1434}},
        --{Text="Accept Enchanted Azsharite Fel Weaponry", At="Enchanted Azsharite Fel Weaponry"},
        --{Text="Deliver Enchanted Azsharite Fel Weaponry at the same guy (might be RP)", Dt={q="Enchanted Azsharite Fel Weaponry", Item="Enchanted Azsharite Felbane Staff"}},
        --{Text="Accept Return to the Blasted Lands", At="Return to the Blasted Lands"},
        --{Text="Run towards Duskwood", Proximity=30, point={x=3938,y=555,m=1434}},
        --{Text="Run to Darkshire, fly to Nethergarde Keep", Taxi="Nethergarde Keep, Blasted Lands", point={x=7749,y=4429,m=1431}},
        --{Text="Deliver Return to the Blasted Lands at Fallen Hero", Dt={q="Return to the Blasted Lands"}, point={x=3427,y=6614,m=1435}},
        --{Text="Accept Uniting the Shattered Amulet", At="Uniting the Shattered Amulet", point={x=3427,y=6614,m=1435}},
        --
        --{Text="Kill Grol (kite-kill if solo hunter. To Nethergarde if needed)", Item={Name="Amulet of Grol", Count=1}, point={x=4262,y=1307,m=1419}},
        --{Text="Kill Grol (kite-kill if solo hunter. To Nethergarde if needed)", Item={Name="Amulet of Grol", Count=1}, point={x=4045,y=3045,m=1419}},
        --{Text="Kill Grol (kite-kill if solo hunter. To Nethergarde if needed)", Item={Name="Amulet of Grol", Count=1}, point={x=4262,y=1307,m=1419}},
        {Text="Restock arrows. Plenty extra", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=4800}, Class="Hunter", point={x=2831,y=7456,m=1434}},
        {Text="If didnt in Ratchet, Relics, pick, Good Luck Half-Charm and Studies in Spirit Speaking from bank. Put in WS/felwood misc", 
            TakeFromBank={"First Relic Fragment", "Second Relic Fragment", "Third Relic Fragment", "Fourth Relic Fragment", "Studies in Spirit Speaking", "Everlook Report", "Jaron's Pick", "Good Luck Half"},
            PutInBank={"Cenarion Beacon", "Cache of Mau'ari", "Corrupted Soul Shard", "Felnok's Package", "Cenarion Plant Salve", "Eridan's Supplies", "Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth", "Umi's Mechanical Yeti"},
            point={x=2655,y=7646,m=1434}},
        
        {Text="If you are a group big enough to finish Fallen Hero chain, do that first"},

        {Text="===Fly to Stormwind===", Taxi="Stormwind, Elwynn", point={x=2753,y=7779,m=1434}},
        {Text="Accept The First and the Last", At="The First and the Last", point={x=7823,y=1798,m=1453}},
        {Text="Deliver The First and the Last (by rogue trainers)", Dt={q="The First and the Last"}, point={x=7578,y=5984,m=1453}},
        {Text="Accept Honor the Dead", At="Honor the Dead", point={x=7578,y=5984,m=1453}},
        {Text="Deliver Honor the Dead", Dt={q="Honor the Dead"}, point={x=7578,y=5984,m=1453}},
        {Text="Accept Flint Shadowmore", At="Flint Shadowmore", point={x=7578,y=5984,m=1453}},


        {Text="Fly or take tram to Ironforge. Tram prolly faster, but fly if need break", Taxi="Ironforge, Dun Morogh", PinAdd="Train skills if needed"},
        {Text="Deliver Return to Tymor", Dt={q="Return to Tymor", Item="Skullspell Orb"}, point={x=3094,y=483,m=1455}},
        --{Text="Set HS in IF", SetHs="Innkeeper Firebrew", point={x=1815,y=5141,m=1455}},
        {Text="Deliver The Smoldering Ruins of Thaurissan (by the king", Dt={q="The Smoldering Ruins of Thaurissan"}, point={x=3838,y=5533,m=1455}},
        {Text="Accept Taking Back Silithus from Cenarion Emissary (this quest may not exist)", At="Taking Back Silithus", point={x=5852,y=4733,m=1455}},

        {Text="Fly to Chillwind Camp", Taxi="Chillwind Camp, Western Plaguelands", point={x=949,y=5969,m=1437}},
    
        
        {Text="Accept Target: Writhing Haunt", At="Target: Writhing Haunt", point={x=4297,y=8450,m=1422}},
        {Text="Accept Scholomance", At="Scholomance", point={x=4270,y=8404,m=1422}},
        {Text="Deliver Scholomance", Dt={q="Scholomance"}, point={x=4267,y=8377,m=1422}},
        {Text="Accept Skeletal Fragments", At="Skeletal Fragments", point={x=4267,y=8377,m=1422}},

        {Text="Deliver Flint Shadowmore", Dt={q="Flint Shadowmore"}, point={x=4360,y=8451,m=1422}},
        {Text="Accept The Eastern Plagues", At="The Eastern Plagues", point={x=4360,y=8451,m=1422}},
        
        {Text="Accept A Plague upon Thee", At="A Plague upon Thee", point={x=4342,y=8483,m=1422}},
        {Text="Deliver Good Luck Charm (mainhouse, 2nd floor)", Dt={q="Good Luck Charm"}, point={x=3840,y=5406,m=1422},
            PinAdd="If real pro, kite Jabbering Ghoul into house and kill immediately after accepting followup"},
        {Text="Accept Two Halves Become One", At="Two Halves Become One", point={x=3840,y=5406,m=1422}},
        {Text="Kill Jabbering Ghoul outside on the farm and loot Good Luck Other-Half-Charm", Item={Name="Good Luck Other-Half-Charm", Count=1}},
        {Text="Click Good Luck Other-Half-Charm to complete Two Halves Become One", UseItem="Good Luck Other-Half-Charm", Ct="Two Halves Become One"},
        {Text="Deliver Two Halves become One", Dt={q="Two Halves Become One"}, point={x=3840,y=5406,m=1422}},


        {Text="Kill Cauldron Lord Razarch, then deliver Target: Writhing Haunt", Dt={q="Target: Writhing Haunt"}, point={x=5303,y=6585,m=1422}},
        {Text="Accept Return to Chillwind Camp", At="Return to Chillwind Camp", point={x=5303,y=6585,m=1422}},
        {Text="Accept The Wildlife Suffers Too", At="The Wildlife Suffers Too", point={x=5373,y=6468,m=1422}},
        
        {Text="Accept Little Pamela (abandon WS version if have it?)", At="Little Pamela", point={x=4917,y=7858,m=1422}},

        {Text="Deliver Return to Chillwind Camp", Dt={q="Return to Chillwind Camp"}, point={x=4297,y=8450,m=1422}},
        {Text="Accept Target: Gahrron's Withering", At="Target: Gahrron's Withering", point={x=4297,y=8450,m=1422}},

        -- this one is lvl 58 mobs, can it wait for later? 
        -- maybe when turning inn wolfs/killing bears probably
        {Text="Kill Cauldron Lord Soulwrath, then deliver Target: Gahrron's Withering", Dt={q="Target: Gahrron's Withering"}, point={x=6254,y=5857,m=1422}},
        {Text="Accept Return to Chillwind Camp", At="Return to Chillwind Camp", point={x=6254,y=5857,m=1422}},

         
        {Text="Run to Tirion in EPL, Accept Demon Dogs", At="Demon Dogs", point={x=757,y=4370,m=1423}},
        {Text="Accept Blood Tinged Skies", At="Blood Tinged Skies", point={x=757,y=4370,m=1423}},
        {Text="Accept Carrion Grubbage", At="Carrion Grubbage", point={x=757,y=4370,m=1423}},

        {Text="Loot SI:7 Insignia (Fredo)", Proximity=10, point={x=2716,y=7496,m=1423}},
        {Text="Loot SI:7 Insignia (Turyen)", Proximity=10, point={x=2882,y=7488,m=1423}},
        {Text="Loot SI:7 Insignia (Rutger)", Proximity=10, point={x=2878,y=7985,m=1423}},

        {Text="Deliver Little Pamela", Dt={q="Little Pamela"}, point={x=3645,y=9079,m=1423}},
        {Text="Accept Pamela's Doll", At="Pamela's Doll", point={x=3645,y=9079,m=1423}},
        {Text="Complete Pamela's Doll", Ct="Pamela's Doll"},
        {Text="Deliver Pamela's Doll", Dt={q="Pamela's Doll"}, point={x=3645,y=9079,m=1423}},
        {Text="Accept Auntie Marlene", At="Auntie Marlene", point={x=3645,y=9079,m=1423}},
        {Text="Accept Uncle Carlin", At="Uncle Carlin", point={x=3645,y=9079,m=1423}},

        {Text="Make sure you have completed 20 Plaguehound Runts, 15 Slab of Carrion Worm Meat, 30 Plaguebat"},
        -- todo: Where the hell do we get arrows????
        {Text="Kill 5 Plaguehounds on the way to Light's Hope, then Accept The Restless Souls", At="The Restless Souls", point={x=7953,y=6404,m=1423}},
        -- zaeldarr requires 55
        {Text="Accept Zaeldarr the Outcast", At="Zaeldarr the Outcast", point={x=7953,y=6404,m=1423}},
        {Text="Restock arrows.", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=3000}, Class="Hunter", point={x=7971,y=6358,m=1423}},
        {Text="Deliver Duke Nicholas Zverenhoff", Dt={q="Duke Nicholas Zverenhoff"}, point={x=8143,y=5982,m=1423}},
        {Text="Deliver Uncle Carlin", Dt={q="Uncle Carlin"}, point={x=8152,y=5976,m=1423}},
        {Text="Accept Defenders of Darrowshire", At="Defenders of Darrowshire", point={x=8152,y=5976,m=1423}},
        {Text="Discover the FP", Proximity=5, point={x=8163,y=5927,m=1423}},
        {Text="Set HS at Light's Hope", SetHs="Jessica Chambers", point={x=8160,y=5810,m=1423}},

        {Text="Deliver Troubled Spirits of Kel'Theril (skip followup)", Dt={q="Troubled Spirits of Kel'Theril"}, point={x=5350,y=2200,m=1423}, PinAdd="Look for Frenzied Plaguehounds on the way"},
        {Text="Kill 5 Frenzied Plaguehound to complete Demon Dogs towards Plaguewood (West)", Ct="Demon Dogs", point={x=4214,y=3826,m=1423}},

        {Text="Complete A Plague upon Thee and Defenders of Darrowshire", Mct={"A Plague Upon Thee", "Defenders of Darrowshire"}, point={x=1964,y=2912,m=1423},
            PinAdd="Cannibal Ghouls for defenders of darrowshire. Can leave a few for the laterz"},
        {Text="Deliver The Restless Souls (skip followup)", Dt={q="The Restless Souls"}, point={x=1445,y=3374,m=1423}},
        {Text="Accept Augustus' Receipt Book", At="Augustus' Receipt Book", point={x=1255,y=3219,m=1423},
            PinAdd="He might be in same house as dwarf, but most likely inside spider cave"},
        {Text="Exit cave, enter the Inn and loot the Receipt book (2nd floor)", Ct="Augustus' Receipt Book", point={x=1745,y=3112,m=1423}},
        {Text="Turn in Augustus' Receipt Book wherever you got it", Dt={q="Augustus' Receipt Book"}},

        {Text="Grind through spider cave, Deliver Demon Dogs", Dt={q="Demon Dogs"}, point={x=757,y=4370,m=1423},
            PinAdd="Watch out for Nerubian Overseer, avoid him"},
        {Text="Deliver Blood Tinged Skies", Dt={q="Blood Tinged Skies"}, point={x=757,y=4370,m=1423}},
        {Text="Deliver Carrion Grubbage", Dt={q="Carrion Grubbage"}, point={x=757,y=4370,m=1423}},
        {Text="Accept Redemption", At="Redemption", point={x=757,y=4370,m=1423}},
        {Text="/sit, then talk to tirion and complete the gossip", CycleGossip="Redemption", point={x=757,y=4370,m=1423}},
        {Text="Deliver Redemption", Dt={q="Redemption"}, point={x=757,y=4370,m=1423}},
        {Text="Accept Of Forgotten Memories", At="Of Forgotten Memories", point={x=757,y=4370,m=1423}},

        {Text="Place a slow trap, Click dirt in the back, run max distance, kite-kill Mercutio Filthgorger", Ct="Of Forgotten Memories" , point={x=2830,y=8688,m=1423}},
        {Text="Go down in the crypt, kill Zaeldarr the Outcast", Ct="Zaeldarr the Outcast"},
        {Text="Accept Hameya's Plea from the scroll", At="Hameya's Plea", point={x=2727,y=8522,m=1423}},
        {Text="Deliver Of Forgotten Memories", Dt={q="Of Forgotten Memories"}, point={x=757,y=4370,m=1423}},
        {Text="Accept Of Lost Honor", At="Of Lost Honor", point={x=757,y=4370,m=1423}},

        {Text="Deliver Auntie Marlene in Sorrow Hill, WPL", Dt={q="Auntie Marlene"}, point={x=4917,y=7857,m=1422}},
        {Text="Accept A Strange Historian", At="A Strange Historian", point={x=4917,y=7857,m=1422}},
        {Text="Complete A Strange Historian by looting grave outside", Ct="A Strange Historian", point={x=4971,y=7674,m=1422}},
        
        {Text="Deliver The Eastern Plagues at Chillwind Camp", Dt={q="The Eastern Plagues"}, point={x=4361,y=8450,m=1422}},
        {Text="Accept The Blightcalled Cometh", At="The Blightcaller Cometh", point={x=4361,y=8450,m=1422}},

        {Text="A Plague Upon Thee", Dt={q="A Plague Upon Thee"}, point={x=4341,y=8483,m=1422}},
        {Text="Accept A Plague Upon Thee", At="A Plague Upon Thee", point={x=4341,y=8483,m=1422}},
        {Text="Deliver Return to Chillwind Point", Dt={q="Return to Chillwind Point"}, point={x=4297,y=8451,m=1422}},
        {Text="Deliver Mission Accomplished!", Dt={q="Mission Accomplished!"}, point={x=4270,y=8403,m=1422}},
        {Text="Accept Alas, Andorhal", At="Alas, Andorhal", point={x=4270,y=8404,m=1422}},

        {Text="Deliver A Strange Historian (2nd floor)", Dt={q="A Strange Historian"}, point={x=3946,y=6675,m=1422}},
        {Text="Accept The Annals of Darrowshire", At="The Annals of Darrowshire", point={x=3946,y=6675,m=1422}},
        {Text="Accept A Matter of Time", At="A Matter of Time", point={x=3946,y=6675,m=1422}},

        {Text="Complete A Matter of Time (use Temporal Displacer at silos)", Ct="A Matter of Time", UseItem="Temporal Displacer", point={x=4500,y=6242,m=1422}},
        {Text="Deliver A Matter of Time", Dt={q="A Matter of Time", Item="Gold Link Belt"}, point={x=3946,y=6675,m=1422}},
        {Text="Accept Counting Out Time", At="Counting Out Time", point={x=3946,y=6675,m=1422}},
        {Text="Complete Counting Out Time (small boxes in houses in Andorhal) and Annals of Darrowshire (book in town hall)", Mct={"Counting Out Time", "The Annals of Darrowshire"},
            PinAdd="The Annals of Darrowshire book is supposed to have smooth gray-white coloring on pages"},
        {Text="Attempt to kill Araj, but skip if you cant", Ct="Alas, Andorhal", point={x=4527,y=6920,m=1422}},
        {Text="Deliver Counting Out Time", Dt={q="Counting Out Time"}, point={x=3946,y=6675,m=1422}},
        {Text="Deliver Annals of Darrowshire", Dt={q="The Annals of Darrowshire"}, point={x=3946,y=6675,m=1422}},
        {Text="Accept Brother Carlin", At="Brother Carlin", point={x=3946,y=6675,m=1422}},
        {Text="Complete Skeletal Fragments on skeletons in Andorhal", Ct="Skeletal Fragments"},

        {Text="Hearthstone to Light's Hope", UseItem="Hearthstone", Proximity=50, point={x=8161,y=5810,m=1423}, PinAdd="Deliver Defenders of Darrowshire whenever it's completed. Wont be a step for it"},
        
        {Text="Deliver Brother Carlin", Dt={q="Brother Carlin"}, point={x=8152,y=5976,m=1423}},
        {Text="Accept Villains of Darrowshire", At="Villains of Darrowshire", point={x=8152,y=5976,m=1423}},
        {Text="Accept Heroes of Darrowshire", At="Heroes of Darrowshire", point={x=8152,y=5976,m=1423}},

        {Text="Deliver Zaeldarr the Outcast", Dt={q="Zaeldarr the Outcast"}, point={x=7971,y=6358,m=1423}},
        {Text="Restock arrows.", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=3000}, Class="Hunter", point={x=7971,y=6358,m=1423}},
        
        {Text="Loot Symbol of Lost Honor", Item={Name="Symbol of Lost Honor", Count=1}, point={x=7128,y=3395,m=1423}},
        {Text="Kill Infiltrator Hameya for key to complete Haeya's Plea", Ct="Hameya's Plea", point={x=7063,y=1640,m=1423}},
        {Text="Loot Skull of Horgus (bottom of lake)", Item={Name="Skull of Horgus", Count=1}, point={x=5111,y=4993,m=1423}},
        {Text="Loot Shattered Sword of Marduk to complete Villains of Darrowshire", Ct="Villains of Darrowshire", point={x=5391,y=6576,m=1423}},
        {Text="Deliver Hameya's Plea (pile of dirt behind crypts)", Dt={q="Hameya's Plea"}, point={x=2804,y=8614,m=1423}},
        {Text="Deliver Of Lost Honor", Dt={q="Of Lost Honor"}, point={x=757,y=4371,m=1423}},
        {Text="Accept Of Love and Family", At="Of Love and Family", point={x=757,y=4371,m=1423}},

        {Text="Run to WPL, West and work on wolves towards termite barrel.", Proximity=20, point={x=4835,y=3199,m=1422}},
        {Text="Talk to Crate, place termites and deliver A Plague Upon Thee to Termite Barrel which spawns", 
            CycleGossip="-", Dt={q="A Plague Upon Thee"}, point={x=4835,y=3199,m=1422}},
        {Text="Accept A Plague Upon Thee", At="A Plague Upon Thee", point={x=4835,y=3199,m=1422}},
        {Text="Accept Unfinished Business (scarlet mobs)", At="Unfinished Business", point={x=5192,y=2805,m=1422}},
        {Text="Complete Unfinished Business", Ct="Unfinished Business", point={x=5134,y=4405,m=1422}},
        {Text="Deliver Unfinished Business", Dt={q="Unfinished Business"}, point={x=5192,y=2805,m=1422}},
        {Text="Accept Unfinished Business", At="Unfinished Business", point={x=5192,y=2805,m=1422}},
        {Text="Kill Huntsman Radley for Unfinished Business", point={x=5781,y=3613,m=1422}},
        {Text="Kill Cavalier Durgen for Unfinished Business", Ct="Unfinished Business", point={x=5464,y=2365,m=1422}},
        {Text="Deliver Unfinished Business", Dt={q="Unfinished Business"}, point={x=5192,y=2805,m=1422}},
        {Text="Accept Unfinished Business", At="Unfinished Business", point={x=5192,y=2805,m=1422}},
        {Text="Loot Davil's Libram inside house at point", Item={Name="Davil's Libram", Count=1}, point={x=4252,y=1899,m=1422},
            PinAdd="Might not be soloable for non-hunters who can pet-pull/reset"},
        {Text="Get to the top of tower at point to complete Unfinished Business", Ct="Unfinished Business", point={x=4568,y=1822,m=1422}},
        {Text="Deliver Unfinished Business", Dt={q="Unfinished Business"}, point={x=5192,y=2805,m=1422}},

        {Text="Complete The Wildlife Suffers Too (Wolves)", Ct="The Wildlife Suffers Too", point={x=4711,y=4601,m=1422}},
        {Text="Deliver The Wildlife Suffers Too", Dt={q="The Wildlife Suffers Too"}, point={x=5373,y=6468,m=1422}},
        {Text="Accept The Wildlife Suffers Too", At="The Wildlife Suffers Too", point={x=5373,y=6468,m=1422}},
        {Text="Loot Redpath's Shield to complete Heroes of Darrowshire", Ct="Heroes of Darrowshire", point={x=6380,y=5719,m=1422},
            PinAdd="Kill Bears for The Wildlife Suffers Too on the way"},
        {Text="Complete The Wildlife Suffers Too (Bears)", Ct="The Wildlife Suffers Too", point={x=5984,y=5189,m=1422}},
        {Text="Deliver The Wildlife Suffers Too", Dt={q="The Wildlife Suffers Too"}, point={x=5373,y=6468,m=1422}},
        {Text="Accept Glyphed Oaken Branch", At="Glyphed Oaken Branch", point={x=5373,y=6468,m=1422}},

        {Text="Deliver Of Love and Family", Dt={q="Of Love and Family"}, point={x=6578,y=7537,m=1422}},
        {Text="Accept Of Love and Family", At="Of Love and Family", point={x=6578,y=7537,m=1422}},

        {Text="HS to Lights Hope if ready, otherwise fly from Chillwind", Taxi="Light's Hope Chapel, Eastern Plaguelands", UseItem="Hearthstone", Proximity=50, point={x=8161,y=5810,m=1423}},

        
        {Text="Deliver Villains of Darrowshire", Dt={q="Villains of Darrowshire"}, point={x=8152,y=5976,m=1423}},
        {Text="Deliver Heroes of Darrowshire", Dt={q="Heroes of Darrowshire"}, point={x=8152,y=5976,m=1423}},
        {Text="Accept Marauders of Darrowshire", At="Marauders of Darrowshire", point={x=8152,y=5976,m=1423}},
        {Text="Complete Marauders of Darrowshire", Ct="Marauders of Darrowshire", point={x=7706,y=4807,m=1423}, UseItem="Mystic Crystal",
            PinAdd="Kill Scourte Captains for Fetid Skull, use Mystic Crystal and (hopefully) get Resonating Skulls"},
        {Text="Deliver Marauders of Darrowshire", Dt={q="Marauders of Darrowshire"}, point={x=8152,y=5976,m=1423}},
        {Text="Accept Return to Chromie", At="Return to Chromie", point={x=8152,y=5976,m=1423}},
        
        {Text="Fly to Chillwind Camp", Taxi="Chillwind Camp, Western Plaguelands", point={x=8163,y=5927,m=1423}},
        {Text="Deliver A Plague upon Thee in Chillwind Camp", Dt={q="A Plague upon Thee"}, point={x=4341,y=8482,m=1422}},
        {Text="Deliver Skeletal Fragments (also deliver Alas, Adorhal if completed)", Dt={q="Skeletal Fragments"}, point={x=4267,y=8376,m=1422}},
        {Text="Accept Mold Rhymes With...", At="Mold Rhymes With...", point={x=4267,y=8376,m=1422}},

        {Text="Deliver Return to Chromie in Andorhal", Dt={q="Return to Chromie"}, point={x=3946,y=6676,m=1422}},
        {Text="Accept The Battle of Darrowshire", At="The Battle of Darrowshire", point={x=3946,y=6676,m=1422}},
        
        {Text="Fly to Menethil", Taxi="Menethil Harbor, Wetlands", point={x=4292,y=8506,m=1422}},
        {Text="Set HS in Wetlands", SetHs="Innkeeper Helbrek", point={x=1069,y=6094,m=1437}},
        {Text="Take the boat to Theramore", Zone="Dustwallow Marsh", point={x=511,y=6349,m=1437}},

        --[[
            Delivery in SW 
            Finish Fallen Hero chain 

            wetlands (set hs)
            auberdine
            darnassus (oaken branch delivery, get silithus intro)
            moonglade
            HS wetlands
            theramore
            tanaris (linken + are we there, yet?)
            ungoro (linken + are we there, yet?)
            silithus 
            HS wetlands
            auberdine
            moonglade delivery
            everlook are we there yet delivery
        ]]
    }
}
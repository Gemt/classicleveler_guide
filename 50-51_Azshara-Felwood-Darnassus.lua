-- Author      : G3m7
-- Create Date : 8/6/2019 6:54:05 PM

--[[
Questlog:
    yuka screwspigot 4324
    super snapper fx 2944
    a little slime goes a long way (felwood) 4512
    favored of elune? 3661
    prayer to elune 3378
    rise of the silithid (highxp) 162
    sprinkle's secret ingredient 2641
    the stone circle 3444
    It's a secret to everybody 3908
    The apes of un'goro 4289

.quest add 4324
.quest add 2944
.quest add 4512
.quest add 3661
.quest add 3378
.quest add 162
.quest add 2641
.quest add 3444
.quest add 3908
.quest add 4289

]]
CLGuide_AzharaFelwood1 = {
    Title="50-51 Azshara-Felwood",
    Pinboard = {},
    Steps = {
        {Text="Loot Stone Circle from box outside house on left as you exit pier", Ct="The Stone Circle", point={x=6250,y=3855}},
        {Text="Accept Volcanic Activity inside the house", At="Volcanic Activity", point={x=6245,y=3873}},
     
        {Text="Fly to Azshara", Taxi="Talrendis Point, Azshara", point={x=6308,y=3716,m=1413}},
        {Text="Accept Spiritual Unrest", At="Spiritual Unrest", point={x=1137,y=7816,m=1447}},
        {Text="Accept A Land Filled with Hatred", At="A Land Filled with Hatred", point={x=1137,y=7816,m=1447}},

        {Text="Complete Spiritual Unrest and Land Filled with Hatred", Mct={"A Land Filled with Hatred", "Spiritual Unrest"}, point={x=1936,y=6320,m=1447}},

        {Text="Deliver Spiritual Unrest", Dt={q="Spiritual Unrest"}, point={x=1137,y=7816,m=1447}},
        {Text="Deliver A Land Filled with Hatred", Dt={q="A Land Filled with Hatred"}, point={x=1137,y=7816,m=1447}},

        {Text="Run to Felwood, follow points", Proximity=20, point={x=8785,y=4366,m=1440}},
        {Text="Run to Felwood, follow points", Proximity=20, point={x=8283,y=4744,m=1440}},
        {Text="Run to Felwood, follow points", Proximity=20, point={x=7744,y=4636,m=1440}},
        {Text="Run to Felwood, follow points", Proximity=20, point={x=7440,y=4747,m=1440}},
        {Text="Run to Felwood, follow points", Proximity=20, point={x=6927,y=5023,m=1440}},
        {Text="Run to Felwood, follow points", Proximity=20, point={x=6695,y=4826,m=1440}},
        {Text="Run to Felwood, follow points", Proximity=20, point={x=5966,y=3978,m=1440}},

        {Text="Accept Cleansing Felwood", At="Cleansing Felwood", point={x=5415,y=8683,m=1448}},
        {Text="Accept Forces of Jaedenar", At="Forces of Jaedenar", point={x=5121,y=8210,m=1448}},
        {Text="Accept The Corruption of the Jadefire", At="The Corruption of the Jadefire", point={x=5135,y=8151,m=1448}},
        
        -- req 53:
        --{Text="Accept To Winterspring!", At="To Winterspring!", point={x=5096,y=8158,m=1448}},
        
        {Text="Accept Verifying the Corruption", At="Verifying the Corruption", point={x=5088,y=8162,m=1448}},
        
        {Text="Accept Timbermaw Ally (may not be in-game)", At="Timbermaw Ally", point={x=5093,y=8501,m=1448}},
        {Text="Grind timbermaws to unfriendly (100 kills on LH)", Ct="Timbermaw Ally", point={x=4902,y=8941,m=1448}},
        {Text=">Complete Timbermaw Ally (if existed)", Ct="Timbermaw Ally", point={x=4902,y=8941,m=1448}},
        {Text=">Deliver Timbermaw Ally", Dt={q="Timbermaw Ally", SkipIfNotHaveQuest=1}, point={x=5093,y=8501,m=1448}},
        {Text=">Accept Speak to Nafien (if timbermaw ally)", At="Speak to Nafien", point={x=5093,y=8501,m=1448}},

        {Text="Complete Jadefire Felsworns for The Corruption of the Jadefire", 
            Ct="The Corruption of the Jadefire", 
            UseItem="Package of Empty Ooze Containers",
            point={x=4371,y=8438,m=1448},
            PinAdd="Get the ooze thingies out for next step too"},

        {Text="Complete Cursed Ooze Jars", UseItem="Empty Cursed Ooze Jar", Item={Name="Filled Cursed Ooze Jar", Count=6}, point={x=4000,y=7259,m=1448}},

        {Text="Complete The Corruption of the Jadefire", Ct="The Corruption of the Jadefire", point={x=3768,y=6890,m=1448}},
        {Text="Xavathras at point", Ct="The Corruption of the Jadefire", point={x=3229,y=6709,m=1448}},

        {Text="Complete Tainted Ooze Jars to complete A little slime (more north)", UseItem="Empty Tainted Ooze Jar", 
            Ct="A Little Slime Goes a Long Way", point={x=4081,y=5910,m=1448}},

        {Text="Complete Forces of Jaedenar", Ct="Forces of Jaedenar", point={x=3844,y=5934,m=1448}},


        {Text="Deliver Forces of Jaedenar", Dt={q="Forces of Jaedenar"}, point={x=5121,y=8210,m=1448}},
        {Text="Accept Collection of the Corrupt Water", At="Collection of the Corrupt Water", point={x=5121,y=8210,m=1448}},

        {Text="Deliver The Corruption of the Jadefire", Dt={q="The Corruption of the Jadefire"}, point={x=5135,y=8151,m=1448}},
        {Text="Accept Further Corruption", At="Further Corruption", point={x=5135,y=8151,m=1448}},
        

        {Text="Complete Collection of the Corrupt Water (back in Jaedenar)", Ct="Collection of the Corrupt Water", UseItem="Empty Canteen", point={x=3517,y=5986,m=1448}},
        {Text="Complete Verifying the Corruption", Ct="Verifying the Corruption", point={x=4058,y=4183,m=1448}},
        
        {Text="Run to Jadefire Run", Proximity=50, point={x=4257,y=2007,m=1448}},
        {Text="Complete Further Corruption. Start by killing Xavaric at point and accept quest he drops)", Ct="Further Corruption", point={x=3906,y=2237,m=1448},
            PinAdd="Loot Xavaric, accept quest from item he drops"},
        {Text="Make sure Flute of Xavaric is complete too (Q from Xavaric drop)", Ct="Flute of Xavaric"},
        

        {Text="Deliver Verifying the Corruption", Dt={q="Verifying the Corruption"}, point={x=5088,y=8162,m=1448}},
        
        {Text="Deliver Flute of Xavaric", Dt={q="Flute of Xavaric"}, point={x=5135,y=8151,m=1448}},
        {Text="Accept Felbound Ancients", At="Felbound Ancients", point={x=5135,y=8151,m=1448}},
        {Text="Deliver Further Corruption", Dt={q="Further Corruption"}, point={x=5135,y=8151,m=1448}},

        {Text="Deliver Collection of the Corrupt Water", Dt={q="Collection of the Corrupt Water"}, point={x=5121,y=8210,m=1448}},
        {Text="Accept Seeking Spiritual Aid", At="Seeking Spiritual Aid", point={x=5121,y=8210,m=1448}},

        {Text="Discover FP at Talonbranch", Proximity=5, point={x=6249,y=2424,m=1448}},
        {Text="Train hunter spells", Class="Hunter", TrainSkill=1, point={x=6189,y=2359,m=1448}},
        {Text="Train pet spells", Class="Hunter", TrainSkill=1, point={x=6220,y=2437,m=1448}},

        {Text="Restock on Arrows if needed", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=2800}, point={x=6232,y=2564,m=1448}},
        {Text="Deliver Speak to Nafien (if existed)", Dt={q="Speak to Nafien", SkipIfNotHaveQuest=1}, point={x=6477,y=813,m=1448}},
        {Text="Run through tunnel to winterspring", Zone="Winterspring", point={x=6845,y=587,m=1448}},
        
        {Text="Deliver It's a Secret to Everybody", Dt={q="It's a Secret to Everybody"}, point={x=3127,y=4517,m=1452}},
        {Text="Accept The Videre Elixir (may need to wait for RP)", At="The Videre Elixir", point={x=3127,y=4517,m=1452}},
        
        -- todo: do we need this FP now, or can we fly from talonbranch instead?
        {Text="Deathwarp to Everlook", DeathWarp=1},
        {Text="Fly to Rut'Teran (Darnassus)", Taxi="Rut'theran Village, Teldrassil", point={x=6233,y=3661,m=1452}},

        {Text="Deliver Favored of Elune?", Dt={q="Favored of Elune?"}, point={x=5550,y=9204}},
        {Text="Deliver The Super Snapper FX (2nd floor)", Dt={q="The Super Snapper FX"}, point={x=5541,y=9222}},
        {Text="Accept Return to Troyas", At="Return to Troyas", point={x=5541,y=9222}},
        {Text="Enter Darnassus", Zone="Darnassus"},
        
        -- todo, can we just wait with acceping this in if, and delivering here, and do it after un'goro. 
        --{Text="Deliver Assisting Arch Druid Staghelm (up in tower)", Dt={q="Assisting Arch Druid Staghelm"}, point={x=3481,y=923}},
        --{Text="Set HS in Darnassus", SetHs="Innkeeper Saelienne", point={x=6741,y=1562}, PinAdd="Can do a cloth turnin if possible"},
        {Text="Get Insect Analysis Report and Violet Tragan from bank, deposit Corrupt Moonwell Water", 
            PutInBank={"Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth", "Corrupt Moonwell Water"}, 
            TakeFromBank={"Insect Analysis Report", "Violet Tragan"}, 
            point={x=4037,y=4244,m=1457}},

        {Text="Use Eridan's Vial water in temple of moon to complete Felbound Ancients", Ct="Felbound Ancients", UseItem="Eridan's Vial", point={x=3951,y=8482,m=1457}},

        {Text="Deliver Rise of the Silithid (2nd floor)", Dt={q="Rise of the Silithid"}, point={x=4178,y=8563,m=1457}},
        {Text="Accept March of the Silithid", At="March of the Silithid", point={x=4178,y=8563,m=1457}},
        {Text="Deliver Prayer to Elune (if you did it in SG)", Dt={q="Prayer to Elune", Item="Kaylari Shoulders", SkipIfNotHaveQuest=1}, point={x=3833,y=8094,m=1457}},

        {Text="Put Vial of Blessed Water in bank", 
            PutInBank={"Vial of Blessed Water"}, 
            point={x=4037,y=4244,m=1457}},

        {Text="If you still dont have morrowgrain stuff, but farmed 20 soils earlier, do indented steps"},
        {Text="> Accept Un'Goro Soil (top of tower)", At="Un'Goro Soil", point={x=3480,y=927,m=1457}},
        {Text="> Deliver Un'Goro Soil outside, behind tower", Dt={q="Un'Goro Soil"}, point={x=3147,y=822,m=1457}},
        {Text="> Go back up in tower, Accept Morrowgrain Research", At="Morrowgrain Research", point={x=3480,y=927,m=1457}},
        {Text="> Deliver Morrowgrain Research at the middle level of tower", Dt={q="Morrowgrain Research"}, point={x=3538,y=839,m=1457}},
        {Text="> Accept Morrowgrain Research", At="Morrowgrain Research", point={x=3538,y=839,m=1457}},
        {Text="> Use Create the first morrowgrain thing, and do on CD from now on", UseItem="Evergreen Pouch"},

        {Text="Hearthstone to Ironforge", UseItem="Hearthstone", Zone="Ironforge"},

        {Text="Deliver A Little Slime Goes a Long Way", Dt={q="A Little Slime Goes a Long Way"}, point={x=7579,y=2344,m=1455}},
        {Text="Accept A Little Slime Goes a Long Way and empty the container", At="A Little Slime Goes a Long Way", point={x=7579,y=2344,m=1455}},
        
        {Text="Fly to Menethil", Taxi="Menethil Harbor, Wetlands", UseItem="Bag of Empty Ooze Containers", point={x=5546,y=4776,m=1455}},
        {Text="Take the boat to Theramore", Zone="Dustwallow Marsh", point={x=517,y=6349,m=1437}},

    }
}


--[[
talonbranch -> donova: 2min45sec
everlook fp -> donova: 2min32sec
152
]]

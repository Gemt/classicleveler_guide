-- Author      : G3m7
-- Create Date : 8/6/2019 6:59:45 PM

--[[
felnok steelspring
cleansed water returns
felbound ancients
linken's memory
cleansing felwood
Find Ranshalla
the new springs
return to tymor
the formation of felbane
the true masters
the smoldering ruins
.quest add 4808
.quest add 5159 
.quest add 4441
.quest add 3942
.quest add 4101
.quest add 979
.quest add 980
.quest add 3461
.quest add 3621
.quest add 4223
.quest add 3701
]]
CLGuide_Felwood_WS2 = {
    Title="56-57 Felwood-Winterspring",
    Pinboard = {},
    Steps = {
        {Text="Accept Are We There, Yeti?", At="Are We There, Yeti?", point={x=6088,y=3762,m=1452}},

        {Text="Accept Enraged Wildkin", At="Enraged Wildkin", point={x=6112,y=3843,m=1452}},
        
        {Text="Set HS in Everlook", SetHs="Innkeeper Vizzie", point={x=6136,y=3884,m=1452}},
        
        {Text="Deliver Felnok Steelspring", Dt={q="Felnok Steelspring"}, point={x=6162,y=3861,m=1452}},
        {Text="Accept Chillwind Horns", At="Chillwind Horns", point={x=6162,y=3861,m=1452}, PinAdd="Kill any chimeras you see for horns now"},
        {Text="Accept Luck Be With You (req lvl 55)", At="Luck Be With You", point={x=6191,y=3830,m=1452}},

        {Text="Farm chillwinds/Yetis towards Donova. Deliver The New Springs", Dt={q="The New Springs"}, point={x=3127,y=4516,m=1452}},
        {Text="Accept Strange Sources", At="Strange Sources", point={x=3127,y=4517,m=1452}},
        {Text="Accept Threat of the Winterfall", At="Threat of the Winterfall", point={x=3127,y=4517,m=1452}},

        {Text="Complete Threat of the Winterfall (go camp to SE if not enough mobs)", Ct="Threat of the Winterfall", point={x=3065,y=3677,m=1452},
            PinAdd="Really hoping to find Empty Firewater Flask"},
        {Text="Farm until Empty Firewater Flask drops, though give up if not dropped after 10min or so", UseItem="Empty Firewater Flask", At="Winterfall Firewater"},
        {Text="Deliver Threat of the Winterfall (accept q from firewater flask if found)", 
            Dt={q="Threat of the Winterfall"}, point={x=3127,y=4517,m=1452}},
        {Text="Deliver Winterfall Firewater & accept followup if found firewater flask", Dt={q="Winterfall Firewater", SkipIfNotHaveQuest=1}, point={x=3127,y=4517,m=1452}},
        {Text="Accept Falling to Corruption (if firewater flask)", At="Falling to Corruption", point={x=3127,y=4517,m=1452}},
        
        
        {Text="Run through tunnel to felwood, Accept Deadwood of the North (may not exist)", At="Deadwood of the North", point={x=6478,y=814,m=1448}},
        
        {Text="Train hunter spells", Class="Hunter", TrainSkill=1, point={x=6189,y=2359,m=1448}},
        {Text="Train pet spells", Class="Hunter", TrainSkill=1, point={x=6220,y=2437,m=1448}},
        {Text="Restock arrows.", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=3000}, Class="Hunter", point={x=6233,y=2564,m=1448}},

        {Text="Deliver Falling to Corruption (if found firewater)", Dt={q="Falling to Corruption", SkipIfNotHaveQuest=1}, point={x=6020,y=584,m=1448}},
        {Text=">Accept Mystery Goo (if failing the corruption)", At="Mystery Goo", point={x=6020,y=584,m=1448}},
        
        {Text="Complete Deadwood of the North", Ct="Deadwood of the North", SkipIfNotHaveQuest=1},
        {Text="Deliver Deadwood of the North", Dt={q="Deadwood of the North", SkipIfNotHaveQuest=1}, point={x=6478,y=814,m=1448}},
        {Text="Accept Speak to Salfa", At="Speak to Salfa", point={x=6478,y=814,m=1448}},

        {Text="Run through tunnel to WS, Deliver Speak to Salfa", Dt={q="Speak to Salfa"}, point={x=2774,y=3450,m=1452}},
        {Text="Accept Winterfall Activity (may not exist)", At="Winterfall Activity", point={x=2773,y=3450,m=1452}},

        {Text="Deliver Mystery Goo at Donova", Dt={q="Mystery Goo"}, point={x=3127,y=4516,m=1452}},
        {Text="Accept Toxic Horrors", At="Toxic Horrors", point={x=3127,y=4516,m=1452}},

        {Text="Run back to felwood, Complete Cleansing Felwood (Warpwood mobs)", Ct="Cleansing Felwood", point={x=5585,y=1690,m=1448}},
        
        {Text="Deliver \"Cleansed Water Returns to Felwood\" to Greta Mosshoof at Emerald Sanctuary", Dt={q="Cleansed Water Returns to Felwood"}, point={x=5122,y=8211,m=1448}},
        {Text="Accept Dousing the Flames of Protection", At="Dousing the Flames of Protection", point={x=5122,y=8211,m=1448}},

        
        {Text="Deliver Felbound Ancients", Dt={q="Felbound Ancients"}, point={x=5135,y=8151,m=1448}},
        {Text="Accept Purified!", At="Purified!", point={x=5135,y=8151,m=1448}},
        {Text="Deliver Purified!", Dt={q="Purified!"}, point={x=5135,y=8151,m=1448}},
        {Text="Deliver Linken's Memory", Dt={q="Linken's Memory"}, point={x=5135,y=8151,m=1448}},
        {Text="Accept Silver Heart", At="Silver Heart", point={x=5135,y=8151,m=1448}},
        {Text="Accept To Winterspring!", At="To Winterspring!", point={x=5096,y=8158,m=1448}},


        {Text="Deliver Cleansing Felwood", Dt={q="Cleansing Felwood"}, point={x=5415,y=8684,m=1448}},
        {Text="Make sure you got a Cenarion Beacon (gossip at npc if not given automatically)", point={x=5415,y=8684,m=1448}},
        {Text="Kill Wolves and Bears for Silver Heart twards Jaedenar, enter Shadow Hold (at point)", Proximity=10, point={x=3539,y=5864,m=1448},
            PinAdd="Don't need to finish Silver Heart before reaching Jadenar"},
        {Text="Click first Brazier", Proximity=8, point={x=3627,y=5630,m=1448}, UseItem="Blood Red Key",
            PinAdd="As soon as \"Blood Red Key\" drops, accept quest and deliver to caged dude near entrance and start followup-escort (you might already have it from previous jaedenar quests."},
        {Text="Click second Brazier", Proximity=10, point={x=3649,y=5518,m=1448}},
        {Text="Click third Brazier (through candle-room and down one level)", Proximity=10, point={x=3674,y=5327,m=1448}},
        {Text="Click 4th Brazier to complete Dousing the Flames of Protection", Ct="Dousing the Flames of Protection", point={x=3768,y=5269,m=1448}},
        {Text="Follow Arko'narin until \"Rescue from Jaedenar\" is complete", Ct="Rescue from Jaedenar"},
        
        {Text="Keep killing Bears/Wolves north to Irontree Woods. Get the Irontree Heart from trees there", Item={Name="Irontree Heart", Count=1}, point={x=4823,y=2786,m=1448},
            PinAdd="Kite-killing easiest along road, kill trees at Irontree Woods for Irontree Heart to complete the quest"},
        {Text="Complete Toxic Horrors (water elementals)", Ct="Toxic Horrors", point={x=4913,y=2379,m=1448}},
        {Text="Make sure Silver Heart is complete", Ct="Silver Heart"},     
        
        
       {Text="Hearthstone to Everlook", UseItem="Hearthstone", Proximity=10, point={x=6136,y=3884,m=1452}},
        
        -- todo what was the offhand again
        --{Text="Get xxx offhand from bank, use offhand+ Hunt Tracker Blade", Class="Hunter"},


        {Text="Deliver To Winterspring!", Dt={q="To Winterspring!"}, point={x=5197,y=3039,m=1452}},
        {Text="Accept The Ruins of Kel'Theril", At="The Ruins of Kel'Theril", point={x=5197,y=3039,m=1452}},
        {Text="Deliver Enraged Wildkin", Dt={q="Enraged Wildkin"}, point={x=5214,y=3043,m=1452}},
        {Text="Accept Enraged Wildkin", At="Enraged Wildkin", point={x=5214,y=3043,m=1452}},
        {Text="Deliver The Ruins of Kel'Theril", Dt={q="The Ruins of Kel'Theril"}, point={x=5214,y=3043,m=1452}},
        {Text="Accept Troubled Spirits of Kel'Theril", At="Troubled Spirits of Kel'Theril", point={x=5214,y=3043,m=1452}},       
        {Text="Restock arrows", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=3000}, Class="Hunter", point={x=5144,y=3083,m=1452}},
        
        
        {Text="Loot Second Relic Fragment", Item={Name="Second Relic Fragment", Count=1}, point={x=5085,y=4171,m=1452}},
        {Text="Loot Fourth Relic Fragment", Item={Name="Fourth Relic Fragment", Count=1}, point={x=5243,y=4151,m=1452}},
        {Text="Loot Third Relic Fragment", Item={Name="Third Relic Fragment", Count=1}, point={x=5332,y=4344,m=1452}},
        {Text="Loot First Relic Fragment", Item={Name="First Relic Fragment", Count=1}, point={x=5515,y=4298,m=1452}},


        {Text="Deliver Enraged Wildkin", Dt={q="Enraged Wildkin"}, point={x=5899,y=5979,m=1452}},
        {Text="Accept Enraged Wildkin", At="Enraged Wildkin", point={x=5899,y=5979,m=1452}},
        {Text="Loot the easy to reach Frostmaul Shards at the path down the valley", point={x=5894,y=6425,m=1452}},
        {Text="Deliver Enraged Wildkin", Dt={q="Enraged Wildkin"}, point={x=6141,y=6067,m=1452}},
        {Text="Accept Enraged Wildkin", At="Enraged Wildkin", point={x=6141,y=6067,m=1452}},
        {Text="Loot Jaron's Supplies on the ground", Item={Name="Jaron's Supplies", Count=1}, point={x=6140,y=6074,m=1452}},
        {Text="Deliver Find Ranshalla", Dt={q="Find Ranshalla"}, point={x=6307,y=5947,m=1452}},
        {Text="Accept Guardians of the Altar", At="Guardians of the Altar", point={x=6307,y=5947,m=1452}},
        {Text="Follow & protect Ranshalla. Click the Fires of Elune in the caves", Ct="Guardians of the Altar"},
        {Text="kill more moonkins if still not found Blue-feathered Amulet for Enraged Wildkin", Ct="Enraged Wildkin", point={x=6377,y=5929,m=1452}},

        {Text="Loot the easy to reach Frostmaul Shards again. This time try to finish the quest", Ct="Luck Be With You", point={x=5894,y=6425,m=1452}},
        {Text="Run south across the bridge until Strange Sources completes", Ct="Strange Sources", point={x=5975,y=7466,m=1452}},

        {Text="Run north, finish Are We There Yeti? (save matriarchs/patriarchs for followup)", Ct="Are We There, Yeti?", point={x=6606,y=4294,m=1452},
            PinAdd="Also want to finish chillwind Horns, but we get 2 more chances so don't go out of your way for it"},
        {Text="Deliver We There, Yeti?", Dt={q="Are We There, Yeti?"}, point={x=6088,y=3762,m=1452}},
        {Text="Accept Are We There, Yeti?", At="Are We There, Yeti?", point={x=6088,y=3762,m=1452}},
        {Text="Deliver Luck Be With You", Dt={q="Luck Be With You"}, point={x=6192,y=3830,m=1452}},
        {Text="Accept Cache of Mau'ari", At="Cache of Mau'ari", point={x=6192,y=3830,m=1452}},
        {Text="Deliver Cache of Mau'ari (maybe a few sec of RP)", Dt={q="Cache of Mau'ari"}, point={x=6192,y=3830,m=1452}},
        
        {Text="Complete Are We There, Yeti? (matriarchs/patriarchs, possibly only in cave)", 
            Ct="Are We There, Yeti?", point={x=6772,y=4174,m=1452}, PinAdd="Kill more chillwinds where you see them"},

        
        {Text="Deliver We There, Yeti?", Dt={q="Are We There, Yeti?"}, point={x=6088,y=3762,m=1452}},       
        -- todo: probably won't finish/deliver this, so skip if need logspace later
        {Text="Accept Are We There, Yeti?", At="Are We There, Yeti?", point={x=6088,y=3762,m=1452}},

        {Text="Deliver Enraged Wildkin", Dt={q="Enraged Wildkin"}, point={x=5214,y=3043,m=1452}},
        {Text="Restock arrows", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=3000}, Class="Hunter", point={x=5144,y=3083,m=1452}},
        
        
        {Text="Complete Chillwind Horns (probably best south of road, west of lake) on the way to Donova. Last Chance", Ct="Chillwind Horns", point={x=3127,y=4516,m=1452}},
        {Text="Deliver Strange Sources", Dt={q="Strange Sources", Item="Deep River Cloak", Use=1}, point={x=3127,y=4516,m=1452}},
        {Text="Deliver Toxic Horrors", Dt={q="Toxic Horrors"}, point={x=3127,y=4516,m=1452}},
        {Text="Accept Winterfall Runners", At="Winterfall Runners", point={x=3127,y=4516,m=1452}},

        {Text="Run to felwood, kill Winterfall Runner at point", Ct="Winterfall Runners", point={x=6020,y=584,m=1448}},
        {Text="Run back to Donova in WS again, deliver Winterfall Runners", Dt={q="Winterfall Runners"}, point={x=3127,y=4516,m=1452}},
        
        {Text="Accept High Chief Winterfall", At="High Chief Winterfall", point={x=3127,y=4516,m=1452}},	
        {Text="Run back to Felwood, Talonbranch, Fly to Rut'theran", Taxi="Rut'theran Village, Teldrassil", point={x=6249,y=2424,m=1448}},

        
        {Text="Deliver Guardians of the Altar", Dt={q="Guardians of the Altar"}, point={x=5550,y=9205,m=1438}},
        {Text="Accept Wildkin of Elune", At="Wildkin of Elune", point={x=5550,y=9205,m=1438}},
        {Text="Enter Darnassus (keep an eye out for Herald Moonstalker for new frontier now)", Zone="Darnassus", point={x=5590,y=8972,m=1438}, PinAdd="Herald Moonstalker is the npc with the 8-shape patrol of darna"},
        {Text="Deliver Wildkin of Elune (top of tower)", Dt={q="Wildkin of Elune", Item="Thornflinger", Use=1}, point={x=3481,y=925,m=1457}},
        {Text="Accept The New Frontier from Herald Moonstalker if find her, then go top of tower at cenarion enclave to deliver/get followup", PinAdd="May not need Herald Moonstalker quest to get followup", point={x=3482,y=931,m=1457}},
        {Text="Deliver The New Frontier (from top of tower) in middle of tower", Dt={q="The New Frontier"}, point={x=3538,y=841,m=1457}},
        {Text="Accept Rabine Saturna", At="Rabine Saturna", point={x=3538,y=841,m=1457}},

        {Text="Hearthstone to Everlook", UseItem="Hearthstone", Proximity=10, point={x=6136,y=3884,m=1452}},
        
        {Text="Deliver Chillwind Horns", Dt={q="Chillwind Horns"}, point={x=6163,y=3862,m=1452}},
        {Text="Accept Return to Tinkee", At="Return to Tinkee", point={x=6163,y=3862,m=1452}},
        
        {Text="Kill High Chief Winterfall (make sure to loot quest-starter)", Ct="High Chief Winterfall", point={x=6945,y=3839,m=1452}},
        {Text="Complete Winterfall Activity (if quest existed)", Ct="Winterfall Activity", point={x=6608,y=3810,m=1452}},

        {Text="Deliver High Chief Winterfall at Donova (accept Crudely-written Log quest)", UseItem="Crudely-written Log", Dt={q="High Chief Winterfall"}, point={x=3127,y=4516,m=1452}},
        {Text="Deliver The Final Piece (from Crudely-written Log)", Dt={q="The Final Piece"}, point={x=3127,y=4516,m=1452}},
        {Text="Accept Words of the High Chief", At="Words of the High Chief", point={x=3127,y=4516,m=1452}},
        
        {Text="Deliver Winterfall Activity (if have)", Dt={q="Winterfall Activity", SkipIfNotHaveQuest=1}, point={x=2774,y=3450,m=1452}},

        {Text="Run to center of tunnel, then jump down and take path to Moonglade.", Proximity=15, Zone="Moonglade", point={x=6537,y=237,m=1448}},
        {Text="Run to Nighthaven", Proximity=50, point={x=4290,y=3470,m=1450}},
        {Text="deliver Rabine Saturna", Dt={q="Rabine Saturna"}, point={x=5168,y=4509,m=1450}},
        {Text="Accept Wasteland", At="Wasteland", point={x=5168,y=4509,m=1450}},
        {Text="Accept A Reliquary of Purity (may not be ingame)", At="A Reliquary of Purity", point={x=5168,y=4509,m=1450}},
        {Text="Fly to Talonbranch", Taxi="Talonbranch Glade, Felwood", point={x=4810,y=6735,m=1450}},
        {Text="Restock arrows", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=3000}, Class="Hunter", point={x=6232,y=2564,m=1448}},

        {Text="Run all the way south to Emerald Sanctuary, Deliver Words of the High Chief", Dt={q="Words of the High Chief"}, point={x=5114,y=8175,m=1448}},

        {Text="Deliver Dousing the Flames of Protection", Dt={q="Dousing the Flames of Protection"}, point={x=5121,y=8211,m=1448}},
        {Text="Accept A Final Blow", At="A Final Blow", point={x=5121,y=8210,m=1448}},
        {Text="Deliver Rescue from Jaedenar if you got it", Dt={q="Rescue from Jaedenar", SkipIfNotHaveQuest=1}, point={x=5135,y=8202,m=1448}},
        {Text="Accept Retribution of the Light (if did rescue)", At="Retribution of the Light", point={x=5135,y=8202,m=1448}},
        {Text="Deliver Silver Heart", Dt={q="Silver Heart"}, point={x=5135,y=8151,m=1448}},
        
        -- todo: might skip this. Highly unlikely we'll finish it before 60 at least, so only accept if got questlog space
        {Text="Accept Aquementas", At="Aquementas", point={x=5135,y=8151,m=1448}},

        {Text="Deliver all Corrupted Soul Shards you have", point={x=5415,y=8684,m=1448}},
        {Text="Only if you got BOTH \"Retribution of the Light\" & \"A Final Blow\" do next > steps. Get Songflower at point if so", point={x=5290,y=8781,m=1448},
            PinAdd="If you don't have both quests, skip all steps marked with \">\" and abandon whichever of the 2 you accepted"},
        {Text=">Enter Shadow Hold (at point). Make sure you got a songflower buff", Proximity=10, point={x=3539,y=5864,m=1448}},
        {Text=">Run through Shadow Hold, further than last time, Kill Rakaiah to complete Retribution of the Light", Ct="Retribution of the Light", point={x=3830,y=5052,m=1448}},
        {Text=">Deliver Retribution to the Light", Dt={q="Retribution of the Light"}, point={x=3851,y=5041,m=1448}},
        {Text=">Accept The Remains of Trey Lightforge", At="The Remains of Trey Lightforge", point={x=3851,y=5041,m=1448}},
        {Text=">Continue further inn and kill the 3 mobs for A Final Blow", Ct="A Final Blow", point={x=3888,y=4679,m=1448}},
        
        {Text=">Backtrack all the way out again. Deliver A Final Blow at Emerald Sanctuary", Dt={q="A Final Blow"}, point={x=5121,y=8211,m=1448},
            PinAdd="Try to refresh songflower buff on the way back"},
        {Text=">Deliver The Remains of Trey Lightforge", Dt={q="The Remains of Trey Lightforge", Item="Hunt Tracker Blade"}, point={x=5135,y=8201,m=1448}},
        
        
        {Text="Hearthstone to Everlook", UseItem="Hearthstone", Proximity=30, point={x=6134,y=3895,m=1452}},
        {Text="Accept Duke Nicholas Zverenhoff", At="Duke Nicholas Zverenhoff", point={x=6135,y=3897,m=1452}},

        {Text="Fly to Ratchet", Taxi="Ratchet, The Barrens", point={x=6234,y=3661,m=1452}},
        
        {Text="Get Good Luck Half-Charm from bank. If boat is leaving, get it in BB instead. Put WS/felwood misc in", 
            TakeFromBank={"First Relic Fragment", "Second Relic Fragment", "Third Relic Fragment", "Fourth Relic Fragment", "Studies in Spirit Speaking", "Everlook Report", "Jaron's Pick", "Good Luck Half"},
            PutInBank={"Cenarion Beacon", "Cache of Mau'ari", "Corrupted Soul Shard", "Felnok's Package", "Cenarion Plant Salve", "Eridan's Supplies", "Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth", "Umi's Mechanical Yeti"},
            point={x=6268,y=3744,m=1413}},

        {Text="Take the boat to Booy Bay", Zone="Stranglethorn Vale", point={x=6365,y=3865,m=1413}},
    }
}
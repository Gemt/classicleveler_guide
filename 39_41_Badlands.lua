-- Author      : G3m7
-- Create Date : 5/11/2019 12:39:06 PM

CLGuide_Badlands = {
    Title="39-41 Badlands",
    Pinboard = {},
    Steps = {
    
        -- todo: consider stabling pet and getting a 40-41 ridge stalker (dash/prowl r2). need lvl 40
        {Text="Set HS in Thelsamar", SetHs="Innkeeper Hearthstove", point={x=3553,y=4841,m=1432}},

        {Text="Accept Badlands Reagent Run (in thelsamar)", At="Badlands Reagent Run", point={x=3707,y=4935,m=1432}},
        {Text="Deliver Ironband Wants You! (Excavation site SE in Loch Modan)", Dt={q="Ironband Wants You!"}, point={x=6593,y=6562,m=1432}},
        {Text="Accept Find Agmond", At="Find Agmond", point={x=6593,y=6562,m=1432}},
        
        --todo Point
        {Text="Run into Badlands", Zone="Badlands",PinAdd="Cayotes and Buzzards for reagent run whenever suitable", point={x=4906,y=666,m=1418}},
        {Text="Accept A Dwarf and His Tools", At="A Dwarf and His Tools", point={x=5342,y=4340,m=1418}},
        {Text="Accept Mirages", At="Mirages", point={x=5380,y=4330,m=1418}},


        {Text="Accept A Sign of Hope (note under tent at dwarves)", At="A Sign of Hope", point={x=5303,y=3394,m=1418}},

        {Text="Complete A Dwarf and His Tools", Ct="A Dwarf and His Tools", point={x=5323,y=3216,m=1418},
            PinAdd="If no drop after clearing camp, do next step and come back for respawns"},
        {Text="Complete Mirages", Ct="Mirages", point={x=6690,y=2342,m=1418}},

        {Text="Deliver A Dwarf and His Tools", Dt={q="A Dwarf and His Tools"}, point={x=5342,y=4340,m=1418}},
        {Text="Deliver A Sign of Hope", Dt={q="A Sign of Hope"}, point={x=5342,y=4340,m=1418}},
        {Text="Accept A Sign of Hope", At="A Sign of Hope", point={x=5342,y=4340,m=1418}},
        {Text="Deliver Mirages", Dt={q="Mirages"}, point={x=5380,y=4330,m=1418}, PinAdd="When Mirages is delivered + you are l40, accept Tremors of the earth directly SE of 2dorfs"},
        {Text="Accept Scrounging", At="Scrounging", point={x=5380,y=4330,m=1418}, PinAdd="When got tremors, look for Boss Cho'Gun (anticlockwise center to west)"},
        
        -- todo: this requires lvl 40 and Mirages completed. Can we put in a requirement to hit 40 before doing other quests?
        -- would be super, so we dont have to do a dedicated trip all over the map looking for ogre boss dude
        -- also not sure if its a dwarf at the camp who starts it or the elf to the south
        {Text="If lvl 40, Accept Tremors of the Earth", At="Tremors of the Earth", point={x=6195,y=5427,m=1418}},
        
        {Text="Deliver Martek the Exiled", Dt={q="Martek the Exiled"}, point={x=4221,y=5268,m=1418}},
        {Text="Accept Indurium", At="Indurium", point={x=4221,y=5268,m=1418}},

        {Text="Accept Barbecued Buzzard Wings", At="Barbecued Buzzard Wings", point={x=4239,y=5293,m=1418}},
        {Text="Accept Power Stones", At="Power Stones", point={x=4239,y=5293,m=1418}},

        {Text="Deliver Find Agmond (corpse)", Dt={q="Find Agmond"}, point={x=5090,y=6240,m=1418}},
        {Text="Accept Murdaloc", At="Murdaloc", point={x=5090,y=6240,m=1418}},


        {Text="Complete Murdaloc and Indurium", Mct={"Murdaloc", "Indurium"}, point={x=4963,y=6632,m=1418}},
        
        -- this may not be completed solo, but pickup in case
        {Text="Deliver Theldurin the Lost (if have it from killing Fozzruk)", Dt={q="Theldurin the Lost"}, point={x=5139,y=7687,m=1418}},
        {Text=">Accept The Lost Fragments (if above)", At="The Lost Fragments"},
        {Text=">Complete The Lost Fragments", Ct="The Lost Fragments", point={x=5254,y=7886,m=1418}},
        {Text=">Deliver The Lost Fragments", Dt={q="The Lost Fragments"}, point={x=5139,y=7687,m=1418}},
        {Text=">Accept Summoning the Princess", At="Summoning the Princess", point={x=5139,y=7687,m=1418}},

        {Text="Deliver Indurium (remember to vendor)", Dt={q="Indurium"}, point={x=4221,y=5268,m=1418}},
        {Text="Accept News for Fizzle (Wait for RP)", At="News for Fizzle", point={x=4221,y=5268,m=1418}},

        {Text="Accept Study of the Elements: Rock. Do whatever frostoil/gyro mats quests you have mats for.", At="Study of the Elements: Rock", point={x=2595,y=4486,m=1418}},

        {Text="Complete Study of the Elements: Rock (Only Lesser elementals)", Ct="Study of the Elements: Rock", 
            point={x=2228,y=4454}, PinAdd="Dont need 5/5 rock elem shards here"},
        {Text="Deliver Study of the Elements: Rock", Dt={q="Study of the Elements: Rock"}, point={x=2595,y=4486,m=1418}},
        {Text="Accept Study of the Elements: Rock", At="Study of the Elements: Rock", point={x=2595,y=4486,m=1418}},

        
        -- south-mid elementals
        {Text="Complete Study of the Elements: Rock", Ct="Study of the Elements: Rock", point={x=3776,y=7544,m=1418}},
        
        {Text="Deliver Study of the Elements: Rock", Dt={q="Study of the Elements: Rock"}, point={x=2595,y=4486,m=1418}},
        {Text="Accept Study of the Elements: Rock", At="Study of the Elements: Rock", point={x=2595,y=4486,m=1418}},

        {Text="Clear out the Buzzards at point for Wings/Gizzards",Point=20,point={x=1700,y=6040,m=1418}},

        -- south-west elementals
        {Text="Complete Study of the Elements: Rock (Greater rock elementals). If 39/40 might want to skip this, lvl 42-44 tanky mobs", Ct="Study of the Elements: Rock", point={x=648,y=7636},
            PinAdd="Make sure you have 5/5 Rock Elemental Shards after this"},
        {Text="Complete Scrounging (ogres by elementals)", Ct="Scrounging", point={x=1238,y=7605,m=1418}},

        {Text="Deliver Study of the Elements: Rock", Dt={q="Study of the Elements: Rock"}, point={x=2595,y=4486,m=1418}},

        -- req finish study of rock & frost oil chain

        {Text="Deliver Barbecued Buzzard Wings", Dt={q="Barbecued Buzzard Wings"}, point={x=4239,y=5293,m=1418}, PinAdd="Vendor at Jazzrik next to delivery guy"},
        {Text="Deliver Tremors of the Earth if done", Dt={q="Tremors of the Earth"}, point={x=6191,y=5428,m=1418}},

        {Text="Deliver Scournging", Dt={q="Scrounging", Item="Salbac Shield"}, point={x=5381,y=4330,m=1418}},
        -- skip Accept Fiery Blaze Enchantments (2% droprate..) 

        -- groups: definitely consider running thelsamar ironbands excavation site first to turn inn murdaloc/reagent run
        -- and get uldaman followups 
        
        -- todo: Should really make sure to be lvl 40 before leaving Badlands. Means training in IF & picking up Brassbolt brothers in IF
        {Text="Deliver A Sign of Hope inside uldaman pre-instance", Dt={q="A Sign of Hope"}, point={x=4770,y=1311,m=1418},
            PinAdd="if solo, deathwarp out again and skip the rest. If 2man+ do solution to doom & Amulet of Secrets (followup to a sign of hope)"},
        {Text="Complete Power Stones. Mages squishy, melees not so much (grind outside)", Ct="Power Stones"},

        {Text="Deliver Power Stones (goblin in mid)", Dt={q="Power Stones"}, point={x=4240,y=5294,m=1418}, PinAdd="Deliver Solution to Doom (SE) if did it, otherwise abandon"},

        -- group: definitely do solution to doom and maybe uldaman reagent run and Agmon's Fate
        --{Text="Run to Loch Modan", Zone="Loch Modan"},
        {Text="Run to Loch Modan & Deliver Murdaloc", Dt={q="Murdaloc"}, point={x=6593,y=6561,m=1432}},
        {Text="Hearthstone Thelsamar", UseItem="Hearthstone"},
        
        {Text="Deliver Badlands Reagent Run", Dt={q="Badlands Reagent Run"}, point={x=3707,y=4938,m=1432}},
        

        -- todo: what if we dont have necklace? do we need to get brassbolts anyway? What about tabetha's task?
        {Text="Fly to Ironforge", Taxi="Ironforge, Dun Morogh", point={x=3394,y=5096,m=1432}},

        {Text="If you found Shattered Necklace, look for 5 Silver Bar on AH", UseItem="Shattered Necklace", point={x=2423,y=7457,m=1455}},
        {Text="Bank your cloth (can do donation in IF/SW), get The Perenolde Tiara, tome of alterac from bank", point={x=3558,y=6053,m=1455},
            TakeFromBank={"Perenolde Tiara", "Tomes of Alterac"},
            PutInBank={"Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth", "Scroll of Myzrael"}},

        {Text=">If Shattered Necklace: Deliver by portal place", Dt={q="The Shattered Necklace"}, point={x=3638,y=359,m=1455}},
        {Text=">If Shattered Necklace: Accept Lore for a Price", At="Lore for a Price", point={x=3638,y=359,m=1455}},
        {Text=">If Shattered Necklace: Deliver Lore for a Price. Skip Followup", Dt={q="Lore for a Price"}, point={x=3638,y=359,m=1455}},
        {Text="If doing ZF, accept Tabetha's Task (by mage trainers)", At="Tabetha's Task", point={x=2711,y=821,m=1455}},
        {Text="Buy arrows. Check for 42 bow", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=2800}, point={x=7175,y=6673,m=1455}},
        {Text="Accept The Brassbolts Brothers in Tinker Town", At="The Brassbolts Brothers", point={x=6791,y=4609,m=1455}},
        
        -- todo: train 40 skills, or maybe its actually shorter pathing in SW

        {Text="Take the Tram to SW", Taxi="Stormwind, Elwynn", Zone="Stormwind City", point={x=7671,y=5115,m=1455}},
    }
}
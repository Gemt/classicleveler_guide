-- Author      : G3m7
-- Create Date : 5/18/2019 3:28:31 PM


CLGuide_Hinterlands = {
    Title="48-48 Hinterlands",
    Pinboard = {"Get the Searing Gorge tunnel lvl 1 char ready for after hinterlands"},
    Steps = {
        {Text="If have Summoning the Princess from badlands, fly to arathi and complete that, otherwise go next step", Dt={q="Summoning the Princess", SkipIfNotHaveQuest=1}},
        {Text=">This is where u do & deliver Summoning the princess", Dt={q="Summoning the Princess", SkipIfNotHaveQuest=1}, UseItem="Scroll of Myzrael", point={x=6254,y=3377,m=1417}},
        {Text="Fly to Southshore (skip the flight and run if you did summoning the princess)", Taxi="Southshore, Hillsbrad"},
        {Text="Run to Hinterlands (NW of durnholde Keep)", Zone="The Hinterlands", point={x=8998,y=2512,m=1424}},
        
        {Text="Accept Skulk Rock Clean-up", At="Skulk Rock Clean-up", point={x=1483,y=4456,m=1425}},
        {Text="Accept Troll Necklace Bounty", At="Troll Necklace Bounty", point={x=1483,y=4456,m=1425}},
        {Text="Set HS (2nd floor)", SetHs="Innkeeper Thulfram", point={x=1414,y=4157,m=1425}},
        {Text="Deliver The Newest Member of the Family (bottom level. If u had 2x elixir of fort)", Dt={q="The Newest Member of the Family"}, point={x=1416,y=4362,m=1425}},
        {Text="Accept Food for Baby (If u had 2x elixir of fort)", At="Food for Baby", point={x=1416,y=4362,m=1425}},
        
        {Text="Go out and up, Deliver To The Hinterlands", Dt={q="To The Hinterlands"}, point={x=1181,y=4676,m=1425}},
        {Text="Accept Gryphon Master Talonaxe", At="Gryphon Master Talonaxe", point={x=1181,y=4676,m=1425}},
        {Text="Discover the FP (at the top)", Proximity=10, point={x=1107,y=4615,m=1425}},

        {Text="Deliver Gryphon Master Talonaxe (inside, straight ahead)", Dt={q="Gryphon Master Talonaxe"}, point={x=976,y=4447,m=1425}},
        {Text="Accept Rhapsody Shindigger", At="Rhapsody Shindigger", point={x=976,y=4447,m=1425}},
        {Text="Accept Witherbark Cages", At="Witherbark Cages", point={x=976,y=4447,m=1425}},

        {Text="Run down and SE, deliver Rhapsody Shindigger (skip followup)", Dt={q="Rhapsody Shindigger"}, point={x=2694,y=4859,m=1425},
            PinAdd="Always look for Wildkin Feather on the ground"},

        {Text="Click First AND Second Witherbark Cage SE (loot trolls for necklaces)", Proximity=10, point={x=2320,y=5876,m=1425}, PinAdd="Don't need to complete Troll Necklace Bounty yet"},
        {Text="Click third Witherbark Cage to the east to complete Witherbark Cages", Ct="Witherbark Cages", point={x=3197,y=5740,m=1425}},
        
        {Text="Deliver Witherbark Cages (Back to Aerie Peak, at the top)", Dt={q="Witherbark Cages"}, point={x=976,y=4447,m=1425}},
        {Text="Accept The Altar of Zul", At="The Altar of Zul", point={x=976,y=4447,m=1425}},

        {Text="Complete Sprinkle's Secret Ingredient (lake to the east)", Ct="Sprinkle's Secret Ingredient", point={x=4069,y=5909,m=1425}},
        {Text="Complete The Altar of Zul by running up on top of it", Ct="The Altar of Zul", point={x=4897,y=6862,m=1425},
            PinAdd="May be a chest at the top, in the back"},

        {Text="Work on Skulk Rock Clean-up (NE)", Ct="Skulk Rock Clean-up", point={x=4899,y=5287,m=1425}},
        {Text="Complete Skulk Rock Clean-up (further NE)", Ct="Skulk Rock Clean-up", point={x=5698,y=4324,m=1425},
            PinAdd="If you find OOX, deliver at lake west of skulk rock"},

        {Text="Complete Food for Baby (Silvermane Stalkers), Troll Necklace Bounty and Favored of Elune?", 
            Mct={"Food for Baby", "Troll Necklace Bounty", "Favored of Elune?"}, point={x=7263,y=5760,m=1425},
            PinAdd="Feathers seem to only exist in western 2/3rds of map. Can complete after HS too if needed"},
        {Text="Jump down from the cliffs by the waterfall, turn inn Cortello's Riddle", Dt={q="Cortello's Riddle"}, point={x=8079,y=4682,m=1425},PinAdd="Use the new bag"},

        {Text="Complete Whiskey Slim's Lost Grog & The Super Snapper FX", Mct={"Whiskey Slim's Lost Grog", "The Super Snapper FX"},
            UseItem="Super Snapper FX", PinAdd="Use super snapper on Gammerita, loot bottles along the shore"},

        {Text="HS to Wildhammer Keep", UseItem="Hearthstone", Proximity=40, point={x=1415,y=4158,m=1425}},
        {Text="Deliver Food for Baby (bottom level. If you have it)", Dt={q="Food for Baby", SkipIfNotHaveQuest=1}, point={x=1416,y=4362,m=1425}},
        {Text=">Accept Becoming a Parent (if above)", At="Becoming a Parent", point={x=1416,y=4362,m=1425}},
        {Text=">Deliver Becoming a Parent (if above)", Dt={q="Becoming a Parent"}, point={x=1416,y=4362,m=1425}},

        {Text="Deliver Skulk Rock Clean-up (by exit)", Dt={q="Skulk Rock Clean-up"}, point={x=1483,y=4456,m=1425}},
        {Text="Deliver Troll Necklace Bounty", Dt={q="Troll Necklace Bounty"}, point={x=1483,y=4456,m=1425}},

        {Text="Deliver The Altar of Zul (all the way at the top)", Dt={q="The Altar of Zul"}, point={x=976,y=4447,m=1425}},
        {Text="Accept Thadius Grimshade", At="Thadius Grimshade", point={x=976,y=4447,m=1425}},
        {Text="Fly to IF", Taxi="Ironforge, Dun Morogh", point={x=1107,y=4615,m=1425}},
    }
}

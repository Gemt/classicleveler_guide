-- Author      : G3m7
-- Create Date : 5/5/2019 5:18:53 PM

CLGuide_Wetlands = {
    Title="26-28 Wetlands",
    Pinboard = {},
    Steps = {
        {Text="Deliver Claws from the Deep at pier", Dt={q="Claws from the Deep"}, point={x=832,y=5855,m=1437}},
        {Text="Accept Reclaiming Goods", At="Reclaiming Goods", point={x=832,y=5855,m=1437}},

        {Text="Buy lvl 25 food from Stuart Flemming (Rockscale Cod)", BuyItem={Npc="Stuart Fleming", Item="Rockscale Cod", Count=20},Class="Hunter",  point={x=804,y=5833,m=1437}},

        -- todo: might not have this now if needing to drop it as per notes
        {Text="Deliver Daily Delivery", Dt={q="Daily Delivery"}, point={x=854,y=5574,m=1437}},
        {Text="Deliver Young Crocolisk Skins", Dt={q="Young Crocolisk Skins"}, point={x=854,y=5574,m=1437}},
        {Text="Accept Apprentice's Duties (same as with Qs above)", At="Apprentice's Duties", point={x=854,y=5574,m=1437}},
        
        --{Text="Accept Report to Captain Stoutfist (just inside Keep)", At="Report to Captain Stoutfist", point={x=1009,y=5690},
        --    PinAdd="Check for Bronze Tube again (left,left)"},
        --{Text="Deliver the report at keep mainhall (second floor)", Dt={q="Report to Captain Stoutfist"}, point={x=985,y=5750,m=1437}},
        {Text="Deliver The Algaz Gauntlet", Dt={q="The Algaz Gauntlet"}, point={x=1010,y=5689,m=1437}},
        {Text="Accept Report to Captain Stoutfist", At="Report to Captain Stoutfist", point={x=1010,y=5689,m=1437}},
        {Text="Enter the keep, take left 2 times, buy Bronze Tube from Neal Allen", 
            BuyItem={Npc="Neal Allen", Item="Bronze Tube", Count=1}, point={x=1074,y=5678,m=1437}},
        {Text="Deliver Report to Captain Stoutfist", Dt={q="Report to Captain Stoutfist"}, point={x=985,y=5750,m=1437}},
        {Text="Accept War Banners", At="War Banners", point={x=985,y=5750,m=1437}},
        {Text="Accept Digging Through Ooze (go out again)", At="Digging Through the Ooze", point={x=1179,y=5798,m=1437}},
        
        {Text="Buy Razor Arrows.", BuyItem={Npc="Edwina Monzor", Item="Razor Arrow", Count=2000}, Class="Hunter", point={x=1111,y=5832,m=1437}},

        {Text="Accept The Third Fleet", At="The Third Fleet", point={x=1089,y=5967,m=1437}},
        {Text="Buy Flagon of Dwarven Honeymead from innkeeper for Q", BuyItem={Npc="Innkeeper Helbrek", Item="Flagon of Dwarven Honeymead", Count=1}, point={x=1069,y=6089,m=1437}},

        {Text="Accept/Deliver The Absent Minded Prospector (second floor Inn, dont wait for rp if deliver)", At="The Absent Minded Prospector", Dt={q="The Absent Minded Prospector", Item="Skullchipper"}, point={x=1084,y=6042,m=1437}},

        {Text="Deliver The Third Fleet", Dt={q="The Third Fleet"}, point={x=1089,y=5967,m=1437}},
        {Text="Accept The Cursed Crew", At="The Cursed Crew", point={x=1089,y=5967,m=1437}},

        -- todo might be several changes here if doing wetlands bit at 23
        {Text="Accept In Search of The Excavation Team (at the bridge out)", At="In Search of The Excavation Team", point={x=1150,y=5213,m=1437},
            PinAdd="Always kill Oozes in wetlands for Digging through the Ooze"},
        {Text="Run to excavation site (only kill oozes on the way) Accept Ormer's Revenge", At="Ormer's Revenge", point={x=3818,y=5089,m=1437}},
        {Text="Accept Uncovering the Past", At="Uncovering the Past", point={x=3880,y=5237,m=1437}},
        {Text="Deliver In Search of The Excavation Team", Dt={q="In Search of The Excavation Team"}, point={x=3891,y=5234,m=1437}},
        {Text="Accept In Search of The Excavation Team", At="In Search of The Excavation Team", point={x=3891,y=5234,m=1437}},
        {Text="Loot Flagonaut's Fossil", Item={Name="Glagongut's Fossil", Count=1}, point={x=3885,y=5219,m=1437}},

        -- todo: is it possible to do this first part before redridge run, to avoid congestion? the mobs are proper lvl.
        
        {Text="Run out of Excavation Site and complete Ormer's Revenge and Prospector", Mct={"Ormer's Revenge", "The Absent Minded Prospector"},
            point={x=3357,y=4078,m=1437}},
        {Text="Turn inn Accept Ormer's Revenge back in Excavation Site", Dt={q="Ormer's Revenge"}, point={x=3818,y=5089,m=1437}},
        {Text="Accept Accept Ormer's Revenge", At="Ormer's Revenge", point={x=3818,y=5089,m=1437}},

        {Text="Complete Ormer's Revenge and Uncovering the Past below", Mct={"Ormer's Revenge", "Uncovering the Past"}},

        {Text="Turn inn Accept Ormer's Revenge back up top", Dt={q="Ormer's Revenge"}, point={x=3818,y=5089,m=1437}},
        {Text="Accept Accept Ormer's Revenge", At="Ormer's Revenge", point={x=3818,y=5089,m=1437}},
        {Text="Turn inn Uncovering the Past", Dt={q="Uncovering the Past", Item="Silk Mantle of Gamn", Use=1}, point={x=3881,y=5239,m=1437}},

        {Text="Kill Sarltooth (NEED TO LOOT IT). Jump down to the left to avoid going around", Ct="Ormer's Revenge", point={x=3398,y=5227,m=1437}},
        {Text="Turn inn Accept Ormer's Revenge back up top", Dt={q="Ormer's Revenge", Item="Raptor's End", Use=1}, point={x=3818,y=5089,m=1437}},

        {Text="Complete War Banners to the east", Ct="War Banners", point={x=4317,y=4145,m=1437}},
        --lvl 26, 13h51min
        {Text="Accept Daily Delivery if not already done", At="Daily Delivery", point={x=4992,y=3937,m=1437}},
        {Text="Clean bags at vendor by xroads. May sell Healing Potion", Proximity=20, point={x=5019,y=3776,m=1437}},

        --{Text="Accept Tramping Paws", At="Tramping Paws", point={x=5643,y=4042,m=1437}},
        --{Text="Complete Tramping Paws (SE)", Ct="Tramping Paws", point={x=6112,y=5717,m=1437}},
        {Text="Deliver Tramping Paws at the Greenwarden", Dt={q="Tramping Paws"}, point={x=5636,y=4040,m=1437}},
        {Text="Accept Fire Taboo", At="Fire Taboo", point={x=5636,y=4040,m=1437}},
        {Text="Wok on fire taboo (Fenrunner, Mistweaver, Trapper only) towards the dam and loot Musquash Root", Item={Name="Musquash Root", Count=1}, point={x=6479,y=7531,m=1437}},
        {Text="Complete Fire Taboo on the way back north ", Ct="Fire Taboo", point={x=6132,y=5925,m=1437}},

        {Text="Deliver Fire Taboo", Dt={q="Fire Taboo"}, point={x=5636,y=4040,m=1437}},
        {Text="Accept Blisters on The Land", At="Blisters on The Land", point={x=5636,y=4040,m=1437}},

        -- grind oozes at ironbrands tomb or not ?
        -- check engineer dude for copper thingie if missing
        {Text="Run by Ironbeard's Tomb and complete Digging Through Ooze", Ct="Digging Through the Ooze", point={x=4352,y=2664},
            PinAdd="Red oozes higher drop than black? 5-10%"},

        {Text="Complete Blisters on The Land while running all the way west (can complete later too)", Ct="Blisters on The Land", point={x=1398,y=3051,m=1437}},
        {Text="Complete Apprentice's Duties around the ships", Ct="Apprentice's Duties", point={x=1398,y=3051,m=1437}},

        {Text="Complete The Cursed Crew. Snuffbox from Snelling in southern ship (below)", Ct="The Cursed Crew", 
            point={x=1398,y=3051,m=1437}},
        --[[
        Careful with this, pull properly back and take it slow
        TIP: go southern ship. Clear around outside first. Then use the crack entrance in the rear of the ship and carefully move upwards.
	        pet on passive, even stay some places.
	        Always palce a frost trap
	        Also enter from water in the front
        ]]
        
        {Text="Deliver Reclaming Goods (crate in mid of murloc camp)", Dt={q="Reclaiming Goods"}, point={x=1350,y=4139,m=1437}},
        {Text="Accept The Search Continues", At="The Search Continues", point={x=1350,y=4139,m=1437}},

        {Text="Deliver In Search of The Excavation Team", Dt={q="In Search of The Excavation Team"}, point={x=1153,y=5215,m=1437}},

        
        {Text="Deliver The Cursed Crew", Dt={q="The Cursed Crew"}, point={x=1091,y=5964,m=1437}},
        {Text="Accept Lifting the Curse", At="Lifting the Curse", point={x=1091,y=5964,m=1437}},
        

        {Text="Buy Razor Arrows (outside)", BuyItem={Npc="Edwina Monzor", Item="Razor Arrow", Count=2000},Class="Hunter",  point={x=1111,y=5832,m=1437}},
        {Text="Deliver Digging Through the Ooze", Dt={q="Digging Through the Ooze"}, point={x=1179,y=5799,m=1437}},
        {Text="Accept Fall of Dun Modr", At="Fall of Dun Modr", point={x=1084,y=5588,m=1437}},
        {Text="Deliver War Banners (in the keep)", Dt={q="War Banners"}, point={x=991,y=5738,m=1437}},
        {Text="Accept Nek'rosh's Gambit", At="Nek'rosh's Gambit", point={x=991,y=5738,m=1437}},

        {Text="Turn inn The Search Continues", Dt={q="The Search Continues"}, point={x=1360,y=3820,m=1437}},
        {Text="Accept Search More Hovels", At="Search More Hovels", point={x=1360,y=3820,m=1437}},
        {Text="Turn inn Search More Hovels", Dt={q="Search More Hovels"}, point={x=1398,y=3477,m=1437}},
        {Text="Accept Return the Statuette", At="Return the Statuette", point={x=1398,y=3477,m=1437}},
        --14h35m 26+80%

        {Text="Kill Captain Halyndor on northern ship. Use rudder north to access him. Loot Key", 
            Item={Name="Intrepid Strongbox Key", Count=1}, point={x=1607,y=2306,m=1437}},
        --[[
        run on the rudder on the back. Put pet on stay. 
	    go into water close to shore. 
	    Send pet on boss
	    call back and kite boss up on land
        ]]
        {Text="Deliver Lifting the Curse on box under boat. Enter towards ocean", Dt={q="Lifting the Curse"}, point={x=1441,y=2401,m=1437}},
        {Text="Accept The Eye of Paleth", At="The Eye of Paleth", point={x=1441,y=2401,m=1437}},
        
        {Text="Make sure Apprentice's Duties is complete", Ct="Apprentice's Duties"},
        {Text="Deliver Apprentice's Duties (take legs if got 5agi cloak)", Dt={q="Apprentice's Duties"}, point={x=854,y=5574,m=1437}},

        {Text="Buy lvl 25 food from Stuart Flemming (Rockscale Cod)", Class="Hunter", BuyItem={Npc="Stuart Fleming", Item="Rockscale Cod", Count=20}, point={x=804,y=5833,m=1437}},
        {Text="Deliver Return the Statuette", Dt={q="Return the Statuette", Item="Mariner Boots", Use=1}, point={x=830,y=5854,m=1437}},
        {Text="Deliver The Eye of Paleth (in the Inn, 1st floor)", Dt={q="The Eye of Paleth"}, point={x=1060,y=6057,m=1437}},
        {Text="Accept Cleansing the Eye", At="Cleansing the Eye", point={x=1060,y=6057,m=1437}},
        {Text="Deliver The Absent Minded Prospector (2nd floor Inn) - No followup", Dt={q="The Absent Minded Prospector", Item="Skullchipper"}, point={x=1082,y=6041,m=1437}},
        
        {Text="Complete Blisters on The Land towards vendor at xroads", Ct="Blisters on The Land", Proximity=20, point={x=5019,y=3776,m=1437},
            PinAdd="Need to be aprox lvl27+? 32 350 XP? now, so grind extra on the way if behind"},
        {Text="Clean bags at vendor by xroads. May sell Healing Potion", Proximity=20, point={x=5019,y=3776,m=1437}},

        {Text="Enter orc place this way", Proximity=40, point={x=4842,y=4507,m=1437}},
        {Text="Deliver Nek'rosh's Gambit the catapult (careful pulls, they net)", Dt={q="Nek'rosh's Gambit"}, point={x=4745,y=4706,m=1437}},
        {Text="Accept Defeat Nek'rosh", At="Defeat Nek'rosh", point={x=4745,y=4706,m=1437}},
        {Text="Run out and around to the South-East. Pull Nek'Rosh from the hill and kite to deliver Blisters on the land", Proximity=30, point={x=5432,y=5485,m=1437}},
        
        {Text="Deliver Blisters on The Land", Dt={q="Blisters on The Land", Item="Fen Keeper Robe", Use=1}, point={x=5636,y=4040,m=1437}},

        {Text="If still not lvl 28 (or within 1k XP) grind something"},
        {Text="Deliver Fall of Dun Modr", Dt={q="Fall of Dun Modr"}, point={x=4980,y=1826,m=1437}},
        {Text="Accept The Thandol Span (on the right)", At="The Thandol Span", point={x=4995,y=1821,m=1437}},
        {Text="On Bridge, enter right door, get to bottom floor and turnin The Thandol Span. Use pet to tank mobs", 
            Dt={q="The Thandol Span"}, point={x=5128,y=795,m=1437}},
        {Text="Accept The Thandol Span pt2", At="The Thandol Span", point={x=5128,y=795,m=1437}},
        {Text="Deliver The Thandol Span back at previous place", Dt={q="The Thandol Span"}, point={x=4995,y=1821,m=1437}},
        {Text="Accept The Thandol Span pt3", At="The Thandol Span", point={x=4995,y=1821,m=1437}},

        
        -- This step only works reliably with some runspeed increase. Hunter improved aspect is reliable
        {Text="Run back to Arathi across bridge, Go on broken bridge to left, jump over on pillar and accept MacKreel's Moonshine", 
            At="MacKreel's Moonshine", point={x=4324,y=9264,m=1417}, PinAdd="Make sure you can do the jump."},

        {Text="Jump into water, Loot Waterlogged Letter", Item={Name="Waterlogged Envelope", Count=1}, point={x=5010,y=778,m=1437}},
        {Text="Swim EAST", Proximity=20, point={x=5244,y=9133,m=1417}},
        {Text="Destroy Cache of Explosives for The Thandol Span (Careful with shadowcasters)", Ct="The Thandol Span", point={x=4874,y=8806,m=1417}},
        {Text="Run back and Deliver The Thandol Span back at previous place", Dt={q="The Thandol Span", Item="Dwarven Guard Cloak", Use=1}, point={x=4995,y=1821,m=1437}},
        {Text="Accept Plea To The Alliance", At="Plea To The Alliance", point={x=4995,y=1821,m=1437}},
        
        {Text="Run to Refuge Pointe (Arathi). Deliver Plea to the alliance", Dt={q="Plea To The Alliance"}, point={x=4584,y=4756,m=1417}},

        {Text="Discover FP", Proximity=20, point={x=4576,y=4612,m=1417}},
        {Text="Run West on road to Hillsbrad Foothills", Zone="Hillsbrad Foothills", point={x=2012,y=2942,m=1417},
            PinAdd="If First Aid: Stormgarde Keep, at the first intersection go right and hug the wall around over the bridge"},
        {Text="Enter the Inn, Deliver MacKreel's Moonshine in the Cellar", Dt={q="MacKreel's Moonshine"}, point={x=5215,y=5868,m=1424}, Class="Hunter"},
        {Text="Fly to Menethil", Taxi="Menethil Harbor, Wetlands", point={x=4934,y=5228,m=1424}},

        {Text="(only if time b4 boat leaves. Can do l8r) Deliver Nek'Rosh (in the keep)", Dt={q="Defeat Nek'rosh", Item="Ancient War Sword"}, point={x=986,y=5749,m=1437}},

        {Text="Take boat to Darkshore (right-side boat)", Zone="Darkshore", point={x=461,y=5715,m=1437}},
        --{Text="Heartstone to Duskwood", Zone="Duskwood", UseItem="Hearthstone"},
    }
}

-- Author      : G3m7
-- Create Date : 5/11/2019 12:40:07 PM

CLGuide_Tanaris1 = {
    Title="43-43 Tanaris",
    Pinboard = {},
    Steps = {
        --[[
            Expected quests:
                The Brassbolt Brothers 2769 410@46
                Tran'rek 2864  390@45
                Goblin Sponsorship 1183  700@37
                News for fizzle 1137 1450@38
                Stoley's Debt 2872 390@45
                Akiris by the bundle 623 
                ... and bugs 1258
                cortellos riddle
                summoning the princess
                to the hinterlands
                Whiskey Slim's Lost Grog
                
.levelup 43
.quest add 2769
.quest add 2864
.quest add 1183
.quest add 2872
.quest add 623
.quest add 1258
.quest complete 1258
.quest add 1112
.quest add 625
.quest add 1137
.quest add 656
.quest add 1449
.quest add 580
        ]]
        {Text="Accept Parts for Kravel (at the pier, lowlvl q)", At="Parts for Kravel", point={x=6335,y=3846}},
        {Text="Fly to Theramore", Taxi="Theramore, Dustwallow Marsh", point={x=6308,y=3716}},
        {Text="Deliver Akiris by the Bundle towards pier", Dt={q="Akiris by the Bundle"}, point={x=6884,y=5323,m=1445}},
        {Text="Deliver ... and Bugs (at the Inn)", Dt={q="... and Bugs"}, point={x=6634,y=4547,m=1445}},

        {Text="Move to point, then swim accross water to the west", Proximity=50, point={x=5900,y=4392,m=1445}},
        {Text="Swim to point, go up from water at point", Proximity=50, point={x=5553,y=5015,m=1445}},
        --{Text="Loot Overdue Package for Ledger to Tanaris", Item={Name="Overdue Package", Count=1}, point={x=5408,y=5591}},
        -- won't bother with razzeric's regardless. The run to shimmering alone is not worth
        --{Text="Loot Seaforium Booster for Razzeric's Tweaking", Ct="Razzeric's Tweaking", point={x=5407,y=5649}},
        -- todo: this may be the right time to deliver ZF quest if done?
        {Text="Continue W-SW to raptor cave and deliver Cortello's Riddle", Dt={q="Cortello's Riddle"}, point={x=3110,y=6615,m=1445},
            PinAdd="Pickup Tiara of the deep as well if doing ZF"},
        {Text="Accept Cortello's Riddle (final part)", At="Cortello's Riddle", point={x=3110,y=6615,m=1445}},
        {Text="Deathwarp back to Theramore", DeathWarp=1},

        -- ZF: run and turn in Tabetha's task and get quests at that lady. Can probably deathwarp back to theramore 

        {Text="Fly to Tanaris", Taxi="Gadgetzan, Tanaris", point={x=6747,y=5130,m=1445}},

        {Text="Deliver Tran'rek", Dt={q="Tran'rek"}, point={x=5156,y=2676,m=1446}},
        {Text="Run to Shimmering Flats", Zone="Thousand Needles", point={x=5125,y=2085,m=1446}},

        -- Shimmering Flats
        {Text="Deliver Parts for Kravel", Dt={q="Parts for Kravel"}, point={x=7779,y=7727,m=1441}},

        {Text="Deliver News for Fizzle", Dt={q="News for Fizzle", Item="Fizzle's Zippy Lighter"}, point={x=7806,y=7712,m=1441}},

        --{Text="Deliver Encrusted Tail Fins (if done)", Dt={q="Encrusted Tail Fins"}, point={x=7814,y=7712,m=1441}},
        {Text="Deliver The Brassbolts Brothers", Dt={q="The Brassbolts Brothers"}, point={x=7814,y=7712,m=1441}},

        {Text="Accept Delivery to the Gnomes (lowlvl)", At="Delivery to the Gnomes", point={x=7779,y=7727,m=1441}},
        {Text="Deliver  Delivery to the Gnomes",  Dt={q="Delivery to the Gnomes"}, point={x=7806,y=7712,m=1441}},

        {Text="Deliver Goblin Sponsorship", Dt={q="Goblin Sponsorship"}, point={x=8018,y=7588,m=1441}},
        {Text="Accept The Eighteenth Pilot", At="The Eighteenth Pilot", point={x=8018,y=7588,m=1441}},
        {Text="Accept Keeping Pace", At="Keeping Pace", point={x=8018,y=7588,m=1441}},
        
        {Text="Deliver The Eighteenth Pilot", Dt={q="The Eighteenth Pilot"}, point={x=8032,y=7609,m=1441}},
        -- ZF: possibly worth picking up Razzeric's tweaking if you end up going to dustwallow to turnin tiara
        --{Text="Accept Razzeric's Tweaking", At="Razzeric's Tweaking", point={x=8032,y=7609}},

        {Text="Talk to Zamek to complete Zamek's Distraction", Dt={q="Zamek's Distraction"}, point={x=7981,y=7703,m=1441}},
        {Text="Follow Zamek, after he distracts goblin in house loot Rizzle's Unguarded Plans to turn inn Keeping Pace", Dt={q="Keeping Pace"}, point={x=7721,y=7739,m=1441}},
        {Text="Accept Rizzle's Schematics", At="Rizzle's Schematics", point={x=7721,y=7739,m=1441}},
        {Text="Deliver Rizzle's Schematics", Dt={q="Rizzle's Schematics"}, point={x=8018,y=7588,m=1441}, PinAdd="Delete the Sample of Indurium Ore after delivering quest"},

        {Text="Run back to Gadgetzan", Zone="Tanaris", point={x=7536,y=9720,m=1441}},
        
        -- Gadgetzan
        {Text="Accept Handle With Care", At="Handle With Care", point={x=5235,y=2691,m=1446}},

        {Text="Accept Water Pouch Bounty", At="Water Pouch Bounty", point={x=5249,y=2844,m=1446}},
        {Text="Accept Wastewander Justice", At="Wastewander Justice", point={x=5246,y=2852,m=1446}},

        -- Steemwheedle port
        {Text="Accept Pirate Hats Ahoy! (Steemwheedle Port)", At="Pirate Hats Ahoy!", point={x=6656,y=2227,m=1446}},
        -- todo: arrows here
        {Text="Make sure you have arrows", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=2800}, point={x=6701,y=2199,m=1446}},

        {Text="Accept Screecher Spirits", At="Screecher Spirits", point={x=6699,y=2236,m=1446}},
        {Text="Accept Southsea Shakedown", At="Southsea Shakedown", point={x=6706,y=2389,m=1446}},
        {Text="Deliver Stoley's Debt", Dt={q="Stoley's Debt"}, point={x=6711,y=2398,m=1446}},
        {Text="Accept Stoley's Shipment", At="Stoley's Shipment", point={x=6711,y=2398,m=1446}},

        {Text="Complete Wastewander Justice & Water Pouch Bounty", Mct={"Wastewander Justice", "Water Pouch Bounty"}, point={x=6278,y=3032,m=1446},
            PinAdd="Bandits, Thiefs and 5 Wastewander Water Pouches. Look for Caliph Scorpidsting"},

        {Text="Deliver Water Pouch Bounty", Dt={q="Water Pouch Bounty"}, point={x=5248,y=2844,m=1446}},
        {Text="Deliver Wastewander Justice", Dt={q="Wastewander Justice"}, point={x=5246,y=2851,m=1446}},
        {Text="Accept More Wastewander Justice", At="More Wastewander Justice", point={x=5246,y=2851,m=1446}},

        {Text="Fly to Thalanaar, Feralas", Taxi="Thalanaar, Feralas", point={x=5100,y=2934,m=1446}},
    }
}
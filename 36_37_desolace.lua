-- Author      : G3m7
-- Create Date : 5/11/2019 12:37:59 PM

CLGuide_Desolace = {
    Title="36-37 Desolace",
    Pinboard = {},
    Steps = {
        {Text="Turn in any cloth you can at gnomes", point={x=7405,y=4813,m=1455}},
        
        {Text="Accept Reclaimers' Business in Desolace", At="Reclaimers' Business in Desolace", point={x=6993,y=2130,m=1455}},
        {Text="Turn inn Letter to Stormpike (Hall of Explorers)", Dt={q="Letter to Stormpike"}, point={x=7465,y=1173,m=1455}},
        {Text="Accept Further Mysteries", At="Further Mysteries", point={x=7465,y=1173,m=1455}},
        {Text="Turn in any cloth you can at dwarves", point={x=4327,y=3161,m=1455}},
        {Text="Turn inn A King's Tribute (SE of IF entrance)", Dt={q="A King's Tribute"}, point={x=3968,y=8522,m=1455}},
        {Text="Wait for RP, then accept A King's Tribute", At="A King's Tribute", point={x=3883,y=8788,m=1455}},
        {Text="Turn inn A King's Tribute at the king", Dt={q="A King's Tribute"}, point={x=3908,y=5623,m=1455}},

        {Text="Fly to Menethil, Wetlands", Taxi="Menethil Harbor, Wetlands", point={x=5550,y=4774,m=1455}},
        {Text="Take the boat to Theramore", Zone="Dustwallow Marsh", point={x=523,y=6362,m=1437}},
        {Text="Fly to Desolace", Taxi="Nijel's Point, Desolace", point={x=6748,y=5131,m=1445}, PinAdd="5min break"},

        {Text="Set HS in Desolace", SetHs="Innkeeper Lyshaerya", point={x=6628,y=655,m=1443}},
        {Text="Buy Razor Arrows", Class="Hunter", BuyItem={Npc="Christi Galvanis", Item="Razor Arrow", Count=2800}, point={x=6662,y=698,m=1443}},
        {Text="Deliver Brother Anton", Dt={q="Brother Anton"}, point={x=6652,y=791,m=1443}},
        {Text="Accept Down the Scarlet Path", At="Down the Scarlet Path", point={x=6652,y=791,m=1443}},
        {Text="Deliver Reclaimers' Business in Desolace", Dt={q="Reclaimers' Business in Desolace"}, point={x=6620,y=962,m=1443}},
        {Text="Accept The Karnitol Shipwreck", At="The Karnitol Shipwreck", point={x=6620,y=962,m=1443}},
        {Text="Accept Reagents for Reclaimers Inc.", At="Reagents for Reclaimers Inc.", point={x=6620,y=962,m=1443}},
        
        {Text="Accept Centaur Bounty", At="Centaur Bounty", point={x=6674,y=1087,m=1443},
            PinAdd="If you are far behind lvl 36, do one of the Alliance quests too"},

        {Text="Accept Vahlarriel's Search", At="Vahlarriel's Search", point={x=6644,y=1182,m=1443}},

        -- exit nijels point
        {Text="Deliver Vahlarriel's Search. Exit Nijels and go west", Dt={q="Vahlarriel's Search"}, point={x=5654,y=1782,m=1443}},
        {Text="Accept Vahlarriel's Search", At="Vahlarriel's Search", point={x=5654,y=1782,m=1443}},

        {Text="Complete Reagents for Reclaimers Inc (Satyrs SE of Nijels)", Ct="Reagents for Reclaimers Inc.", point={x=7175,y=2125,m=1443}},
        
        -- only worth if we can HS already now. Main reason for doing is because its a green quest already at 36
        -- if hs is not ready, just wait with completing centaur bounty and run to nijels after Reagents is done
        {Text="Complete Centaur Bounty (further south)", Ct="Centaur Bounty", point={x=7201,y=4324,m=1443}},
        {Text="Hearthstone to Nijels", UseItem="Hearthstone", Proximity=50, point={x=6626,y=662,m=1443}},


        {Text="Buy Razor Arrows", Class="Hunter", BuyItem={Npc="Christi Galvanis", Item="Razor Arrow", Count=2800}, point={x=6662,y=698,m=1443}},
        {Text="Deliver Reagents for Reclaimers Inc", Dt={q="Reagents for Reclaimers Inc."}, point={x=6620,y=963,m=1443}},
        {Text="Accept Reagents for Reclaimers Inc.", At="Reagents for Reclaimers Inc.", point={x=6620,y=962,m=1443}},
        -- if we did the early HS
        {Text="Deliver Centaur Bounty", Dt={q="Centaur Bounty"}, point={x=6674,y=1087,m=1443}},
        {Text="Deliver Vahlarriel's Search", Dt={q="Vahlarriel's Search"}, point={x=6644,y=1182,m=1443}},
        {Text="Accept Vahlarriel's Search", At="Vahlarriel's Search", point={x=6644,y=1182,m=1443}},

        
        -- Exit nijels again
        {Text="Accept Bone Collector (to the south)", At="Bone Collector", point={x=6233,y=3898,m=1443},
            PinAdd="Kill scorpions on the way for venom"},
        

        -- kodo roundup/bone collector/aged kodo for reagents
        {Text="Accept Kodo Roundup", At="Kodo Roundup", point={x=6087,y=6186,m=1443},
            PinAdd="Kill Aged Kodos for hides"},
        {Text="Complete Kodo Roundup and Bone Collector", Mct={"Kodo Roundup", "Bone Collector"}, point={x=5418,y=6171,m=1443}},
        {Text="Deliver Kodo Roundup", Dt={q="Kodo Roundup", Item="Wrangling Spaulders"}, point={x=6087,y=6186,m=1443}},

        -- run to shipwreck
        {Text="Deliver The Karnitol Shipwreck", Dt={q="The Karnitol Shipwreck"}, point={x=3611,y=3046,m=1443}},
        {Text="Accept The Karnitol Shipwreck", At="The Karnitol Shipwreck", point={x=3611,y=3046,m=1443}},
        {Text="Accept Claim Rackmore's Treasure (book on barrel)", At="Claim Rackmore's Treasure!", point={x=3607,y=3040,m=1443}},
        {Text="Farm the Drysnaps in the water for Rackmore's Silver Key", Item={Name="Rackmore's Silver Key", Count=1}},

        --lighthouse
        -- must come before thunder axe fortress
        {Text="Accept Sceptre of Light (by the lighthouse)", At="Sceptre of Light", point={x=3888,y=2717,m=1443}},


        -- run to thunder axe fortress
        --thunder axe fortress
        {Text="Complete Sceptre of Light (Burning Blade Seer in tower)", Ct="Sceptre of Light", point={x=5517,y=3015,m=1443}},
        {Text="Deliver Vahlarriel's Search (inside thunder axe fortress)", Dt={q="Vahlarriel's Search"}, point={x=5486,y=2613,m=1443}},
        {Text="Accept Search for Tyranis", At="Search for Tyranis", point={x=5486,y=2613,m=1443}},
        {Text="Complete Search for Tyranis by killing Tyranis Malem (go out, building on right)", Ct="Search for Tyranis", point={x=5301,y=2908,m=1443}},
        {Text="Deliver Search for Tyranis (back in other building)", Dt={q="Search for Tyranis"}, point={x=5486,y=2613,m=1443}},
        {Text="Accept Return to Vahlarriel (ESCORT QUEST)", At="Return to Vahlarriel", point={x=5486,y=2613,m=1443}},
        {Text="Complete Return to Vahlarriel", Ct="Return to Vahlarriel", point={x=5769,y=3090,m=1443}},
        -- Ct scorpions/kodos on way back to nijel if not yet done

        {Text="Delliver Bone Collector (to the south)", Dt={q="Bone Collector", Item="Kodobone Necklace", Use=1}, point={x=6233,y=3898}, PinAdd="Equip the new neck"},
        {Text="Complete Reagents for Reclaimers Inc before returning to Nijel", Ct="Reagents for Reclaimers Inc.", point={x=6665,y=2435,m=1443}},


        -- run to nijel, deliver stufff
        {Text="Deliver Return to Vahlarriel", Dt={q="Return to Vahlarriel"}, point={x=6645,y=1182,m=1443}},
        {Text="Deliver The Karnitol Shipwreck", Dt={q="The Karnitol Shipwreck"}, point={x=6620,y=963,m=1443}},
        {Text="Accept The Karnitol Shipwreck", At="The Karnitol Shipwreck", point={x=6620,y=963,m=1443}},
        {Text="Deliver Reagents for Reclaimers Inc.", Dt={q="Reagents for Reclaimers Inc."}, point={x=6620,y=963,m=1443}},
        {Text="Accept Reagents for Reclaimers Inc.", At="Reagents for Reclaimers Inc.", point={x=6620,y=963,m=1443}},
        {Text="Buy Razor Arrows", Class="Hunter", BuyItem={Npc="Christi Galvanis", Item="Razor Arrow", Count=2800}, point={x=6662,y=698,m=1443}},


        -- water outside lighthouse quests
        {Text="Deliver Sceptre of Light (by the lighthouse)", Dt={q="Sceptre of Light"}, point={x=3888,y=2717,m=1443}},
        {Text="Accept Book of the Ancients", At="Book of the Ancients", point={x=3888,y=2717,m=1443}},
        {Text="Complete Claim Rackmore's Treasure by finding the Golden Key on nagas close to shore", 
            Ct="Claim Rackmore's Treasure!", point={x=3686,y=2551,m=1443}},

        {Text="Turn inn Claim Rackmore's Treasure (at outer isle)", Dt={q="Claim Rackmore's Treasure!"}, point={x=3000,y=871,m=1443}},
        {Text="Complete Book of the Ancients; Click statue, kill 38 mob that spawns for loot", Ct="Book of the Ancients", point={x=2820,y=662,m=1443}},
        {Text="Complete The Karnitol Shipwreck. Drops from Sea Witch and Tidehunter on and around the isle", Ct="The Karnitol Shipwreck", PinAdd="This has a low droprate (probably 5% or so)"},
        {Text="Deliver Book of the Ancients (by the lighthouse)", Dt={q="Book of the Ancients", Item="Silkstream Cuffs", Use=1}, point={x=3888,y=2717,m=1443}},

        {Text="Accept Ghost-o-plasm Round Up", At="Ghost-o-plasm Round Up", point={x=4783,y=6182,m=1443}},

        -- THIS IS NOT A GOOD QUEST FOR GROUPS
        {Text="Complete Reagents for Reclaimers Inc. (at demon ruins)", Ct="Reagents for Reclaimers Inc.", point={x=5142,y=8266,m=1443},
            PinAdd="Can clear out, then go next step, and complete on way back to deliver Ghost-o-plasm"},
        
        {Text="Complete Down the Scarlet Path and Ghost-o-plasm Round Up", UseItem="Crate of Ghost Magnets",Mct={"Down the Scarlet Path", "Ghost-o-plasm Round Up"}, point={x=6383,y=9177,m=1443}},
        {Text="If reagents is done, deathwarp then next step. Otherwise run north and complete demons", DeathWarp=1},
        {Text="Deliver Ghost-o-plasm Round Up", Dt={q="Ghost-o-plasm Round Up", Item="Condor Bracers", Use=1}, point={x=4783,y=6183,m=1443}},
        -- not doing UseItem hs so the condor bracers button will be shown instead
        {Text="Hearthstone to Nijels", Proximity=50, point={x=6626,y=662,m=1443}},
        

        {Text="Buy Razor Arrows/Vendor", Class="Hunter", BuyItem={Npc="Christi Galvanis", Item="Razor Arrow", Count=2800}, point={x=6662,y=698,m=1443}},
        {Text="Deliver Down the Scarlet Path", Dt={q="Down the Scarlet Path"}, point={x=6652,y=791,m=1443}},
        {Text="Accept Down the Scarlet Path", At="Down the Scarlet Path", point={x=6652,y=791,m=1443}},

        {Text="Deliver The Karnitol Shipwreck", Dt={q="The Karnitol Shipwreck"}, point={x=6620,y=963,m=1443}},
        {Text="Accept The Karnitol Shipwreck", At="The Karnitol Shipwreck", point={x=6620,y=963,m=1443}},
        {Text="Deliver Reagents for Reclaimers Inc.", Dt={q="Reagents for Reclaimers Inc."}, point={x=6620,y=963,m=1443}},
        {Text="Accept Reagents for Reclaimers Inc.", At="Reagents for Reclaimers Inc.", point={x=6620,y=963,m=1443}},
        {Text="Fly to Theramore, Dustwallow Marsh", Taxi="Theramore, Dustwallow Marsh", point={x=6466,y=1053,m=1443}, PinAdd="5min break"},
        




        -- TODO: Test if setting HS in theramore to HS after finishing these steps works. 
        -- Question is if it takes enough time for it to be ready
        -- theramore quests to retake. This is continuation from end of 32_34 section
        {Text="Accept The Deserters (the keep)", At="The Deserters", point={x=6821,y=4862,m=1445}},
        {Text="Buy Cured Ham Steak for pet (keep, first floor)", Class="Hunter", BuyItem={Npc="Dwane Wertle", Item="Cured Ham Steak", Count=20}, point={x=6817,y=4734,m=1445}},
        {Text="Turn inn Morgan Stern (At the Inn)", Dt={q="Morgan Stern"}, point={x=6634,y=4547,m=1445}},
        {Text="Accept Mudrock Soup and Bugs (At the Inn)", At="Mudrock Soup and Bugs", point={x=6634,y=4547,m=1445}},
        {Text="Farm Turtles for Mudrock Soup up to this point", Proximity=50, point={x=6100,y=2775,m=1445},
            PinAdd="Low droprate on mudrock, only complete if lucky/behind"},
        {Text="Accept Jarl Needs Eyes", At="Jarl Needs Eyes", point={x=5544,y=2627,m=1445}},
        
        -- Assuming we still have hungry from 32_34 section here
        {Text="Finish Hungry! and Mudrock Soup", Mct={"Mudrock Soup and Bugs", "Hungry!"}, point={x=5746,y=1702,m=1445}},
        {Text="Accept Stinky's Escape (unless you did at lvl ~32)", At="Stinky's Escape", point={x=4688,y=1753,m=1445}},
        {Text="Complete Stinky's Escape (finishes at point)", Ct="Stinky's Escape", point={x=4885,y=2460,m=1445}},
        {Text="Complete Jarl Needs Eyes", Ct="Jarl Needs Eyes", point={x=3460,y=2182,m=1445}},

        {Text="Deliver Hungry!", Dt={q="Hungry!"}, point={x=3515,y=3826,m=1445}},

        {Text="Defeat Barlos Jacken, then Deliver The Deserters", Dt={q="The Deserters"}, point={x=3609,y=5431,m=1445}},
        {Text="Accept The Deserters", At="The Deserters", point={x=3609,y=5431,m=1445}},
        
        {Text="Deathwarp to Theramore", DeathWarp=1},
        -- run to jarl takes aproximately 1.5min as hunter
        {Text="Deliver Jarl Needs Eyes (skip followup)", Dt={q="Jarl Needs Eyes"}, point={x=5544,y=2627,m=1445}},
        {Text="Deathwarp or run back to Theramore", DeathWarp=1},

        -- after getting back to theramore
        {Text="Deliver Mudrock Soup and Bugs (At the Inn)", Dt={q="Mudrock Soup and Bugs"}, point={x=6634,y=4547,m=1445}},
        {Text="Accept ... and Bugs", At="... and Bugs", point={x=6634,y=4547,m=1445}},
        {Text="Deliver Stinky's Escape (same guy)", Dt={q="Stinky's Escape"}, point={x=6634,y=4547,m=1445}},
        
        {Text="Deliver The Deserters (in the keep)", Dt={q="The Deserters"}, point={x=6821,y=4862,m=1445}},
        {Text="Buy arrows & check for bows", Class="Hunter", BuyItem={Item="Razor Arrow", Count=2800}, point={x=6794,y=4991,m=1445}},
        -- TODO: How the hell do you get Vime's report after this??
        
        -- boat to menethil
        {Text="Take boat to Menethil", Zone="Wetlands", point={x=7152,y=5632,m=1445}},
        
    }
}
-- Author      : G3m7
-- Create Date : 6/19/2019 7:17:41 PM


CLGuide_Ashenvale2 = {
    Title="28-29 Ashenvale",
    Pinboard = {},
    Steps = {
        
        {Text="Deliver The Corruption Abroad (skip followup)", Dt={q="The Corruption Abroad"}, point={x=3833,y=4304,m=1439}},
        {Text="Fly to Astranaar", Taxi="Astranaar, Ashenvale", point={x=3633,y=4558,m=1439}},
        {Text="Run west to maestra's Post, accept The Tower of Althalaxx", At="The Tower of Althalaxx", point={x=2620,y=3870,m=1440}},
        {Text="Accept The Howling Vale", At="The Howling Vale", point={x=2224,y=5298,m=1440}},
        {Text="Accept Vile Satyr! Dryads in Danger!", At="Vile Satyr! Dryads in Danger!", point={x=2174,y=5334,m=1440}},
        
        {Text="Run back to Astranaar, accept Kayneth Stillwind", At="Kayneth Stillwind", point={x=3467,y=4884,m=1440}},
        {Text="Accept Raene's Cleansing", At="Raene's Cleansing", point={x=3662,y=4958,m=1440}},
        {Text="Accept An Aggressive Defense", At="An Aggressive Defense", point={x=3663,y=4958,m=1440}},
        {Text="Accept Fallen Sky Lake", At="Fallen Sky Lake", point={x=3735,y=5180,m=1440}},
        
        {Text="Go east to point", Proximity=15, point={x=4949,y=5468,m=1440}},
        {Text="Deliver Raene's Cleansing at the moonwell", Dt={q="Raene's Cleansing"}, point={x=5353,y=4621,m=1440}},
        {Text="Accept Raene's Cleansing", At="Raene's Cleansing", point={x=5353,y=4621,m=1440}},
        {Text="Run south, east to Raynewood Retreat then up towards felwood. Kill trees you find on way for Wooden Key.", point={x=5441,y=3540,m=1440}},
        {Text="Loot Worn Chest after finding Wooden Key to complete Raene's Cleansing", Ct="Raene's Cleansing", point={x=5441,y=3540,m=1440}},
        {Text="Complete The Howling Vale up above worn chest by worgens", Ct="The Howling Vale", point={x=5049,y=3913,m=1440}},
        {Text="Run towards point", Proximity=50, point={x=6540,y=4430,m=1440}},

        {Text="Deliver Vile Satyr! Dryads in Danger!", Dt={q="Vile Satyr! Dryads in Danger!"}, point={x=7832,y=4483,m=1440}},
        {Text="Accept The Branch of Cenarius", At="The Branch of Cenarius", point={x=7832,y=4483,m=1440}},
        {Text="Kill Geltharis to complete The Branch of Cenarius", Ct="The Branch of Cenarius", point={x=7795,y=4239,m=1440}},

        {Text="Deliver Kayneth Stillwind to the east", Dt={q="Kayneth Stillwind"}, point={x=8524,y=4471,m=1440}},
        {Text="Accept Forsaken Diseases", At="Forsaken Diseases", point={x=8524,y=4471,m=1440}},

        {Text="Run into Azshara", Zone="Azshara", point={x=9536,y=4835,m=1440}},
        {Text="Fly back to Astranaar", Taxi="Astranaar, Ashenvale", point={x=1190,y=7759,m=1447}},


        {Text="Run West, Deliver The Howling Vale", Dt={q="The Howling Vale"}, point={x=2224,y=5298,m=1440}},
        {Text="Deliver The Branch of Cenarius!", Dt={q="The Branch of Cenarius", Item="Faerie Mantle", Use=1}, point={x=2174,y=5334,m=1440}},
        {Text="Accept Satyr Slaying!", At="Satyr Slaying!", point={x=2174,y=5334,m=1440}},

        {Text="Go east to point", Proximity=15, point={x=4949,y=5468,m=1440}},
        {Text="Deliver Raene's Cleansing (there are no shortcuts)", Dt={q="Raene's Cleansing"}, point={x=5353,y=4621,m=1440}},
        {Text="Accept Raene's Cleansing", At="Raene's Cleansing", point={x=5353,y=4621,m=1440}},

        {Text="Complete An Aggressive Defense", Ct="An Aggressive Defense", point={x=5069,y=6122,m=1440}},
        {Text="Check bow vendor for bows. Restock arrows if needed", point={x=5028,y=6728,m=1440}},
        {Text="Accept Elemental Bracers", At="Elemental Bracers", point={x=4981,y=6722,m=1440}},
        {Text="Loot 5 Intact Elemental Bracer in the lake", Item={Name="Intact Elemental Bracer", Count=5}},
        {Text="Use Divining Scroll to complete Elemental Bracer", Ct="Elemental Bracers", UseItem="Divining Scroll"},
        {Text="Deliver Elemental Bracers (skip followup)", Dt={q="Elemental Bracers"}, point={x=4981,y=6722,m=1440}},
        {Text="Train Hunter skills", TrainSkill=1, point={x=5013,y=6795,m=1440}},

        {Text="Run E-NE to road, across bridge then south, Kill Shadethicket Oracle for Fallen Sky Lake", Ct="Fallen Sky Lake", point={x=6667,y=8219,m=1440}},
        {Text="Kill oozes, loot the box they spawn, complete Raene's Cleansing", Ct="Raene's Cleansing", point={x=7298,y=7327,m=1440}},
        {Text="Complete Forsaken Diseases", Ct="Forsaken Diseases", point={x=7532,y=7193,m=1440}},

        {Text="Deliver Forsaken Diseases (skip followup)", Dt={q="Forsaken Diseases"}, point={x=8524,y=4471,m=1440}},
        {Text="Free the highborne soul for Tower of Althalax (go next step manually after)", Ct="The Tower of Althalax", point={x=8159,y=4857,m=1440},
            PinAdd="We're completing Satyr Slaying! as well now. Get as many horns as can in eastern camp. Not enough mobs west"},
        {Text="Free the highborne spirit to complete Tower of Althalax at Night's Run", Ct="The Tower of Althalax", point={x=6662,y=5700,m=1440}},
        {Text="Complete Satyr Slaying!", Ct="Satyr Slaying!"},

        {Text="Run all the way back west to moonwell, deliver Raene's Cleansing", Dt={q="Raene's Cleansing"}, point={x=5353,y=4621,m=1440}},
        {Text="Accept Raene's Cleansing (wait for RP)", At="Raene's Cleansing", point={x=5353,y=4621,m=1440}},
        {Text="Go a bit south, east then up along hidden path, Deliver Raene's Cleansing", Dt={q="Raene's Cleansing"}, point={x=5639,y=4925,m=1440}},
        {Text="Accept Raene's Cleansing", At="Raene's Cleansing", point={x=5639,y=4925,m=1440}},
        {Text="Run back to moonwell, deliver Raene's Cleansing", Dt={q="Raene's Cleansing"}, point={x=5353,y=4621,m=1440}},
        {Text="Accept Raene's Cleansing", At="Raene's Cleansing", point={x=5353,y=4621,m=1440}},

        -- todo: do we use the shoulders from this q?
        {Text="Run to Astranaar, deliver Fallen Sky Lake (no followup)", Dt={q="Fallen Sky Lake", Item="Snapbrook Armor", Use=1}, point={x=3737,y=5179,m=1440}},
        
        {Text="Turn An Aggressive Defense", Dt={q="An Aggressive Defense"}, point={x=3661,y=4959,m=1440}},
        {Text="Deliver Raene's Cleansing", Dt={q="Raene's Cleansing"}, point={x=3662,y=4958,m=1440}},
        {Text="Accept Raene's Cleansing", At="Raene's Cleansing", point={x=3662,y=4958,m=1440}},
        
        {Text="Check bow-vendor at mystral lake for 30bow (restock arrows too if needed", Class="Hunter", BuyItem={Item="Dense Shortbow", Count=1}, point={x=5027,y=6725,m=1440}},

        {Text="Run past Silverwind Refuge, east of lake, south, Deliver Raene's Cleansing", Dt={q="Raene's Cleansing"}, UseItem="Dartol's Rod of Transformation", point={x=5085,y=7508,m=1440}},
        {Text="Accept Raene's Cleansing", At="Raene's Cleansing", point={x=5085,y=7508,m=1440}},
        {Text="Kill Ran Bloodtooth (MAKE SURE TO LOOT) + 4 other mobs, complete Raene's Cleansing", Ct="Raene's Cleansing", point={x=5476,y=7958,m=1440}},
        {Text="Deliver Raene's Cleansing back at last guy", Dt={q="Raene's Cleansing"}, UseItem="Dartol's Rod of Transformation", point={x=5085,y=7508,m=1440}},
        {Text="Accept Raene's Cleansing", At="Raene's Cleansing", point={x=5085,y=7508,m=1440}},
        {Text="Deathwarp to Astranaar", DeathWarp=1},
        {Text="Deliver Raene's Cleansing in Astranaar", Dt={q="Raene's Cleansing", Item="Glacial Stone"}, point={x=3662,y=4959,m=1440}},
        
        -- todo: point, useitem in tower step etc
        {Text="Run west, deliver Tower of Althalax", Dt={q="The Tower of Althalaxx"}, UseItem="Ring of Pure Silver", point={x=2620,y=3870,m=1440}},
        {Text="If still lvl 29, accept Supplies to Auberdine, complete it (escort), return to deliver then go next step", point={x=2621,y=3887,m=1440}},
        {Text="Deliver Satyr Slaying!", Dt={q="Satyr Slaying!"}, point={x=2174,y=5334,m=1440}},
        
        -- todo: probably not worth doing retrieval for mauren, wont be in SW to deliver until like lvl 34 and quest is lvl 26
        {Text="Run back to Astranaar, fly to Stonetalon Peak", Taxi="Stonetalon Peak, Stonetalon Mountains", point={x=3441,y=4799,m=1440}},
        
        {Text="Accept Reclaiming the Charred Vale", At="Reclaiming the Charred Vale", point={x=3710,y=811,m=1442}},
        {Text="Complete harpies in charred vale", Ct="Reclaiming the Charred Vale", point={x=3462,y=6048,m=1442}},
        {Text="Run into Desolace", Zone="Desolace", point={x=2961,y=7830,m=1442}},
        {Text="Run to Nijel's Point", Proximity=20, point={x=6725,y=1608,m=1443}},
        {Text="Discover FP", Proximity=10, point={x=6466,y=1054,m=1443}},
        
        {Text="If lvl 30+, stable pet, tame a 30-31 scorpion to the south, get claw R4", Class="Hunter"},
        {Text="Fly to Stonetalon Peak", Taxi="Stonetalon Peak, Stonetalon Mountains", point={x=6465,y=1054,m=1443}},
        {Text="Deliver Reclaiming the Charred Vale", Dt={q="Reclaiming the Charred Vale"}, point={x=3710,y=811,m=1442}},
        {Text="Accept Reclaming the Charredd Vale", At="Reclaiming the Charred Vale", point={x=3710,y=811,m=1442}},

        {Text="Hearthstone to Duskwood", UseItem="Hearthstone", Zone="Wetlands"},
        
    }
}
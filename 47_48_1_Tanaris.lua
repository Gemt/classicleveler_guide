-- Author      : G3m7
-- Create Date : 5/18/2019 11:11:06 AM


CLGuide_Tanaris2 = {
    Title="47-48 Tanaris",
    Pinboard = {},
    Steps = {
        {Text="Accept The Thirsty Goblin", At="The Thirsty Goblin", point={x=5181,y=2866,m=1446}},
        {Text="Set HS in Gadgetzan", SetHs="Innkeeper Fizzgrimble", point={x=5251,y=2792,m=1446}},
        {Text="Make sure you got enough food, sold by Dirge @ inn", Class="Hunter"},

        {Text="Accept The Dunemaul Compound", At="The Dunemaul Compound", point={x=5282,y=2740,m=1446}},
        {Text="Deliver the Hippogryph egg at the machine thing", Dt={q="The Super Egg-O-Matic"}, point={x=5239,y=2696,m=1446}},
        {Text="Deliver The Borrower", Dt={q="The Borrower"}, point={x=5236,y=2691,m=1446}},
        {Text="Accept The Super Snapper FX", At="The Super Snapper FX", point={x=5236,y=2691,m=1446}},
        {Text="Deliver whichever egg you got from egg-crate to same npc", UseItem="Egg Crate", point={x=5236,y=2691,m=1446}},
        {Text="Accept WANTED: Caliph Scorpidsting", At="WANTED: Caliph Scorpidsting", point={x=5183,y=2702}},
        {Text="Accept WANTED: Andre Firebeard", At="WANTED: Andre Firebeard", point={x=5183,y=2702}},
        {Text="Accept Thistleshrub Valley", At="Thistleshrub Valley", point={x=5156,y=2676,m=1446}},
        
        
        {Text="Accept Gadgetzan Water Survey", At="Gadgetzan Water Survey", point={x=5021,y=2748,m=1446}},
        {Text="Run west to complete Gadgetzan Water Survey", UseItem="Untapped Dowsing Widget", Ct="Gadgetzan Water Survey", point={x=3903,y=2915,m=1446,m=1446}},
        {Text="Deliver Gadgetzan Water Survey (Gadgetzan)", Dt={q="Gadgetzan Water Survey"}, point={x=5021,y=2748,m=1446}},
        {Text="Accept Noxious Lair Investigation", At="Noxious Lair Investigation", point={x=5021,y=2748,m=1446}},


        {Text="Put Super snapper & Cloth in bank",
            PutInBank={"Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth", "Super Snapper FX"}, 
            point={x=5229,y=2892,m=1446}},

        -- wastewanders/caliph
        {Text="Complete More Wastewander Justice & Caliph Scorpidsting", Mct={"More Wastewander Justice", "WANTED: Caliph Scorpidsting"}, point={x=5833,y=3704,m=1446},
            PinAdd="Caliph patrols between all oil towers"},

        -- pirates
        {Text="Kill/loot Andre Firebeard for Wanted quest", Ct="WANTED: Andre Firebeard", point={x=7336,y=4715,m=1446}},
        {Text="Complete Stoley's Shipment (loot, 2nd floor)", Ct="Stoley's Shipment", point={x=7222,y=4677,m=1446}},
        {Text="Complete Southsea Shakedown and Pirate Hats", Mct={"Pirate Hats Ahoy!", "Southsea Shakedown"}},
        {Text="Make sure you are at least 47+ 113 650 xp before leaving", Lvl={lvl=47,xp=113650}},
        
        -- steemwheedle port
        -- todo: arrows
        {Text="Deliver Pirate Hats Ahoy! (Steemwheedle Port)", Dt={q="Pirate Hats Ahoy!"}, point={x=6656,y=2227,m=1446},
        PinAdd="Open any boxes you found at pirates. Accept Ship Schedules if found in a box."},
        {Text="Deliver Screecher Spirits", Dt={q="Screecher Spirits"}, point={x=6699,y=2235,m=1446},
            PinAdd="Accept The Prophecy of Mosh'aru after Screecher Spirits if doing ZF"},

        {Text="Buy arrows", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=3000}, point={x=6701,y=2199,m=1446}},
        
        {Text="Deliver WANTED: Andre Firebeard (Steemwheedle port)", Dt={q="WANTED: Andre Firebeard"}, point={x=6706,y=2389,m=1446}},
        {Text="Deliver Ship Schedules if found in pirate box", Dt={q="Ship Schedules"}, point={x=6706,y=2389,m=1446}},
        {Text="Deliver Southsea Shakedown", Dt={q="Southsea Shakedown", Item="Southsea Head Bucket"}, point={x=6706,y=2389,m=1446}},
        {Text="Deliver Stoley's Shipment", Dt={q="Stoley's Shipment"}, point={x=6711,y=2398,m=1446}},
        {Text="Accept Deliver to MacKinley", At="Deliver to MacKinley", point={x=6711,y=2398,m=1446}},
        {Text="Accept Yuka Screwspigot (req. lvl 48)", At="Yuka Screwspigot", point={x=6704,y=2401,m=1446,m=1446}},
               

        -- pick stuff at goblin in center of tanaris
        {Text="Deliver The Sunken Temple (center in tanaris)", Dt={q="The Sunken Temple"}, point={x=5271,y=4592,m=1446}},
        {Text="Accept The Stone Circle", At="The Stone Circle", point={x=5271,y=4592,m=1446}},
        {Text="Accept Gahz'ridian", At="Gahz'ridian", point={x=5271,y=4592,m=1446}, PinAdd="Keep an eye out for the Gahz'ridian fragments on ground now. Maybe use head?"},

        {Text="Complete Noxious Lair Investigation far west", Ct="Noxious Lair Investigation", point={x=3663,y=4568,m=1446}},
        {Text="Complete The Dunemaul Compound", Ct="The Dunemaul Compound", point={x=4102,y=5729,m=1446}},
        {Text="Complete Gahz'ridian at ruins to the south", Mct={"The Dunemaul Compound", "Gahz'ridian"}, point={x=3990,y=7123,m=1446}},

        {Text="Complete The Thirsty Goblin & Thistleshrub Valley", Mct={"The Thirsty Goblin", "Thistleshrub Valley"}, point={x=3146,y=6743,m=1446},
            PinAdd="Dew Gland for Thirsty goblin only drops from Dew Collectors"},

        {Text="If not 47+ now you screwed up. Grind, then run to un'goro", Level={lvl=47}},

        -- todo: look into doing entire raptor chain now as we want 20+ soils & another free questlogspace anyway
        {Text="Run down path to Ungoro, accept The Apes of Un'Goro. See note about Soils", At="The Apes of Un'Goro", point={x=7164,y=7595,m=1449},
            PinAdd="If we have Morrowgrain items, get 6-10 Ungoro Soils and start using on CD, If it was not avail in darna or feralas, get 20+ Soils now"},
        {Text="Accept It's a Secret to Everybody (from the wrecked boat)", At="It's a Secret to Everybody", point={x=6302,y=6852,m=1449}},
        {Text="Click the bag under water, Deliver It's a secret", Dt={q="It's a Secret to Everybody"}, point={x=6309,y=6904,m=1449}},
        {Text="Accept It's a Secret to Everybody (don't open the bag)", At="It's a Secret to Everybody", point={x=6309,y=6904,m=1449}},
        {Text="Farm mobs along eastern mountains (lowest lvl mobs) to marshal's refuge. /codex object Un'Goro Dirt Pile", Proximity=100, point={x=4615,y=1359,m=1449}},
        
        {Text="Take contents from \"A Small Pack\" and deliver It's A Secret (can vendor first if need bagspace)", 
            UseItem="A Small Pack", Dt={q="It's a Secret to Everybody"}, point={x=4466,y=810,m=1449},
            PinAdd="Delete Faded Photograph afterwards"},
        {Text="Accept It's a Secret to Everybody", At="It's a Secret to Everybody", point={x=4466,y=810,m=1449}},
        
        {Text="Discover FP", Proximity=10, point={x=4524,y=582,m=1449}},

        {Text="Hearthstone to Gadgetzan", UseItem="Hearthstone", Proximity=40, point={x=5251,y=2792,m=1446}},

        {Text="Deliver More Wastewander Justice (run to Gadgetzan)", Dt={q="More Wastewander Justice"}, point={x=5246,y=2851,m=1446}},
        {Text="Deliver WANTED: Caliph Scorpidsting", Dt={q="WANTED: Caliph Scorpidsting"}, point={x=5246,y=2851,m=1446}},
        {Text="Bank Stoley's Bottle", 
            PutInBank={"Stoley's Bottle", "Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth"}, 
            point={x=5231,y=2884,m=1446}},


        -- Gadgetzan
        {Text="Deliver The Thirsty Goblin", Dt={q="The Thirsty Goblin"}, point={x=5181,y=2866,m=1446}},
        {Text="Accept In Good Taste", At="In Good Taste", point={x=5181,y=2866,m=1446}},

        {Text="Deliver The Dunemaul Compound", Dt={q="The Dunemaul Compound"}, point={x=5282,y=2740,m=1446}},
        {Text="Deliver Thistleshrub Valley", Dt={q="Thistleshrub Valley"}, point={x=5157,y=2676,m=1446}},
        
        {Text="Deliver In Good Taste", Dt={q="In Good Taste"}, point={x=5106,y=2687,m=1446}},
        {Text="Accept Sprinkle's Secret Ingredient", At="Sprinkle's Secret Ingredient", point={x=5106,y=2687,m=1446}},
        {Text="Deliver Noxious Lair Investigation", Dt={q="Noxious Lair Investigation"}, point={x=5089,y=2696,m=1446}},
        {Text="Accept The Scrimshank Redemption", At="The Scrimshank Redemption", point={x=5021,y=2748,m=1446}},
        
        {Text="Deliver Gahz'ridian (middle of tanaris)", Dt={q="Gahz'ridian", Item="Staff of Lore", Use=1}, point={x=5271,y=4593,m=1446}},

        -- todo: for some goddamn reason all the findItem in inventory functions we have fails to find OOX-...
        {Text="Deliver Distress Beacon if found, otherwise skip", UseItem="OOX-17/TN Distress Beacon", Dt={q="Find OOX-17/Tn!"}, point={x=6023,y=6471,m=1446}},

        {Text="Scrimshrank redemtpion, enter at point", Proximity=10, point={x=5578,y=6893,m=1446}},
        {Text="Take a right, go straight to point", Proximity=10, point={x=5760,y=7067,m=1446}},
        {Text="Take a right, go straight and loot surveying gear", Ct="The Scrimshank Redemption", point={x=5596,y=7118,m=1446}},
        {Text="Deathwarp to Gadgetzan", DeathWarp=1},
        {Text="Deliver The Scrimshank Redemption", Dt={q="The Scrimshank Redemption"}, point={x=5021,y=2748,m=1446}},
        {Text="Accept Insect Part Analysis", At="Insect Part Analysis", point={x=5021,y=2748,m=1446}},
        {Text="Deliver Insect Part Analysis", Dt={q="Insect Part Analysis"}, point={x=5089,y=2696,m=1446}},
        {Text="Accept Insect Part Analysis", At="Insect Part Analysis", point={x=5089,y=2696,m=1446}},
        {Text="Deliver Insect Part Analysis", Dt={q="Insect Part Analysis"}, point={x=5021,y=2748,m=1446}},
        {Text="Accept Rise of the Silithid", At="Rise of the Silithid", point={x=5021,y=2748,m=1446}},

        {Text="Get Super Snapper FX/Scroll of Myzrael from bank", 
            PutInBank={"Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth", "Insect Analysis Report"}, 
            TakeFromBank={"Scroll of Myzrael", "Super Snapper FX"},
            point={x=5229,y=2892,m=1446}},

        -- do this right before leaving tanaris as it starts timed run to Hinterlands
        {Text="If you have 2x Elixir of Fortitude, run to shimmering using steps below. Otherwise skip & fly Theramore"},

        {Text="Deliver An Orphan Looking For a Home (shimmering flats)", Dt={q="An Orphan Looking For a Home"}, point={x=7835,y=7473,m=1441}},
        {Text=">Accept A Short Incubation", At="A Short Incubation", point={x=7835,y=7473,m=1441}, PinAdd="If you did not have Elixir of Fortitude, abandon and skip"},
        {Text=">Deliver A Short Incubation", Dt={q="A Short Incubation"}, point={x=7835,y=7473,m=1441}},
        {Text=">Accept The Newest Member of the Family (if you had elixirs)", At="The Newest Member of the Family", point={x=7835,y=7473,m=1441}},

        {Text="Run back to Tanaris", Zone="Tanaris", point={x=7553,y=9763,m=1446}},
        {Text="Fly to Theramore", Taxi="Theramore, Dustwallow Marsh", point={x=5101,y=2934,m=1446}},
        {Text="Buy arrows (need enough for hinterlands, so extra stacks if fast bow)", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=3000}, point={x=6794,y=4989,m=1445}},
        {Text="Boat it up to Menethil", point={x=7150,y=5636,m=1445}},
    }
}
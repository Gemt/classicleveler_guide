-- Author      : G3m7
-- Create Date : 5/19/2019 10:28:07 AM
CLGuide_BlastedLands_Azshara = {
    Title="55-56 BL -> Azshara",
    Pinboard = {},
    Steps = {
        -------------
        -- Blasted Lands
        -------------
        {Text="Accept Petty Squabbles at top of tower, unless you did in Blasted Lands 1. If you did, just skip to end of >", At="Petty Squabbles", point={x=6757,y=1930,m=1419}},
        {Text=">Turn inn Petty Squabbles at Fallen Hero", Dt={q="Petty Squabbles"}, point={x=3430,y=6612,m=1435}},
        {Text=">Accept A Tale of Sorrow", At="A Tale of Sorrow", point={x=3430,y=6612,m=1435}},
        {Text=">Complete A Tale of Sorrow (hold shift to stop auto-gossip-cycle)", CycleGossip="A Tale of Sorrow", point={x=3430,y=6612,m=1435}},
        {Text=">Deliver A Tale of Sorrow (skip followup)", Dt={q="A Tale of Sorrow"}, point={x=3430,y=6612,m=1435}},

        {Text="Accept The Stones That Bind Us", At="The Stones That Bind Us", point={x=3430,y=6613,m=1435}},

        {Text="Kill all servants of Grol around building at point (2 on LH, outside)", point={x=4381,y=1517,m=1419}},
        {Text="Kill all 3 servants of Sevine, from point and up the road", point={x=4276,y=3996,m=1419}},
        {Text="Kill third Servant of Grol at point (probably, was that on LH)", point={x=4827,y=4289,m=1419}},

        {Text="Kill all Servant of Razzelikh around dark portal", point={x=5353,y=5851,m=1419}},
        {Text="Kill all Servant of Allistarj, from point and up north towards cave", Ct="The Stones That Bind Us", point={x=6414,y=4657,m=1419}},
        
        {Text="Deliver The Stones That Bind Us at swamp border (make sure u finished it)", Dt={q="The Stones That Bind Us"}, point={x=3430,y=6613,m=1435}},
        {Text="Accept Heroes of Old", At="Heroes of Old", point={x=3430,y=6613,m=1435}},
        {Text="Deliver Heroes of Old", Dt={q="Heroes of Old"}, point={x=3331,y=6602,m=1435}},
        {Text="Accept Heroes of Old", At="Heroes of Old", point={x=3331,y=6602,m=1435}},
        {Text="Deliver Heroes of Old on the chest", Dt={q="Heroes of Old", Item="Shard of Afrasa", Use=1}, point={x=3326,y=6623,m=1435}},

        {Text="Accept Kirith", At="Kirith", point={x=3430,y=6613,m=1435}},
        {Text="Enter cave in BL at point", Proximity=15, point={x=6514,y=3282,m=1419}},
        {Text="Kill Kirith the Damned, 55 elite. Once dead, deliver Kirith at Spirit of Kirith", Dt={q="Kirith"}, point={x=6905,y=3103,m=1419}},
        {Text="Accept The Cover of Darkness from Spirit of Kirith", At="The Cover of Darkness"},
        {Text="Deliver The Cover of Darkness back at fallen hero", Dt={q="The Cover of Darkness"}, point={x=3430,y=6613,m=1435}},
        {Text="Accept The Demon Hunter", At="The Demon Hunter", point={x=3430,y=6613,m=1435}},

        {Text="Fly to Booty Bay from Nethergarde Keep", Taxi="Booty Bay, Stranglethorn", point={x=6554,y=2434,m=1419}},
        {Text="Restock arrows (need enough for azshara2+everlook->talonbranch)", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=3400}, point={x=2830,y=7457,m=1434}},
        {Text="Take the boat to Ratchet", Zone="The Barrens", point={x=2596,y=7318,m=1434}},

        -------------
        -- RATCHET
        -------------
        {Text="Get Un'Goro Ash and Corrupt Moonwell Water from bank. Put Good Luck Half-Charm in", 
            TakeFromBank={"Un'Goro", "Corrupt Moonwell Water"},
            PutInBank={"Good Luck Half", "Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth"},
            point={x=6265,y=3746,m=1413}},
        {Text="Deliver Volcanic Activity", Dt={q="Volcanic Activity"}, point={x=6245,y=3874,m=1413}},
        {Text="Set HS in Ratchet", SetHs="Innkeeper Wiley", point={x=6205,y=3941,m=1413}},
        {Text="Follow the shore south (dont swim), Deliver Seeking Spiritual Aid", Dt={q="Seeking Spiritual Aid"}, point={x=6583,y=4377,m=1413}},
        {Text="Wait for RP, then accept Cleansed Water Returns to Felwood", At="Cleansed Water Returns to Felwood", point={x=6583,y=4377,m=1413}},
        {Text="Fly to Talrendis Point, Azshara", Taxi="Talrendis Point, Azshara", point={x=6309,y=3716,m=1413}},

        -------------
        -- AZSHARA
        -------------

        -- loramus
        {Text="Move straight east towards delivering The Demon Hunter (don't follow the road)", Proximity=50, point={x=2427,y=7870,m=1447}},
        {Text="Follow arrow", Proximity=50, point={x=4856,y=7241,m=1447}},
        {Text="Complete The Demon Hunter by talking to him (big isle south in bay)", CycleGossip="The Demon Hunter", Ct="The Demon Hunter", point={x=6082,y=6635,m=1447}},
        {Text="Deliver The Demon Hunter", Dt={q="The Demon Hunter"}, point={x=6082,y=6635,m=1447}},
        {Text="Accept Loramus", At="Loramus", point={x=6082,y=6635,m=1447}},
        {Text="Complete Loramus by gossipping", CycleGossip=1, Ct="Loramus", point={x=6082,y=6635,m=1447}},
        {Text="Deliver Loramus", Dt={q="Loramus"}, point={x=6082,y=6635,m=1447}},
        {Text="Accept Breaking the Ward", At="Breaking the Ward", point={x=6082,y=6635,m=1447}},
        {Text="Deliver Breaking the Ward (some RP waiting)", Dt={q="Breaking the Ward"}, point={x=6082,y=6635,m=1447}},
        {Text="Accept The Name of the Beast", At="The Name of the Beast", point={x=6082,y=6635,m=1447}},

        {Text="Get back on the shore (aka don't swim straight towards point), then move to point", Proximity=50, point={x=4624,y=3923,m=1447}},

        {Text="Deliver The Hunter's Charm", Class="Hunter", Dt={q="The Hunter's Charm"}, point={x=4240,y=4262,m=1447}},
        {Text="Accept Courser Antlers", Class="Hunter", At="Courser Antlers", point={x=4240,y=4262,m=1447},
            PinAdd="Kill any elk thingies u see from now on for Courser Antlers"},


        {Text="Accept Kim'jael Indeed!", At="Kim'jael Indeed!", point={x=5345,y=2182,m=1447}},
        {Text="Complete Kim'jael Indeed (loot boxes)", Ct="Kim'jael Indeed!", point={x=5609,y=2982,m=1447}},
        {Text="Deliver Kim'jael Indeed!", Dt={q="Kim'jael Indeed!"}, point={x=5345,y=2182,m=1447}},
        {Text="Accept Kim'jael's \"Missing\" Equipment", At="Kim'jael's \"Missing\" Equipment", point={x=5345,y=2182,m=1447}},
        
        {Text="Complete Courser Antlers", Class="Hunter", Ct="Courser Antlers"},
        {Text="Deliver Courser Antlers", Class="Hunter", Dt={q="Courser Antlers"}, point={x=4240,y=4262,m=1447}},
        {Text="Accept Wavethrashing", Class="Hunter", At="Wavethrashing", point={x=4240,y=4262,m=1447}},

        -- western temple/hydra in center etc
        {Text="Clear out Wavethrashers around the lighthouse. Once cleared go next step. Don't need to complete", Ct="Wavethrashing", Class="Hunter", point={x=8884,y=2902,m=1447}},

        {Text="Deliver The Name of the Beast (don't swim there. Second floor in building)", Dt={q="The Name of the Beast"}, point={x=7713,y=4277,m=1447}},
        {Text="Accept The Name of the Beast", At="The Name of the Beast", point={x=7713,y=4277,m=1447}},
        {Text="Kill & loot Hetaera (3headed hydra). Should be on one of the isles around point. Swim straight to her", Ct="The Name of the Beast", point={x=5787,y=4354,m=1447}},
        {Text="Swim back for delivery, follow arrow", Proximity=50, point={x=7185,y=3589,m=1447}}, 
        {Text="Deliver The Name of the Beast.", Dt={q="The Name of the Beast"}, point={x=7713,y=4277,m=1447}},
        {Text="Accept The Name of the Beast", At="The Name of the Beast", point={x=7713,y=4277,m=1447}},

        {Text="Complete Wavethrashing around lighthouse. Might be more further north on shore too", Class="Hunter", Ct="Wavethrashing", point={x=8576,y=3091,m=1447}},
        {Text="Deliver Wavethrashing (don't swim, run on land)", Class="Hunter", Dt={q="Wavethrashing"}, point={x=4240,y=4262,m=1447}},

        -- central ruins
        {Text="Loot obelisk 1 at point", Proximity=15, point={x=3960,y=5031,m=1447},
            PinAdd="Working on finding Kim'jael's \"Missing\" Equipment from a naga here too"},
        {Text="Loot obelisk 2 at point", Proximity=15, point={x=3688,y=5320,m=1447}},
        {Text="Loot obelisk 3 at point", Proximity=15, point={x=3937,y=5543,m=1447}},
        {Text="Loot obelisk 4 at point", Proximity=15, point={x=4242,y=6411,m=1447}},

        {Text="Make sure Kim'jael's quest is complete (grind nagas towards point)", Ct="Kim'jael's \"Missing\" Equipment", point={x=4621,y=3920,m=1447}},
        {Text="Get up at point (we're delivering Kim'Jael)", Proximity=40, point={x=4621,y=3920,m=1447}},
        {Text="Deliver Kim'jael's \"Missing\" Equipment", Dt={q="Kim'jael's \"Missing\" Equipment"}, point={x=5345,y=2182,m=1447}},

        -- loramus
        {Text="Deliver The Name of the Beast at Loramus (run on the shore)", Dt={q="The Name of the Beast"}, point={x=6082,y=6635,m=1447}},
        {Text="Accept Azsharite", At="Azsharite", point={x=6082,y=6635,m=1447}},

        {Text="Complete Azsharite (get up at point)", Ct="Azsharite", point={x=5538,y=7622,m=1447},
            PinAdd="Azsharite looted from blue crystals found spread throughout the area"},

        {Text="Get to point and jump down to water (carefully to not die to fall-dmg)", Proximity=20, point={x=7422,y=8783,m=1447}},
        {Text="Swim to helipad, use Flare Gun and deliver Arcane Runes", Dt={q="Arcane Runes"}, UseItem="Standard Issue Flare Gun", point={x=7780,y=9132,m=1447}},
        {Text="Accept Return to Tymor", At="Return to Tymor", point={x=7780,y=9132,m=1447}},

        {Text="Swim North/NW toLoramus, deliver Azsharite at Loramus", Dt={q="Azsharite"}, point={x=6082,y=6635,m=1447}},
        {Text="Accept The Formation of Felbane", At="The Formation of Felbane", point={x=6082,y=6635,m=1447}},

        {Text="Hearthstone to Ratchet", UseItem="Hearthstone", Proximity=50, point={x=6205,y=3941,m=1413}},
        {Text="Get Vial of Blessed Water from bank", TakeFromBank={"Vial of Blessed Water"}, point={x=6265,y=3745,m=1413}},
        {Text="Fly to everlook, WS", Taxi="Everlook, Winterspring", point={x=6308,y=3717,m=1413},
            PinAdd="Remember to get Vial of Blessed Water from bank if didnt in Booty Bay"},
    }
}

-- Author      : G3m7
-- Create Date : 5/5/2019 5:17:26 PM

CLGuide_Ashenvale_Redridge = {
    Title="23-23 Ashenvale-Redridge",
    Pinboard = {},
    Steps = {
        {Text="Take the boat to menethil", Zone="Wetlands", point={x=3242,y=4383,m=1439}},
        {Text="Accept Young Crocolisk Skins", At="Young Crocolisk Skins", point={x=855,y=5574,m=1437,m=1437}},
        {Text="Accept Claws from the Deep", At="Claws from the Deep", point={x=832,y=5852,m=1437}},

        {Text="Discover the FP", Proximity=15, point={x=949,y=5968,m=1437}},

        {Text="Accept The Greenwarden (outside the inn)", At="The Greenwarden", point={x=1089,y=5967,m=1437}},
        {Text="Deliver The Absent Minded Prospector (second floor, Inn) Skip followup.", Dt={q="The Absent Minded Prospector"}, point={x=1084,y=6042,m=1437}},
        
        -- todo: not correct npc? need to check how these 2 guys patrol/move
        {Text="Restock on Arrows / buy Medium Quiver if not done yet",
            BuyItem={Npc="Naela Trance", Item="Sharp Arrow", Count=2000}, Class="Hunter", point={x=1128,y=5834,m=1437}},

        {Text="Enter the keep, take left 2 times, buy Bronze Tube from Neal Allen", 
            BuyItem={Npc="Neal Allen", Item="Bronze Tube", Count=1},Class="Hunter",  point={x=1074,y=5678,m=1437}},
        
        {Text="Complete Claws from the Deep (Gobbler is point). KEEP THE MURLOC FINS", Ct="Claws from the Deep", point={x=1807,y=3974,m=1437}},
        
        {Text="If you do not have a Bronze Tube, run NE and check Fradd Swiftgear", 
            BuyItem={Npc="Fradd Swiftgear", Item="Bronze Tube", Count=1}, point={x=2640,y=2576,m=1437}},
        {Text="If you went for bronze tube: Check Wenna Silkbeard inside house for Saber Leggings", 
            BuyItem={Npc="Wenna Silkbeard", Item="Saber Leggings", Count=1}, Class="Hunter", point={x=2561,y=2581,m=1437}},

        {Text="Accept Daily Delivery", At="Daily Delivery", point={x=4992,y=3937,m=1437}},
        {Text="Turn inn The Greenwarden. Skip followup. Work on 4 young croc skins on the way", Dt={q="The Greenwarden"}, point={x=5643,y=4042,m=1437}},
        {Text="Accept Tramping Paws", At="Tramping Paws", point={x=5643,y=4042,m=1437}},
        {Text="Complete Tramping Paws (SE) and Croc skins", Mct={"Tramping Paws", "Young Crocolisk Skins"}, point={x=6112,y=5717,m=1437}},

        {Text="Run to Loch Modan", Zone="Loch Modan", point={x=5385,y=7034,m=1437}},
        {Text="Accept The Algaz Gauntlet (just as you exit tunnels into Loch Modan)", At="The Algaz Gauntlet", point={x=2544,y=1038,m=1432}},
        {Text="Accept Stormpike's Order (Mountaineer Stormpike inside building)", At="Stormpike's Order", point={x=2467,y=1835,m=1432}},
        {Text="Accept A Dark Threat Looms (on top of the dam)", At="A Dark Threat Looms", point={x=4605,y=1361,m=1432}},
        {Text="Deliver A Dark Threat Looms (click barrel)", Dt={q="A Dark Threat Looms"}, point={x=5605,y=1324,m=1432}},
        {Text="Accept A Dark Threat Looms", At="A Dark Threat Looms", point={x=5605,y=1324,m=1432}},
        {Text="Deliver A Dark Threat Looms", Dt={q="A Dark Threat Looms"}, point={x=4605,y=1361,m=1432}},
        --{Text="Accept A Dark Threat Looms", At="A Dark Threat Looms", point={x=4605,y=1361,m=1432}},

        {Text="Run back down tunnel towards wetlands, kill orcs for quest. When done killing, run to wetlands to complete.", Ct="The Algaz Gauntlet"},
        {Text="Pull something to die and deathwarp to Thelsamar. MAKE SURE MINIMAP SAYS DUN'ALGAZ WHERE U DIE (close to tunnel)",
            DeathWarp=1},
            
        {Text="Accept Stonegear's Search (Mountaineer Kadrell, patrolling)", At="Stonegear's Search", point={x=3384,y=4831,m=1432}},
        {Text="Get the FP", Proximity=15, point={x=3394,y=5096,m=1432}},
        {Text="Run to Dun Morogh", Zone="Dun Morogh", point={x=2173,y=6892,m=1432}},
        {Text="Die as soon as possible to deathwarp to Kharanos", DeathWarp=1},
        {Text="Turn inn Stonegear's Search (skip followup) at Steelgrill's Depot", Dt={q="Stonegear's Search"}, point={x=4965,y=4856,m=1426}},
        {Text="Check npc Loslor Rudge for bronze tube", BuyItem={Npc="Loslor Rudge", Item="Bronze Tube", Count=1}, point={x=5008,y=4942,m=1426}},

        {Text="Run to Ironforge", Zone="Ironforge", point={x=5296,y=3734,m=1426}},
        {Text="Put wetlands stuff + cloth in bank. Get Book: Powers below and 60 wool if have", 
            PutInBank={"Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth", 
            "Gobbler's Head", "Bundle of Crocolisk Skins", "Waterlogged Envelope", "Young Crocolisk Skin", 
            "Flagongut's Fossil", "Strange Smelling Powder", "Stone of Relu"},
            TakeFromBank={"Book: The Powers Below"},
            point={x=3571,y=6038,m=1455}},
        --{Text="Train Wep skills if needed wanted", point={x=6121,y=8947,m=1455}},

        {Text="Discover FP", Proximity=50, point={x=5549,y=4770,m=1455}},

        {Text="Turn inn Wool cloth if have 60", Dt={q="A Donation of Wool"}, point={x=7409,y=4821,m=1455}},
        
        {Text="if found book in master's glavie: Run to Forlorn Cavern and turn inn", 
           Dt={q="The Powers Below"}, UseItem="Book: The Powers Below", point={x=5100,y=958,m=1455}},

         {Text="If you do not have a Bronze Tube. Gearcutter Cogspinner at Tinker Town", 
            BuyItem={Npc="Gearcutter Cogspinner", Item="Bronze Tube", Count=1}, point={x=6783,y=4251,m=1455}},
        {Text="Accept Speak With Shoni from Gnoarn", At="Speak With Shoni", point={x=6917,y=5054,m=1455}},
        {Text="Turn inn An Old Colleague by Tram", Dt={q="An Old Colleague"}, point={x=7206,y=5185,m=1455}},

        -- todo: point. Also copy this dude for Massive longbow at lvl 30 (42 bow)
        --{Text="Check Skolmin Goldfury for Sturdy Recurve (1g94s)", BuyItem={Item="Sturdy Recurve", Npc="Skolmin Goldfury", Count=1, point={x=7174,y=6671,m=1455}}},


        {Text="Take Tram to SW", Zone="Stormwind City"},
        {Text="If you do not have a Bronze Tube. Billibub Cogspinner", 
            BuyItem={Npc="Billibub Cogspinner", Item="Bronze Tube", Count=1}, point={x=5521,y=705,m=1453}},
        {Text="Deliver Speak With Shoni", Dt={q="Speak With Shoni"}, point={x=5551,y=1251,m=1453}},
        
        -- todo: not enough questlog space for underground assault and the 2 other DM quests below unless actually doing DM asap
        --{Text="IF DM: Accept Underground Assault", At="Underground Assault", point={x=5551,y=1251,m=1453}},
        {Text="Turn Stormpike's Order", Dt={q="Stormpike's Order"}, point={x=5809,y=1655,m=1453}},
        {Text="Train Hunter skills", TrainSkill=1, point={x=6166,y=1519,m=1453}},
        {Text="Train Pet skills", TrainSkill=1, point={x=6156,y=1599,m=1453}},

        --{Text="IF DM: Accept Collecting Memories", At="Collecting Memories", point={x=6545,y=2118,m=1453}},
        --{Text="IF DM: Oh Brother...", At="Oh Brother. . .", point={x=6545,y=2118,m=1453}},


        
        {Text="Train 2H Swords (across AH). Polearms?", TrainSkill=1,point={x=5706,y=5751,m=1453}},
        {Text="Discover FP", Proximity=8, point={x=6629,y=6215,m=1453}},
        {Text="Run out of SW", Zone="Elwynn Forest", point={x=7051,y=8763,m=1453}},
        {Text="Run out of SW to Tower of Azora. Accept A Watchful Eye (top)", At="A Watchful Eye", point={x=6522,y=6971,m=1429}},
        {Text="Run to Redridge", Zone="Redridge Mountains", point={x=9384,y=7221,m=1429}},
    }
}

--[[

Turn in An Old Colleague (Tinker Town) - NOTE: IF possible to get 4 minor mana pot and 2 elixir of minor fortitude, do that followup
These quests probably wont work on classic (seasonal)
	find Courier Hammerfall (patroll anti-clockwise?) around entire outer circle. Accept The reason for the season (CHECK BETA ON THIS ONE; IS SEASONAL)
	Turnin The reason for the season (if found) innermost in hall of explorers 
	Deliver followup to king

]]
-- Author      : G3m7
-- Create Date : 5/8/2019 5:39:23 PM

--[[
arrows by mail
arrows in 40+ zones

moving panther mastery pt3 (shadowmaw) to stv2 would speed up stv1 quite a bit
    but is there space to leave it in, and time to do both?
    pt3 with pt3 raptors and pt4 with pt4 raptors?
learn mining for tracking in alterac cave


look into bite learning on cat?
waterhbreathing:
    ashenvale1: water elementals
    stv1: murlocs (if doing)
    desolace keys
    stv2: the boss dude from haunted isle chain by murlocs

need to start training additional weapon skills when approaching 40, as random green drops can be upgrasdes
    1h sword/axe for offhands

fozruk: https://www.twitch.tv/videos/449036409
remove arcane resist from pettraining
put water breathing elixir in bank during ashenvale->if->sw->redridge run, and get out again before STV?

get 30 bow and 42 bow
which vendors sell lvl 30 bow? 
If the SW guy, add buy step at 28/29 SW trip (with missing diplomat)

find the other vendors for {Text="Check for Massive Longbow on Frederick Stover", BuyItem={Npc="Frederick Stover", Item="Massive Longbow", Count=1}, point={x=5006,y=5767,m=1453}},

write down cost of lvl 5 skills in guide

default autopick questrewards and everything else to true for myself
make sure questie arrow wont interfere when that is implemented
/target + mark macro/button for finding mobs part of current step?
1H weapons: figure out which ones to keep in bank in case finding a good OH (morbent fel)

CANT TRAIN ALL RESISTANCES AT LVL 30
    wont have enough training points for lvl 36 greater armor

The shattered necklace, rando drop from mobs outside ulda. No mention in guide atm

missing arrow-buying steps in all 40+ sections

respec pet to frost resist before around lvl 40-42. Either IF before SoS or SW after SoS
add steps for food restock in tanaris at dirge and feralas at fish vendor in bighouse
missing repairs too

Problem:
    need to add food buying steps in 40+ zones
    also double-check arrow steps

    consider only getting cortellos riddle in ships in stv if <44, do captains at 50 with ape.
    if so, get it while getting charts/orders etc
    lvl 42+42pet is impossible for fleet master. 44+44 might work, but would be hard
Pet:
    balanced resistances until lvl 40
    at lvl 40, respec (if needed?) to be able to max lvl 40 frost resistance for feralas grind
    at lvl 47 in darnassus, after grind, respec pet again away from max frost res to balanced resist

    
    missing point on the step below
    {Text="Deliver Myzrael's Allies in Forlorn Cavern if you killed Fozzruk", Dt={q="Myzrael's Allies"}},
    can we delay zanzil bosses (jon-jon etc)? lvl 44 mobs at lvl 41, maaaybe 42
]]
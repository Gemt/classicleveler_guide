-- Author      : G3m7
-- Create Date : 5/5/2019 9:29:51 AM

CLGuide_Darkshore = {
    Title="11-20 Darkshore",
    Pinboard = {},
    Steps = {
	    {Text="Accept washed ashore (by FP)", At="Washed Ashore", point={x=3662,y=4559,m=1439}},
	    {Text="Turn inn Flight to Auberdine", Dt={q="Flight to Auberdine"}, point={x=3677,y=4428,m=1439}},
	    {Text="Accept Return to Nessa", At="Return to Nessa", point={x=3677,y=4428,m=1439}},
	    {Text="==== RESTOCK On Longjaw Mud Snapper ====", BuyItem={Npc="Laird", Item="Longjaw Mud Snapper", Count=20}, Class="Hunter", point={x=3674,y=4429,m=1439}},
	    {Text="Accept buzzbox (second floor innkeeper)", At="Buzzbox 827", point={x=3697,y=4413,m=1439}},
	    {Text="Accept Bashal'Aran (Thundris Windweaver, north mainhouse)", At="Bashal'Aran", point={x=3740,y=4013,m=1439}},
	    {Text="=== Arrows & as many 6slotters as can afford === ", BuyItem={Npc="Dalmond", Item="Sharp Arrow", Count=1600}, Class="Hunter", point={x=3744,y=4051,m=1439}},
        
	    {Text="Accept Plagued Lands (bear quest dude)", At="Plagued Lands", point={x=3884,y=4341,m=1439}},
	    {Text="Accept How Big a Threat?", At="How Big a Threat?", point={x=3938,y=4348,m=1439}},
	    {Text="Tame a moonstalker", point={x=4418,y=3630,m=1439}, PinAdd="Work on 5 strider meat whenever see strider"},
	    {Text="Turn inn Bashal'Aran", Dt={q="Bashal'Aran"}, point={x=4418,y=3630,m=1439}},
	    {Text="Accept Bashal'Aran pt2", At="Bashal'Aran", point={x=4418,y=3630,m=1439}},
	    {Text="Complete Bashal'Aran pt2 (rings)", Ct="Bashal'Aran" },
	    {Text="Turn inn Bashal'Aran pt2", Dt={q="Bashal'Aran"}, point={x=4418,y=3630,m=1439}},
	    {Text="Accept Bashal'Aran p3", At="Bashal'Aran", point={x=4418,y=3630,m=1439}},
	    {Text="Complete Bashal'Aran pt3 (Ancient Moonstone from satyrs)", Ct="Bashal'Aran" },
	    {Text="Turn inn Bashal'Aran pt3", Dt={q="Bashal'Aran"}, point={x=4418,y=3630,m=1439}},
	    {Text="Accept Bashal'Aran pt4", At="Bashal'Aran", point={x=4418,y=3630,m=1439}, PinRemove="Grind as much as you can here if alone"},
	    {Text="trap a grizzly bear (Plagued Lands)", Ct="Plagued Lands", UseItem="Tharnariun's Hope", point={x=4421,y=3196,m=1439}},
        {Text="Grind to close to on moonkins, have at leas 9 eggs. Should be about lvl 12.5", Item={Name="Small Egg", Count=9}, point={x=4517,y=4519,m=1439}},
	    
        {Text="Do How big a threat (discover the area, north-ish)", Ct="How Big a Threat?", point={x=3898,y=5396,m=1439}},
        {Text="Complete Washed Ashore (beached seacreature) and Buzzbox (crabs)", point={x=3640,y=5090,m=1439}, Mct={"Washed Ashore", "Buzzbox 827"}},

	    {Text="Turn inn buzzbox 827", Dt={q="Buzzbox 827"}, point={x=3665,y=4627,m=1439}},
	    {Text="Accept Buzzbox 411", At="Buzzbox 411", point={x=3665,y=4627,m=1439}},
	    {Text="Turn inn washed ashore", Dt={q="Washed Ashore"}, point={x=3662,y=4558,m=1439}},
	    {Text="Accept washed ashore. Accept pt2", At="Washed Ashore", point={x=3662,y=4558,m=1439}},

        {Text="==== RESTOCK On Longjaw Mud Snapper ====", BuyItem={Npc="Laird", Item="Longjaw Mud Snapper", Count=20},Class="Hunter",  point={x=3674,y=4429,m=1439}},

	    {Text="Accept cave mushroms (outside inn)", point={x=3732,y=4365,m=1439}, At="Cave Mushrooms"},
        {Text="Accept the red crystal (outside inn)", point={x=3771,y=4339,m=1439}, At="The Red Crystal"},

	    {Text="Turn inn plagued lands", Dt={q="Plagued Lands"}, point={x=3884,y=4341,m=1439}},
	    {Text="Accept Cleansing of the Infected", At="Cleansing of the Infected", point={x=3884,y=4341,m=1439}},
        {Text="Turn inn how big a threat", Dt={q="How Big a Threat?"}, point={x=3938,y=4348,m=1439}},
	    {Text="Accept how big a threat ", At="How Big a Threat?", point={x=3938,y=4348,m=1439}},
	    
        {Text="Accept Thundris Windweaver", At="Thundris Windweaver", point={x=3938,y=4348,m=1439}},
        
	    
        {Text="Accept Deep Ocean, Vast Sea (if lvl 13)", At="Deep Ocean, Vast Sea", point={x=3811,y=4118,m=1439}},
        {Text="Skill cooking to 10, Buy 9 Mild Spices from Gorbold Steelhand by forge", BuyItem={Npc="Gorbold Steelhand", Item="Mild Spices", Count=9},point={x=3811,y=4116,m=1439}},

	    {Text="Accept easy strider living (cooking)", At="Easy Strider Living", point={x=3769,y=4065,m=1439}},
        {Text="Turn inn Easy Strider Living", Dt={q="Easy Strider Living"}, point={x=3769,y=4065,m=1439}},
	    {Text="Turn inn thundris windweaver", Dt={q="Thundris Windweaver"}, point={x=3739,y=4011,m=1439}},
	    
        {Text="Accept The Cliffspring River", At="The Cliffspring River", point={x=3739,y=4011,m=1439}},
	    {Text="Accept Tools of the Highborne", At="Tools of the Highborne", point={x=3739,y=4011,m=1439}},

        {Text="=== RESTOCK Sharp Arrow. Buy remaining bags ===", BuyItem={Npc="Dalmond", Item="Sharp Arrow", Count=1600}, Class="Hunter",  point={x=3744,y=4051,m=1439}},
        
       
	    {Text="Accept for love eternal (pier)", At="For Love Eternal", point={x=3574,y=4371,m=1439}},
	    {Text="Loot Sea Turtle Remains (jump in water by boat to menethil)", Ct="Washed Ashore", point={x=3187,y=4628,m=1439},
            PinAdd="Try to kill some treshers that are just a bit south of the turtle remains"},
	    {Text="Turn inn Washed Ashore", Dt={q="Washed Ashore", Item="Dryweed Belt", Use=1}, point={x=3662,y=4559,m=1439}},
	

        -- first medium south run
	    {Text="Complete How Big a Threat? (kill mobs)", Ct="How Big a Threat?", point={x=3937,y=5293,m=1439}},
	    {Text="Accept The Fall of Ameth'Aran", At="The Fall of Ameth'Aran", point={x=4029,y=5973,m=1439}},
	    {Text="Complete: Bashal'Aran, For Love Eternal (5m respawn, by brazier), The Fall of Ameth'Aran, Tools of the HIghborne", Mct={"The Fall of Ameth'Aran", "For Love Eternal", "Bashal'Aran", "Tools of the Highborne"}},
	    {Text="Deliver The fall of Ameth'Aran", Dt={q="The Fall of Ameth'Aran"}, point={x=4029,y=5973,m=1439}},
        {Text="Vendor at fishing vendor to clear bags", Proximity=15, point={x=3698,y=5635,m=1439}},
        {Text="Loot beached sea turtle at 37,62, south-west of ameth'aran", At="Beached Sea Turtle", point={x=3712,y=6211,m=1439}, 
            PinAdd="Kill rabid bears anywhere u run now"},
	    {Text="Loot beached sea creature at 36,70, north-west of onu place", At="Beached Sea Creature",  point={x=3604,y=7089,m=1439}},
        --{Text="Continue killing bears towards onu, turn inn Grove of the ancients (if have)", Dt={q="Grove of the Ancients"}, point={x=4355,y=7627,m=1439}},
        {Text="Keep killing bears for towards treshers", Ct="Cleansing of the Infected", point={x=3471,y=5941,m=1439}},
	    {Text="Buzzbox 411 Threshers in water west of ameth'aran", Ct="Buzzbox 411", point={x=3471,y=5941,m=1439}},
	    {Text="Complete 20 bears for Cleansing the infected", Ct="Cleansing of the Infected"},





        -- delivering q in auberdine after south #1
	    {Text="GRIND/RUN BACK TO AUBERDINE - Turn inn Beached Sea turtle", Dt={q="Beached Sea Turtle"}, point={x=3662,y=4559,m=1439}},
	    {Text="Turn inn Beached Sea creature", Dt={q="Beached Sea Creature"}, point={x=3662,y=4559,m=1439}},
	    {Text="==== RESTOCK On Longjaw Mud Snapper ====", BuyItem={Npc="Laird", Item="Longjaw Mud Snapper", Count=20}, Class="Hunter", point={x=3674,y=4429,m=1439}},
	    {Text="Turn inn For love eternal", Dt={q="For Love Eternal"}, point={x=3574,y=4371,m=1439}},
	    {Text="Make sure you have Quest: The Red Crystal", Ht="The Red Crystal", point={x=3771,y=4339,m=1439}},
        {Text="Turn inn Tools of the highborne", Dt={q="Tools of the Highborne", Item="Ivy Cuffs", Use=1}, point={x=3739,y=4011,m=1439}},
	    {Text="=== RESTOCK Sharp Arrow. If still missing bags, get now ===", BuyItem={Npc="Dalmond", Item="Sharp Arrow", Count=1600},Class="Hunter",  point={x=3744,y=4051,m=1439}},
	    {Text="Accept Deep Ocean, Vast Sea (if lvl 13)", At="Deep Ocean, Vast Sea", point={x=3811,y=4118,m=1439}},
	    {Text="Turn inn Cleansing the infected", Dt={q="Cleansing of the Infected"}, point={x=3885,y=4342,m=1439}},
        {Text="Accept Tharnariun's Hope", At="Tharnariun's Hope", point={x=3885,y=4342,m=1439}},
	    {Text="Turn inn How big a threat", Dt={q="How Big a Threat?"}, point={x=3937,y=4348,m=1439}},
        {Text="Accept A Lost Master", At="A Lost Master", point={x=3937,y=4348,m=1439}},
	    {Text="Accept The Tower of Althalaxx (SECOND FLOOR)", At="The Tower of Althalaxx", point={x=3905,y=4356,m=1439}},
	
	
        -- second north-ish run
	    {Text="GRIND to the red crystal. If possible, grind to 15, or close to.", Ct="The Red Crystal", point={x=4737,y=4870,m=1439}},
	    {Text="GRIND to Bashal'aran and turn in Bashal'Aran", Dt={q="Bashal'Aran", Item="Vagabond Leggings", Use=1}, point={x=4418,y=3630,m=1439}},
	    {Text="Accept Beached Sea Creature at 42,31 (nw of Bashal'aran)", At="Beached Sea Creature", point={x=4192,y=3155,m=1439}},
	    {Text="Do Deep ocean, vast sea west of sea creature", Ct="Deep Ocean, Vast Sea", point={x=3884,y=2941,m=1439}},
        {Text="Turn inn buzzbox 411 at 42,28 (close to sea creature above)", Dt={q="Buzzbox 411"}, point={x=4194,y=2862,m=1439}},
        {Text="Accept 323", At="Buzzbox 323", point={x=4194,y=2862,m=1439}, PinAdd="Look for moonstalkers for Buzzbox323"},
        {Text="Grind Moonstalkers towards the beached sea Turtle west of river", At="Beached Sea Turtle", point={x=4417,y=2064,m=1439}},
        
        
        {Text="Do Tharnariun's Hope (The den mother). Should be 15 before attempting. Grind moonstalkers to 15", Ct="Tharnariun's Hope", point={x=5231,y=3572,m=1439},
            PinAdd="Kill 2x small bears, reset, then nuke den mother"},
        {Text="Attempt to finish moonstalkers", Ct="Buzzbox 323"},
        {Text="Fill Empty Sampling Tube (kill moonstalkers on the way)", Ct="The Cliffspring River", UseItem="Empty Sampling Tube", point={x=5086,y=2559,m=1439}},
	    {Text="Turn inn Buzzbox 323 if managed to complete", Dt={q="Buzzbox 323"}, point={x=5127,y=2457,m=1439}},
        {Text="Accept Buzzbox 525 if managed to complete", At="Buzzbox 525", point={x=5127,y=2457,m=1439}},
	    {Text="Do Cave Mushrooms (careful with pulls, 2x=dead). Pet should be 15", Ct="Cave Mushrooms", point={x=5439,y=3167,m=1439}},
	    
        {Text="Grind until lvl 15, or out of arrows. (20bow total: 1g36c01c, 16bow total: 78s87c", Lvl={lvl=15}},
        --1g31s98c
        --99s58c hunterskills 32s40
        --97s42c petskills    2s16c
        --63s11c 16bow        34s31c
        --68s64 +5s53c from vendor
        --10s staff           10s
        --20bow               57s14c
        --1g36s01c total cost @ honored
        
        -- darnassus lvl 15
	    {Text="== HEARTSTONE TO DARNASSUS ==", UseItem="Hearthstone", Zone="Darnassus"},
        
        -- add a vendor step in darnassus before training perhaps, if gold is tight?
	    {Text="Train Hunter skills", TrainSkill=1, point={x=3995,y=1555,m=1457}},
        {Text="Train Pet skills (up the ramp)", TrainSkill=1},
			
	    {Text="Buy Reinforced Bow (lvl 16) at wep masters", BuyItem={Npc="Ariyell Skyshadow", Item="Reinforced Bow", Count=1}, Class="Hunter", 
            point={x=5871,y=4459,m=1457}, PinAdd="Equip Reinforced Bow at lvl 16"},
			
        {Text="Train staves", TrainSkill=1, point={x=5755,y=4675,m=1457}},
        {Text="if enough gold, get 20 bow (57s14c @honored)", BuyItem={Npc="Landria", Item="Heavy Recurve Bow", Count=1}, Class="Hunter", point={x=6334,y=6639,m=1457}},
        
        {Text="Train Herbalism (go up ramp to temple then right)", Proximity=10, point={x=4794,y=6802,m=1457}},

        {Text="Accept Trouble in darkshore (outside temple of moon)", At="Trouble In Darkshore?", point={x=3116,y=8467,m=1457}},
        
	    {Text="Run to FP", Zone="Teldrassil", point={x=3060,y=4152,m=1457}},
	    {Text="Turn inn Return to Nessa", Dt={q="Return to Nessa"}, point={x=5625,y=9245,m=1438}},
	    {Text="Fly to Auberdine", Taxi="Auberdine, Darkshore", point={x=5841,y=9403,m=1438}},
	  
        
        -- turning inn quests from pre-darnassus HS in auberdine
        {Text="Turn inn Beached Sea Turtle", Dt={q="Beached Sea Turtle"}, point={x=3662,y=4560,m=1439}},
        {Text="Turn inn beached Sea Turtle", Dt={q="Beached Sea Creature"}, point={x=3662,y=4559,m=1439}},
        {Text="Accept Fruit of the sea (fishing dude under FP)", At="Fruit of the Sea", point={x=3609,y=4492,m=1439}},
        {Text="==== RESTOCK On Longjaw Mud Snapper ====", BuyItem={Npc="Laird", Item="Longjaw Mud Snapper", Count=20}, Class="Hunter", point={x=3674,y=4429,m=1439}},
        {Text="Put HS in Auberdine", SetHs="Innkeeper Shaussiy", point={x=3705,y=4413,m=1439}},
	
	    {Text="Accept Wanted: Murkdeep (outside inn, wanted poster)", At="WANTED: Murkdeep!", point={x=3722,y=4420,m=1439}},
        {Text="Turn inn Cave Mushrooms (outside inn)", Dt={q="Cave Mushrooms", Item="Gustweald Cloak", Use=1}, point={x=3732,y=4365,m=1439}},
        {Text="Accept Onu", At="Onu", point={x=3732,y=4365,m=1439}},
	    {Text="Turn inn the red crystal", Dt={q="The Red Crystal"}, point={x=3771,y=4339,m=1439}},
	    {Text="Accept as water cascades", At="As Water Cascades", point={x=3771,y=4339,m=1439}},
	    {Text="Turn inn Trouble in Darkshore", Dt={q="Trouble In Darkshore?"}, point={x=3744,y=4183,m=1439}},
        {Text="Accept The Absent Minded Prospector", At="The Absent Minded Prospector", point={x=3744,y=4183,m=1439}},
        {Text="Turn inn The Cliffspring River", Dt={q="The Cliffspring River"}, point={x=3740,y=4012,m=1439}},
        {Text="Accept The Blackwood Corrupted", At="The Blackwood Corrupted", point={x=3740,y=4012,m=1439}},
	    
        {Text="=== RESTOCK Sharp Arrow === (IMPORTANT with max arrows now)", BuyItem={Npc="Dalmond", Item="Sharp Arrow", Count=1600}, Class="Hunter",  point={x=3744,y=4051,m=1439}},
	    {Text="Turn inn Deep ocean, vast sea", Dt={q="Deep Ocean, Vast Sea", Item="Noosegrip Gauntlets"}, point={x=3811,y=4115,m=1439}},
	    {Text="Turn inn Tharnariun's Hope", Dt={q="Tharnariun's Hope", Item="Evergreen Gloves", Use=1}, point={x=3884,y=4342,m=1439}},
	    
        {Text="=>RUN TO THE MOONWELL IN AUBERDINE AND FILL THE BOWLN FROM the blackwood corrupted", Item={Name="Filled Cleansing Bowl", Count=1}, UseItem="Empty Cleansing Bowl", point={x=3777,y=4406,m=1439}},
	    {Text="Fill empty water tube in the moonwell", UseItem="Empty Water Tube", Ct="As Water Cascades", point={x=3777,y=4408,m=1439}},

        {Text="Grind to the Red Crystal and deliver As Water Cascades, look for moonstalkers", Dt={q="As Water Cascades"}, UseItem="Evergreen Gloves", point={x=4737,y=4870,m=1439},
            PinAdd="Can kill as many moonkins as feasible here. We want 16 asap now"},
	    {Text="Accept The Fragment Within", At="The Fragments Within", point={x=4737,y=4870,m=1439}},  
	    {Text="Turn inn The Fragment Within", Dt={q="The Fragments Within", Item="Oakthrush Staff", Use=1}, point={x=3771,y=4338,m=1439}},
        
        
        
	    --{Text="==== RESTOCK On Longjaw Mud Snapper ====", BuyItem={Npc="Laird", Item="Longjaw Mud Snapper", Count=20}, point={x=3674,y=4429,m=1439}},


        -- first real south run
        
        {Text="Do Murkdeep (clear the murloc camp and walk to the fire to spawn him)", Ct="WANTED: Murkdeep!", point={x=3643,y=7652,m=1439}},
        {Text="Accept Beached Sea Creature (can kill crabs here too for Fruit of the Sea)", At="Beached Sea Creature", point={x=3279,y=8105,m=1439},
            PinAdd="Only take the next 3 sea creatures if some1 else is there clearing mobs. Doing it later otherwise"},
        {Text="Turn inn The Absent Minded Prospector. Skip followup unless lvl 18+ or with someone else", 
            point={x=3573,y=8369,m=1439}, PinAdd="Kill highlvl moonstalkers for A Lost Master and Buzzbox 323", Dt={q="The Absent Minded Prospector"}},
        {Text="Work on A Lost Master and Buzzbox towards Onu", Mct={"A Lost Master", "Buzzbox 525"}, point={x=4358,y=7626,m=1439}},
        
        {Text="Grind to Onu, turn inn Onu. Focus finishing moonstalkers. Get pet to 16 asap", Dt={q="Onu"}, point={x=4358,y=7626,m=1439}},

        {Text="Accept The Master's Glaive", At="The Master's Glaive", point={x=4358,y=7626,m=1439}, PinAdd="MAKE SURE YOU GOT A PHIAL OF SCRYING"},
        {Text="Turn inn Grove of the Ancients (may not have it, no big deal)", Dt={q="Grove of the Ancients"}, point={x=4358,y=7626,m=1439}},
        {Text="Vendor to clean bags", point={x=4369,y=7663,m=1439}, Proximity=5},
        {Text="Grind Sires towards Master's Glaive 1: Discover the area to complete quest", Ct="The Master's Glaive", point={x=3865,y=8768,m=1439}},
        {Text="Master's Glaive 2: Use Phial of Scrying in water and turnin", Dt={q="The Master's Glaive"}, UseItem="Phial of Scrying"},
        {Text="Accept The Twilight Camp from Phial of Scrying you spawned", At="The Twilight Camp"},
        {Text="Deliver The Twilight Camp at the small book next to altar", Dt={q="The Twilight Camp"}, point={x=3854,y=8605,m=1439}},
        {Text="Accept Return to Onu from the book", At="Return to Onu", point={x=3854,y=8605,m=1439}},
	    {Text="Grind for a while, try to get drop: Book: The Powers Below",Item={Name="Book: The Powers Below", Count=1} },
        {Text="Do Therylune's Escape (escort) if npc spawns (won't assist unless attacked first)", At="Therylune's Escape", point={x=3864,y=8734,m=1439}},

        {Text="Turn inn Return to Onu", Dt={q="Return to Onu"}, point={x=4357,y=7629,m=1439}, PinAdd="Delete Phial of Scrying"},
        {Text="WAIT FOR RP, then accept Mathystra Relics", At="Mathystra Relics", point={x=4357,y=7629,m=1439}},

        






        -- back in auberdine after south
        -- Uncertain if we can HS here, or if we need to save it for trip to the north.
        {Text="Heartstone to Auberdine", Proximity=20, point={x=3701,y=4412}, UseItem="Hearthstone"},
        {Text="==== RESTOCK On Longjaw Mud Snapper ====", BuyItem={Npc="Laird", Item="Longjaw Mud Snapper", Count=20},Class="Hunter",  point={x=3674,y=4429,m=1439}},
        {Text="Turn inn Beached Sea Creature #1", Dt={q="Beached Sea Creature"}, point={x=3662,y=4559,m=1439}},
        {Text="Turn inn Fruit of the sea (if complete, can also be done later)", 
            Dt={q="Fruit of the Sea", Item="Crustacean Boots", SkipIfUncomplete=1}, point={x=3609,y=4493,m=1439}},
        {Text="Turn inn WANTED: Murkdeep! (if complete, can also be done later)", Dt={q="WANTED: Murkdeep!", Item="Ridgeback Bracers", Use=1, SkipIfUncomplete=1}, point={x=3770,y=4340,m=1439}},
        {Text="=== RESTOCK Sharp Arrow & REPAIR ===", BuyItem={Npc="Dalmond", Item="Sharp Arrow", Count=1600},Class="Hunter",  point={x=3744,y=4051,m=1439}},
        

        -- final full north
        {Text="Run NE to, Loot Blackwood Grain Stores", Item={Name="Blackwood Grain Sample", Count=1}, point={x=5062,y=3496,m=1439}},
        {Text="Loot Blackwood Nut Stores", Item={Name="Blackwood Nut Sample", Count=1}, point={x=5181,y=3356,m=1439}},
        {Text="Loot Blackwood Fruit Stores", Item={Name="Blackwood Fruit Sample", Count=1}, point={x=5285,y=3343,m=1439}},
        {Text="Use Filled Cleansing Bowl (by fire) to summon Xabraxxis. Kill and loot Demon Bag on ground",
                Ct="The Blackwood Corrupted", point={x=5241,y=3338,m=1439}, UseItem="Filled Cleansing Bowl"},
        {Text="Turn inn Buzzbox 323 (unless done)", Dt={q="Buzzbox 323"}, point={x=5127,y=2457,m=1439}},
        {Text="Accept Buzzbox 525", At="Buzzbox 525", point={x=5127,y=2457,m=1439}},
        {Text="Turn inn The Tower of Althalaxx", Dt={q="The Tower of Althalaxx"}, point={x=5497,y=2488,m=1439}},
        {Text="Accept The Tower of Althalaxx pt2", At="The Tower of Althalaxx", point={x=5497,y=2488,m=1439}},
        {Text="Complete The Tower of Althalaxx pt2.", Ct="The Tower of Althalaxx", point={x=5536,y=2655,m=1439}},
        {Text="Turn inn The Tower of Althalaxx pt2", Dt={q="The Tower of Althalaxx"}, point={x=5497,y=2488,m=1439}},
        {Text="Accept The Tower of Althalaxx pt3", At="The Tower of Althalaxx", point={x=5497,y=2488,m=1439}},
        
        {Text="Grind here to 18 if possible. Want 18 for gyromast", Lvl={lvl=19} },
        {Text="Complete Mathystra Relics (small statues scattered around)", Ct="Mathystra Relics", point={x=5783,y=2137}, PinAdd="Look for Lady Vespira (rare), almost center of map-icon"},
        {Text="If still not lvl 18, do it here on nagas", Lvl={lvl=19} },
        {Text="Accept Gyromast's Retrieval", At="Gyromast's Retrieval", point={x=5666,y=1348,m=1439}},
        {Text="If did not complete Buzzbox or A Lost Master (pelts), do it now", point={x=6019,y=1195,m=1439}},
        {Text="Complete Gyromast's Retrieval (murlocs, Forestriders and crabs)", Ct="Gyromast's Retrieval"},
        {Text="Deliver Gyromast's Retrieval", Dt={q="Gyromast's Retrieval"}, point={x=5666,y=1348,m=1439}},
        {Text="Accept Gyromast's Revenge", At="Gyromast's Revenge", point={x=5666,y=1348,m=1439}},
        {Text="Talk to the Treshwackonator 4100", Proximity=10, point={x=5582,y=1823,m=1439},
            PinAdd="Pet passive, wingclip, keep aggro and just run+let serpent do it's job. Pref with mana pot"},
        {Text="Move back to Gyromast and kill.", Proximity=20, point={x=5667,y=1437,m=1439}},
        {Text="Kill and loot to complete. Deliver quest", Dt={q="Gyromast's Revenge"}, point={x=5666,y=1348,m=1439},
            PinAdd="Delete Gyromast's Key after delivering quest"},

        {Text="If didnt do before Darnassus, get beached sea Turtle north-east of river", At="Beached Sea Turtle", point={x=5310,y=1808,m=1439}},
        {Text="Make sure Fruit of the sea is complete now. (not from the lvl 20 crabs maybe)", Ct="Fruit of the Sea",PinAdd="Delete Gyromast's Key after delivering quest"},
            
        
        -- todo: wonky ordering here. Depending on HS or deathwarp, it will be different
        {Text="Get back to Auberdine. Hearthstone if up, otherwise grind/run", UseItem="Hearthstone", DeathWarp=1},
        {Text="=== RESTOCK Sharp Arrow ===", BuyItem={Npc="Dalmond", Item="Sharp Arrow", Count=1600}, Class="Hunter", point={x=3744,y=4051,m=1439}},
        {Text="Deliver The Blackwood Corrupted", Dt={q="The Blackwood Corrupted", Item="Wildkeeper Leggings", Use=1}, point={x=3740,y=4016,m=1439}},
        {Text="Turn inn A Lost Master", Dt={q="A Lost Master"}, point={x=3937,y=4347,m=1439}},
        {Text="Accept A Lost Master pt2", At="A Lost Master", point={x=3937,y=4347,m=1439}},
        {Text="==== RESTOCK On Longjaw Mud Snapper ====", BuyItem={Npc="Laird", Item="Longjaw Mud Snapper", Count=20}, Class="Hunter", point={x=3674,y=4429,m=1439}},
        {Text="Deliver Beached Sea Turtle", Dt={q="Beached Sea Turtle"}, point={x=3662,y=4560,m=1439}},
        {Text="Deliver Fruit of the Sea (make sure have boots already)", Dt={q="Fruit of the Sea", Item="Crustacean Boots"}, point={x=3609,y=4493,m=1439}},


        -- need some work on these last steps
        {Text="Complete Buzzbox 525 and Absent minded prospector", Mct={"Buzzbox 525","The Absent Minded Prospector"}, 
            point={x=3572,y=8369,m=1439}},
        {Text="Accept Beached Sea Turtle", At="Beached Sea Turtle", point={x=3169,y=8363,m=1439}},
        {Text="Accept Beached Sea Turtle", At="Beached Sea Turtle", point={x=3121,y=8551,m=1439}},
        {Text="Accept Beached Sea Creature", At="Beached Sea Creature", point={x=3129,y=8744,m=1439}},

        {Text="Deliver Buzzbox 525", Dt={q="Buzzbox 525"}, point={x=4136,y=8053,m=1439}},
        {Text="Deliver Mathystra Relics (vendor too)", Dt={q="Mathystra Relics", Item="Woodsman Sword"}, point={x=4356,y=7631,m=1439}},
        {Text="Accept The Sleeper Has Awakened if it's up",At="The Sleeper Has Awakened", point={x=4440,y=7643,m=1439}},
            
        {Text="FIRST LOOT THE BOX FOR QUEST Turn inn A Lost Master", Dt={q="A Lost Master"}, point={x=4505,y=8534,m=1439}},
        {Text="Accept Escape Through Force", At="Escape Through Force", point={x=4505,y=8534,m=1439}},
        {Text="Complete Escape Through Force", Ct="Escape Through Force"},

        {Text="Accept One Shot. One Kill", At="One Shot.  One Kill.", point={x=4595,y=9028,m=1439}},
        {Text="Complete One Shot. One Kill (takes a few minutes)", Ct="One Shot.  One Kill.", PinAdd="letting the final boss engage with her before you engage seems to prevent the several minutes wait at the end"},

        {Text="Turn inn One Shot. One Kill.", Dt={q="One Shot.  One Kill."}, point={x=2659,y=3670,m=1440}},
        {Text="Turn inn The Sleeper Has Awakened", Dt={q="The Sleeper Has Awakened", Item="Owlsight Rifle"}, point={x=2728,y=3554,m=1440}},
        
        {Text="If within grinding distance of lvl 20 grind it and goto guide 20-22 Ashenvale from guidelist. If not (> a few bars from 20) continue as normal with 18-22 Ashenvale"},

        --[[
            21300

            1950+1150 absent minded prospector
            1350 therylune's escape
            725 beached sea creature 
            725 beached sea turtle
            725 beached sea turtle
            1750 escape through force
            8375
        ]]
    }
}

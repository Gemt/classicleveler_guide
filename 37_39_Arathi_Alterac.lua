-- Author      : G3m7
-- Create Date : 5/11/2019 12:38:40 PM

CLGuide_Arathi_Alterac = {
    Title="37-39 Arathi/Alterac",
    Pinboard = {"Keep an eye out for Fozzruk in alterac so you know where he is when you need him"},
    Steps = {
        -- fly to arathi highlands
        {Text="Set HS in Menethil", SetHs="Innkeeper Helbrek", point={x=1072,y=6137,m=1437}},
        {Text="Fly to Arathi Highlands", Taxi="Refuge Pointe, Arathi", point={x=950,y=5969}},
        {Text="Buy Razor Arrows", Class="Hunter", BuyItem={Npc="Vikki Lonsav", Item="Razor Arrow", Count=2800}, point={x=4644,y=4760}},
        {Text="Accept Worth Its Weight in Gold", At="Worth Its Weight in Gold", point={x=4620,y=4775}},
        {Text="Accept Wanted! Marez Cowl", At="Wanted!  Marez Cowl", point={x=4605,y=4777}},
        {Text="Accept Stromgarde Badges", At="Stromgarde Badges", point={x=4583,y=4755}},
        -- GROUPS:
        --{Text="Accept Wanted! Otto and Falconcrest", At="Otto and Falconcrest", point={x=4605,y=4777}},
        -- pick up on hints of a new plague chain where u left off at 32
        {Text="Accept The Princess Trapped", At="The Princess Trapped", point={x=6242,y=3369,m=1417}},
        {Text="Complete The Princess Trapped to the east", Ct="The Princess Trapped", point={x=7965,y=4012,m=1417}},
        {Text="Deliver The Princess Trapped (in the cave)", Dt={q="The Princess Trapped"}, point={x=8431,y=3095,m=1417}},
        {Text="Accept Stones of Binding", At="Stones of Binding", point={x=8431,y=3095,m=1417}},
        
        {Text="Get Cresting Key (NE)", Item={Name="Cresting Key", Count=1}, point={x=6670,y=2977,m=1417}},
        
        {Text="Attempt Hints of a new plague again", PinAdd="If not killed courier, be on lookup during zone", At="Hints of a New Plague?", point={x=6019,y=5384,m=1417},
            PinAdd="Can farm a bit on the farm to wait for respawns. If getting it, complete the chain at any point."},
        
        {Text="Complete Worth Its Weight in Gold (SE) (do mobs in cave)", Ct="Worth Its Weight in Gold", point={x=6610,y=6224,m=1417}},
        
        {Text="Get Thundering Key (east of refuge pointe)", Item={Name="Thundering Key", Count=1}, point={x=5202,y=5068,m=1417}},

        {Text="Buy Razor Arrows", Class="Hunter", BuyItem={Npc="Vikki Lonsav", Item="Razor Arrow", Count=2800}, point={x=4644,y=4760,m=1417}},
        {Text="Deliver Worth Its Weight in Gold (Refuge Pointe)", Dt={q="Worth Its Weight in Gold"}, point={x=4620,y=4775,m=1417}},
        {Text="Accept Wand over Fist", At="Wand over Fist", point={x=4665,y=4702,m=1417}},

        {Text="Complete Wand over Fist (Kor'gresh in ogre cave south)", Ct="Wand over Fist", point={x=5476,y=8190,m=1417}},
        
        -- Groups: accept Trelane's Defenses
        --{Text="Accept Trelane's Defenses", At="Trelane's Defenses", point={x=4665,y=4702,m=1417}},

        {Text="Get Burning Key (far west). Stones of binding should now be completed", Ct="Stones of Binding", point={x=2548,y=3015,m=1417}},
        
        -- what you do here depends on grp or not
        {Text="Complete Marez Cowl and badges if lucky", Mct={"Stormgarde Badges"}, point={x=2977,y=6236,m=1417}},
        
        {Text="Deliver Stones of Binding", Dt={q="Stones of Binding"}, point={x=3622,y=5739,m=1417}},
        {Text="Accept Breaking the Keystone", At="Breaking the Keystone", point={x=3622,y=5739,m=1417}, PinAdd="If you kill fozzruk, also accept the followup. Delivery and followup is at Keystone in center here"},
        
        -- TODO: Add the faldir cove quests!!!!!
        {Text="Accept Land Ho!", At="Land Ho!", point={x=3178,y=8268,m=1417}},
        {Text="Deliver Land Ho!", Dt={q="Land Ho!"}, point={x=3228,y=8137,m=1417}},
        {Text="Accept Deep Sea Salvage", At="Deep Sea Salvage", point={x=3280,y=8148,m=1417}},
        {Text="Accept Sunken Treasure", At="Sunken Treasure", point={x=3387,y=8054,m=1417}},
        {Text="Accept Drowned Sorrows", At="Drowned Sorrows", point={x=3400,y=8078,m=1417}},
        {Text="Complete Sunken Treasure (follow the gnome, 2 water elementals spawn)", Ct="Sunken Treasure"},
        {Text="Deliver Sunken Treasure", Dt={q="Sunken Treasure"}, point={x=3385,y=8045,m=1417}},
        {Text="Accept Sunken Treasure", At="Sunken Treasure", point={x=3385,y=8045,m=1417}},

        {Text="Use goggles to track elven gems. Maiden's Folly Log (first floor below deck)", UseItem="Goggles of Gem Hunting", Item={Name="Maiden's Folly Log", Count=1}, point={x=2340,y=8510,m=1417}},
        {Text="Maiden's Folly Charts (first floor below deck, by the mast)", Item={Name="Maiden's Folly Charts", Count=1}, point={x=2303,y=8450,m=1417}},
        {Text="Spirit of Silverpine Charts (first floor below deck, by canons)", Item={Name="Spirit of Silverpine Charts", Count=1}, point={x=2046,y=8562,m=1417}},
        {Text="Spirit of Silverpine Log (bottom floor))", Item={Name="Spirit of Silverpine Log", Count=1}, point={x=2065,y=8510,m=1417}},

        {Text="Complete all 3 quests", Mct={"Sunken Treasure", "Drowned Sorrows", "Deep Sea Salvage"}},

        {Text="Deliver Deep Sea Salvage", Dt={q="Deep Sea Salvage"}, point={x=3280,y=8148,m=1417}, PinAdd="Re-equip your normal head"},
        {Text="Deliver Drowned Sorrows", Dt={q="Drowned Sorrows"}, point={x=3400,y=8078,m=1417}},
        {Text="Deliver Sunken Treasure", Dt={q="Sunken Treasure", Item="Servomechanic Sledgehammer"}, point={x=3385,y=8045,m=1417}},
        {Text="Accept Sunken Treasure", At="Sunken Treasure", point={x=3385,y=8045,m=1417}},
        {Text="Deliver Sunken Treasure", Dt={q="Sunken Treasure"}, point={x=3228,y=8138,m=1417}},
        {Text="Accept Sunken Treasure", At="Sunken Treasure", point={x=3228,y=8138,m=1417}},

        {Text="Go to Refuge Pointe & buy cured ham steak for pet if needed. If not yet killed Fozzruk, now is the time (no more reminders)", Class="Hunter", BuyItem={Npc="Narj Deepslice", Item="Cured Ham Steak", Count=20}, point={x=4562,y=4757,m=1417}},
        {Text="Deliver Wand over Fist", Dt={q="Wand over Fist"}, point={x=4665,y=4702,m=1417}},
        {Text="Buy Razor Arrows", Class="Hunter", BuyItem={Npc="Vikki Lonsav", Item="Razor Arrow", Count=2800}, point={x=4644,y=4760,m=1417}},
        {Text="Deliver any quests you did at stormgarde in Refuge Pointe, then fly to Southshore. Abandon the rest", Taxi="Southshore, Hillsbrad", point={x=4618,y=4639,m=1417}},

        -- fly southshore
        -- TODO: open question if we will still have the perenolde tiara/preserving knowledge here
        -- TODO: as a group you will want to have Crushridge Warmongers instead of Crushridge Bounty, which should have been done at 30_32 section
        {Text="Accept Preserving Knowledge (only if u found book in duskwood)", At="Preserving Knowledge", point={x=5057,y=5709,m=1424},
            PinAdd="Deliver Hints of a new plague outside the Inn too if done"},
        {Text="Accept Crushridge Bounty", At="Crushridge Bounty", point={x=4947,y=5870,m=1424}},
        {Text="Accept Noble Deaths", At="Noble Deaths", point={x=4814,y=5911,m=1424}},
        {Text="Deliver Further Mysteries", Dt={q="Further Mysteries"}, point={x=4814,y=5911,m=1424}},
        {Text="Accept Dark Council", At="Dark Council", point={x=4814,y=5911,m=1424}},
        {Text="Deliver Hints of a New Plague? (if completed)", Dt={q="Hints of a New Plague?", Item="Lightstep Leggings", Use=1}, point={x=5034,y=5901,m=1424}},
        {Text="Deliver Down the scarlet path (if done. Second floor inn)", Dt={q="Down the Scarlet Path"}, point={x=5145,y=5840,m=1424}},
        -- If doing SM, accept in the name of the light...

        -- todo: any way we can set HS in wetlands when coming from theramore instead,
        -- use a deathwarp back to southshore and hs? 
        --{Text="Set HS at Innkeeper Anderson", SetHs="Innkeeper Anderson", point={x=5117,y=5892}},

        --todo: forgot to bring this quest from BB on first iteration. Need to find best spot to put it:
        --{Text="Run towards Alterac Mountains, Dalaran area", Zone="Alterac Mountains", point={x=3100,y=2708}},
        {Text="Deliver Magical Analysis (by dalaran in Alterac Mountains)", Dt={q="Magical Analysis"}, point={x=1883,y=7849,m=1416}},
        {Text="Accept Ansirem's Key", At="Ansirem's Key", point={x=1883,y=7849,m=1416}},

        {Text="Kill Ogres towards elites.", point={x=4949,y=5308}},
        {Text="Get Worn Leather Book for Preserving Knowledge. (only if u found book in duskwood)", point={x=3846,y=4639,m=1416},
            PinAdd="tomes drop from nonelites too"},
        {Text="Try to find Grel'Borg (The Perenolde Tiara). Patrolls from cellar in keep and out to entrance of ogre place", 
            Ct="The Perenolde Tiara", point={x=3725,y=5298,m=1416}},
        {Text="Make sure Preserving Knowledge and Crushridge Bounty is complete", Mct={"Preserving Knowledge","Crushridge Bounty"}},
        
        {Text="Get Chillwind Camp FP", Proximity=8, point={x=4293,y=8506,m=1422}},
        
        {Text="Run back, then NW to complete Noble Deaths and Dark Council (Generally 1 Shadow mage in each camp/house)", 
            Mct={"Noble Deaths", "Dark Council"}, PinAdd="Remember Nagaz all the way NW to complete dark council + chest in house", point={x=3967,y=1602,m=1416}},
        {Text="Loot chest at point, 1st floor in house", Item={Name="Ensorcelled Parchment", Count=1}, point={x=3918,y=1469,m=1416}},
        {Text="If close to 63425/85700 xp @ lvl 39 grind to it to hit 40 from turnins to train in IF. (that is if you have not skipped any quests)"},
        -- heartstone or deathwarp to southshore
        {Text="Deathwarp back to Southshore. Click scroll and accept quest", UseItem="Ensorcelled Parchment", DeathWarp=1},

        {Text="Deliver The Ensorcelled Parchment", Dt={q="The Ensorcelled Parchment"}, point={x=5057,y=5709,m=1424}},
        {Text="Accept Stormpike's Deciphering", At="Stormpike's Deciphering", point={x=5057,y=5709,m=1424}},
        {Text="Deliver Preserving Knowledge", Dt={q="Preserving Knowledge"}, point={x=5057,y=5709,m=1424}},
        {Text="Accept Return to Milton", At="Return to Milton", point={x=5057,y=5709,m=1424}},
        {Text="Deliver Crushridge Bounty (skip followup)", Dt={q="Crushridge Bounty"}, point={x=4947,y=5870,m=1424}},
        {Text="Deliver Noble Deaths", Dt={q="Noble Deaths"}, point={x=4814,y=5911,m=1424}},
        {Text="Deliver Dark Council", Dt={q="Dark Council"}, point={x=4814,y=5911,m=1424}},

        -- HS menethil/fly IF
        {Text="Get to IF. Either HS menethil or fly directly to IF", Zone="Wetlands", UseItem="Hearthstone"},
        {Text="Fly to IF", Taxi="Ironforge, Dun Morogh", PinAdd="Abandon Breaking the Keystone if you did not complete it"},
        -- todo: cloth donation. Should have tonnes of silk by now
        
        {Text="Deliver The Karnitol Shipwreck (Hall of Explorers)", Dt={q="The Karnitol Shipwreck", Item="Hellion Boots", Use=1}, point={x=6987,y=2126,m=1455}},
        {Text="Deliver Reagents for Reclaimers Inc.", Dt={q="Reagents for Reclaimers Inc.", Item="Auric Bracers"}, point={x=6987,y=2126,m=1455}},

        {Text="Deliver Stormpike's Deciphering", Dt={q="Stormpike's Deciphering"}, point={x=7466,y=1173,m=1455}},
        {Text="Accept Ironband Wants You!", At="Ironband Wants You!", point={x=7466,y=1173,m=1455}},
        
        -- todo: points
        {Text="Deliver Myzrael's Allies in Forlorn Cavern if you killed Fozzruk", Dt={q="Myzrael's Allies"}},
        {Text=">Accept Theldurin the Lost (if Myzrael's Allies)", At="Theldurin the Lost"},

        {Text="Deliver any cloth you can", point={x=4322,y=3159,m=1455}},
        {Text="Get Fizzle Brassbolts' Letter from bank, put rest of cloth & Perenolde Tiara/Tomes of Alterac, Sample Elven Gem in", 
            PutInBank={"Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth", "Perenolde Tiara", "Tomes of Alterac", "Sample Elven Gem"},
            TakeFromBank={"Fizzle Brassbolts' Letter"},
            point={x=3557,y=6066,m=1455}},

        -- todo: buy Gyrochronatom (engineer vendor) IF got frost oil
        -- todo: healing potion & lesser invisibility potion (if got did gyrochronatom)
        -- todo: bank cloth and/or deliver cloth donation
        {Text="If possible now, get Frost Oil > Gyrochronatom > Healing Potion + Lesser Invis Pot > patterned bronze bracers"},
        {Text="Train hunter skills", TrainSkill=1, point={x=7088,y=8363,m=1455}},
        {Text="Train pet skills", TrainSkill=1, point={x=7084,y=8574,m=1455}},
        {Text="Buy arrows. Lvl 40 ones if close (get 3-5 extra stacks outside quiver unless slow bow). Check for 42 bow", point={x=7175,y=6673,m=1455}},
        {Text="Fly to Thelsamar, Loch Modan", Taxi="Thelsamar, Loch Modan", point={x=5551,y=4776,m=1455}},
            
        -- groups: get uldaman q too
        -- todo: class/pet skills
        -- todo: check AH for frost oil
    }
}
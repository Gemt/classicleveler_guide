-- Author      : G3m7
-- Create Date : 5/5/2019 5:18:43 PM

CLGuide_Duskwood1 = {
    Title="24-26 Duskwood",
    Pinboard = {},
    Steps = {
        -- Antonio Perelli, salesman on road to Darkshire sells Wolf Bracers (35s) and 3 lesser healing potion
        -- Kzixx, goblin on road, sells 3 healing potion and 3 lesser mana potion


        {Text="Accept The Legend of Stalvan (Madame Eva in town)", At="The Legend of Stalvan", point={x=7581,y=4531,m=1431}},
        {Text="Accept The Totem of Infliction", At="The Totem of Infliction", point={x=7581,y=4531,m=1431}},

        {Text="If you dont have Bronze tube yet check Herble Baubbletump", 
            BuyItem={Npc="Herble Baubbletump", Item="Bronze Tube", Count=1}, point={x=7801,y=4835,m=1431}},

        {Text="If you have Bronze Tube: Accept Look to The Stars", At="Look to The Stars", point={x=7980,y=4800,m=1431}},
        {Text="If you have Bronze Tube: Deliver Look to The Stars", Dt={q="Look to The Stars"}, point={x=7980,y=4800,m=1431}},
        {Text="If you have Bronze Tube: Accept Look to The Stars pt2", At="Look to The Stars", point={x=7980,y=4800,m=1431}},

        {Text="Accept Raven Hill", At="Raven Hill", point={x=7532,y=4869,m=1431}},
        {Text="Accept The Hermit", At="The Hermit", point={x=7532,y=4869,m=1431}},
        {Text="Accept Deliveries to Sven", At="Deliveries to Sven", point={x=7532,y=4869,m=1431}},
        {Text="Accept The Night Watch", At="The Night Watch", point={x=7360,y=4686,m=1431}},

        {Text="Deliver The Legend of Stalvan", Dt={q="The Legend of Stalvan"}, point={x=7253,y=4685,m=1431}},
        {Text="Accept The Legend of Stalvan (Maybe RP?)", At="The Legend of Stalvan", point={x=7253,y=4685,m=1431}},

        {Text="Put HS in Darkshire", SetHs="Innkeeper Trelayne", point={x=7387,y=4441,m=1431}},

        --{Text="Accept Seasoned Wolf Kabobs", At="Seasoned Wolf Kabobs", point={x=7379,y=4332}},

        {Text="If Bronze Tube: Deliver at Blind Mary", Dt={q="Look to The Stars"}, point={x=8197,y=5910,m=1431}},
        {Text="If Bronze Tube: Accept followup", At="Look to The Stars", point={x=8197,y=5910,m=1431}},


        {Text="Kill Insane Ghoul (if look to the stars) and complete The Night Watch. Dont need to finish Skeletal Fingers",
            Mct={"Look To The Stars", "The Night Watch"}, point={x=8022,y=7095,m=1431}},

        {Text="Accept Eight-Legged Menaces at xroads.", At="Eight-Legged Menaces", point={x=4513,y=6702,m=1431},
            PinAdd="Make sure to get 6 Gooey Spider Legs from spiders"},

        {Text="Deliver Raven Hill", Dt={q="Raven Hill"}, point={x=1834,y=5626,m=1431}},
        {Text="Accept Jitters' Growling Gut", At="Jitters' Growling Gut", point={x=1834,y=5626,m=1431}},

        --todo: remind about gooey spider legs earlier. They can be farmed on the way west 
        {Text="Deliver Deliveries to Sven. Kill Pygmy Venom Spiders on the way (try to complete, turns grey at lvl 25)", 
            Dt={q="Deliveries to Sven"}, point={x=778,y=3406,m=1431}},

        {Text="Accept Sven's Revenge", At="Sven's Revenge", point={x=778,y=3406,m=1431}},
        {Text="Accept Wolves at Our Heels", At="Wolves at Our Heels", point={x=771,y=3321,m=1431}},

        {Text="Deliver The Hermit. Kill wolves/Pygmy Venom Spiders on the way", Dt={q="The Hermit"}, point={x=2811,y=3147,m=1431}},
        {Text="Accept Supplies from Darkshire", At="Supplies from Darkshire", point={x=2811,y=3147,m=1431}},

        {Text="Complete Eight-Legged Menaces and Wolf at our heels unless extremely congested (in which case complete on run to Goldshire)", 
            Mct={"Eight-Legged Menaces", "Wolves at Our Heels"}, point={x=771,y=3319,m=1431}},
        {Text="Turn inn Wolves at Our Heels", Dt={q="Wolves at Our Heels"}, point={x=771,y=3319,m=1431}},
        {Text="Make sure you got 6 Gooey Spider Legs before leaving"},
        -- TODO: How many arrows do i need to get to 25 at this point? Maybe already and dont buy any?
        {Text="Run toward Sentinel Hill, Accept The Defias Brotherhood at tower.", At="The Defias Brotherhood", point={x=5630,y=4759,m=1436}},
        {Text="Restock on lvl 10 arrows if not 25. Vendor. Dont accept quest",
            BuyItem={Npc="Quartermaster Lewis", Item="Sharp Arrow", Count=1}, Class="Hunter", point={x=5693,y=4720,m=1436}},
        {Text="Get FP in Sentinel Hill.", Proximity=8, point={x=5656,y=5264,m=1436}},

        {Text="(Place freze trap) Deliver The Legend of Stalvan. If need bronze tube, look in NW corner in house for Box", 
            Dt={q="The Legend of Stalvan"}, point={x=4152,y=6673,m=1436}},
        {Text="Accept The Legend of Stalvan. If need bronze tube, look in NW corner in house for Box", 
            At="The Legend of Stalvan", point={x=4152,y=6673,m=1436}},

        {Text="Heartstone to Darkshire", Zone="Duskwood", UseItem="Hearthstone"},
        {Text="Turn Jitters' Growling Gut (inn) ", Dt={q="Jitters' Growling Gut"}, point={x=7381,y=4325,m=1431}},
        {Text="Accept Dusky Crab Cakes", At="Dusky Crab Cakes", point={x=7381,y=4325,m=1431}},
        {Text="Deliver Dusky Crab Cakes (if you had legs from earlier)", Dt={q="Dusky Crab Cakes"}, point={x=7381,y=4325,m=1431}},
        {Text="Accept Return to Jitters (if delivered dusky crab cakes)", At="Return to Jitters", point={x=7381,y=4325,m=1431}},
        
        {Text="Turn The Legend of Stalvan", Dt={q="The Legend of Stalvan"}, point={x=7255,y=4685,m=1431}},
        {Text="Accept The Legend of Stalvan", At="The Legend of Stalvan", point={x=7255,y=4685,m=1431}},

        {Text="Turn inn The Night Watch", Dt={q="The Night Watch"}, point={x=7360,y=4692,m=1431}},
        {Text="Accept The Night Watch", At="The Night Watch", point={x=7360,y=4692,m=1431}},
        
        --{Text="Accept Worgen in the Woods", At="Worgen in the Woods", point={x=7556,y=4804,m=1431}},
        

        {Text="Turn Supplies from Darkshire", Dt={q="Supplies from Darkshire"}, point={x=7586,y=4528,m=1431}},
        {Text="Accept Ghost Hair Thread", At="Ghost Hair Thread", point={x=7586,y=4528,m=1431}},
        {Text="Discover the FP in Darkshire", Proximity=8, point={x=7749,y=4428,m=1431}},
        {Text="If had Bronze Tube: Turn inn Look To The Stars. Else check vendor Skip followup for now.", Dt={q="Look To The Stars"}, point={x=7980,y=4798,m=1431}},
        
        {Text="Deliver Ghost Hair Thread (blind mary to the south)", Dt={q="Ghost Hair Thread"}, point={x=8198,y=5908,m=1431}},
        {Text="Accept Return the Comb", At="Return the Comb", point={x=8198,y=5908,m=1431}},
        {Text="Deliver Return the Comb", Dt={q="Return the Comb"}, point={x=7581,y=4531,m=1431}},
        {Text="Accept Deliver the Thread", At="Deliver the Thread", point={x=7581,y=4531,m=1431}},

        -- Don't require 25 here actually. We're not taking the 25 quest in SW yet, and 
        -- being 25 will make more mobs in Redridge grey, so might as well avoid it.
        --{Text="Grind Spiders/wolves along northern bank until 25, then run Goldshire",
        --    Lvl={lvl=25}, point={x=6380,y=1650,m=1431}},
        --{Text="Run to Goldshire. Kill wolves/spiders on the way", Zone="Elwynn Forest"},

        {Text="Run to Goldshire (grind on way), Turn inn The Legend of Stalvan (innkeeper)", Dt={q="The Legend of Stalvan"}, point={x=4377,y=6580,m=1429}},
        {Text="Accept The Legend of Stalvan", At="The Legend of Stalvan", point={x=4377,y=6580,m=1429}},
        {Text="Complete The Legend of Stalvan (chest second floor master bedroom)", Ct="The Legend of Stalvan", point={x=4432,y=6583,m=1429}},
        {Text="Turn inn The Price of Shoes (Blacksmith building)", Dt={q="The Price of Shoes"}, point={x=4172,y=6555,m=1429}},
        {Text="Accept Return to Verner", At="Return to Verner", point={x=4172,y=6555,m=1429}},

        {Text="Run to Stormwind", Zone="Stormwind City", point={x=3249,y=4971,m=1429}, PinAdd="If lvl 25, also accept Accept A Noble Brew at lock trainer while in SW"},
        {Text="Turn inn A Scroll from Mauren (Mage Quarter) Skip Followup", Dt={q="A Scroll from Mauren"}, point={x=4309,y=8038,m=1453}},
        {Text="If 25 Accept A Noble Brew at lock trainer while in SW", At="A Noble Brew", point={x=2922,y=7399,m=1453}},

        {Text="Turn inn The Legend of Stalvan (South of Park)", Dt={q="The Legend of Stalvan"}, point={x=2957,y=6193,m=1453}},
        {Text="Accept The Legend of Stalvan", At="The Legend of Stalvan", point={x=2957,y=6193,m=1453}},
        {Text="Deliver The Legend of Stalvan (chest next to you)", Dt={q="The Legend of Stalvan"}, point={x=2943,y=6150,m=1453}},
        {Text="Accept The Legend of Stalvan", At="The Legend of Stalvan", point={x=2943,y=6150,m=1453}},

        --{Text="Go through trade district, outer ring toward old down, buy Stormwind Seasoning Herbs", 
        --    BuyItem={Npc="Felicia Gump", Item="Stormwind Seasoning Herbs", Count=1}, point={x=6425,y=6063}},

        -----------------------------------------------------
        -------------------- LAKESHIRE ----------------------
        -----------------------------------------------------

        {Text="Fly to Lakeshire", Taxi="Lakeshire, Redridge", point={x=6631,y=6228,m=1453}},

        {Text="Accept Blackrock Bounty", At="Blackrock Bounty", point={x=3154,y=5787,m=1433}},
        {Text="Accept Blackrock Menace", At="Blackrock Menace", point={x=3351,y=4896,m=1433}},
        {Text="Accept The Everstill Bridge", At="The Everstill Bridge", point={x=3216,y=4866,m=1433}},
        {Text="Deliver Return to Verner", Dt={q="Return to Verner"}, point={x=3098,y=4727,m=1433}},
        {Text="Accept A Baying of Gnolls (skip underbelly scales)", At="A Baying of Gnolls", point={x=3098,y=4727,m=1433}},

        {Text="Accept Solomon's Law (town hall)", At="Solomon's Law", point={x=2967,y=4433,m=1433}},
        
        -- DONT put hs here, we want it in duskwood later
                
        {Text="Deliver The Defias Brotherhood (second floor inn) Skip followup", Dt={q="The Defias Brotherhood"},
            point={x=2647,y=4534,m=1433}, PinAdd="If not completed Bellygrub quest, be on the lookout for it"},
        {Text="Complete A Baying of Gnolls (Take the west path). Can finish everstil later", 
            Ct="A Baying of Gnolls", point={x=1731,y=4424,m=1433}},
        {Text="Turn inn A Baying of Gnolls", Dt={q="A Baying of Gnolls"}, point={x=3098,y=4728,m=1433}},
        {Text="Accept Howling in the Hills (can turn in Everstil Bridge if done)", At="Howling in the Hills", point={x=3098,y=4728,m=1433}},
        {Text="Turn inn The Everstill Bridge (if completed)", Dt={q="The Everstill Bridge", Item="Smith's Trousers", SkipIfUncomplete=1}, point={x=3216,y=4864,m=1433}},
        {Text="Accept Wanted: Lieutenant Fangore (posted outside inn)", At="Wanted: Lieutenant Fangore", point={x=2674,y=4646,m=1433}},
        {Text="Kill Yowler for Howling in the Hills, complete everstill bridge", Mct={"Howling in the Hills", "The Everstill Bridge"}, point={x=2774,y=2146,m=1433}},
        {Text="Follow road north, complete Blackrock Quests (need to go in cave)",
            Mct={"Blackrock Bounty", "Blackrock Menace"}, PinAdd="Trackers have MASSIVE aggro range"},


        {Text="Deliver Blackrock Menace", Dt={q="Blackrock Menace"}, point={x=3351,y=4895,m=1433}},
        {Text="Accept Tharil'Zun", At="Tharil'zun", point={x=3350,y=4897,m=1433}},
        {Text="Accept Shadow Magic", At="Shadow Magic", point={x=3350,y=4897,m=1433}},
        {Text="Turn inn The Everstill Bridge (if not already done)", Dt={q="The Everstill Bridge", Item="Smith's Trousers"}, point={x=3216,y=4864,m=1433}},
        {Text="Deliver Howling in the Hills", Dt={q="Howling in the Hills", Item="Ring of Iron Will", Use=1}, point={x=3098,y=4727,m=1433}},

        {Text="Run east, kill reachable Blackrock Shadowcaster", Ct="Shadow Magic", point={x=6727,y=5127,m=1433}},
        {Text="Deliver A Watchful Eye to the SE on statue. Skip followup", Dt={q="A Watchful Eye"}, point={x=8430,y=4690,m=1433}},
        {Text="Complete Solomon's Law and kill/loot Fangore. Pull Tharil'Zun and Blackrock Shadowcasters from point",
            Mct={"Wanted: Lieutenant Fangore", "Solomon's Law", "Tharil'zun", "Shadow Magic"}, point={x=7131,y=5888,m=1433},
            PinAdd="Can drop Glowing Shadowhide Pendant, if so keep in bags. Can grind for a bit to get itcd"},
        
        {Text="Run back to lakeshire, Deliver Tharil'Zun", Dt={q="Tharil'zun", Item="Orc Crusher"}, point={x=3350,y=4897,m=1433}},
        {Text="Deliver Shadow Magic", Dt={q="Shadow Magic"}, point={x=3350,y=4897,m=1433}},
        {Text="Deliver Solomon's Law (town hall)", Dt={q="Solomon's Law"}, point={x=2963,y=4427,m=1433}},
        {Text="Deliver Wanted: Lieutenant Fangore (town hall) (dont take messenger q)", Dt={q="Wanted: Lieutenant Fangore"}, point={x=3002,y=4447,m=1433}},
        {Text="Make sure got arrows/food b4 leaving. Food at inn, 25 arrows at inn, 10 arrows outside town-hall"},
        {Text="Deliver Blackrock Bounty", Dt={q="Blackrock Bounty"}, point={x=3154,y=5787,m=1433}},

        -- deliver Glowing Shadowhide Pendant when going logging camp
        --{Text="Run to Logging Camp east in Elwynn", Zone="Elwynn Forest", point={x=972,y=7145}},
        {Text="Run to Logging Camp east in Elwynn, Deliver The Legend of Stalvan", Dt={q="The Legend of Stalvan"}, point={x=8460,y=6935,m=1429}},
        {Text="Accept The Legend of Stalvan", At="The Legend of Stalvan", point={x=8460,y=6935,m=1429}},
        {Text="Loot Faded Journal from chest (second floor in house, place trap)", Ct="The Legend of Stalvan", point={x=8568,y=6956,m=1429}},
        {Text="Deliver The Legend of Stalvan", Dt={q="The Legend of Stalvan"}, point={x=8460,y=6935,m=1429}},
        {Text="Accept The Legend of Stalvan", At="The Legend of Stalvan", point={x=8460,y=6935,m=1429}},

        {Text="If you found Glowing Shadowhide Pendant, accept the Q and turnin in tower to the west", 
            Dt={q="Theocritus' Retrieval"}, point={x=6460,y=6942,m=1429}, UseItem="Glowing Shadowhide Pendant"},




        -- make sure we already have night watch at this point
        --{Text="Accept The Night Watch", At="The Night Watch", point={x=7359,y=4690}},

        {Text="Run south to Duskwood and grind northern shore to Abercrombie and turn inn Deliver the Thread", 
            Dt={q="Deliver the Thread"}, point={x=2809,y=3145,m=1431}, PinAdd="Complete 8legged Menaces, wolf kabobs and Wolves at our heels on the way"},
        {Text="Accept Zombie Juice", At="Zombie Juice", point={x=2809,y=3145,m=1431}},
        
        -- attempting to complete this while in west the first run
        --{Text="Make sure Eight-Legged Menaces and Wolf at our heels are done", 
        --    Mct={"Eight-Legged Menaces", "Wolves at Our Heels"}, point={x=771,y=3319,m=1431}},
        --{Text="Turn inn Wolves at Our Heels", Dt={q="Wolves at Our Heels"}, point={x=771,y=3319,m=1431}},

        {Text="Complete The Night Watch at cemetary", Ct="The Night Watch", point={x=1664,y=4668,m=1431}},
        {Text="Turn inn Return to Jitters", Dt={q="Return to Jitters"}, point={x=1828,y=5652,m=1431}},
        {Text="Turn inn Eight-Legged Menaces", Dt={q="Eight-Legged Menaces"}, point={x=4509,y=6702,m=1431}},
        {Text="Turn Sven's Revenge (mound of dirt between houses at farm)", Dt={q="Sven's Revenge"}, point={x=4985,y=7771,m=1431}},
        {Text="Accept Sven's Camp", At="Sven's Camp", point={x=4985,y=7771,m=1431}},
        
        {Text="Heartstone to Darkshire", Proximity=50, UseItem="Hearthstone", point={x=7389,y=4441,m=1431}},
        --{Text="Deliver Dusky Crab Cakes (if not done already)", Dt={q="Dusky Crab Cakes"}, point={x=7381,y=4325,m=1431}},
        --{Text="Accept Return to Jitters (if delivered dusky crab cakes)", At="Return to Jitters", point={x=7381,y=4325,m=1431}},
        --{Text="Turn inn Seasoned Wolf Kabobs (chef)", Dt={q="Seasoned Wolf Kabobs"}, point={x=7387,y=4348,m=1431}},
        {Text="Turn inn Zombie Juice (tavernkeep smits)", Dt={q="Zombie Juice"}, point={x=7408,y=4470,m=1431}},
        {Text="Accept Gather Rot Blossoms", At="Gather Rot Blossoms", point={x=7408,y=4470,m=1431}},
        

        {Text="Deliver The Legend of Stalvan", Dt={q="The Legend of Stalvan"}, point={x=7408,y=4470,m=1431}},
        {Text="Accept The Legend of Stalvan", At="The Legend of Stalvan", point={x=7408,y=4470,m=1431}},

        {Text="Deliver The Legend of Stalvan (outside inn)", Dt={q="The Legend of Stalvan"}, point={x=7359,y=4690,m=1431}},
        {Text="Accept The Legend of Stalvan", At="The Legend of Stalvan", point={x=7359,y=4690,m=1431}},
        {Text="Turn inn The Night Watch", Dt={q="The Night Watch"}, point={x=7360,y=4692,m=1431}},
        {Text="Accept The Night Watch (pt3)", At="The Night Watch", point={x=7360,y=4692,m=1431}},

        {Text="Deliver The Legend of Stalvan (Townhall)", Dt={q="The Legend of Stalvan"}, point={x=7260,y=4685,m=1431}},
        {Text="Accept The Legend of Stalvan", At="The Legend of Stalvan", point={x=7260,y=4685,m=1431}},

        {Text="Deliver The Legend of Stalvan (outside inn)", Dt={q="The Legend of Stalvan"}, point={x=7359,y=4690,m=1431}},
        {Text="Accept The Legend of Stalvan", At="The Legend of Stalvan", point={x=7359,y=4690,m=1431}},

        {Text="Fly to Stormwind", Taxi="Stormwind, Elwynn",  point={x=7749,y=4429,m=1431}},
        -- DONT add book from svens farm to this, Merrin's Letter?
        {Text="Get wetlands items from bank. Put Duskwood items in. Get cloth if pos",
            PinAdd="Gobbler's Head, Bundle of Crocolisk Skins, Young Crocolisk Skin",
            PutInBank={"Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth", 
            "Vial of Spider Venom", "Skeleton Finger" },
            TakeFromBank={"Gobbler's Head", "Bundle of Crocolisk Skins", "Young Crocolisk Skin", "Flagongut's Fossil", "Stone of Relu"}},

        {Text="Do wool cloth delivery if had 60 wool", point={x=4426,y=7401,m=1453}},
        {Text="Accept A Noble Brew at lock trainer", At="A Noble Brew", point={x=2922,y=7399,m=1453}},
        {Text="Accept The Corruption Abroad (in the park)", At="The Corruption Abroad", point={x=2140,y=5580,m=1453}},
    
        {Text="Train Hunter skills", TrainSkill=1, point={x=6166,y=1519,m=1453}},
        {Text="Train Pet skills", TrainSkill=1, point={x=6156,y=1599,m=1453}},         

        {Text="Fly to Menethil Harbor, Wetlands. Tram into flying if don't need a break", Taxi="Menethil Harbor, Wetlands", point={x=6631,y=6217,m=1453},
            PinAdd="Delete remaining Sharp Arrows while flying, restocking in Menethil"},
    }
}

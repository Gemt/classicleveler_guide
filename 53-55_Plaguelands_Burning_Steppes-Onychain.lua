-- Author      : G3m7
-- Create Date : 5/20/2019 9:23:30 PM


CLGuide_BurningSteppes = {
    Title="53-55 Plaguelands-BS-Ony",
    Pinboard = {},
    Steps = {
        ---------------
        -- Plaguelands
        ---------------
        {Text="Fly to WPL", Taxi="Chillwind Camp, Western Plaguelands", point={x=949,y=5969,m=1437}},
        {Text="Deliver A Call to Arms: The Plaguelands!", Dt={q="A Call to Arms: The Plaguelands!"}, point={x=4270,y=8404,m=1422}},
        {Text="Accept Clear the Way", At="Clear the Way", point={x=4270,y=8404,m=1422}},
        {Text="Deliver Argent Dawn Commission & eq. trinket (if possible, maybe pserver thing)", Dt={q="Argent Dawn Commission", Item="Argent Dawn Commission", Use=1}, point={x=4283,y=8373,m=1422}},
        {Text="Deliver The Everlook Report", Dt={q="The Everlook Report"}, point={x=4297,y=8355,m=1422}},

        {Text="Complete Clear the Way", Ct="Clear the Way", point={x=4850,y=8093,m=1422}},
        {Text="Deliver Clear the Way", Dt={q="Clear the Way"}, point={x=4270,y=8404,m=1422}},
        
        {Text="Accept The Scourge Cauldrons", At="The Scourge Cauldrons", point={x=4270,y=8403,m=1422}},
        {Text="Accept All Along the Watchtowers", At="All Along the Watchtowers", point={x=4270,y=8403,m=1422}},

        {Text="Deliver The Scourge Cauldrons", Dt={q="The Scourge Cauldrons"}, point={x=4297,y=8450,m=1422}},
        {Text="Accept Target: Felstone Field", At="Target: Felstone Field", point={x=4297,y=8450,m=1422}},

        {Text="Mark 1st tower at point (take west (left) road)", UseItem="Beacon Torch", point={x=4005,y=7160,m=1422}},
        {Text="Kill Cauldron Lord Bilemaw, get key and deliver Target: Felstone Field", Dt={q="Target: Felstone Field"}, point={x=3721,y=5689,m=1422}},
        {Text="Accept Return to Chillwind Camp", At="Return to Chillwind Camp", point={x=3721,y=5689,m=1422}},
        {Text="Enter the left house, accept Better Late Than Never (2nd floor)", At="Better Late Than Never", point={x=3840,y=5405,m=1422}},
        {Text="Enter the barn and deliver Better Late Than Never", Dt={q="Better Late Than Never"}, point={x=3872,y=5524,m=1422}},
        {Text="Accept Better Late Than Never (from same object)", At="Better Late Than Never", point={x=3872,y=5524,m=1422}},

        {Text="Mark 2nd tower at point", UseItem="Beacon Torch", point={x=4229,y=6615,m=1422}},

        {Text="Deliver Return to Chillwind Camp (go west road again)", Dt={q="Return to Chillwind Camp"}, point={x=4297,y=8450,m=1422}},
        {Text="Accept Target: Dalson's Tears", At="Target: Dalson's Tears", point={x=4297,y=8450,m=1422}},

        {Text="Mark 3rd tower at point (go west (left) road)", UseItem="Beacon Torch", point={x=4431,y=6329,m=1422}},

        {Text="Kill Cauldron Lord Malvinious, loot key and deliver Target: Dalson's Tears", Dt={q="Target: Dalson's Tears"}, point={x=4616,y=5201,m=1422}},
        {Text="Accept Return to Chillwind Camp", At="Return to Chillwind Camp", point={x=4616,y=5201,m=1422}},
        {Text="Enter the Barn, deliver/accept Mrs. Dalson's Diary", At="Mrs. Dalson's Diary", Dt={q="Mrs. Dalson's Diary"}, point={x=4780,y=5067,m=1422}},
        {Text="Go behind barn, kill a wandering skeleton, get key then click outhouse. Be prepared for 56 spawn", At="Locked Away", Dt={q="Locked Away"}, point={x=4811,y=4965,m=1422}},
        {Text="Kill Farmer Dalson and loot his key", Item={Name="Dalson Cabinet Key", Count=1}},
        {Text="Enter mainhouse, deliver Locked Away at cabinet (2nd floor)", At="Locked Away", Dt={q="Locked Away"}, point={x=4738,y=4961,m=1422}},
        {Text="Mark 4th tower at point (go east side of Andorhal)", UseItem="Beacon Torch", Ct="All Along the Watchtowers", point={x=4664,y=7108,m=1422}},

        {Text="Deliver All Along the Watchtowers", Dt={q="All Along the Watchtowers"}, point={x=4270,y=8403,m=1422}},
        {Text="Deliver Return to Chillwind Camp (skipp followup)", Dt={q="Return to Chillwind Camp"}, point={x=4297,y=8450,m=1422}},
        
        {Text="Hearthstone to Menethil. If on CD just go next step and fly directly to IF", UseItem="Hearthstone", Zone="Wetlands"},
        {Text="Fly to Ironforge", Taxi="Ironforge, Dun Morogh", point={x=949,y=5969,m=1437}},

        ---------------
        -- IF
        ---------------
        {Text="Deliver A Little Slime Goes a Long Way", Dt={q="A Little Slime Goes a Long Way"}, point={x=7575,y=2337,m=1455}},
        {Text="Accept Passing the Burden (library)", At="Passing the Burden", point={x=7754,y=1184,m=1455}},
        {Text="Deliver Passing the Burden (mystic ward)", Dt={q="Passing the Burden"}, point={x=3096,y=484,m=1455}},
        {Text="Accept Arcane Runes", At="Arcane Runes", point={x=3096,y=484,m=1455}},
        {Text="Accept An Easy Pickup", At="An Easy Pickup", point={x=3096,y=484,m=1455}},
        
        {Text="Accept The Smoldering Ruins of Thaurissan (by the king)", At="The Smoldering Ruins of Thaurissan", point={x=3837,y=5531,m=1455}},
        {Text="Do the gossip", CycleGossip="The Smoldering Ruins of Thaurissan", Ct="The Smoldering Ruins of Thaurissan", point={x=3837,y=5532,m=1455}},
        {Text="Deliver The Smoldering Ruins of Thaurissan", Dt={q="The Smoldering Ruins of Thaurissan"}, point={x=3837,y=5532,m=1455}},
        {Text="Accept The Smoldering Ruins of Thaurissan", At="The Smoldering Ruins of Thaurissan", point={x=3837,y=5532,m=1455}},

        {Text="Deliver An Easy Pickup (by BG masters)", Dt={q="An Easy Pickup"}, point={x=7086,y=9458,m=1455}},
        {Text="Accept Signal for Pickup", At="Signal for Pickup", point={x=7086,y=9458,m=1455}},
        {Text="Deliver Signal for Pickup", Dt={q="Signal for Pickup"}, point={x=7086,y=9458}, PinAdd="Make sure you now have the item: \"Standard Issue Flare Gun\""},

        {Text="Accept The Hunter's Charm", At="The Hunter's Charm", Class="Hunter", point={x=7088,y=8360,m=1455}},
        {Text="Train hunter skills", TrainSkill=1, point={x=7088,y=8360,m=1455}},
        {Text="Train pet skills", TrainSkill=1, point={x=7085,y=8582,m=1455}},
        {Text="Restock arrows (2nd floor)", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=2800}, point={x=7176,y=6669,m=1455}},
        {Text="Fly to Lakeshire, Redridge (about 5min)", Taxi="Lakeshire, Redridge", point={x=5579,y=4774,m=1455}},

        ---------------
        -- BS
        ---------------



        
        {Text="Run to Burning Steppes, Morgan's Vigil, Discover FP", Proximity=10, point={x=8434,y=6833,m=1428}},
        {Text="Accept Extinguish the Firegut", At="Extinguish the Firegut", point={x=8455,y=6867,m=1428}},
        {Text="Accept FIFTY! YEP!", At="FIFTY! YEP!", point={x=8455,y=6867,m=1428}},
        {Text="Accept Dragonkin Menace", At="Dragonkin Menace", point={x=8582,y=6894,m=1428}},

        {Text="Run NW to Flame crest, Deliver Yuka Screwspigot (skip followup)", Dt={q="Yuka Screwspigot"}, point={x=6607,y=2195,m=1428}},

        {Text="Accept Broodling Essence. Kill ogre/loot Thaurissan Relic on the way", At="Broodling Essence", point={x=6524,y=2402,m=1428},
            PinAdd="Always kill worgs for FIFTY! YEP! (Blackrock Medalions)"},
        {Text="Accept Tablet of the Seven", At="Tablet of the Seven", point={x=6519,y=2383,m=1428}},

        {Text="Complete Tablet of the Seven (click@point) and Smoldering Ruins of Thaurissan", Mct={"Tablet of the Seven", "The Smoldering Ruins of Thaurissan"}, point={x=5412,y=4076,m=1428}},
        
        {Text="Work towards NE cave and Accept A Taste of Flame. Use Draco-Incarcanatrix on whelps for Broodling Essence", 
            UseItem="Draco-Incarcanatrix 900",
            PinAdd="He may have 2 quests, we already got egg thing so take whichever does not require u to kill a dragon",
            point={x=9500,y=3157,m=1428}},
        {Text="Deliver A Taste of Flame", Dt={q="A Taste of Flame"}, point={x=9500,y=3157,m=1428}},
        {Text="Complete Broodling Essence & Dragonkin Menace",
            Mct={"Dragonkin Menace", "Dragonkin Menace"}},

        {Text="Deliver Broodling Essence", Dt={q="Broodling Essence"}, point={x=6524,y=2400,m=1428}},
        {Text="Accept Felnok Steelspring", At="Felnok Steelspring", point={x=6524,y=2400,m=1428}},
        {Text="Deliver Tablet of the Seven", Dt={q="Tablet of the Seven"}, point={x=6515,y=2390,m=1428}},
        
        {Text="Complete Extinguish the Firegut", Ct="Extinguish the Firegut", point={x=7432,y=3919,m=1428}},
        
        {Text="Deliver Dragonkin Menace", Dt={q="Dragonkin Menace"}, point={x=8582,y=6895,m=1428}},
        {Text="Accept The True Masters", At="The True Masters", point={x=8582,y=6895,m=1428}},

        {Text="Deliver Extinguish the Firegut", Dt={q="Extinguish the Firegut"}, point={x=8456,y=6868,m=1428}},
        {Text="Accept Gor'tesh the Brute Lord", At="Gor'tesh the Brute Lord", point={x=8456,y=6868,m=1428}},
        
        {Text="Complete FIFTY! YEP! and kill Gor'tesh", Mct={"FIFTY! YEP!", "Gor'tesh the Brute Lord"}, point={x=3933,y=5530,m=1428}},

        {Text="Deliver FIFTY! YEP!", Dt={q="FIFTY! YEP!"}, point={x=8456,y=6868,m=1428}},
        {Text="Deliver Gor'tesh the Brute Lord", Dt={q="Gor'tesh the Brute Lord"}, point={x=8456,y=6868,m=1428}},
        {Text="Accept Ogre Head On A Stick = Party", At="Ogre Head On A Stick = Party", point={x=8456,y=6868,m=1428}},
        {Text="Complete Ogre Head on a stick on the top (click dirt or item)", UseItem="Gor'tesh's Lopped Off Head", Ct="Ogre Head On A Stick = Party", point={x=8099,y=4678,m=1428}},
        {Text="Deliver Ogre Head On A Stick = Party", Dt={q="Ogre Head On A Stick = Party", Item="Choking Band", Use=1}, point={x=8456,y=6868,m=1428}},
        
        
        {Text="Fly to Lakeshire", Taxi="Lakeshire, Redridge", point={x=8433,y=6834,m=1428}},
        {Text="Deliver The True Masters", Dt={q="The True Masters"}, point={x=2998,y=4445,m=1433}},
        {Text="Accept The True Masters", At="The True Masters", point={x=2998,y=4445,m=1433}},
        {Text="Fly to SW", Taxi="Stormwind, Elwynn", point={x=3058,y=5941,m=1433}},
        {Text="Deliver Better Late Than Never (entrance at point, 2nd floor)", Dt={q="Better Late Than Never"}, point={x=4776,y=3130,m=1453}},
        {Text="Accept Good Natured Emma", At="Good Natured Emma", point={x=4847,y=3057,m=1453}},
        {Text="Find Emma (/tar ol' ), patrol from building at point (2nd floor) to & around trade district", Dt={q="Good Natured Emma"}, point={x=5312,y=4317,m=1453}},
        {Text="Accept Good Luck Charm", At="Good Luck Charm"},

        --{Text="Set HS in SW", SetHs="Innkeeper Allison", point={x=5263,y=6570,m=1453}},
        {Text="Deliver The True Masters (by the king)", Dt={q="The True Masters"}, point={x=7822,y=1798,m=1453}},
        {Text="Accept The True Masters", At="The True Masters", point={x=7822,y=1798,m=1453}},
        {Text="Complete gossip at Lady Katrana Prestor", CycleGossip="The True Masters", Ct="The True Masters", point={x=7810,y=1774,m=1453}},
        {Text="Deliver The True Masters", Dt={q="The True Masters"}, point={x=7822,y=1798,m=1453}},
        {Text="Accept The True Masters", At="The True Masters", point={x=7822,y=1798,m=1453}},
        {Text="Fly to Lakeshire", Taxi="Lakeshire, Redridge", point={x=6627,y=6213,m=1453}},
        -- todo: can we pick it and keep in qlog?
        {Text="Deliver The True Masters (skip the rest for now)", Dt={q="The True Masters"}, point={x=2998,y=4445,m=1433}},
        {Text="Accept The True Masters", At="The True Masters", point={x=2998,y=4445,m=1433}},
        
        {Text="Fly to Nethergarde Keep, Blasted Lands", Taxi="Nethergarde Keep, Blasted Lands", point={x=3059,y=5941,m=1433}},

        -- lakeshire delivery gives 5450 xp, but the rest are low xp turnins, so we wait with those until doing BRD
        --{Texi="Fly to Burning Steppes", Taxi="Morgan's Vigil, Burning Steppes", point={x=3058,y=5941,m=1433}},
        --{Text="Deliver The True Masters", Dt={q="The True Masters"}, point={x=8474,y=6902,m=1428}},
        --{Text="Accept The True Masters", At="The True Masters", point={x=8474,y=6902,m=1428}},
        --{Text="Complete gossip at Ragged John (by horde FP to the north)", Ct="The True Masters", point={x=6501,y=2375,m=1428}},
        --{Text="Deliver The True Masters", Dt={q="The True Masters"}, point={x=8474,y=6902,m=1428}},
        --{Text="Accept Marshal Windsor", At="Marshal Windsor", point={x=8474,y=6902,m=1428}},
    }
}
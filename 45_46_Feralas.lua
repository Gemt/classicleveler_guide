-- Author      : G3m7
-- Create Date : 5/11/2019 12:40:23 PM



CLGuide_Feralas = {
    Title="43-47 Feralas",
    Pinboard = {},
    Steps = {
        {Text="Run west (south of road) Accept Freedom for All Creatures", At="Freedom for All Creatures", point={x=6594,y=4565,m=1444},
            PinAdd="Pass Camp Mojache on south side. Go North off the road AFTER passing the Grimtotem camp"},

        {Text="Complete the Q by clearing Grimtotems and opening the cage", Ct="Freedom for All Creatures", point={x=6666,y=4674,m=1444}},
        {Text="Deliver Freedom for All Creatures", Dt={q="Freedom for All Creatures"}, point={x=6594,y=4565,m=1444}},
        {Text="Accept Doling Justice", At="Doling Justice", point={x=6595,y=4561,m=1444}},
        {Text="Complete Doling Justice (kill grimtotems, more to the NE)", Ct="Doling Justice", point={x=6666,y=4674,m=1444}},
        {Text="Deliver Doling Justice", Dt={q="Doling Justice"}, point={x=6595,y=4561,m=1444}},
        {Text="Accept Doling Justice", At="Doling Justice", point={x=6595,y=4561,m=1444}},
        
        -- todo: can you safely delete the wand once screecher spirits is completed?
        {Text="Run West towards Feathermoon Strongold (swim unless boat just leaving)", point={x=4445,y=4364,m=1444},
            PinAdd={"Look for Rogue Vale Screechers on the way, kill and use wand on them", "You can delete Bamboo Cage Key"}},

        {Text="Stable pet", Class="Hunter", Proximity=8, point={x=3145,y=4315,m=1444}},
        {Text="Set HS in Feathermoon", SetHs="Innkeeper Shyria", point={x=3097,y=4349,m=1444}},
        






        








        -- Feralas
        -- quest has extra space at the end
        -- todo can we tame anything here for some skill? or are we gonna go for a serpent later
        -- Longtooth Runner (40-41, Feralas) * Bite 6, Dash 2, Furious Howl 3
        -- Feathermoon
        {Text="Buy arrows. If far from 46-47 buy extra stacks for naga grind", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=2800}, point={x=3065,y=4343,m=1444}},
        {Text="Accept The Mark of Quality (outside Inn)", At="The Mark of Quality", point={x=3063,y=4271,m=1444}},
        
        -- this quest has a typo in nostcore, extra space at the end
        {Text="Accept The Ruins of Solarsal", At="The Ruins of Solarsal ", point={x=3027,y=4617},m=1444},
        {Text="Accept The Missing Courier", At="The Missing Courier", point={x=3038,y=4617,m=1444,m=1444}},

        {Text="Accept In Search of Knowledge", At="In Search of Knowledge", point={x=3178,y=4550,m=1444}},
        {Text="Accept The High Wilderness", At="The High Wilderness", point={x=3183,y=4561,m=1444}},
        {Text="Deliver The Missing Courier (2nd floor)", Dt={q="The Missing Courier"}, point={x=3186,y=4513,m=1444}},
        {Text="Accept The Missing Courier", At="The Missing Courier", point={x=3186,y=4513,m=1444}},
        
        
        {Text="Tame a Longtooth Runner (pref lvl 41). Try to learn dash & bite", Class="Hunter", point={x=2970,y=4828,m=1444}},
        {Text="Deliver The Ruins of Solarsal (SW of Feathermoon)", Dt={q="The Ruins of Solarsal "}, point={x=2631,y=5234,m=1444}},
        
        
        
        {Text="Accept Return to Feathermoon Stronghold", At="Return to Feathermoon Stronghold", point={x=2631,y=5234,m=1444}},
        {Text="Deliver Return to Feathermoon Stronghold (back in Feathermoon)", Dt={q="Return to Feathermoon Stronghold"}, point={x=3027,y=4617,m=1444}},
        {Text="Accept Against the Hatecrest", At="Against the Hatecrest", point={x=3027,y=4617,m=1444}},
        {Text="Deliver Against the Hatecrest", Dt={q="Against the Hatecrest"}, point={x=3038,y=4617,m=1444}},
        {Text="Accept Against the Hatecrest", At="Against the Hatecrest", point={x=3038,y=4617,m=1444}},
        {Text="Complete Against the Hatecrest (kill & loot nagas south)", Ct="Against the Hatecrest", point={x=2679,y=5415,m=1444}},
        {Text="Deliver Against the Hatecrest", Dt={q="Against the Hatecrest"}, point={x=3038,y=4617,m=1444}},
        {Text="Accept Against Lord Shalzaru", At="Against Lord Shalzaru", point={x=3038,y=4617,m=1444}},

        {Text="Get old pet back regardless of dash or not", Class="Hunter", point={x=3145,y=4315,m=1444}},

        -- todo: hunters can grind here (not good for other classes), but to what level? Maybe 45 at least to open Zapped Giants/Fuel for Zapping
        -- At the very least until hearthstone is ready.
        -- Edit: 47 seems like a good target if grind is smooth. Will make it easy to go straight into finishing all Tanaris quests
        {Text="Swim to Isle of Dread and kill Lord Shalzaru in cave", Ct="Against Lord Shalzaru", point={x=2594,y=6735,m=1444}},
        {Text="Grind the area until 47, minimum until HS is ready. Can HS on CD to clear bags/buy arrows if needed", Lvl={lvl=47},
            PinAdd="Can HS Feathermoon to vendor/restock, but make sure HS will be ready when grind is done"},

        -- the boath on southern shore
        {Text="When <= 17min CD on HS, Go out of cave and move to point b4 swimming across", Proximity=10, point={x=3819,y=6805,m=1444}},
        {Text="Deliver The Missing Courier (boat wreck in water)", Dt={q="The Missing Courier"}, point={x=4544,y=6497,m=1444},
            PinAdd="Go out of cave, follow shore east, then a tiny bit south before swimming to mainland"},
        {Text="Accept Boat Wreckage", At="Boat Wreckage", point={x=4544,y=6497,m=1444}},

        
        
        --{Text="Hearthstone to Feathermoon", UseItem="Hearthstone", Proximity=40, point={x=3098,y=4342,m=1444}},

        
        
        {Text="Deathwarp to Feathermoon (at point or further west, or end up at DM)", DeathWarp=1, point={x=4352,y=6341,m=1444}},
        {Text="Deliver Against Lord Shalzaru", Dt={q="Against Lord Shalzaru"}, point={x=3038,y=4617,m=1444}},
        {Text="Accept Delivering the Relic", At="Delivering the Relic", point={x=3038,y=4617,m=1444}},
        {Text="Deliver Delivering the Relic", Dt={q="Delivering the Relic", Item="Dawnrider's Chestpiece", Use=1}, point={x=3007,y=4506,m=1444}},
        
        
        
        -- Darnassus
        -- todo: not sure if this is a nelf thing only (for mount+quests), or everyone wants to do it (quests)
        -- try to be 44 before arriving for skills?
        {Text="Fly to Darnassus (8min40s)", Taxi="Rut'theran Village, Teldrassil", point={x=3024,y=4325,m=1444}},
        {Text="Deliver Handle With Care (in Ruth'theran)", Dt={q="Handle With Care"}, point={x=5550,y=9204,m=1438}},
        {Text="Accept Favored of Elune?", At="Favored of Elune?", point={x=5550,y=9204,m=1438}},
        {Text="Deliver In Search of Knowledge (2nd Floor)", Dt={q="In Search of Knowledge"}, point={x=5541,y=9223,m=1438}},
        {Text="Accept Feralas: A History (book, floor, 2nd floor in the back)", At="Feralas: A History", point={x=5522,y=9144,m=1438}},
        {Text="Deliver Feralas: A History", Dt={q="Feralas: A History"}, point={x=5541,y=9223,m=1438}},
        {Text="Accept The Borrower", At="The Borrower", point={x=5541,y=9223,m=1438}},

        {Text="Run into Darnassus", Zone="Darnassus", point={x=5592,y=8972,m=1438}},
        {Text="Get cloth for darnassus donations from bank, if any", point={x=4037,y=4251,m=1457}},
        -- todo: double check i actually have better bracers at this point
        {Text="Deliver Doling Justice (Temple, 2nd floor)", Dt={q="Doling Justice", Item="Nightscale Girdle"}, point={x=3910,y=8160,m=1457}},
        {Text="Check Bow vendor for 44 bow if dont have 42 bow or can afford somehow", point={x=6334,y=6639,m=1457,m=1457}},
        {Text="Look for 1 Elixir of Fortitude on AH (2 if dont have from dustwallow)", point={x=5647,y=5364,m=1457}},
        {Text="Complete any cloth donations you can", point={x=6402,y=2302,m=1457}},
        
        {Text="Accept Assisting Arch Druid Staghelm", At="Assisting Arch Druid Staghelm", point={x=6743,y=1564,m=1457}},

        -- Nelfs:
        -- Don't dare to make this auto-training, too much gold at stake
        {Text="Train Tiger Riding if nelf", point={x=3871,y=1585,m=1457}},
        {Text="Buy mount if Nelf", BuyItem={Npc="Lelanai", Item="Reins of the Striped Nightsaber", Count=1}, point={x=3826,y=1538,m=1457}},
        
        -- training before resetting talents, in case dont have enough gold for both
        -- todo pet respec for frost resist (feralas), nature resist (tanaris, un'goro), and?
        {Text="Train Hunter Skills", TrainSkill=1},
        {Text="Reset Talents back to regular spec", ResetTalents=1},
        {Text="Train Pet skills up the ramp", TrainSkill=1},
        {Text="Reset Pet talents", PinAdd="Balanced armor+resist. Lvl 40 NR/SR?"},

        {Text="Deliver Assisting Arch druid Staghelm (top of the tower). Skip Un'goro soil quest", Dt={q="Assisting Arch Druid Staghelm"}, point={x=3480,y=927,m=1457}},
        {Text="(this may not be avail) Accept Morrowgrain Research (top of the tower)", At="Morrowgrain Research", point={x=3480,y=927,m=1457}},
        {Text=">Deliver Morrowgrain Research at the middle level of tower", Dt={q="Morrowgrain Research"}, point={x=3538,y=839,m=1457}},
        {Text=">Accept Morrowgrain Research", At="Morrowgrain Research", point={x=3538,y=839,m=1457},
            PinAdd="Morrowgrain Research is different on LH. Gotta figure out if anything special needs doing yourself"},
        
        {Text="Hearthstone to Feathermoon. Check innkeeper for Jonespyre's request", UseItem="Hearthstone", Zone="Feralas"},
        
        {Text="Buy arrows", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=2800}, point={x=3065,y=4343,m=1444}},
        {Text="Repair", Proximity=5, point={x=3079,y=4316,m=1444}},

        {Text="Deliver Boat Wreckage (2nd floor)", Dt={q="Boat Wreckage"}, point={x=3186,y=4512,m=1444}},
        {Text="Accept The Knife Revealed", At="The Knife Revealed", point={x=3186,y=4512,m=1444}},
        
        {Text="Skip if got Morrowgrain Q in Darna (may not be avail): Buy Evergreen Pouch (top of the tower)", BuyItem={Item="Evergreen Pouch", Count=1}, point={x=3245,y=4379,m=1444}, 
            PinAdd="Dnu if need questdelivery to buy, so if cannot buy, deliver quest then try again"},
        {Text="Skip if got Morrowgrain Q in Darna (may not be avail): Buy 20x Packet of Tharlendris Seeds", BuyItem={Item="Packet of Tharlendris Seeds", Count=20}, point={x=3245,y=4379,m=1444}},

        {Text="Deliver The Knife Revealed (top of the tower)", Dt={q="The Knife Revealed"}, point={x=3245,y=4378,m=1444}},
        {Text="Accept Psychometric Reading (may need to wait for RP)", At="Psychometric Reading", point={x=3245,y=4378,m=1444}},
        {Text="Deliver Psychometric Reading (back to prev. guy 2nd floor)", Dt={q="Psychometric Reading"}, point={x=3186,y=4512,m=1444}},
        {Text="Accept The Woodpaw Gnolls", At="The Woodpaw Gnolls", point={x=3186,y=4512,m=1444}},


        {Text="Accept Zapped Giants. Swim unless you can see boat coming", At="Zapped Giants", point={x=4481,y=4342,m=1444}},
        {Text="Accept Fuel for the Zapping", At="Fuel for the Zapping", point={x=4481,y=4342,m=1444}},
        {Text="Complete Zapping quests and Screecher Spirits along the shore", Mct={"Zapped Giants", "Fuel for the Zapping", "Screecher Spirits"}, UseItem="Zorbin's Ultra-Shrinker"},

        {Text="Deliver Zapped Giants", Dt={q="Zapped Giants"}, point={x=4481,y=4342,m=1444}},
        {Text="Deliver Fuel for the Zapping", Dt={q="Fuel for the Zapping"}, point={x=4481,y=4342,m=1444},
            PinAdd="Can delete Zorbin's Ultra-shrinker after completing zapper quests"},

        {Text="Complete The Mark of Quality. Deliver OOX-22/FE if found", Ct="The Mark of Quality", UseItem="OOX-22/FE Distress Beacon", point={x=5560,y=5622,m=1444},
            PinAdd={"Follow road SE for a bit. Drops from Yetis", "Keep looking for Screechers for Sceecher Spirits"}},

        {Text="Find a Hippogryph Egg on the ground far south, up the hillsides", Item={Name="Hippogryph Egg", Count=1}, point={x=5827,y=7633,m=1444}, --, point={x=5592,y=7604,m=1444}
            PinAdd="Want one of these for Tanaris. Foun in the hills all the way south"},

        {Text="Complete The High Wilderness (ogres). Brutes/locks north, locks/Shamns south", Ct="The High Wilderness", point={x=5952,y=6295}},
        {Text="Accept An Orphan Looking For a Home (skip this if don't have 2 elixir of fortitude)", At="An Orphan Looking For a Home", point={x=6595,y=4565,m=1444}},

        {Text="Deliver The Woodpaw Gnolls (backpack on tree)", Dt={q="The Woodpaw Gnolls"}, PinAdd="Go north to road, then east", point={x=7332,y=5632,m=1444}},
        {Text="Accept The Writhing Deep", At="The Writhing Deep", point={x=7332,y=5632,m=1444}},
        {Text="Go South, enter cave at point (the western of the 2, bottom SW entrance)", Proximity=10, point={x=7311,y=6390,m=1444}},
        {Text="Take a right and deliver The Writhing Deep", Dt={q="The Writhing Deep"}, point={x=7208,y=6376,m=1444}},
        {Text="Accept Freed from the Hive", At="Freed from the Hive", point={x=7208,y=6376,m=1444}},
        {Text="Complete Freed from the Hive (just wait around until it completes, you can not run further than point before completion)", Ct="Freed from the Hive", point={x=7316,y=6387,m=1444}},

        {Text="Accept quest from Parcel you got earlier. Hs if ready; else: Run to Thalanaar", UseItem="Undelivered Parcel", 
            Dt={q="Thalanaar Delivery"}, point={x=8963,y=4656,m=1444}},
        {Text="If at Thalanar: fly Feathermoon. Else skip step", UseItem="Hearthstone", Taxi="Feathermoon, Feralas", point={x=8950,y=4585,m=1444}},
        
        
        {Text="Buy arrows", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=3000}, point={x=3065,y=4343,m=1444}},
        {Text="Deliver The Mark of Quality", Dt={q="The Mark of Quality", Item="Pratt's Handcrafted Boots", Use=1}, point={x=3063,y=4270,m=1444}},
        
        
        {Text="Accept The Sunken Temple (req lvl 46)", At="The Sunken Temple", point={x=3183,y=4561,m=1444}},
        
        {Text="Deliver The High Wilderness", Dt={q="The High Wilderness"}, point={x=3183,y=4561,m=1444}},
        {Text="Deliver Freed from the Hive (2nd floor)", Dt={q="Freed from the Hive"}, point={x=3186,y=4513,m=1444}},
        {Text="Accept A Hero's Welcome", At="A Hero's Welcome", point={x=3186,y=4513,m=1444}},
        {Text="Deliver A Hero's Welcome (Skip rise of the silithid)", Dt={q="A Hero's Welcome", Item="Sanctimonial Rod", Use=1}, point={x=3028,y=4617,m=1444}},
        --{Text="Accept Rise of the Silithid", At="Rise of the Silithid", point={x=3028,y=4617,m=1444}},
        
        
        {Text="If HSed to feathermoon, fly Thalanaar, deliver quest, then fly Gadgetzan", point={x=3024,y=4325,m=1444}},
        {Text="If already been to Thalanaar, fly directly to Tanaris", point={x=3024,y=4325,m=1444}},
    }
}
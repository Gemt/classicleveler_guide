-- Author      : G3m7
-- Create Date : 5/5/2019 9:24:30 AM
-- {Text="Suecide and ress at GY", DeathWarp=1},
CLGuide_Teldrassil = {
	Title="1-11 Teldrassil",
	Pinboard = {},
	Steps = {
        --[[
            second sathrath loc wrong
            deathwarp with unstuck button button
            add starter food to vendorlist? dosent seem needed
            delete hearthstone immediately for more vendorspace. Get it back at dolanaar?

            finish tumors before second darna to get followup
            hit 10 without turning in apples etc if possible
            deliver them with second beast taming
            get oakenshield whatever
            do him with owl trainingmultiple owls behind him
            deathwarp at furbolgs after second darna trip to dolanaar
            deliver road to darnassus on way to darna for last time instead
        ]]
		{Text="Accept The Balance of Nature", At="The Balance of Nature", point={x=5869,y=4427,m=1438}},
		{Text="Complete {The Balance of Nature}", Ct="The Balance of Nature"},
		{Text="Deliver The Balance of Nature", Dt={q="The Balance of Nature", Item="Archery Training Gloves", Use=1}, point={x=5869,y=4427,m=1438}},
		{Text="Accept The Balance of Nature pt2", At="The Balance of Nature", point={x=5869,y=4427,m=1438}},
		
		{Text="Accept The Woodland Protector", At="The Woodland Protector", point={x=5993,y=4248,m=1438}},
        {Text="Vendor, buy 1 stack of arrows", BuyItem={Npc="Keina", Item="Rough Arrow", Count=200}, Class="Hunter", point={x=5932,y=4110,m=1438}},
		{Text="Accept A Good Friend", At="A Good Friend", point={x=6088,y=4196,m=1438}},
			
		{Text="Complete The Balance of Nature p2", Ct="The Balance of Nature", point={x=6184,y=3555,m=1438}},
		{Text="Deliver A Good Friend", Dt={q="A Good Friend"}, point={x=5458,y=3298,m=1438}},
		{Text="Accept A Friend in Need", At="A Friend in Need", point={x=5458,y=3298,m=1438},
            PinAdd="Start by delivering Balance of nature if you have less than 2s85c. Unequip bow too if can afford bow"},
		
        {Text="Accept Webwood Venom", At="Webwood Venom", point={x=5781,y=4164,m=1438}},
		{Text="Deliver The Balance of Nature pt2 (need 1 bagspace)", Dt={q="The Balance of Nature", Item="Blackened Leather Belt", Use=1}, point={x=5869,y=4427,m=1438}},

		{Text="Vendor, buy lvl 3 bow (2s 85c) & 1 stack of arrows(manual if > 2s95s)", BuyItem={Npc="Keina", Item="Hornwood Recurve Bow", Count=1}, Class="Hunter", point={x=5932,y=4110,m=1438}},
        
		{Text="Deliver A Friend in Need", Dt={q="A Friend in Need"}, point={x=6088,y=4195,m=1438}},
		{Text="Accept Iverron's Antidote", At="Iverron's Antidote", point={x=6088,y=4195,m=1438}},

		{Text="Deliver The Woodland Protector", Dt={q="The Woodland Protector"}, point={x=5774,y=4513,m=1438}},
		{Text="Accept The Woodland Protector pt2", At="The Woodland Protector", point={x=5774,y=4513,m=1438}},
			
		{Text="Get 8 Fel Moss for The Woodland Protector pt2", Ct="The Woodland Protector", point={x=5469,y=4398,m=1438}},
		{Text="Make sure you also get 7 Hyacinth Mushroom", Item={Name="Hyacinth Mushroom", Count=7}, point={x=5469,y=4398,m=1438}},
		{Text="Get 4 Moonpetal Lily", Item={Name="Moonpetal Lily", Count=4}, point={x=5772,y=3710,m=1438}},
		{Text="Complete Webwood Venom", Ct="Webwood Venom", point={x=5649,y=3031,m=1438}},
		{Text="Make sure you got 1 Webwood Ichor to complete Iverron's Antidode", Ct="Iverron's Antidote", point={x=5649,y=3031,m=1438}},
		{Text="Hearthstone, deathwarp or farm back. Need 2 bagspace", UseItem="Hearthstone", DeathWarp=1, Proximity=50, point={x=5858,y=4436,m=1438}},

        {Text="Accept Etched Sigil", At="Etched Sigil", point={x=5869,y=4427,m=1438}},
		{Text="Turn in The Woodland Protector pt2", Dt={q="The Woodland Protector", Item="Canopy Leggings", Use=1}, point={x=5774,y=4513,m=1438}},

        {Text="Vendor, buy 1 stack of arrows", BuyItem={Npc="Keina", Item="Rough Arrow", Count=200}, Class="Hunter", point={x=5932,y=4110,m=1438},
            PinAdd="Buy lvl 3 Bow if not done"},

		{Text="Turn inn Iverron's Antidode", Dt={q="Iverron's Antidote"}, point={x=6085,y=4200,m=1438}},
		{Text="Accept Iverron's Antidote pt2", At="Iverron's Antidote", point={x=6085,y=4200,m=1438}},
        

		{Text="Turn in Webwood Venom", Dt={q="Webwood Venom", Item="Thistlewood Dagger", Use=1}, point={x=5781,y=4164,m=1438}},
		{Text="Accept Webwood Egg", At="Webwood Egg", point={x=5781,y=4164,m=1438}},
		{Text="Turn inn Iverron's Antidote", Dt={q="Iverron's Antidote", Item="Barkmail Vest"}, point={x=5458,y=3298,m=1438}},
		{Text="Complete (loot a) Webwood Egg", Ct="Webwood Egg", point={x=5669,y=2645,m=1438}},
        {Text="Deathwarp if possible, otherwise hearth, then vendor", DeathWarp=1, UseItem="Hearthstone"},
        
        {Text="Vendor, buy 1 stack of arrows", BuyItem={Npc="Keina", Item="Rough Arrow", Count=200}, Class="Hunter", point={x=5932,y=4110,m=1438}},

		{Text="Turn inn Webwood Egg", Dt={q="Webwood Egg", Item="Woodland Tunic", Use=1}, point={x=5780,y=4165,m=1438}},
		{Text="Accept Tenaron's Summons", At="Tenaron's Summons", point={x=5780,y=4165,m=1438}},
			
		{Text="Turn inn Etched Sigil (hunter trainer, halfway up tower)", Dt={q="Etched Sigil"}, point={x=5866,y=4045,m=1438}},
        
        {Text="Train Hunter skills", TrainSkill=1, point={x=5866,y=4045,m=1438}},

		{Text="Turn inn Tenaron's Summons (top of tower)", Dt={q="Tenaron's Summons"}, point={x=5906,y=3943,m=1438}},
		{Text="Accept Crown of the Earth", At="Crown of the Earth", point={x=5906,y=3943,m=1438}},
		{Text="Complete Crown of the Earth (fill vial in moonwell)", Ct="Crown of the Earth", UseItem="Crystal Phial", point={x=5994,y=3303,m=1438}},
		{Text="Turn inn Crown of the Earth (top of tower)", Dt={q="Crown of the Earth"}, point={x=5906,y=3943,m=1438}},
		{Text="Accept Crown of the Earth p2", At="Crown of the Earth", point={x=5906,y=3943,m=1438}},
		{Text="Accept Dolanaar Delivery", At="Dolanaar Delivery", point={x=6115,y=4765,m=1438}},
        
        -- exit of starting area
			
        {Text="Accept Zenn's Bidding", At="Zenn's Bidding", point={x=6044,y=5615,m=1438}},
        {Text="Accept Denalan's Earth", At="Denalan's Earth", point={x=5608,y=5772,m=1438},
            PinAdd="Look for patrol at road to darna for Q"},
        {Text="Accept A Troubling Breeze", At="A Troubling Breeze", point={x=5595,y=5728,m=1438}},
        {Text="Accept Twisted Hatred (top of tower)", At="Twisted Hatred", point={x=5557,y=5695,m=1438},
            PinAdd="Buy 1 Small Brown Pouch if can afford (5s)"},
        {Text="Accept The Emerald Dreamcatcher", At="The Emerald Dreamcatcher", point={x=5557,y=5695,m=1438}},

        {Text="=== RESTOCK Arrow ===", BuyItem={Npc="Jeena Featherbow", Item="Rough Arrow", Count=400},Class="Hunter",  point={x=5589,y=5921,m=1438}},
        {Text="Deliver Dolanaar Delivery", Dt={q="Dolanaar Delivery", Item="Darnassian Bleu"}, point={x=5562,y=5979,m=1438}},
        {Text="Set HS in Dolanaar", SetHs="Innkeeper Keldamyr"},
        {Text="Train lvl 6 skills", TrainSkill=1},
        {Text="Deliver Crown of the Earth", Dt={q="Crown of the Earth"}, point={x=5614,y=6171,m=1438}},
        {Text="Accept Crown of the Earth", At="Crown of the Earth", point={x=5614,y=6171,m=1438}},
        {Text="Kill spiders/owls/panthers towards moonwell", Proximity=20, point={x=6337,y=5812,m=1438}},
        {Text="Fill the vial", UseItem="Jade Phial", Ct="Crown of the Earth", point={x=6337,y=5812,m=1438}},
        
        {Text="Loot cabinet for emerald dreamcatcher, kill more beasts on way", Ct="The Emerald Dreamcatcher", point={x=6802,y=5966,m=1438}},
        {Text="Deliver A Troubling Breeze (second floor house, jum and run after)", Dt={q="A Troubling Breeze"}, point={x=6626,y=5852,m=1438}},
        {Text="Accept Gnarlpine Corruption", At="Gnarlpine Corruption", point={x=6626,y=5852,m=1438}},

        {Text="Deliver Denalan's Earth", Dt={q="Denalan's Earth"}, point={x=6090,y=6849,m=1438}},
        {Text="Accept Timberling Sprouts", At="Timberling Sprouts", point={x=6090,y=6849,m=1438}},
        {Text="Accept Timberling Seeds", At="Timberling Seeds", point={x=6090,y=6849,m=1438}},
        {Text="Complete Timberling Seeds/sprouts. Can also snipe remaining beasts", Mct={"Timberling Seeds", "Timberling Sprouts"}, point={x=5980,y=6963,m=1438}},
        {Text="Deliver Timberling Seeds", Dt={q="Timberling Seeds"}, point={x=6090,y=6849,m=1438}},
        {Text="Accept Rellian Greenspyre", At="Rellian Greenspyre", point={x=6090,y=6849,m=1438}},
        {Text="Deliver Timberling Sprouts", Dt={q="Timberling Sprouts", Item="Graystone Bracers"}, point={x=6090,y=6849,m=1438}},
        {Text="Complete Zenn's Bidding", Ct="Zenn's Bidding", point={x=6044,y=5614,m=1438}},
        {Text="Deliver Zenn's Bidding", Dt={q="Zenn's Bidding", CustomBar="Severed Voodoo Claw"}, point={x=6044,y=5614,m=1438}},

        {Text="Complete Twisted Hatred in cave (/tar lord melen)", Ct="Twisted Hatred", point={x=5465,y=5258,m=1438}},
        {Text="Deathwarp/run to dolanaar", DeathWarp=1},

        
        {Text="Deliver Crown of the Earth", Dt={q="Crown of the Earth"}, point={x=5614,y=6171,m=1438}},
        {Text="Accept Crown of the Earth", At="Crown of the Earth", point={x=5614,y=6171,m=1438},
            PinAdd="Do cooking thing if already have 7 small spider legs"},
        
        {Text="Accept Seek Redemption!", At="Seek Redemption!", point={x=5600,y=5953,m=1438}},
        {Text="=== RESTOCK Arrow ===", BuyItem={Npc="Jeena Featherbow", Item="Rough Arrow", Count=400}, Class="Hunter", point={x=5589,y=5921,m=1438},
            PinAdd="Train skills if lvl 8"},
        
        {Text="Deliver Gnarlpine Corruption (skip followup)", Dt={q="Gnarlpine Corruption"}, point={x=5596,y=5727,m=1438}},
        
        {Text="Deliver Twisted Hatred (top of tower). Can deathwarp", Dt={q="Twisted Hatred", Item="Feral Bracers", Use=1}, point={x=5557,y=5695,m=1438},
            PinAdd="Look for patrol at road to darna for Q"},
        {Text="Deliver The Emerald Dreamcatcher", Dt={q="The Emerald Dreamcatcher"}, point={x=5557,y=5695,m=1438}},
        {Text="Accept Ferocitas the Dream Eater", At="Ferocitas the Dream Eater", point={x=5557,y=5695,m=1438}},
        
        {Text="Complete Seek Redemption! (fel cones) on the way to ferocitas (NE)", Ct="Seek Redemption!", 
            PinAdd="Make sure to find 7 Small Spider Legs if not yet done", point={x=6850,y=5104,m=1438}},

        {Text="Complete Ferocitas the Dream Eater", Ct="Ferocitas the Dream Eater", point={x=6936,y=5337,m=1438},
            PinAdd="Can deathwarp to Dolanaar when done unless Fel Cones not completed"},

        {Text="Deliver Seek Redemption, or deathwarp to Dolanaar if already done", DeathWarp=1, Dt={q="Seek Redemption!"}, point={x=6044,y=5615,m=1438},
            IconRemove="Fel Cone"},


        {Text="Train lvl 8 skills & Deliver cooking quest if not yet done", TrainSkill=1},
        {Text="=== RESTOCK Arrow ===", BuyItem={Npc="Jeena Featherbow", Item="Rough Arrow", Count=600}, Class="Hunter", point={x=5589,y=5921,m=1438}},

        {Text="Deliver Ferocitas the Dream Eater (top of tower)", Dt={q="Ferocitas the Dream Eater"}, point={x=5557,y=5695,m=1438},
            PinAdd={"Buy 1 bag now if not done already","Look for patrol at road to darna for Q"}},

        
        {Text="Run to the moonwell", Proximity=30, point={x=4239,y=6709,m=1438}},
        {Text="Fill the vial", Ct="Crown of the Earth", UseItem="Tourmaline Phial", point={x=4239,y=6709,m=1438}},
        {Text="Accept The Glowing Fruit (SW)", At="The Glowing Fruit", point={x=4257,y=7611,m=1438}},
        {Text="Make sure you got 7 Small Spider Legs, then Deathwarp to Dolanaar", DeathWarp=1},



        {Text="Deliver Crown of the Earth", Dt={q="Crown of the Earth"}, point={x=5614,y=6171,m=1438}},
        {Text="Accept Crown of the Earth", At="Crown of the Earth", point={x=5614,y=6171,m=1438}},
        {Text="If you got the spider legs, train cooking and accept/deliver cooking quest", Proximity=8, point={x=5712,y=6130,m=1438}},
        {Text="Complete The Road To Darnassus", Ct="The Road to Darnassus", point={x=4736,y=5315,m=1438},
            PinAdd="If you still dont have the quest, get it on the way there"},

        
        
        
        {Text="Grind to Darnassus", Zone="Darnassus", point={x=8375,y=3646,m=1457}},
        {Text="Turn inn Relian Greenspyre", Dt={q="Rellian Greenspyre"}, point={x=3818,y=2164,m=1457}},
        {Text="Accept Tumors", At="Tumors", point={x=3818,y=2164,m=1457}},
        --{Text="Put small egg in bank if you have any", PutInBank={"Small Egg"}, point={x=3993,y=4186,m=1457}},
        {Text="Accept The Temple of the Moon", At="The Temple of the Moon", point={x=2893,y=4582,m=1457}},
        {Text="Deliver The Temple of the Moon", Dt={q="The Temple of the Moon"}, point={x=3664,y=8596,m=1457}},
        {Text="Accept Tears of the Moon", At="Tears of the Moon", point={x=3664,y=8596,m=1457}},

        {Text="Work on Tumors & find Satrath", Mct={"Tumors", "Tears of the Moon"}, point={x=4805,y=2504,m=1438}},
        {Text="Complete Crown of the Earth (fill vial in moonwell)", Ct="Crown of the Earth", UseItem="Amethyst Phial", point={x=3843,y=3398,m=1438}},
        {Text="Accept The Enchanted Glade", At="The Enchanted Glade", point={x=3832,y=3436,m=1438}},
        {Text="Accept Mist once you are nearly done with the enchanted glade (9min timer)", At="Mist", point={x=3154,y=3162,m=1438}},
        {Text="Complete The Enchanted Glade", Ct="The Enchanted Glade", point={x=3463,y=3520,m=1438}},
        {Text="Deliver The Enchanted Glade", Dt={q="The Enchanted Glade", Item="Rain-spotted Cape", Use=1}, point={x=3832,y=3436,m=1438}},
        {Text="Accept Teldrassil", At="Teldrassil", point={x=3832,y=3436,m=1438}},
        {Text="Deliver Mist", Dt={q="Mist", Item="Crag Buckler"}, point={x=3832,y=3436,m=1438}},
        {Text="Grind on harpies or something until 2850/6500 xp lvl 9", Level={lvl=9,xp=2850}},
        {Text="Accept The Shimmering Frond", At="The Shimmering Frond", point={x=3459,y=2884,m=1438}},
        
        {Text="Deathwarp to Darnassus", DeathWarp=1},
        
        {Text="Deliver Tumors if complete", Dt={q="Tumors", SkipIfUncomplete=1, Item="Pruning Knife", Use=1}, point={x=3820,y=2164,m=1457}},
        {Text="Accept Return to Denalan (if tumors done)", At="Return to Denalan", point={x=3820,y=2164,m=1457}},

        {Text="Deliver Tears of the Moon", Dt={q="Tears of the Moon"}, point={x=3666,y=8591,m=1457}},
        {Text="Accept Sathrah's Sacrifice", At="Sathrah's Sacrifice"},
        {Text="Jump down to moonwell, click Sathrath's Sacrifice", UseItem="Sathrah's Sacrifice", Ct="Sathrah's Sacrifice"},
        {Text="Deliver Sathrah's Sacrifice", Dt={q="Sathrah's Sacrifice", Item="Cushioned Boots", Use=1}, point={x=3666,y=8591,m=1457}},


        -- hunter beast taming
        {Text="Hearthstone to Dolanaar (deliver crown of the earth if not 10 yet)", UseItem="Hearthstone", Proximity=15, point={x=5567,y=5977,m=1438}},
		
        
        {Text="Train Pet skills", TrainSkill=1, point={x=5678,y=5978,m=1438}},
        {Text="Accept Taming the Beast", At="Taming the Beast", point={x=5668,y=5949,m=1438}},
        {Text="Train Hunter Skills", TrainSkill=1, point={x=5668,y=5949,m=1438}},
        {Text="Tame a spider directly east or west", UseItem="Taming Rod", Ct="Taming the Beast"},
        {Text="Deliver Taming the Beast", Dt={q="Taming the Beast"}, point={x=5668,y=5949,m=1438}},
        {Text="Accept Taming the Beast", At="Taming the Beast", point={x=5668,y=5949,m=1438}},

        {Text="Deliver Crown of the Earth.", Dt={q="Crown of the Earth"}, UseItem="Cushioned Boots", point={x=5614,y=6171,m=1438}},
        {Text="Accept Crown of the Earth", At="Crown of the Earth", point={x=5614,y=6171,m=1438}},

        {Text="Deliver Return to Denalan", Dt={q="Return to Denalan"}, UseItem="Moss-twined Heart", PinAdd="Deliver Moss-twined heart too if you found the rare. Remember the followup that gives pet", point={x=6090,y=6848,m=1438}},
        {Text="Accept Oakenscowl", At="Oakenscowl", point={x=6090,y=6848,m=1438}},
        {Text="Deliver The Shimmering Frond", Dt={q="The Shimmering Frond"}, point={x=6090,y=6848,m=1438}},
        {Text="Deliver The Glowing Fruit", Dt={q="The Glowing Fruit"}, point={x=6090,y=6848,m=1438}},
        {Text="Click a plant and complete The Sprouted Fronds", Dt={q="The Sprouted Fronds"}, point={x=6080,y=6859,m=1438}},
        {Text="Tame a Nightsaber Stalker", UseItem="Taming Rod", Ct="Taming the Beast", point={x=6420,y=6877,m=1438}},
        {Text="Deliver Taming the Beast (enough gold to deathwarp?)", Dt={q="Taming the Beast"}, point={x=5668,y=5949,m=1438}},
        {Text="Accept Taming the Beast", At="Taming the Beast", point={x=5668,y=5949,m=1438}},
        {Text="Tame a Strigid Screecher to the south, across lake", UseItem="Taming Rod", Ct="Taming the Beast", point={x=5350,y=7629,m=1438}},
        {Text="Kite-kill Oakenscowl back to Denalan (east on lake)", Ct="Oakenscowl", point={x=5308,y=7451,m=1438}},
        {Text="Deliver Oakenscowl", Dt={q="Oakenscowl", Item="Moss-covered Gauntlets"}, point={x=6090,y=6848,m=1438}},
        {Text="Deliver Taming the Beast", Dt={q="Taming the Beast"}, point={x=5668,y=5949,m=1438}},
        {Text="Accept Training the Beast", At="Training the Beast", point={x=5668,y=5949,m=1438}},

        {Text="Buy Laminated Recurve Bow", BuyItem={Npc="Jeena Featherbow", Item="Laminated Recurve Bow", Count=1},Class="Hunter",  point={x=5589,y=5921,m=1438}},
        {Text="Buy Medium Quiver if already got 2+ bags", point={x=5589,y=5921,m=1438}},

        {Text="Run west on road, tame a Strigid Hunter (lvl 8-9). Make sure to learn claw", point={x=3922,y=4579,m=1438},PinAdd="Make sure claw is on, and learned"},
        {Text="Complete Tumors if not yet completed", Ct="Tumors", Dt={q="Tumors", SkipIfNotHaveQuest=1}, point={x=4207,y=4205,m=1438}},
        
		{Text="Run to Darnassus, Accept Nessa Shadowsong", At="Nessa Shadowsong", point={x=7055,y=4535,m=1457},PinAdd="Abandon the owl after learning claw"},

        {Text="Set HS in Darnassus", SetHs="Innkeeper Saelienne", point={x=6744,y=1568,m=1457}},
        {Text="Deliver Training the Beast at hunter trainer", Dt={q="Training the Beast"}, point={x=4039,y=854,m=1457}},
        
        {Text="Deliver Crown of the Earth", Dt={q="Crown of the Earth", Item="Thicket Hammer"}, point={x=3481,y=928,m=1457}},
        {Text="Deliver Teldrassil", Dt={q="Teldrassil"}, point={x=3481,y=928,m=1457}},
        {Text="Accept Grove of the Ancients", At="Grove of the Ancients", point={x=3481,y=928,m=1457}},
        {Text="Deliver Tumors (if not already done, skip followup)", Dt={q="Tumors", SkipIfNotHaveQuest=1, Item="Pruning Knife", Use=1}, point={x=3820,y=2164,m=1457}},
        
        {Text="(if not done) Buy Laminated Recurve Bow", BuyItem={Npc="Ariyell Skyshadow", Item="Laminated Recurve Bow", Count=1},Class="Hunter",  point={x=5880,y=4454,m=1457}},
        {Text="(if not done) Buy Medium Quiver", BuyItem={Npc="Ariyell Skyshadow", Item="Medium Quiver", Count=1}, Class="Hunter", point={x=5880,y=4454,m=1457}},

        {Text="Get small eggs from bank if you put any there", Proximity=20, point={x=3993,y=4186,m=1457}},
		{Text="Run out to Ruth'Theran village", Zone="Teldrassil", point={x=3018,y=4135,m=1457}},
		{Text="Turn inn Nessa Shadowsong", Dt={q="Nessa Shadowsong"}, point={x=5626,y=9240,m=1438}},
		{Text="Accept The Bounty of Teldrassil", At="The Bounty of Teldrassil", point={x=5626,y=9240,m=1438}},
		{Text="Turn inn The Bounty of Teldrassil", Dt={q="The Bounty of Teldrassil"}, point={x=5841,y=9403,m=1438}},
		{Text="Accept Flight to Auberdine", At="Flight to Auberdine", point={x=5626,y=9240,m=1438}},
		{Text="Fly to Auberdine (not autoflying to be sure u got quest)", point={x=5841,y=9403,m=1438},
            PinAdd="Delete all old arrows while flying"},

        -- darna deathwarp #1 feels unnecessary unless gold becomes NP closer to 10+
        -- run out on first trip and do gnarlspines (gotta make sure to have it then),
        -- followed by spider and Tumors and NW quests and apple,
        -- should hit 10 without delivering all this so HS to dolanaar from NW, turnins, then 
        -- run darna after hunter Q to turnin spider/Tumors. 
        -- can consider running dolanaar through gnarlspines after first darnassus to get final vial q
        -- elder timberlings (Tumors) are ... unkillable solo
        -- if shadra is hard too we just skip the entire first darna trip unless 2man
        -- shadra is easily kitable, but does hit hard and lots of mobs around, need some clearing
        -- edit: dosent seem that easy to hit 10 without shadra or some heavy grind so may need to stick to that
        -- bloodfeathers: sorceress are easy, dont move, harpies are tanky. Droprate seems high.
            -- AVOID matriarchs. 
            -- may want some food to eat here though if killing sorceress. they are easy, but deals high dmg
            -- taking way elss dmg from kiting the others thoughs
        
        -- actually, the tramplers south on the nothern lake are easy.
        -- should be done asap there until other ppl get to that part. Droprate was 100% for me
        -- delivering fruits while doing taming quest would be nice, but dosent seem doable
        -- was 2.5 bars from 11 when exiting darna
	}
}
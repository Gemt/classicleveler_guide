CLGuide_GuideTable = {
	CLGuide_Teldrassil,
	CLGuide_Darkshore,
    CLGuide_Ashenvale18,
    CLGuide_Ashenvale,
    CLGuide_Ashenvale_Redridge,
    CLGuide_Redridge,
    CLGuide_Duskwood1,
    CLGuide_Wetlands,

    CLGuide_Ashenvale2,
    CLGuide_Duskwood2,
    CLGuide_Southshore,

    CLGuide_TheramoreShimmering,
    CLGuide_Stv1,
    CLGuide_Desolace,
    CLGuide_Arathi_Alterac,
    CLGuide_Badlands,
    CLGuide_SwampOfSorrows,
    CLGuide_Stv2,
    CLGuide_Tanaris1,
    CLGuide_Feralas,
    CLGuide_Tanaris2,
    CLGuide_Hinterlands,
    CLGuide_SearingGorge,
    CLGuide_BlastedLands,

    CLGuide_AzharaFelwood1,
    CLGuide_Ungoro,
    CLGuide_BurningSteppes,
    CLGuide_BlastedLands_Azshara,
    CLGuide_Felwood_WS2,
    CLGuide_WesternPlaguelands,
    CLGuide_Silithus,

    -- extra
    CLGuide_SouthshoreRun,
}

--[[
CLGuide_GuideTable = {
    { 
        Title="Section Title",
        PinBoard = {"pinboardItem1", "pinboardItem"}
        Steps = {
            {
                Text="Do Quest1 and Quest3", 
                [TriggerItem], 
                [Point], 
                [UseItem="itemname"], 
                [PinAdd="pinboardItem1"], 
                [PinRemove="pinboardItem1"],
                [Class="Classname"]
            }
        }
    }
}
TriggerItem (a step can only have ONE trigger):
    *At="questname"                                     quest Accepted: system message
    *Ct="questname"                                     quest is completed in questlog
    *Ht="questname"                                     quest is in the questlog
    *Dt={q="questname", [Item="itemname"], [Use=1],[Vendor="itemname"],[SkipIfUncomplete=1],[SkipIfNotHaveQuest=1]}
                                                        quest is delivered. If quest has multiple rewards to choose from, 
                                                        you can optionally specify which reward to choose.
                                                        (TODO) If use=1, the item will also be equipped after receiving it
                                                        (TODO) if Vendor is specified with an itemname, that item will be vendored after this step is complete
                                                        if SkipIfUncomplete=1 specified, step is skipped if the quest is not completed at this stage
                                                        if SkipIfNotHaveQuest=1 specified, skip step if dt quest is not in questlog
    *Mct={"quest1","quest","quest3"...}                 All mentioned quests are complete in questlog
    Lvl={lvl=1 [,xp=500]}                               Goto next step when level is reached. Optionally add xp requirement as well 
    Item={Name="itemname", Count=1}                     Goto next step when inventory contains >= Count of Name
    Proximity=5                                         Goto next step when distance to [Point] is <= Proximity
    Zone="zonename"                                     Goto next step when zone changed to Zone
    *Taxi="taxidest"                                    Start flying to taxidest and goto next step
    *SetHs="innkeeper-name"                             Set hs on "innkeeper-name"
    *BuyItem={Npc="npcName", Item="itemname", Count=1}  Buys the "itemname" from a vendor with name "npcName" until inventory contains Count
    *TrainSkill=1                                       Will attempt to select the first gossip option of type "trainer" when talking to an NPC
                                                         
    
    
Point:
    Point={x=1,y=1}

UseItem:
    When a itemname is specified here, show a clickable button for the specified item
PinAdd/PinRemove:
    The step will Add a new item to the pinboard, or remove an existing one (removed if strings of a pinboard item matches the specified PinRemove string)

Class: (TODO)
    The step will only be shown if you are playing the named class. 

Triggers marked with * are disabled as long as SHIFT modifier is pressed
]]

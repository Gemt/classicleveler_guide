-- Author      : G3m7
-- Create Date : 5/5/2019 5:19:05 PM

CLGuide_Duskwood2 = {
    Title="29-31 Duskwood",
    Pinboard = {},
    Steps = {
        {Text="Complete A Noble Brew by looting Tear of Tiloa at Manor Mistmantle", Ct="A Noble Brew", point={x=7836,y=3595,m=1431},
            PinAdd="Check for bronze tube if still dont have"},
        {Text="Complete The Legend of Stalvan. Kite him towards Darkshire", Ct="The Legend of Stalvan", point={x=7739,y=3602,m=1431}},
        {Text="Deliver The Legend of Stalvan", Dt={q="The Legend of Stalvan", Item="Ring of Forlorn Spirits", Use=1}, point={x=7581,y=4530,m=1431}},

        {Text="Accept Worgen in the Woods (pt1)", At="Worgen in the Woods", point={x=7558,y=4791,m=1431}},
        
        {Text="Complete worgen in the woods (6 shadowcasters) to the west", Ct="Worgen in the Woods", point={x=6280,y=4560,m=1431}},
        {Text="Turn Worgen in the Woods.", Dt={q="Worgen in the Woods"}, point={x=7558,y=4791,m=1431}},
        {Text="Accept Worgen in the Woods (pt2)", At="Worgen in the Woods", point={x=7558,y=4791,m=1431}},
        
        {Text="Complete Worgen in the Woods pt2 (runners)", Ct="Worgen in the Woods", point={x=6240,y=6990,m=1431}},

        -- could delay this one round, especially if still 27. Main reason to do now is if HS has long cd and we need to wait for it anyway
        -- CORRECTION: Definitely delay. WE get another quest to go down there later (Ogre Thieves)
        --{Text="IF lvl 28+: Complete Look to the stars (Zzarc'Vul in orgre cave)", Ct="Look To The Stars", point={x=3402,y=7669,m=1431}},
        
        {Text="Turn Sven's Camp. (grind rot blossoms on any Skeletal Horror/Fiend)", Dt={q="Sven's Camp"}, point={x=778,y=3406,m=1431}},
        {Text="Accept The Shadowy Figure.", At="The Shadowy Figure", point={x=778,y=3406,m=1431}},

        {Text="Complete Gather Rot Blossoms (any Skeletal Horror/Fiend, NOT the higher lvl ones)", Ct="Gather Rot Blossoms"},
        --24-25 skeletons ~30k xph
	    --ghouls about the same
        {Text="Grind to lvl 28", Lvl={lvl=28}},

        {Text="Accept The Weathered Grave from grave behind house on top of hill", At="The Weathered Grave", point={x=1773,y=2909,m=1431}},
        {Text="Either HS (grind a while longer if just barely 28) or grind back to Darkshire", Zone="Scarlet Raven Tavern", UseItem="Hearthstone"},
        
        --16h3min

        {Text="Turn inn Gather Rot Blossoms (in the inn)", Dt={q="Gather Rot Blossoms"}, point={x=7377,y=4448,m=1431}},
        {Text="Accept Juice Delivery", At="Juice Delivery", point={x=7377,y=4448,m=1431}},
        
        {Text="Turn The Shadowy Figure (Madam Eva)", Dt={q="The Shadowy Figure"}, point={x=7581,y=4530,m=1431}},
        {Text="Accept The Shadowy Search Continues", At="The Shadowy Search Continues", point={x=7581,y=4530,m=1431}},


        {Text="Turn inn The Shadowy Search Continues (town hall)", Dt={q="The Shadowy Search Continues"}, point={x=7252,y=4685,m=1431}},
        {Text="Accept Inquire at the Inn", At="Inquire at the Inn", point={x=7252,y=4685,m=1431}},	

        {Text="Turn The Weathered Grave (town hall)", Dt={q="The Weathered Grave"}, point={x=7263,y=4761,m=1431}},
        {Text="Accept Morgan Ladimore", At="Morgan Ladimore", point={x=7263,y=4761,m=1431}},
        {Text="Turn inn Morgan Ladimore (outside town hall)", Dt={q="Morgan Ladimore"}, point={x=7360,y=4689}, PinAdd="Can delete The Story of Morgan Ladimore (book)"},
        {Text="Accept Mor'Ladim", At="Mor'Ladim", point={x=7360,y=4689,m=1431}},
        

        {Text="Turn inn Inquire at the Inn", Dt={q="Inquire at the Inn"}, point={x=7377,y=4448,m=1431}},
        {Text="Accept Finding the Shadowy Figure", At="Finding the Shadowy Figure", point={x=7377,y=4448,m=1431}},
        	
        {Text="Buy Razor Arrows (west outside inn)", Class="Hunter", BuyItem={Npc="Avette Fellwood", Item="Razor Arrow", Count=2000}, point={x=7302,y=4448,m=1431}},

        --{Text="Complete A Noble Brew by looting Tear of Tiloa at Manor Mistmantle", Ct="A Noble Brew", point={x=7836,y=3595,m=1431}},
        --{Text="Complete The Legend of Stalvan (if not yet done). Kite him towards Darkshire", Ct="The Legend of Stalvan"},

        {Text="Turn Worgen in the Woods pt2", Dt={q="Worgen in the Woods"}, point={x=7558,y=4791,m=1431}},
        {Text="Accept Worgen in the Woods pt3", At="Worgen in the Woods", point={x=7558,y=4791,m=1431}},
        {Text="If had Bronze Tube: Accept Look To The Stars, otherwise check vendor", At="Look To The Stars", point={x=7980,y=4798,m=1431}},
        {Text="Fly to SW", Taxi="Stormwind, Elwynn", point={x=7750,y=4428,m=1431}, PinAdd="Delete story of mor'ladim"},

        {Text="If you found \"An Old History Book\", accept the quest", At="An Old History Book", UseItem="An Old History Book"},
        {Text="Cloth in bank, delivery if pos. Get Vial of Spider Venom/Skeleton finger out of bank", 
            PutInBank={"Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth"},
            TakeFromBank={"Spider Venom", "Skeleton Finger"},
            PinAdd="MAKE SURE TO GET VIAL OF SPIDER VENOM AND SKELETON FINGERS FROM BANK"},

            -- todo: get 30 bow instead if this vendor has it?
        {Text="Check for 30/42 bow on Frederick Stover", point={x=5006,y=5767,m=1453}},
        {Text="(if had cloth) Turn inn A Donation of Wool (second floor)", Dt={q="A Donation of Wool"}, At="A Donation of Wool", point={x=4418,y=7379,m=1453}},
        {Text="(if had cloth) Turn inn A Donation of Silk (second floor)", Dt={q="A Donation of Silk"}, At="A Donation of Silk", point={x=4418,y=7379,m=1453}},
        --{Text="test", Dt={q="The Totem of Infliction", SkipIfUncomplete=1}, point={x=4418,y=7379}},
        
        {Text="Deliver A Noble Brew at lock trainer", Dt={q="A Noble Brew"}, point={x=2922,y=7399,m=1453}},
        {Text="Accept A Noble Brew pt2", At="A Noble Brew"},

        {Text="Deliver Cleansing the Eye (cathedral)", Dt={q="Cleansing the Eye"}, point={x=3960,y=2721,m=1453}},

        {Text="Accept The Missing Diplomat from Thomas, patrolling in cathedral", At="The Missing Diplomat"},
        -- tinkmaster overspark?
        {Text="If old history book: Accept Speaking of Fortitude (outside cathedral)", At="Speaking of Fortitude", point={x=4569,y=3841,m=1453}},
        
        {Text="Train Hunter skills", TrainSkill=1, point={x=6153,y=1650,m=1453}},
        {Text="Train Pet skills", TrainSkill=1, point={x=6153,y=1650,m=1453}},

        {Text="Enter keep, first right, deliver A Noble Brew (No followup)", Dt={q="A Noble Brew"}, point={x=7522,y=3170,m=1453}},
        {Text="Back to main hallway, first left to library. Deliver An Old History Book if u had it", Dt={q="An Old History Book"}, point={x=7416,y=748,m=1453}},
        {Text="Accept Southshore (if you had old history book)", At="Southshore", point={x=7416,y=748,m=1453}},
        {Text="If old history book: Deliver Speaking of Fortitude", Dt={q="Speaking of Fortitude"}, point={x=7416,y=748,m=1453}},

        {Text="Back to corridor, to king and first right for Missing Diplomat", Dt={q="The Missing Diplomat"}, point={x=7830,y=2547,m=1453}},
        {Text="Accept The Missing Diplomat", At="The Missing Diplomat", point={x=7830,y=2547,m=1453}},
        {Text="Jorgen, fishing at gates of SW, deliver Diplomat (jump from FP)", Dt={q="The Missing Diplomat"}, point={x=7312,y=7840,m=1453}},
        {Text="Accept The Missing Diplomat", At="The Missing Diplomat", point={x=7312,y=7840,m=1453}},

        {Text="Elling Trias, cheese vendor, first building on right side when entering SW (second floor)", Dt={q="The Missing Diplomat"}, point={x=5990,y=6414,m=1453}},
        {Text="Accept The Missing Diplomat", At="The Missing Diplomat", point={x=5990,y=6414,m=1453}},
        
        -- todo: can we ghetto heart with stockades back to darkshire instead of flying? 
        {Text="Fly to Darkshire", Taxi="Darkshire, Duskwood", point={x=6627,y=6218,m=1453}},

        {Text="Watcher Backus, patrolling from darkshire to north on road. Diplomat", Dt={q="The Missing Diplomat"}, point={x=7318,y=3248,m=1431}},
        {Text="Accept The Missing Diplomat", At="The Missing Diplomat", point={x=7318,y=3248,m=1431}},

        {Text="Complete Worgen in the Woods pt3 (track humanoid)", Ct="Worgen in the Woods", point={x=7256,y=6710,m=1431}},
        {Text="If not enough mobs => goto point to find more.", Ct="Worgen in the Woods", point={x=6539,y=8029,m=1431}},

        {Text="Turn inn Finding the Shadowy Figure at raven hill", Dt={q="Finding the Shadowy Figure"}, point={x=1835,y=5631,m=1431}},
        {Text="Accept Return to Sven", At="Return to Sven", point={x=1835,y=5631,m=1431}},
        {Text="Turn inn Return to Sven (NW corner of map)", Dt={q="Return to Sven"}, point={x=775,y=3405,m=1431}},
        {Text="Accept Proving Your Worth", At="Proving Your Worth", point={x=775,y=3405,m=1431}},
        
        {Text="Grind over to Abercrombie (kill some raiders, too few of them) and turn inn Juice Delivery", Dt={q="Juice Delivery"}, point={x=2812,y=3146,m=1431}},
        {Text="Accept Ghoulish Effigy", At="Ghoulish Effigy", point={x=2812,y=3146,m=1431}},
        
        {Text="Complete: Ghoulish Effingy, Totem of Infliction, Proving your Worth, The Night Watch",
            Mct={"Ghoulish Effigy", "The Totem of Infliction", "Proving Your Worth", "The Night Watch"},
            PinAdd="Can do Mor'Ladim kite (or grp) anytime now"},

        {Text="Turn inn Proving Your Worth (At Sven, NW)", Dt={q="Proving Your Worth"}, point={x=775,y=3405,m=1431}},
        {Text="Accept Seeking Wisdom", At="Seeking Wisdom", point={x=775,y=3405,m=1431}},

        {Text="Deliver Ghoulish Effigy (Abercrombie)", Dt={q="Ghoulish Effigy"}, point={x=2812,y=3146,m=1431}},
        {Text="Accept Ogre Thieves", At="Ogre Thieves", point={x=2812,y=3146,m=1431}, PinAdd="Now is a good time to kite Mor'Ladim towards ogres"},
        
        {Text="Missing diplomat, loot defias locket in house on farm south of raven hill. (Skip if kiting mor'ladim)", Ct="The Missing Diplomat", point={x=2392,y=7204,m=1431}},

        {Text="Complete Ogre Thieves outside ogre cave (loot box)", Ct="Ogre Thieves", point={x=3344,y=7636,m=1431}},
        {Text="Complete Look to the stars (Zzarc'Vul in orgre cave)", Ct="Look To The Stars", point={x=3402,y=7669,m=1431}},

        {Text="Missing diplomat, loot defias locket in house on farm south of raven hill. (If skipped earlier)", Ct="The Missing Diplomat", point={x=2392,y=7204,m=1431}},

        {Text="Turn inn Ogre Thieves (Abercrombie)", Dt={q="Ogre Thieves"}, point={x=2812,y=3146,m=1431}},
        {Text="Accept Note to the Mayor", At="Note to the Mayor", point={x=2812,y=3146,m=1431}, PinAdd="Another chance to kite Mor'Ladim towards darkshire now, but not necessary"},
        
         -- TODO: how much xp from these quests? grind so dinging 30 from them
        {Text="Grind to 29+27000(29050 if no mor'ladim)", Lvl={lvl=29,xp=27000}},
        
        {Text="Heartstone to Darkshire", UseItem="Hearthstone", Proximity=100, point={x=7394,y=4453,m=1431}},

        {Text="Turn in The Totem of Infliction", Dt={q="The Totem of Infliction"}, point={x=7581,y=4530,m=1431}},

        {Text="Mor'Ladim", Dt={q="Mor'Ladim"}, point={x=7359,y=4688},
            PinAdd="Night's Watch turnin will autoaccept normal bag (10slot), if already have, SHIFT ON TURNIN to select Quiver"},
        {Text="Accept The Daughter Who Lived", At="The Daughter Who Lived", point={x=7360,y=4689,m=1431}},

        {Text="Turn inn The Night Watch", Dt={q="The Night Watch", Item="Gunnysack of the Night Watch", Use=1}, point={x=7359,y=4688,m=1431}},


        {Text="Turn in Note to the Mayor (town hall)", Dt={q="Note to the Mayor"}, point={x=7193,y=4653,m=1431}},
        {Text="Accept Translate Abercrombie's Note", At="Translate Abercrombie's Note", point={x=7193,y=4653,m=1431}},
        {Text="Deliver Translate Abercrombie's Note (on way out of Town Hall)", Dt={q="Translate Abercrombie's Note"}, point={x=7264,y=4763,m=1431}},
        {Text="Accept Wait for Sirra to Finish", At="Wait for Sirra to Finish", point={x=7264,y=4763,m=1431}},
        {Text="Turn in Wait for Sirra to Finish (wait for RP)", Dt={q="Wait for Sirra to Finish"}, point={x=7264,y=4763,m=1431}},
        {Text="Accept Translation to Ello", At="Translation to Ello", point={x=7264,y=4763,m=1431}},
        {Text="Turn in Translation to Ello (back to prev. guy)", Dt={q="Translation to Ello"}, point={x=7193,y=4653,m=1431}},
        {Text="Accept Bride of the Embalmer", At="Bride of the Embalmer", point={x=7193,y=4653,m=1431}},

        {Text="Turn in Worgen in the Woods", Dt={q="Worgen in the Woods"}, point={x=7533,y=4796,m=1431}},
        {Text="Accept Worgen in the Woods", At="Worgen in the Woods", point={x=7533,y=4796,m=1431}},
        {Text="Turn in Worgen in the Woods", Dt={q="Worgen in the Woods", Item="Cloak of the Faith", Use=1, Vendor="Resilient Poncho"}, point={x=7532,y=4884,m=1431}},

        {Text="Turn in Look To The Stars", Dt={q="Look To The Stars", Item="Zodiac Gloves", Use=1, Vendor="Windfelt Gloves"}, point={x=7982,y=4794,m=1431}},

        {Text="Turn inn The Daughter Who Lived (patrolls inside Darkshire)", Dt={q="The Daughter Who Lived"}, point={x=7474,y=4686,m=1431}},
        {Text="Accept A Daughter's Love", At="A Daughter's Love"},

        {Text="Watcher Backus, patrolling from darkshire to north on road. Diplomat", Dt={q="The Missing Diplomat"}, point={x=7318,y=3248,m=1431}},
        {Text="Accept The Missing Diplomat", At="The Missing Diplomat", point={x=7318,y=3248,m=1431}},

        -- 17h37m, 29+74% (32819/44300)

        {Text="Fly to SW. If found \"An Old History Book\" and not yet done, accept the quest", Taxi="Stormwind, Elwynn", UseItem="An Old History Book", point={x=7750,y=4428,m=1431}},

        {Text="Elling Trias, cheese vendor, first building on right side when entering SW (second floor). Jump off FP instead of running around", Dt={q="The Missing Diplomat"}, point={x=5990,y=6414,m=1453}},
        {Text="Accept The Missing Diplomat", At="The Missing Diplomat", point={x=5990,y=6414,m=1453}},

        -- wait with buying arrows to wetlands so we got an easier time switching quiver
        --{Text="Buy Razor Arrows", BuyItem={Npc="Marda Weller", Item="Razor Arrow", Count=2000}, point={x=5737,y=5679,m=1453}},
        {Text="Buy Heavy Quiver", Class="Hunter", BuyItem={Npc="Marda Weller", Item="Heavy Quiver", Count=1}, point={x=5737,y=5679,m=1453}},
        {Text="Check for 30/42 bow on Frederick Stover", point={x=5006,y=5767,m=1453}},
        {Text="Put cloth and, bow (if got), Sarah's Ring in bank (Can do wool/silk delivery if not done SW yet)", 
            PutInBank={"Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth", "Sarah's Ring"}},
        --{Text="Accept Retrieval for Mauren", At="Retrieval for Mauren", point={x=4308,y=8038,m=1453}},
        {Text="Accept Malin's Request (outside mage tower in mage quarter)", At="Malin's Request", point={x=3977,y=8146,m=1453}},
        {Text="Accept James Hyal (Connor Rivers in Inn next to Malin)", At="James Hyal", point={x=4061,y=9183,m=1453}},

        {Text="Deliver Seeking Wisdom (cathedral)", Dt={q="Seeking Wisdom"}, point={x=3910,y=2788,m=1453}},
        {Text="Accept The Doomed Fleet", At="The Doomed Fleet", point={x=3910,y=2788,m=1453}},

        -- training skills in IF instead, shorter run
        --{Text="Train Hunter skills", TrainSkill=1 point={x=6153,y=1650} },
        --{Text="Train Pet skills", TrainSkill=1, point={x=6153,y=1650} },
        {Text="If An Old History Book if u had it", Dt={q="An Old History Book", SkipIfNotHaveQuest=1}, point={x=7416,y=748,m=1453}},
        {Text="If had old history book: Accept Southshore", At="Southshore", point={x=7416,y=748,m=1453}},

        {Text="Center of Old Town, Dashel Stonefist Diplomat (freeze trap down for addspawn, CDs and nuke Dashel)", Dt={q="The Missing Diplomat"}, point={x=7050,y=4484,m=1453}},
        {Text="Accept The Missing Diplomat", At="The Missing Diplomat", point={x=7050,y=4484,m=1453}},
        {Text="Turn inn The Missing Diplomat (same guy)", Dt={q="The Missing Diplomat"}, point={x=7050,y=4484,m=1453}},
        {Text="Accept The Missing Diplomat", At="The Missing Diplomat", point={x=7050,y=4484,m=1453}},
        {Text="Elling Trias, cheese vendor, Turn inn The Missing Diplomat", Dt={q="The Missing Diplomat"}, point={x=5990,y=6414,m=1453}},
        {Text="Accept The Missing Diplomat", At="The Missing Diplomat", point={x=5990,y=6414,m=1453}},
        
        {Text="Fly to Ironforge", Taxi="Ironforge, Dun Morogh", point={x=6627,y=6218,m=1453},
            PinAdd="Change Quiver while flying"},

        {Text="Get Waterlogged Envelope from bank. 60 cloths if possible too.", 
            PutInBank={"Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth"},
            TakeFromBank={"Waterlogged Envelope"}},
        {Text="Accept the Waterlogged envelope quest", At="Sully Balloo's Letter", UseItem="Waterlogged Envelope"},
        {Text="Train Hunter skills", TrainSkill=1, point={x=7088,y=8363,m=1455}},
        {Text="Train Pet skills", TrainSkill=1, point={x=7086,y=8587,m=1455}},
        {Text="Accept The Brassbolts Brothers (behind battlemasters)", At="The Brassbolts Brothers", point={x=7273,y=9405,m=1455}},
        {Text="Check bow vendor (second floor) for bows", point={x=7216,y=6762,m=1455}},
        {Text="Turn in Sully Balloo's Letter", Dt={q="Sully Balloo's Letter"}, point={x=6380,y=6774,m=1455}},
        {Text="Accept Sara Balloo's Plea (maybe some RP first?)", At="Sara Balloo's Plea", point={x=6380,y=6774,m=1455}},
        {Text="Turn in Sara Balloo's Plea (at king)", Dt={q="Sara Balloo's Plea"}, point={x=3911,y=5616,m=1455}},
        {Text="Accept A King's Tribute", At="A King's Tribute", point={x=6380,y=6774,m=1455}},
        {Text="Turn inn A King's Tribute (SE of IF entrance)", Dt={q="A King's Tribute"}, point={x=3968,y=8522,m=1455}},
        {Text="Accept A King's Tribute", At="A King's Tribute", point={x=3968,y=8522,m=1455}},
        {Text="Fly to Menethil", Taxi="Menethil Harbor, Wetlands", point={x=5546,y=4776,m=1455}},

        {Text="Place immo trap outside inn, Turn inn The Missing Diplomat at Mikhail", Dt={q="The Missing Diplomat"}, point={x=1059,y=6079,m=1437}},

        {Text="Accept The Missing Diplomat, GO STRAIGHT TO ENTRANCE AND DEFEAT Tapoke", At="The Missing Diplomat", point={x=1059,y=6079,m=1437}},
        {Text="Defeat Tapoke outside (maybe kite if needed)", Ct="The Missing Diplomat", point={x=1078,y=5946,m=1437}},
        {Text="Turn inn The Doomed Fleet next to Mikhail (Glorin Steelbrow)", Dt={q="The Doomed Fleet"}, point={x=1058,y=6058,m=1437}},
        {Text="Accept Lightforge Iron", At="Lightforge Iron", point={x=1058,y=6058,m=1437}},
        {Text="Turn inn James Hyal by the fireplace", Dt={q="James Hyal"}, point={x=1082,y=6042,m=1437}},
        {Text="Accept James Hyal", At="James Hyal", point={x=1082,y=6042,m=1437}},
        {Text="Set HS in wetlands", SetHs="Innkeeper Helbrek", point={x=1069,y=6096,m=1437}},
        {Text="Turn inn The Missing Diplomat at Mikhail again (a bit of waiting)", Dt={q="The Missing Diplomat"}, point={x=1059,y=6079,m=1437}},
        {Text="Accept The Missing Diplomat (From Tapoke at entrance)", At="The Missing Diplomat", point={x=1055,y=6025,m=1437}},
        {Text="Turn inn The Missing Diplomat at Mikhail again", Dt={q="The Missing Diplomat"}, point={x=1059,y=6079,m=1437}},
        {Text="Accept The Missing Diplomat", At="The Missing Diplomat", point={x=1059,y=6079,m=1437}},
        
        {Text="Go out, behind Inn, loot Waterlogged Chest for Lightforge Iron", Dt={q="Lightforge Iron"}, point={x=1210,y=6420,m=1437}},
        {Text="Accept The Lost Ingots", At="The Lost Ingots", point={x=1210,y=6420,m=1437}},
        
        {Text="Complete The Lost Ingots (drop from Murlocs in area) (check for Sludginn, rare, at point)", Ct="The Lost Ingots", point={x=1265,y=6753,m=1437}},
        {Text="Turn inn The Lost Ingots (Back in the Inn)", Dt={q="The Lost Ingots"}, point={x=1058,y=6058,m=1437}},
        {Text="Accept Blessed Arm", At="Blessed Arm", point={x=1058,y=6058,m=1437}},

        --{Text="Buy Razor Arrows (outside, Edwina Monzor)", BuyItem={Npc="Edwina Monzor", Item="Razor Arrow", Count=2800}, point={x=1111,y=5833,m=1437}},
        {Text="(if not done yet) Deliver Nek'Rosh (in the keep)", Dt={q="Defeat Nek'rosh", Item="Ancient War Sword", SkipIfNotHaveQuest=1}, point={x=986,y=5749,m=1437}},
        --{Text="Buy lvl 25 food from Stuart Flemming (Rockscale Cod)", BuyItem={Npc="Stuart Fleming", Item="Rockscale Cod", Count=20}, point={x=804,y=5833,m=1437}},
        {Text="Fly to Arathi Highlands", Taxi="Refuge Pointe, Arathi", point={x=949,y=5970,m=1437}},
    }
}

--[[
=================================

Questlog:
The doomed fleet
The missing diplomat
Malin's Request
James Hyal

]]
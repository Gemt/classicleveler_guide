## Title: ClassicLeveler_Guide
## Version: 1.0
## Author: G3m7
## Interface: 11302

1-10-Teldrassil.lua
11-20_Darkshore.lua
18_22-Ashenvalelua.lua
EXTRA_20-22_Ashenvale.lua
23-23_Ashenvale_Redridge.lua
23-24_Redridge.lua
24-26_Duskwood.lua
26-28_Wetlands.lua
29-31_Duskwood.lua

28-29_Ashenvale.lua
EXTRA_Southshore_Run.lua
31_32_Southshore.lua

32_34_Theramore_ShimmeringFlats.lua
34_36_Stv.lua
36_37_desolace.lua
37_39_Arathi_Alterac.lua
39_41_Badlands.lua
41_42_SwampOfSorrows.lua
42_44_Stv.lua
44_45_Tanaris.lua
45_46_Feralas.lua
47_48_1_Tanaris.lua
47_48_2_Hinterlands.lua
48_49_Searing_Gorge.lua
49_50_Blasted_Lands.lua

50-51_Azshara-Felwood-Darnassus.lua
51-53_Ungoro.lua
53-55_Plaguelands_Burning_Steppes-Onychain.lua
55-56_BlastedLands-Azshara.lua
56-57_Felwood-Winterspring.lua
57-60_Plaguelands.lua
60-60_SilithusExtra.lua

GuideSteps.lua

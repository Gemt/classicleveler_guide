-- Author      : G3m7
-- Create Date : 5/19/2019 3:59:04 PM

CLGuide_Ungoro = {
    Title="51-53 Un'Goro",
    Pinboard = {},
    Steps = {
        {Text="Buy arrows", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=2800}, point={x=6795,y=4989,m=1445}},
        {Text="Fly to Tanaris", Taxi="Gadgetzan, Tanaris", point={x=6747,y=5130,m=1445}},

        {Text="Set HS in Gadgetzan", SetHs="Innkeeper Fizzgrimble", point={x=5247,y=2792,m=1446}},
        {Text="Restock on Roasted Quail", Class="Hunter", BuyItem={Item="Roasted Quail", Count=20}, point={x=5264,y=2811,m=1446}},

        {Text="Accept Super Sticky", At="Super Sticky", point={x=5156,y=2676,m=1446}},
        {Text="Deliver Sprinkle's Secret Ingredient", Dt={q="Sprinkle's Secret Ingredient"}, point={x=5106,y=2687,m=1446}},
        {Text="Deliver March of the Silithid", Dt={q="March of the Silithid"}, point={x=5089,y=2697,m=1446}},
        {Text="Accept Bungle in the Jungle", At="Bungle in the Jungle", point={x=5089,y=2697,m=1446}},
        {Text="Accept Delivery for Marin (wait for RP if not done yet)", At="Delivery for Marin", point={x=5106,y=2687,m=1446}},

        {Text="Deliver Delivery for Marin", Dt={q="Delivery for Marin"}, point={x=5180,y=2866,m=1446}},
        {Text="Accept Noggenfogger Elixir", At="Noggenfogger Elixir", point={x=5180,y=2866,m=1446}},
        {Text="(wait for RP) Deliver Noggenfogger Elixir", Dt={q="Noggenfogger Elixir"}, point={x=5180,y=2866,m=1446}},

        {Text="Deliver The Stone Circle (south of gadgetzan, skip followup)", Dt={q="The Stone Circle"}, point={x=5271,y=4593,m=1446}},
        {Text="Fly to Un'goro", Taxi="Marshal's Refuge, Un'Goro Crater", point={x=5101,y=2934,m=1446},
            PinAdd={"Use Evergreen Pouch on CD as soon as you got Un'Goro Soil", "Make sure to Get 7 of each colored crystal"}},


        --[[
            yuka
            seeking spiritual aid
            further corruption
            verifying the corruption
            cleansing felwood
            return to troyas
            a little slime goes a long way (ungoro)
            bungle in the jungle 
            super sticky
            
          
            the videre elixir
            the apes of ungoro
            volcanic activity
        ]]

        
        {Text="Accept Alien Ecology", At="Alien Ecology", point={x=4389,y=725,m=1449}},
        {Text="Accept Roll the Bones", At="Roll the Bones", point={x=4350,y=742,m=1449}},
        {Text="Accept Beware of Pterrordax (wanted poster)", At="Beware of Pterrordax", point={x=4355,y=842,m=1449}},
        {Text="Accept Lost!", At="Lost!", point={x=4362,y=850,m=1449}},
        {Text="Accept Muigin and Larion", At="Muigin and Larion", point={x=4294,y=965,m=1449}},
        {Text="Accept Shizzle's Flyer", At="Shizzle's Flyer", point={x=4424,y=1158,m=1449}},
        {Text="Accept Chasing A-Me 01", At="Chasing A-Me 01", point={x=4637,y=1344,m=1449}},


        {Text="Complete Apes of Un'Goro", Ct="The Apes of Un'Goro", UseItem="Empty Pure Sample Jar", point={x=6274,y=1706,m=1449},
            PinAdd={"Kill blodpetals everywhere for Muigin and Larion", "Kill any Oozes & use empty pure sample jar for A little slime"}},
        {Text="Deliver Chasing A-Me 01 (in the cave). Do & deliver escort if you have mats", Dt={q="Chasing A-Me 01"}, point={x=6766,y=1676,m=1449}},
        

        {Text="Farm Bloodpetals and Oozes twards delivery of Apes of Un'Goro", Dt={q="The Apes of Un'Goro"}, point={x=7164,y=7596,m=1449}},
        {Text="Accept The Mighty U'cha", At="The Mighty U'cha", point={x=7164,y=7596,m=1449}},
        {Text="Accept The Fare of Lar'korwi", At="The Fare of Lar'korwi", point={x=7164,y=7595,m=1449}},
        {Text="Loot the dead dino", Ct="The Fare of Lar'korwi", point={x=6865,y=5673,m=1449}},
        {Text="Deliver The Fare of Lar'korwi", Dt={q="The Fare of Lar'korwi"}, point={x=7164,y=7595,m=1449}},
        {Text="Accept The Scent of Lar'korwi", At="The Scent of Lar'korwi", point={x=7164,y=7595,m=1449}},
        {Text="Complete The Scent of Lar'korwi (step on eggs to spawn dino)", Ct="The Scent of Lar'korwi", point={x=6733,y=7304,m=1449}},
        {Text="(another egg location)", Ct="The Scent of Lar'korwi", point={x=6323,y=7734,m=1449}},
        {Text="(another egg location)", Ct="The Scent of Lar'korwi", point={x=6287,y=8047,m=1449}},
        {Text="Deliver The Scent of Lar'korwi", Dt={q="The Scent of Lar'korwi"}, point={x=7164,y=7595,m=1449}},
        {Text="Accept The Bait for Lar'korwi (DONT open the bag)", At="The Bait for Lar'korwi", point={x=7164,y=7595,m=1449}},


        {Text="Enter cave at point", Proximity=100, point={x=5032,y=7939,m=1449}},
        {Text="Complete Alient Ecology by using item at point", UseItem="Unused Scraping Vial", Ct="Alien Ecology", point={x=4866,y=8531,m=1449}},
        {Text="Get Gorishi Scent Gland from mobs here before leaving (bungle in the jungle)", Ct="Bungle in the Jungle"},

        {Text="Deliver Lost!", Dt={q="Lost!"}, point={x=5257,y=5136,m=1449}},
        {Text="Accept A Little Help From My Friends", At="A Little Help From My Friends", point={x=5192,y=4984,m=1449}, PinAdd="Should be done with collecting 7 of each crystal and bloodpetal kills before starting ringo"},
        {Text="Escort Ringo to Marshal's Refuge. Make sure Muigin and Larion gets completed & you got 7 of each crystal", Ct="A Little Help From My Friends", UseItem="Spraggle's Canteen", point={x=4362,y=849,m=1449},
            PinAdd="Make sure got AT LEAST 5 Un'Goro Soil extra for Bungle in the Jungle before reaching Marshals Refuge"},
        
        {Text="Deliver A Little Help From My Friends", Dt={q="A Little Help From My Friends", Item="Clayridge Helm", Use=1}, point={x=4361,y=851}},
        {Text="Accept Williden's Journal from A Mangled Journal if found (itembutton if found)", At="Williden's Journal", UseItem="A Mangled Journal", point={x=4294,y=963,m=1449}},

        {Text="Deliver Muigin and Larion", Dt={q="Muigin and Larion"}, point={x=4294,y=963,m=1449}},
        {Text="Accept A Visit to Gregan", At="A Visit to Gregan", point={x=4294,y=963,m=1449}},

        {Text="Deliver Alien Ecology", Dt={q="Alien Ecology"}, point={x=4389,y=724,m=1449}},
        {Text="Deliver Williden's Journal (if you found it)", Dt={q="Williden's Journal", SkipIfNotHaveQuest=1}, point={x=4395,y=713,m=1449}},

        {Text="Make sure got at least 5 Un'Goro soil for Bungle in the Jungle before HSing", Item={Name="Un'Goro Soil", Count=5}},
        {Text="Hearthstone to Gadgetzan", UseItem="Hearthstone"},

        {Text="Deliver Bungle in the Jungle (skip followup)", Dt={q="Bungle in the Jungle"}, point={x=5089,y=2696,m=1446}},
        {Text="Accept Pawn Captures Queen", At="Pawn Captures Queen", point={x=5089,y=2696,m=1446}},
        
        {Text="Put all Ungoro items in bank except Bloodpetal", 
            PutInBank={"Green Power Crystal", "Yellow Power Crystal", "Blue Power Crystal", "Red Power Crystal", 
            "Webbed Pterrordax Scale", "Torwa's Pouch", "Super Sticky Tar", "Webbed Diemetradon Scale", "Gorishi Queen Lure",
            "Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth"},
            point={x=5228,y=2890,m=1446}},
        
        {Text="Fly to Feathermoon, Feralas", Taxi="Feathermoon, Feralas", point={x=5104,y=2934,m=1446}},



        ---------------------
        -- Feralas 2
        ---------------------
        {Text="Accept Improved Quality", At="Improved Quality", point={x=3062,y=4271,m=1444}},
        {Text="Buy arrows", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=2800}, point={x=3065,y=4343,m=1444}},
        {Text="Deliver Return to Troyas", Dt={q="Return to Troyas"}, point={x=3178,y=4550,m=1444}},
        {Text="Accept The Stave of Equinex (may need to wait for RP)", At="The Stave of Equinex", point={x=3178,y=4550,m=1444}},

        {Text="If got 10 Morrowgrain, Accept & Deliver THE MYSTERY of morrowgrain, otherwise keep making on CD", point={x=3245,y=4379,m=1444},
            PinAdd="Remember to keep making Morrowgrains if not yet done"},

        {Text="Get to the mainland towards point", Proximity=50, point={x=4649,y=3758,m=1444}},
        {Text="Complete Improved Quality", Ct="Improved Quality", point={x=5161,y=3221,m=1444}},
        
        {Text="Deliver A Visit to Gregan (skip followup)", Dt={q="A Visit to Gregan"}, point={x=4512,y=2557,m=1444}},
        {Text="Buy Bait (gossip then vendor)", Item={Name="Bait", Count=1}, point={x=4512,y=2557,m=1444}},

        {Text="Accept The Giant Guardian", At="The Giant Guardian", point={x=4240,y=2199,m=1444}},
        
        {Text="Loot Evoroot inside building for The Videre Elixir", point={x=4456,y=1014,m=1444}, UseItem="Bait", Item={Name="Evoroot", Count=1},
            PinAdd="Drop bait by Miblon and gate should open, if not try walljump in behind building"},

        {Text="Loot Flame of Embel", Proximity=10, point={x=3993,y=945,m=1444}},
        {Text="Loot Flame of Samah", Proximity=10, point={x=4054,y=1265,m=1444}},
        {Text="Loot Flame of Byltan", Proximity=10, point={x=3851,y=1579,m=1444}},
        {Text="Go this way", Proximity=15, point={x=3694,y=1346,m=1444}},
        {Text="Loot Flame of Lahassa", Proximity=10, point={x=3775,y=1218,m=1444}},
        {Text="Use Troyas' Stave & Deliver The Stave of Equinex", Dt={q="The Stave of Equinex"}, UseItem="Troyas' Stave", point={x=3883,y=1308,m=1444}},
        {Text="Accept The Morrow Stone", At="The Morrow Stone", point={x=3883,y=1308,m=1444}},
        
        -- so we hit lvl 53 from quest turnins before flying darna where we need Starfall (req 53)
        -- update: not doing starfall now so not really necessary
        --{Text="Grind harpies until at least 142 050 / 160 400 in lvl 52", Level={lvl=52,xp=142050}},

        {Text="Deliver The Giant Guardian", Dt={q="The Giant Guardian"}, point={x=3822,y=1030,m=1444}},
        {Text="Accept Wandering Shay", At="Wandering Shay", point={x=3822,y=1030,m=1444}},
        {Text="Loot Shay's Bell", Item={Name="Shay's Bell", Count=1}, point={x=3825,y=1029,m=1444}},
        {Text="Escort Shay back to Stone Dude, use bell to stop her wandering off", Ct="Wandering Shay", UseItem="Shay's Bell", point={x=4239,y=2197,m=1444}},
        {Text="Deliver Wandering Shay", Dt={q="Wandering Shay", Item="Vinehedge Cinch"}, point={x=4239,y=2197,m=1444}},
        
        {Text="Deliver The Videre Elixir to complete.. the videre elixir %)", Dt={q="The Videre Elixir"}, point={x=4512,y=2557,m=1444}},
        
        {Text="Deathwarp to Feathermoon from point or further west. If east of point you end up at DM", DeathWarp=1, point={x=4129,y=2599,m=1444}},
        {Text="Deliver The Morrow Stone", Dt={q="The Morrow Stone"}, point={x=3178,y=4550,m=1444}},

        {Text="If now have 10 Morrowgrain, Accept & Deliver THE MYSTERY of morrowgrain, this is the last chance", point={x=3245,y=4379,m=1444}},
        
        {Text="Stable pet", Proximity=10, Class="Hunter", point={x=3146,y=4315,m=1444}},
        {Text="Deliver Improved Quality", Dt={q="Improved Quality"}, point={x=3063,y=4271,m=1444}},
        {Text="If you found Pristine Yeti Hide (itembutton), accept and deliver", Dt={q="Pristine Yeti Hide"}, UseItem="Pristine Yeti Hide", point={x=3063,y=4271,m=1444}},
        
        {Text="Fly to Rut'Theran (darnassu)", Taxi="Rut'theran Village, Teldrassil", point={x=3024,y=4325,m=1444}},
        {Text="Accept Moontouched Wildkin", At="Moontouched Wildkin", point={x=5550,y=9205,m=1438}},
        --{Text="Accept Starfall (2nd floor)", At="Starfall", point={x=5541,y=9223,m=1438}},

        -- HUNTER
        {Text="Fly to Talonbranch Glade, Felwood", Class="Hunter", Taxi="Talonbranch Glade, Felwood", point={x=5840,y=9402,m=1438}},
        {Text="Tame a Felpaw Ravager", Class="Hunter", point={x=6152,y=1736,m=1448}},
        {Text="Farm mobs towards point, when Bite 7 is learned abandon and tame an Ironbeak HUNTER for Claw 7", Class="Hunter",
            PinAdd="/codex unit Ironbeak Hunter", point={x=5033,y=1598,m=1448}},
        {Text="Grind back up to Talonbranch until Claw 7 is learned", Class="Hunter", point={x=6249,y=2424,m=1448}},
        --- END HUNTER
        


        {Text="Fly to Everlook", Taxi="Everlook, Winterspring"},
        {Text="Accept Trouble in Winterspring! from Meggi (patrols inside everlook)", At="Trouble in Winterspring!"},
        -- todo: we can skip everlook report for later WS2 if lack of quest space
        {Text="Accept The Everlook Report", At="The Everlook Report", point={x=6135,y=3897,m=1452}},
        {Text="Get pet from stable", Class="Hunter", point={x=6039,y=3792,m=1452}},

        {Text="Deliver Trouble in Winterspring! at Donova (Skip followup). Complete Moontouched Wildkin on the way (south of road, after Lake Kel'Theril)", Dt={q="Trouble in Winterspring!"}, point={x=3127,y=4517,m=1452}},
        {Text="Deliver The Videre Elixir", Dt={q="The Videre Elixir"}, point={x=3127,y=4517,m=1452}},
        {Text="Accept Meet at the Grave", At="Meet at the Grave", point={x=3127,y=4517,m=1452}},
        {Text="Make sure Moontouched Wildkin is completed", Ct="Moontouched Wildkin"},

        {Text="Hearthstone to Gadgetzan", UseItem="Hearthstone", Zone="Tanaris"},

        {Text="Get all Ungoro items from bank, put in Everlook Report", 
            TakeFromBank={"Green Power Crystal", "Yellow Power Crystal", "Blue Power Crystal", "Red Power Crystal", 
            "Webbed Pterrordax Scale", "Torwa's Pouch", "Super Sticky Tar", "Webbed Diemetradon Scale", "Gorishi Queen Lure"},
            PutInBank={"First Relic Fragment", "Second Relic Fragment", "Third Relic Fragment", "Fourth Relic Fragment", "Studies in Spirit Speaking", 
            "Everlook Report", "Jaron's Pick", "Moontouched Feather", "Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth"},
            point={x=5228,y=2890,m=1446}},
        {Text="Run to the GY and use the videre elixir. DO NOT RESS AFTERWARDS (if somehow lost elixir just unstuck-kill yourself)", UseItem="Videre Elixir", point={x=5396,y=2848,m=1446}},
        {Text="DO NOT RESS - Deliver Meet at the Grave", Dt={q="Meet at the Grave"}, point={x=5393,y=2334,m=1446}},
        {Text="Accept A Grave Situation", At="A Grave Situation", point={x=5393,y=2334,m=1446}},
        {Text="Run back to corpse, ress, and deliver A Grave Situation at the tombstone", Dt={q="A Grave Situation"}, point={x=5381,y=2906,m=1446}},
        {Text="Accept Linken's Sword", At="Linken's Sword", point={x=5381,y=2906,m=1446}},
        {Text="Fly to Ungoro", Taxi="Marshal's Refuge, Un'Goro Crater", point={x=5101,y=2934,m=1446}},

        ---------------------------
        -- Ungoro 2
        ---------------------------
        {Text="Deliver Linken's Sword", Dt={q="Linken's Sword"}, point={x=4466,y=810,m=1449}, PinAdd="Keep Using Evergreen Pouch for seeeds"},
        {Text="Accept A Gnome's Assistance", At="A Gnome's Assistance", point={x=4466,y=810,m=1449}},
        
        {Text="Accept Crystals of Power (in cave)", At="Crystals of Power", point={x=4191,y=270,m=1449}},
        {Text="Deliver Crystals of Power", Dt={q="Crystals of Power"}, point={x=4191,y=270,m=1449}},
        {Text="Accept The Northern Pylon", At="The Northern Pylon", point={x=4191,y=270,m=1449}},
        {Text="Accept The Western Pylon", At="The Western Pylon", point={x=4191,y=270,m=1449}},
        {Text="Accept The Eastern Pylon", At="The Eastern Pylon", point={x=4191,y=270,m=1449}},
        
        {Text="Deliver A Gnome's Assistance inside cave (don't wait for RP, just walk out)", Dt={q="A Gnome's Assistance"}, point={x=4191,y=270,m=1449}},

        {Text="Exit cave, Accept Expedition Salvation", At="Expedition Salvation", point={x=4394,y=713,m=1449}},
        {Text="Buy arrows", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=2800}, point={x=4328,y=775,m=1449}},

        {Text="Complete Super Sticky (any tar mob)", Ct="Super Sticky", point={x=4719,y=1790,m=1449}, UseItem="Empty Pure Sample Jar",
            PinAdd="Keep killing oozes until A little slime is complete"},
        
        {Text="Grind towards northern pylon, click it", Ct="The Northern Pylon", point={x=5649,y=1243,m=1449}, point={x=5646,y=1247,m=1449}},

        {Text="Complete The Mighty U'cha (innermost part of gorilla cave)", Ct="The Mighty U'cha", point={x=6369,y=1645,m=1449}},

        {Text="Loot Crate of Foodstuffs at point", Item={Name="Crate of Foodstuffs", Count=1}, point={x=6854,y=3654,m=1449}},

        {Text="Work your way south to eastern pylon, click it", Ct="The Eastern Pylon", point={x=7724,y=4992,m=1449}},
        {Text="Open Torwa's Pouch, get items out (get ready for lvl 57 mob)", 
            UseItem="Torwa's Pouch", Item={Name="Preserved Threshadon Meat", Count=1}, point={x=7991,y=4990,m=1449}},
        {Text="Put down Preserved Thresadon meat on the altar", UseItem="Preserved Threshadon Meat", point={x=7991,y=4990,m=1449}},
        {Text="Put down Preserved Pheromone Mixture on altar", UseItem="Preserved Pheromone Mixture", point={x=7991,y=4990,m=1449}},
        {Text="Complete The Bait for Lar'korwi", Ct="The Bait for Lar'korwi", point={x=7991,y=4990,m=1449}},

        {Text="Deliver The Mighty U'cha", Dt={q="The Mighty U'cha", Item="Beastslayer"}, point={x=7165,y=7597,m=1449}},
        {Text="Deliver The Bait for Lar'korwi", Dt={q="The Bait for Lar'korwi", Item="Outrider Leggings"}, point={x=7164,y=7596,m=1449}},
        
        {Text="Get 10/10 Pterrordax at point and camp further west", point={x=5620,y=8843,m=1449}},

        {Text="Enter bug cave at point", Proximity=50, point={x=5006,y=8096,m=1449}},
        {Text="Go right, then straight to point, Use Gorishi Queen Lure and complete Pawn Captures Queen", UseItem="Gorishi Queen Lure", Ct="Pawn Captures Queen", 
            point={x=4349,y=8095,m=1449}, PinAdd="If solo, play it safe and keep stay at entrance, leesh-kite away if needed"},

        {Text="Run out, Loot Research Equipment (NW)", Item={Name="Research Equipment", Count=1}, point={x=3846,y=6604,m=1449}},
        {Text="Grind towards Western Pylon, click it", Ct="The Western Pylon", point={x=2378,y=5917,m=1449}},
        {Text="Accept Finding the Source (req lvl 51)", At="Finding the Source", point={x=3093,y=5045,m=1449}},

        {Text="Grind mobs for remaining quests towards top of volcano in mid (finding the source)", Proximity=100, point={x=4970,y=4578,m=1449}},
        {Text="Use Krakle's Thermometer at hotspot", UseItem="Krakle's Thermometer", Ct="Finding the Source", point={x=4970,y=4578,m=1449}},
        {Text="Complete Volcanic Activity", Ct="Volcanic Activity"},
        
        {Text="Turn inn Finding the Source to the west", Dt={q="Finding the Source"}, point={x=3093,y=5045,m=1449}},
        
        {Text="Accept The New Springs", At="The New Springs", point={x=3093,y=5045,m=1449}},
        
        {Text="Complete Shizzle Flyer, Roll the Bones, Beware of Pterrordax & A little slime towards marshal's refuge", UseItem="Empty Pure Sample Jar", point={x=4381,y=752,m=1449},
            Mct={"Shizzle's Flyer", "Roll the Bones", "Beware of Pterrordax", "A Little Slime Goes a Long Way"},
            PinAdd="Dino Bones from both Pterrordax & Diemetradons. Scales (shizzle flyer) from each respective mob. Try to move towards volcano/center"},


        {Text="Run to Marshal's Refuge (make sure all quests are done), Start by vendoring for bagspace", Proximity=10, point={x=4328,y=773,m=1449}},
        {Text="Buy arrows", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=2800}, point={x=4328,y=775,m=1449}},

        {Text="Deliver Beware of Pterrordax", Dt={q="Beware of Pterrordax"}, point={x=4362,y=850,m=1449}},
        {Text="Deliver Shizzle's Flyer", Dt={q="Shizzle's Flyer"}, point={x=4423,y=1159,m=1449}},
        {Text="Deliver Roll the Bones", Dt={q="Roll the Bones", Item="Archaeologist's Quarry Boots"}, point={x=4350,y=742,m=1449}},
        {Text="Run into cave and accept Linken's Memory", At="Linken's Memory", point={x=4192,y=270,m=1449}},
        {Text="Deliver The Northern Pylon", Dt={q="The Northern Pylon"}, point={x=4192,y=270,m=1449}},
        {Text="The Eastern Pylon", Dt={q="The Eastern Pylon"}, point={x=4192,y=270,m=1449}},
        {Text="The Western Pylon", Dt={q="The Western Pylon"}, point={x=4192,y=270,m=1449}},
        {Text="Accept Making Sense of it", At="Making Sense of it", point={x=4192,y=270,m=1449}},
        {Text="Deliver Making Sense of it (may need to wait for RP)", Dt={q="Making Sense of it"}, point={x=4192,y=270,m=1449},
            PinAdd="Delete Crystal Pylon User's Manual"},

        {Text="Run out, Deliver Expedition Salvation", Dt={q="Expedition Salvation"}, point={x=4395,y=713,m=1449}},
        {Text="Deliver Williden's Journal (if you found it and not yet done)", Dt={q="Williden's Journal"}, UseItem="A Mangled Journal", point={x=4395,y=713,m=1449}},
        
        {Text="Fly to Gadgetzan", Taxi="Gadgetzan, Tanaris", point={x=4523,y=583,m=1449}},
        {Text="Put Un'goro Ash in bank. Get Everlook report, Black dragonflight box/molt and Moontouched Feather out",
            PutInBank={"Un'Goro Ash", "Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth"},
            TakeFromBank={"Everlook Report", "Black Dragonflight Molt", "Hoard of the Black Dragonflight", "Moontouched Feather"},
            point={x=5231,y=2892,m=1446}},
        {Text="Deliver Super Sticky", Dt={q="Super Sticky"}, point={x=5157,y=2676,m=1446}},
        {Text="Deliver Pawn Captures Queen (if completed)", Dt={q="Pawn Captures Queen"}, point={x=5089,y=2696,m=1446}},
        {Text="Accept Calm Before the Storm (if pawn captures queen)", At="Calm Before the Storm", point={x=5089,y=2696,m=1446}},

        {Text="Fly to Theramore", Taxi="Theramore, Dustwallow Marsh", point={x=5101,y=2934,m=1446}},
        --{Text="Buy arrows", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=2800}, point={x=6795,y=4989,m=1445}},
        {Text="Boat to Menethil", Zone="Wetlands", point={x=7154,y=5631,m=1445}},
        
        {Text="Set HS in Menethil", SetHs="Innkeeper Helbrek", point={x=1070,y=6095,m=1437}},
        {Text="Take boat to Auberdine", Zone="Darkshore", point={x=462,y=5720,m=1437}},
        {Text="Fly to Rut'teran", Taxi="Rut'theran Village, Teldrassil", point={x=3633,y=4555,m=1439}},

        {Text="Deliver Moontouched Wildkin", Dt={q="Moontouched Wildkin"}, point={x=5550,y=9205,m=1438}},
        {Text="Accept Find Ranshalla", At="Find Ranshalla", point={x=5550,y=9205,m=1438}},
        {Text="Enter Darnassus", Zone="Darnassus"},
        {Text="Deliver Calm Before the Storm (2nd lvl). Keep an eye out for Herald Moonstalker (Call to arms)", Dt={q="Calm Before the Storm"}, point={x=4183,y=8562,m=1457}},
        {Text="Accept Calm Before the Storm", At="Calm Before the Storm", point={x=4183,y=8562,m=1457}},
        {Text="Deliver Calm Before the Storm (by bank)", Dt={q="Calm Before the Storm"}, point={x=3961,y=4263,m=1457}},


        {Text="Deliver Morrowgrain Research if have 10. Accept and deliver Ungoro soil outside behind if not done", point={x=3538,y=840,m=1457}},

        {Text="Find Herald Moonstalker A Call to Arms: The Plaguelands! (if not yet done)", At="A Call to Arms: The Plaguelands!",
            PinAdd="Her patrol: 8-shape Tradesmans->Temple->bank->rut'theran portal->Warriors->craftsmans->cenarions enclave->bank->warriors->tradesmans"},


        {Text="Hearthstone to Menethil", UseItem="Hearthstone", Zone="Wetlands"}
    }
}
-- Author      : G3m7
-- Create Date : 8/23/2019 4:45:05 AM


CLGuide_Silithus = {
    Title="Silithus 58+",
    Pinboard = {},
    Steps = {
        {Text="Fly to Tanaris", Taxi="Gadgetzan, Tanaris", point={x=6748,y=5131,m=1445}},
        --{Text="Deliver Mold Rhymes With...", Dt={q="Mold Rhymes With..."}, point={x=5147,y=2881,m=1446}},
        --{Text="Accept Fire Plume Forged", At="Fire Plume Forged", point={x=5147,y=2881,m=1446}},
        
        {Text="Get Eridan's Supplies and Umi's Mechanical Yeti from bank", 
            PutInBank={"Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth"},
            TakeFromBank={"Eridan's Supplies", "Umi's Mechanical Yeti"}, point={x=5230,y=2892,m=1446}},

        {Text="Use yeti thing to scare Sprinkle", UseItem="Umi's Mechanical Yeti", Proximity=5, point={x=5106,y=2687,m=1446}},

        
        {Text="Run to Lost Rigger Cove (pirates). Open Eridan's Supplies", UseItem="Eridan's Supplies", Proximity=30, point={x=6873,y=4153,m=1446}},
        {Text="Run to point & use Book of Aquor. Kill mob/wait for Aquementas to complete", Ct="Aquementas", UseItem="Book of Aquor", point={x=7042,y=4994,m=1446}},
        {Text="Run back to Gadgetzan, Fly to Un'Goro", Taxi="Marshal's Refuge, Un'Goro Crater", point={x=5101,y=2934,m=1446}},
        {Text="Deliver Aquementas in crystal cave", Dt={q="Aquementas"}, point={x=4193,y=270,m=1449}},
        {Text="Accept Linken's Adventure", At="Linken's Adventure", point={x=4466,y=810,m=1449}},
        {Text="Deliver Linken's Adventure outside cave", Dt={q="Linken's Adventure"}, point={x=4466,y=810,m=1449}},
        {Text="Accept It's Dangerous to Go Alone", At="It's Dangerous to Go Alone"},
        {Text="Run to fire plume ridge, kill Blazerunner (equip and use totem OH on him)", UseItem="Silver Totem of Aquementas", Ct="It's Dangerous to Go Alone", point={x=4932,y=4908,m=1449}},
        
        -- todo: offhand or mainhand?
        {Text="Loot Golden Flame to complete It's Dangerous to Go Alone", Ct="It's Dangerous to Go Alone", point={x=5026,y=4997,m=1449}},

        {Text="Deliver It's Dangerous to Go Alone back at Marshal's Refuge", Dt={q="It's Dangerous to Go Alone"}},

        {Text="Use yeti thing to scare Quixxil", UseItem="Umi's Mechanical Yeti", Proximity=5, point={x=4368,y=939,m=1449}},

        {Text="Run towards Silithus", Zone="Silithus", Proximity=50, point={x=3117,y=1966,m=1449}},
        {Text="Run to Silithus", Zone="Silithus", Proximity=50, point={x=2959,y=1593,m=1449}},
        {Text="Deliver Wasteland", Dt={q="Wasteland"}, point={x=8188,y=1894,m=1451}},
        {Text="Accept The Spirits of Southwind", At="The Spirits of Southwind", point={x=8188,y=1894,m=1451}},

        {Text="Discover the FP", Proximity=10, point={x=5058,y=3446,m=1451}},
        {Text="Deliver Taking Back Silithus (if it existed in IF)", Dt={q="Taking Back Silithus", SkipIfNotHaveQuest=1}, point={x=5115,y=3829,m=1451}},
        {Text="Accept and do whatever quest may exist here, probably none"},
        {Text="Loot Reliquary of Purity if you have the quest", Ct="A Reliquary of Purity", point={x=6323,y=5536,m=1451}},
        {Text="Complete The Spirits of Southwind", Ct="The Spirits of Southwind"},
        {Text="Deliver The Spirits of Southwind", Dt={q="The Spirits of Southwind"}, point={x=8187,y=1893,m=1451}},
        {Text="Accept Hive in the Tower", At="Hive in the Tower", point={x=8188,y=1894,m=1451}},
        {Text="Kill a Hive'Ashi Ambusher at top of tower & loot", Ct="Hive in the Tower", point={x=6020,y=5257,m=1451}},
        {Text="Deliver Hive in the Tower", Dt={q="Hive in the Tower"}, point={x=8187,y=1893,m=1451}},
        {Text="Accept Umber, Archivist", At="Umber, Archivist", point={x=8188,y=1894,m=1451}},
        
        {Text="When done with any other silithusquests, HS to Menethil", Zone="Wetlands", UseItem="Hearthstone"},
        {Text="Take boat to Auberdine", Zone="Darkshore", point={x=460,y=5715,m=1437}},
        {Text="Fly Rut'Theran", Taxi="Rut'theran Village, Teldrassil", point={x=3633,y=4557,m=1439}},
        {Text="Enter Darnassus", Zone="Darnassus"},
        {Text="Deliver Glyphed Oaken Branch (mid of tower)", Dt={q="Glyphed Oaken Branch", Item="Cerise Drape", Use=1}, point={x=3538,y=849,m=1457}},
        {Text="Run out of Darnassus, fly to Moonglade", Taxi="Moonglade"},
        {Text="Deliver Umber, Archivist", Dt={q="Umber, Archivist"}, point={x=4487,y=3559,m=1450}},
        {Text="Accept Uncovering Past Secrets", At="Uncovering Past Secrets", point={x=4487,y=3559,m=1450}},
        {Text="Deliver Uncovering Past Secrets", Dt={q="Uncovering Past Secrets", Item="Glowing Crystal Ring"}, point={x=5169,y=4510,m=1450}},
        {Text="Deliver A Reliquary of Purity", Dt={q="A Reliquary of Purity", SkipIfNotHaveQuest=1}, point={x=5169,y=4510,m=1450}},

        
        {Text="Fly to Everlook", Taxi="Everlook, Winterspring", point={x=4805,y=6734,m=1450}},
        {Text="Scare Legacki with yeti", Ct="Are We There, Yeti?", UseItem="Umi's Mechanical Yeti", point={x=6154,y=3861,m=1452}},
        {Text="Deliver Are We There, Yeti?", Dt={q="Are We There, Yeti?"}, point={x=6089,y=3763,m=1452}},

        {Text="No more quests. Options: Finish Blasted Lands, Deliver Blightcaller SW, finish Ony Preq, farm FR Jujus (Shardtooth) or smthing else"},
    }
}
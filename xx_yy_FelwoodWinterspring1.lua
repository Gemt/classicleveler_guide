-- Author      : G3m7
-- Create Date : 6/28/2019 10:20:26 PM


-- hinterlands feathers?

CLGuide_Felwood = {
    Title="??-?? Felwood/WS",
    Pinboard = {},
    Steps = {
        {Text="Accept Cleansing Felwood", At="Cleansing Felwood", point={x=5415,y=8683,m=1448}},
        {Text="Accept Forces of Jaedenar", At="Forces of Jaedenar", point={x=5121,y=8210,m=1448}},
        {Text="Accept The Corruption of the Jadefire", At="The Corruption of the Jadefire", point={x=5135,y=8151,m=1448}},
        -- req 53:
        {Text="Accept To Winterspring!", At="To Winterspring!", point={x=5096,y=8158,m=1448}},
        {Text="Accept Verifying the Corruption", At="Verifying the Corruption", point={x=5088,y=8162,m=1448}},
        
        {Text="Accept Timbermaw Ally (may not be in-game)", At="Timbermaw Ally", point={x=5093,y=8501,m=1448}},
        {Text=">Complete Timbermaw Ally (if existed)", Ct="Timbermaw Ally", point={x=4902,y=8941,m=1448}},
        {Text=">Deliver Timbermaw Ally (may not be in-game)", Dt={q="Timbermaw Ally"}, point={x=5093,y=8501,m=1448}},
        {Text=">Accept Speak to Nafien", At="Speak to Nafien", point={x=5093,y=8501,m=1448}},

        {Text="Complete The Corruption of the Jadefire (see next step for more mobs)", Ct="The Corruption of the Jadefire", point={x=4371,y=8438,m=1448}},
        {Text="Complete The Corruption of the Jadefire", Ct="The Corruption of the Jadefire", point={x=3768,y=6890,m=1448}},
        {Text="Xavathras at point", Ct="The Corruption of the Jadefire", point={x=3229,y=6709,m=1448}},

        {Text="Complete Forces of Jaedenar", Ct="Forces of Jaedenar", point={x=3844,y=5934,m=1448}},


        {Text="Deliver Forces of Jaedenar", Dt={q="Forces of Jaedenar"}, point={x=5121,y=8210,m=1448}},
        {Text="Accept Collection of the Corrupt Water", At="Collection of the Corrupt Water", point={x=5121,y=8210,m=1448}},


        {Text="Deliver The Corruption of the Jadefire", Dt={q="The Corruption of the Jadefire"}, point={x=5135,y=8151,m=1448}},
        {Text="Accept Further Corruption", At="Further Corruption", point={x=5135,y=8151,m=1448}},
        
        

        {Text="Complete Collection of the Corrupt Water", Ct="Collection of the Corrupt Water", UseItem="Empty Canteen", point={x=3517,y=5986,m=1448}},

        {Text="Complete Verifying the Corruption", Ct="Verifying the Corruption", point={x=4058,y=4183,m=1448}},

        {Text="Run to Jadefire Run", Proximity=50, point={x=4257,y=2007,m=1448}},
        {Text="Complete Further Corruption (start by killing Xavaric at point)", Ct="Further Corruption", point={x=3906,y=2237,m=1448},
            PinAdd="Loot Xavaric, accept quest from item he drops"},
        {Text="Make sure Flute of Xavaric is complete too (Q from Xavaric drop)", Ct="Flute of Xavaric"},

        {Text="Complete Cleansing Felwood", Ct="Cleansing Felwood", point={x=5585,y=1690,m=1448}},

        {Text="Discover FP", Proximity=5, point={x=6249,y=2424,m=1448}},
        {Text="Restock on Arrows if needed", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=2800}, point={x=6232,y=2564,m=1448}},







        {Text=">Deliver Speak to Nafien (if existed)", Dt={q="Speak to Nafien", SkipIfNotHaveQuest=1}, point={x=6477,y=813,m=1448}},
        {Text="Accept Deadwood of the North (may not exist)", At="Deadwood of the North", point={x=6477,y=813,m=1448}},
        {Text=">Complete Deadwood of the North (if existed)", Ct="Deadwood of the North", point={x=6299,y=884,m=1448}},
        {Text=">Deliver Deadwood of the North (if existed)", Dt={q="Deadwood of the North", SkipIfNotHaveQuest=1}, point={x=6477,y=813,m=1448}},
        {Text="Accept Speak to Salfa (may not exist)", At="Speak to Salfa", point={x=6477,y=813,m=1448}},

        {Text="Run through the tunnel, deliver Speak to Salfa", Dt={q="Speak to Salfa",SkipIfNotHaveQuest=1}, point={x=2773,y=3450,m=1452}},
        {Text="Accept Winterfall Activity (may not exist)", At="Winterfall Activity", point={x=2773,y=3450,m=1452}},

        -- donova snowden, far west WS
        {Text="Accept Threat of the Winterfall (may req. breadcrumb from everlook)", At="Threat of the Winterfall", point={x=3127,y=4517,m=1452}},
        {Text="Deliver The New Springs", Dt={q="The New Springs"}, point={x=3127,y=4517,m=1452}},
        {Text="Accept Strange Sources", At="Strange Sources", point={x=3127,y=4517,m=1452}},
        {Text="Deliver It's a Secret to Everybody", Dt={q="It's a Secret to Everybody"}, point={x=3127,y=4517,m=1452}},
        {Text="Accept The Videre Elixir (wait for RP)", At="The Videre Elixir", point={x=3127,y=4517,m=1452}},

        
        -- starfall village
        {Text="Deliver To Winterspring!", Dt={q="To Winterspring!"}, point={x=5197,y=3039,m=1452}},
        {Text="Accept The Ruins of Kel'Theril", At="The Ruins of Kel'Theril", point={x=5197,y=3039,m=1452}},
        {Text="Deliver The Ruins of Kel'Theril", Dt={q="The Ruins of Kel'Theril"}, point={x=5214,y=3043,m=1452}},
        -- req 53
        {Text="Accept Troubled Spirits of Kel'Theril", At="Troubled Spirits of Kel'Theril", point={x=5214,y=3043,m=1452}},
        --{Text="Deliver Enraged Wildkin", Dt={q="Enraged Wildkin"}, point={x=5214,y=3043,m=1452}},
        -- req 53
        {Text="Accept Enraged Wildkin (if not avil. need breadcrumb in everlook first)", At="Enraged Wildkin", point={x=5214,y=3043,m=1452}},


        -- everlook
        {Text="Accept Are We There, Yeti?", At="Are We There, Yeti?", point={x=6088,y=3762,m=1452},
            PinAdd="Don't accept Sister Pamela in Everlook"},
        {Text="Accept Enraged Wildkin", At="Enraged Wildkin", point={x=6112,y=3843,m=1452}},
        -- todo: requires burning steppes first
        -- req 54 (also to get quest from BS)
        {Text="Deliver Felnok Steelspring", Dt={q="Felnok Steelspring"}, point={x=6162,y=3861,m=1452}},
        {Text="Accept Chillwind Horns", At="Chillwind Horns", point={x=6162,y=3861,m=1452}, PinAdd="Kill any chimeras you see for horns now"},
        {Text="Accept Luck Be With You (req lvl 55)", At="Luck Be With You", point={x=6191,y=3830,m=1452}},

        {Text="Discover the FP", Proximity=5, point={x=6234,y=3660,m=1452}},
        {Text="Complete Winterfall Activity (if you have it). If not have, may still want 8 den watchers", Ct="Winterfall Activity", point={x=6628,y=3464,m=1452}},

        {Text="Complete Are We There, Yeti?", Ct="Are We There, Yeti?", point={x=6528,y=4100,m=1452}},

        -- southern moonkins
        {Text="Deliver Enraged Wildkin", Dt={q="Enraged Wildkin"}, point={x=5899,y=5979,m=1452}},
        {Text="Accept Enraged Wildkin", At="Enraged Wildkin", point={x=5899,y=5979,m=1452}},
        {Text="Deliver Enraged Wildkin", Dt={q="Enraged Wildkin"}, point={x=6141,y=6067,m=1452}},
        {Text="Accept Enraged Wildkin", At="Enraged Wildkin", point={x=6141,y=6067,m=1452}},
        {Text="Loot Jaron's Supplies on the ground", Item={Name="Jaron's Supplies", Count=1}, point={x=6140,y=6074,m=1452}},
        {Text="Kill moonkins for Blue-feathered Amulet to complete Enraged Wildkin", Ct="Enraged Wildkin", point={x=6377,y=5929,m=1452}},

        {Text="If it seems feasible to solo, complete Luck Be With You", Ct="Luck Be With You"},
        {Text="Run south until Strange Sources completes", Ct="Strange Sources", point={x=5975,y=7466,m=1452}},
        
        --lake
        {Text="Loot First Relic Fragment", Item={Name="First Relic Fragment", Count=1}, point={x=5515,y=4298,m=1452}},
        {Text="Loot Third Relic Fragment", Item={Name="Third Relic Fragment", Count=1}, point={x=5332,y=4344,m=1452}},
        {Text="Loot Fourth Relic Fragment", Item={Name="Fourth Relic Fragment", Count=1}, point={x=5243,y=4151,m=1452}},
        {Text="Loot Second Relic Fragment", Item={Name="Second Relic Fragment", Count=1}, point={x=5085,y=4171,m=1452}},

        {Text="Try to complete Chillwind Horns", Ct="Chillwind Horns"},

        -- everlook
        {Text="Deliver Are We There, Yeti?", Dt={q="Are We There, Yeti?"}, point={x=6088,y=3762,m=1452}},
        {Text="Deliver Chillwind Horns", Dt={q="Chillwind Horns"}, point={x=6163,y=3861,m=1452}},
        -- todo: will we ever deliver this and have space for it?
        {Text="Accept Return to Tinkee", At="Return to Tinkee", point={x=6163,y=3861,m=1452}},
        {Text="Deliver Luck Be With You if done, otherwise abandon", Dt={q="Luck Be With You"}, point={x=6191,y=3830,m=1452}},
        -- pick as late as possible. Probably full qlog
        {Text="Accept The Everlook Report", At="The Everlook Report", point={x=6135,y=3897,m=1452}},
        {Text="Accept Duke Nicholas Zverenhoff", At="Duke Nicholas Zverenhoff", point={x=6135,y=3897,m=1452}},
        

        -- starfall village
        {Text="Deliver Enraged Wildkin", Dt={q="Enraged Wildkin"}, point={x=5214,y=3043,m=1452}},

        {Text="Complete Threat of the Winterfall (there's another camp in NW corner)", Ct="Threat of the Winterfall", point={x=4030,y=4343,m=1452}},

        {Text="Deliver Strange Sources", Dt={q="Strange Sources", Item="Deep River Cloak", Use=1}, point={x=3127,y=4517,m=1452}},
        {Text="Deliver Threat of the Winterfall", Dt={q="Threat of the Winterfall"}, point={x=3127,y=4517,m=1452}},

        {Text="Deliver Winterfall Activity (if it was available)", Dt={q="Winterfall Activity", SkipIfNotHaveQuest=1}, point={x=2776,y=3449,m=1452}},
        


        {Text="Deliver Verifying the Corruption", Dt={q="Verifying the Corruption"}, point={x=5088,y=8162,m=1448}},

    }
}
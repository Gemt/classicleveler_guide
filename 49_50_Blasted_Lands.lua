-- Author      : G3m7
-- Create Date : 5/19/2019 9:15:49 AM
CLGuide_BlastedLands = {
    Title="49-50 Blasted Lands",
    Pinboard = {"Need to hit 50 in blasted lands (BB quest deliveries help"},
    Steps = {
        {Text="Enter tower in Nethergarde and deliver Thadius Grimshade (at the top)", Dt={q="Thadius Grimshade"}, point={x=6690,y=1947}},
        {Text="If >= lvl 50, accept Petty Squabbles", At="Petty Squabbles", point={x=6757,y=1930,m=1419}},
        
        
        {Text="Grind the mats in pinboard note", PinAdd="6 Blasted Boar Lung, 6 Scorpok Pincer, 14 Vulture Gizzard, 11 Basilisk Brain, 5 Snickerfang Jowl"},
        {Text="If you found any Imperfect Draenethyst Fragment, deliver at point", UseItem="Imperfect Draenethyst Fragment", point={x=5197,y=3568,m=1419}},
        {Text="If you found any Flawless Draenethyst Sphere, deliver at point", UseItem="Flawless Draenethyst Sphere", point={x=5197,y=3568,m=1419}},
        
      
        --[[
            The Decisive Striker(25agi)     3 Scorpok Pincer,       2 Vulture Gizzard,      1 Blasted Boar Lung
            Vulture's Vigor     (25spi)     10 Vulture Gizzard,     2 Snickerfang Jowl
            Basilisk's Bite     (25int)     10 Basilisk Brain,      2 Vulture Gizzards
            Snickerfang Jowls   (25str)     3 Snickerfang Jowl,     2 Blasted Boar Lung,    1 Scorpok Pincer
            A Boar's Vitality   (25stam)    3 Blasted Boar Lung,    2 Scorpok Pincer,       1 Basilisk Brain
            Total:
                6 Blasted Boar Lung
                6 Scorpok Pincer
                14 Vulture Gizzard
                11 Basilisk Brain
                5 Snickerfang Jowl
            4700 x 5 = 23500 xp 
            2300 scroll
            3900 deliver to mackinley
            4700 whiskey slim's lost grog
            (7300+2300 king mukla)
            Total: 34400 (44000) XP
        ]]


        {Text="Deliver all the quests at point", point={x=5062,y=1417,m=1419}},

        {Text=">Turn inn Petty Squabbles at Fallen Hero", Dt={q="Petty Squabbles"}, point={x=3430,y=6612,m=1435}},
        {Text=">Accept A Tale of Sorrow", At="A Tale of Sorrow", point={x=3430,y=6612,m=1435}},
        {Text=">Complete A Tale of Sorrow (hold shift to stop auto-gossip-cycle)", CycleGossip="A Tale of Sorrow", point={x=3430,y=6612,m=1435}},
        {Text=">Deliver A Tale of Sorrow (skip followup)", Dt={q="A Tale of Sorrow"}, point={x=3430,y=6612,m=1435}},

        -- todo: classes who can't train spells in Darnassus, fly to SW for training before going BB

        {Text="Fly to Booty Bay", Taxi="Booty Bay, Stranglethorn", point={x=6554,y=2433,m=1419}},
        {Text="If did not do bloodsail captains at 42-44, Accept The Bloodsail Buccaneers", At="The Bloodsail Buccaneers", point={x=2717,y=7701,m=1434}}, 

        {Text="Take pinboard items from bank.", 
            PutInBank={"Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth", "Hoard of the Black Dragonflight", "Black Dragonflight Molt"},
            TakeFromBank={"Super Snapper FX", "Snapshot of Gammerita", "Wildkin Feather", "Stoley's Bottle", "Pupellyverbos Port", "Carefully Folded Note", "Gorilla Fang"},
            point={x=2654,y=7657,m=1434},
            PinAdd="Take out: Stoley's Bottle, Pupellyverbos Port, Super Snapper FX, Snapshot of Gammerita, Wildkin Feathers, Carefully Folded Note", "Gorilla Fang"},
        
        {Text="Deliver Whiskey Slim's Lost Grog (inn)", Dt={q="Whiskey Slim's Lost Grog"}, point={x=2713,y=7745,m=1434},
            PinAdd="Can try the elite on isle west of BB as well if its up"},
        {Text="Deliver \"Deliver to MacKinley\"", Dt={q="Deliver to MacKinley", Item="Shinkicker Boots"}, point={x=2778,y=7707,m=1434}},
        
        {Text="If not yet done Stranglethorn Fever, accept it", At="Stranglethorn Fever", point={x=2761,y=7673,m=1434}},
        {Text="Complete Stranglethorn Fever", Ct="Stranglethorn Fever", point={x=3511,y=6068,m=1434},
            PinAdd="Need 10 Gorilla Fangs to summon"},

        {Text="If you have Carefully Folded Note, take quest and swm to isle east of BB. Can look for it along shore if not", UseItem="Carefully Folded Note",
            Dt={q="Message in a Bottle"}, point={x=3853,y=8058,m=1434}, PinAdd="See if anyone else needs to do Message in a Bottle Gorilla"},
        {Text="Accept Message in a Bottle", At="Message in a Bottle", point={x=3853,y=8058,m=1434}},
        {Text="Kite-kill & loot King Mukla", Ct="Message in a Bottle", point={x=4096,y=8387,m=1434}},
        {Text="Deliver Message in a Bottle", Dt={q="Message in a Bottle"}, point={x=3853,y=8058,m=1434},
            PinAdd="Delete the Carefully Folded Note now if still have it"},

        {Text="If did not complete Bloodsail captains at 42-44, do it now", Ct="The Bloodsail Buccaneers"},
        
        {Text="Deliver Stranglethorn Fever in BB if did it", Dt={q="Stranglethorn Fever", Item="Medicine Blanket", Use=1, SkipIfNotHaveQuest=1}, point={x=2760,y=7673,m=1434}},
        
        {Text="Deliver The Bloodsail Buccaneers if did (outside by Baron)", Dt={q="The Bloodsail Buccaneers", SkipIfNotHaveQuest=1}, point={x=2717,y=7701,m=1434}}, 
        
        {Text="Buy Arrows. Get plenty extra (like 10stacks or so)", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=4800}, point={x=2831,y=7457,m=1434}},
        
        {Text="Take the boat to Ratchet", Zone="The Barrens", point={x=2583,y=7310,m=1434}},

        --[[ The steps below are required if you plan on doing Azshara, but we'll try skipping that for now
        
        {Text="Hearthstone to Ironforge", UseItem="Hearthstone", Zone="Ironforge"},

        -- todo, can we just wait with acceping this, and instead take it from darnassus innkeeper when delivering soil samples in darna after ungoro?
        ---{Text="Accept Assisting Arch Druid Staghelm (req. lvl 50)", At="Assisting Arch Druid Staghelm", point={x=1813,y=5142}},
        {Text="Deliver Rise, Obsidion! (if you did it. Curator patrol in library)", Dt={q="Rise, Obsidion!",SkipIfNotHaveQuest=1}, point={x=7035,y=1762}},
        {Text="Accept A Little Slime Goes a Long Way (DONT OPEN THE CONTAINER YET)", At="A Little Slime Goes a Long Way", point={x=7579,y=2344}},
        {Text="Accept Passing the Burden (library)", At="Passing the Burden", point={x=7754,y=1184}},
        {Text="Deliver Passing the Burden (mystic ward)", Dt={q="Passing the Burden"}, point={x=3096,y=484}},
        {Text="Accept Arcane Runes", At="Arcane Runes", point={x=3096,y=484}},
        {Text="Accept An Easy Pickup", At="An Easy Pickup", point={x=3096,y=484}},
        {Text="Deliver An Easy Pickup (by BG masters)", Dt={q="An Easy Pickup"}, point={x=7086,y=9458}},
        {Text="Accept Signal for Pickup", At="Signal for Pickup", point={x=7086,y=9458}},
        {Text="Deliver Signal for Pickup", Dt={q="Signal for Pickup"}, point={x=7086,y=9458}, PinAdd="Make sure you now have the item: \"Standard Issue Flare Gun\""},

        
        {Text="Accept The Hunter's Charm", At="The Hunter's Charm", Class="Hunter", point={x=7088,y=8360}},
        -- todo: training
        
        -- todo: we don't need to pick smoldering ruins now right? would screw the questlog for ungoro

        {Text="Fly to Menethil Harbor", Taxi="Menethil Harbor, Wetlands", point={x=5549,y=4778}},
        {Text="Take boat to Darkshore (northern pier)", Zone="Darkshore", point={x=461,y=5716}},
        ]]
    }
}

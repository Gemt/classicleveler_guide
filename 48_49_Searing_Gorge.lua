-- Author      : G3m7
-- Create Date : 5/18/2019 6:28:52 PM

--[[
Questlog:
    Yuka Screwspigot
    Super Snapper fx
    favored of elune?
    whiskey slim's lost grog
    delivery to mackinley
    rise of the silitid 2
    sprinke's secret ingredient
    the stone circle
    thadius grimshade
    it's a secret to everybody
    (the apes of ungoro)

.quest add 4324
.quest add 2944
.quest add 3661
.quest add 580
.quest add 2874
.quest add 162
.quest add 2641
.quest add 3444
.quest add 2990
.quest add 3908
.quest add 4289
]]

CLGuide_SearingGorge = {
    Title="48-49 Searing Gorge",
    Pinboard = {},
    Steps = {
        --todo: train skills
        {Text="Bank - Need 15 silk for SG, can do cloth turnin if possible", 
            PutInBank={"Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth", "Violet Tragan", "Super Snapper FX", "Snapshot of Gammerita", "Wildkin Feather", "Pupellyverbos Port"},
            PinAdd="GET 15 SILK CLOTH FROM BANK. auto-banked: Violet Tragan, Super Snapper FX, Snapshot of Gammerita, Wildkin Feather, Pupellyverbos Port", 
            point={x=3525,y=6102,m=1455}},
        {Text="Extra idiot check that you have 15 Silk Cloth", Item={Name="Silk Cloth", Count=15}},
        
        {Text="Put HS in Ironforge", SetHs="Innkeeper Firebrew", point={x=1814,y=5142,m=1456}},

        {Text="Fly to Thelsamar, Loch Modan", Taxi="Thelsamar, Loch Modan", point={x=5551,y=4774,m=1456}},
        


        {Text="Run to Searing Gorge, either through badlands or through tunnel if some1 can open", Zone="Searing Gorge",
            PinAdd="If moving through tunnel, move the lvl 1 to SG side of tunnel after opening"},
        
        {Text="Kill Margol, 48 elite Kodo up in the hills", Item={Name="Margol's Horn", Count=1}, point={x=7159,y=7353,m=1427}},
        {Text="Accept The Horn of the Beast", UseItem="Margol's Horn", At="The Horn of the Beast"},

        {Text="Accept Caught! (at the toilet)", At="Caught!", point={x=6553,y=6224,m=1427}},
        {Text="Clear a bit, then accept Suntara Stones from \"dead\" dwarf", At="Suntara Stones", point={x=6393,y=6101,m=1427}},
        {Text="Escort the dwarf (4 mobs will spawn)", Ct="Suntara Stones", point={x=7445,y=1930,m=1427}},
        {Text="Deliver Suntara Stones (scroll on the ground)", Dt={q="Suntara Stones"}, point={x=7445,y=1930,m=1427}},
        {Text="Accept Suntara Stones", At="Suntara Stones", point={x=7445,y=1930,m=1427}},

        {Text="Deliver The Horn of the Beast. See pinboard", Dt={q="The Horn of the Beast"}, point={x=1821,y=8398,m=1432},
            PinAdd="If you have some1 to open gate at tunnel, go through it, otherwise go Thorium Point & fly thelsamar"},

        {Text="Accept Proof of Deed", At="Proof of Deed", point={x=1821,y=8398,m=1432}},
        {Text="Hearthstone to IF if ready, otherwise fly from Thelsamar", UseItem="Hearthstone", Zone="Ironforge", Taxi="Ironforge, Dun Morogh", point={x=3394,y=5095,m=1432}},
        {Text="Deliver Proof of Deed (Curator Thorius, patrolls Hall of Explorers)", Dt={q="Proof of Deed"}, point={x=7247,y=1559,m=1455}},
        {Text="Accept At Last!", At="At Last!"},
        {Text="Deliver Suntara Stones", Dt={q="Suntara Stones"}},
        {Text="Accept Dwarven Justice", At="Dwarven Justice"},
        {Text="Fly to Thelsamar", Taxi="Thelsamar, Loch Modan", point={x=5552,y=4774,m=1455}},
        {Text="Deliver At Last!", Dt={q="At Last!"}, point={x=1821,y=8398,m=1432}},
        {Text="Use your new key to open gate and run into Searing Gorge", Proximity=20, point={x=7881,y=1714,m=1427}},

        -- TODO: if we can spare 1 questlog space to the end of SG, wait with completing this until doing 4 towers,
        -- as we may be going to outhouse again then anyway
        {Text="Complete Caught!", Ct="Caught!", point={x=6919,y=3306,m=1427}},
        

        
        -- uncertain if we bother with ledger. Not sure it's worth doing the SoS and dustwallow trips....
        --{Text="Accept Ledger from Tanaris", At="Ledger from Tanaris", point={x=6553,y=6224,m=1427}},
        --{Text="Loot Goodsteel Ledger on the ground (might take a few sec b4 spawn)", Item={Name="Goodsteel Ledger", Count=1}, point={x=6553,y=6224,m=1427}},
        --{Text="Get 20 Solid Crystal Leg Shaft from spiders", Item={Name="Solid Crystal Leg Shaft", Count=20}, point={x=6220,y=7164,m=1427}},

        
        
        {Text="Accept Divine Retribution at point", At="Divine Retribution", point={x=3907,y=3899,m=1427}},
        {Text="Complete Divine Retribution (hold shift to stop auto-gossip-cycle)", CycleGossip="Divine Retribution", point={x=3907,y=3899,m=1427}},
        {Text="Deliver Divine Retribution", Dt={q="Divine Retribution"}, point={x=3907,y=3899,m=1427}},
        {Text="Accept The Flawless Flame", At="The Flawless Flame", point={x=3907,y=3899,m=1427}},
        
        
        {Text="Accept STOLEN: Smithing Tuyere and Lookout's Spyglass", At="STOLEN: Smithing Tuyere and Lookout's Spyglass", point={x=3763,y=2652,m=1427}},
        {Text="Accept JOB OPPORTUNITY: Culling the Competition", At="JOB OPPORTUNITY: Culling the Competition", point={x=3763,y=2652,m=1427}},
        {Text="Accept Curse These Fat Fingers", At="Curse These Fat Fingers", point={x=3858,y=2780,m=1427}},
        {Text="Accept Fiery Menace!", At="Fiery Menace!", point={x=3858,y=2780,m=1427}},
        {Text="Accept Incendosaurs? Whateverosaur is More Like It", At="Incendosaurs? Whateverosaur is More Like It", point={x=3858,y=2780,m=1427}},
        {Text="Accept What the Flux?", At="What the Flux?", point={x=3880,y=2848,m=1427}},
        {Text="Restock on arrows/vendor", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=3000}, point={x=3881,y=2854,m=1427}},

        -- flight+running takes a bit more than 4min to quest dude in loch, so if waiting for HS, 
        -- grind until <= 4min left on cd

        -- aerie peak -> IF = 5min
        -- arie peak running around should be about 3 min?
        -- IF running around about 2 min
        -- If to thelsamar 1:42
        -- thelsamar to SG run 4min?
        -- total travel roughly 16min
        -- 15min on first quest+escort+getting to TP, so about 30 min of grind needed for HS to be ready

        --{Text="Fly to Thelsamar", Taxi="Thelsamar, Loch Modan", point={x=3794,y=3087,m=1427}},
        --{Text="Deliver The Horn of the Beast", Dt={q="The Horn of the Beast"}, point={x=1821,y=8398,m=1432}},
        --{Text="Accept Proof of Deed", At="Proof of Deed", point={x=1821,y=8398,m=1432}},
        
        --{Text="Fly to Ironforge", Taxi="Ironforge, Dun Morogh", point={x=3394,y=5096,m=1432}},
        --{Text="Hearthstone to IF", UseItem="Hearthstone", Proximity=50, point={x=1902,y=5161,m=1455}},

        --{Text="Deliver Proof of Deed (Curator Thorius, patrolls Hall of Explorers)", Dt={q="Proof of Deed"}, point={x=7247,y=1559,m=1455}},
        --{Text="Accept At Last!", At="At Last!"},
        --{Text="Deliver Suntara Stones", Dt={q="Suntara Stones"}},
        --{Text="Accept Dwarven Justice", At="Dwarven Justice"},
        
        

        --{Text="Fly to Thelsamar", Taxi="Thelsamar, Loch Modan", point={x=5552,y=4774,m=1455}},
        --{Text="Deliver At Last!", Dt={q="At Last!"}, point={x=1821,y=8398,m=1432}},
        --{Text="Use your new key to open gate and run into Searing Gorge", Proximity=20, point={x=7881,y=1714,m=1427}},
        
        {Text="Kill Spiders/elementals western half to complete Flawless Flame & Fiery Menace", 
            Mct={"Fiery Menace!", "The Flawless Flame"}, point={x=3072,y=6452,m=1427}},
        
        {Text="Accept WANTED: Overseer Maltorius", At="WANTED: Overseer Maltorius", point={x=3763,y=2652,m=1427}},
        {Text="Deliver Fiery Menace!", Dt={q="Fiery Menace!", Item="Seared Mail Girdle", Use=1}, point={x=3857,y=2781,m=1427}},
        {Text="Restock on arrows", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=3000}, point={x=3881,y=2854,m=1427}},
        
        {Text="Deliver the Flawless Flame", Dt={q="The Flawless Flame"}, point={x=3906,y=3899,m=1427}},
        {Text="Accept Forging the Shaft", At="Forging the Shaft", point={x=3906,y=3899,m=1427}},

        {Text="Jump into the slag pit", Proximity=20, point={x=3534,y=4262,m=1427}},
        {Text="Deliver Dwarven Justice", Dt={q="Dwarven Justice"}, point={x=4113,y=2555,m=1427}},

        {Text="Accept Release Them", At="Release Them", point={x=4113,y=2555,m=1427}},
        {Text="Complete Forging the Shaft, Overseer Maltorius and Job Opportunity", 
            Mct={"Forging the Shaft", "WANTED: Overseer Maltorius","JOB OPPORTUNITY: Culling the Competition"}, point={x=4160,y=3558,m=1427},
            PinAdd="Remember to loot Secret Plans by Overseer Maltorius (second level, at point)"},

        {Text="From the bridge by Overseer Maltorius, jump down and complete Incendosaurs", Ct="Incendosaurs? Whateverosaur is More Like It", point={x=4790,y=4153,m=1427}},
        
        {Text="Run out of slag pit, west to point and take elevator up", Proximity=10, point={x=3961,y=5409,m=1427}},

        {Text="Deliver Forging the Shaft", Dt={q="Forging the Shaft"}, point={x=3907,y=3899,m=1427}},
        {Text="Accept The Flame's Casing", At="The Flame's Casing", point={x=3907,y=3899,m=1427},
            PinAdd="Also killing golems and Dark iron lookouts/steamsmiths now, but dont need to complete yet"},

        {Text="Deliver What the Flux?", Dt={q="What the Flux?"}, point={x=3881,y=2850,m=1427}},
        --{Text="Restock on arrows", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=3000}, point={x=3881,y=2854,m=1427}},
        {Text="Deliver Incendosaurs? Whateverosaur is More Like It", Dt={q="Incendosaurs? Whateverosaur is More Like It", Item="Luffa", Use=1}, point={x=3857,y=2781,m=1427}},
        {Text="Deliver JOB OPPORTUNITY: Culling the Competition", Dt={q="JOB OPPORTUNITY: Culling the Competition"}, point={x=3897,y=2752,m=1427}},
        {Text="Deliver WANTED: Overseer Maltorius", Dt={q="WANTED: Overseer Maltorius", Item="Seared Mail vest"}, point={x=3774,y=2654,m=1427}},
        
        {Text="Restock on arrows/vendor", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=3000}, point={x=3881,y=2854,m=1427}},

        {Text="Get to point, kill as few mobs as possible", Proximity=30, point={x=2497,y=3117,m=1427},
            PinAdd=">Prayer to elune & >Release them (item in lava) might be too hard solo. Skip and only do Flame's Casing in that case"},
        
        {Text="Accept Prayer to Elune. Kill as few mobs as possible getting there. See PinBoard note", At="Prayer to Elune", point={x=2951,y=2627,m=1427}},
        {Text=">Talk to the same npc to gossip", CycleGossip="Prayer to Elune", point={x=2951,y=2627,m=1427}},
        {Text=">Deliver Prayer to Elune", Dt={q="Prayer to Elune"}, point={x=2951,y=2627,m=1427}},
        {Text=">Accept Prayer to Elune", At="Prayer to Elune", point={x=2951,y=2627,m=1427}},
        {Text=">Loot thing in lava for Release Them (stand on top of it to loot)", Ct="Release Them", point={x=2910,y=2591,m=1427}},
        {Text=">Complete Prayer to Elune; drop from any elite in the area", Ct="Prayer to Elune"},

        {Text="Make sure you got Symbol of Ragnaros from any of the elites", Ct="The Flame's Casing"},

        {Text="Deliver The Flame's Casing", Dt={q="The Flame's Casing"}, point={x=3906,y=3899,m=1427}},
        {Text="Accept The Torch of Retribution", At="The Torch of Retribution"},
        {Text="Wait for RP, then deliver The Torch of Retribution", Dt={q="The Torch of Retribution"}, point={x=3905,y=3898,m=1427}},
        {Text="Accept The Torch of Retribution", At="The Torch of Retribution"},
        {Text="Deliver The Torch of Retribution on the staff", Dt={q="The Torch of Retribution"}},
        {Text="Accept Squire Maltrake", At="Squire Maltrake"},
        {Text="Deliver Squire Maltrake", Dt={q="Squire Maltrake"}},
        {Text="Accept Set Them Ablaze!", At="Set Them Ablaze!"},

        {Text="Equip Staff from Q, Light torch in first tower", Proximity=5, UseItem="Torch of Retribution", point={x=3330,y=5448,m=1427},
            PinAdd="Make sure to get the item from Lookouts + killing golems while doing towers now"},
        {Text="Second tower", Proximity=5, point={x=3567,y=6069,m=1427}},
        {Text="Third tower", Proximity=5, point={x=4404,y=6091,m=1427}},
        {Text="Deliver Caught! (at the toilet). Skip followup (ledger). If found outhousekey accept quest (itembutton)", Dt={q="Caught!"}, UseItem="Grimesilt Outhouse Key", point={x=6553,y=6224,m=1427}},
        {Text="If found Grimesilt Outhouse Key deliver at same toilet", Dt={q="The Key to Freedom", SkipIfNotHaveQuest=1}, point={x=6553,y=6224,m=1427}},

        {Text="Fourth tower", Proximity=5, Ct="Set Them Ablaze!", point={x=5006,y=5474,m=1427}},
        
        {Text="Complete STOLEN:.., Lookouts (east/south by 4th tower) and Steamsmiths (south of TP)", Ct="STOLEN: Smithing Tuyere and Lookout's Spyglass"},
        {Text="Complete Curse These Fat Fingers (war golems)", Ct="Curse These Fat Fingers"},

        {Text="Deliver Set Them Ablaze!", Dt={q="Set Them Ablaze!"}, point={x=3917,y=3901,m=1427},
            PinAdd="Re-equip your normal weapon, delete the staff"},
        {Text="Click the chest and accept Trinkets (may need to wait for RP)", At="Trinkets...", point={x=3884,y=3898,m=1427}},
        {Text="Click the chest again and turn in Trinkets", Dt={q="Trinkets..."}, point={x=3884,y=3898,m=1427},
            PinAdd="Open the box you got, but dont empty it, only check if any upgrade items in it"},

        {Text="Deliver Curse These Fat Fingers", Dt={q="Curse These Fat Fingers"}, point={x=3857,y=2781,m=1427}},
        {Text="Deliver STOLEN: Smithing Tuyere and Lookout's Spyglass", Dt={q="STOLEN: Smithing Tuyere and Lookout's Spyglass", Item="Slagplate Leggings"}, point={x=3897,y=2752,m=1427}},
        
        {Text="Restock on arrows/vendor", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=3000}, point={x=3881,y=2854,m=1427}},

        {Text="If completed Release Them, jump into slag Pit", Proximity=10, Dt={q="Release Them", SkipIfUncomplete=1}, point={x=3521,y=4254,m=1427}},
        {Text=">Deliver Release Them if completed", Dt={q="Release Them", SkipIfUncomplete=1}, point={x=4124,y=2539,m=1427}},
        {Text=">IF GROUPED, and release them complete, do Rise, Obsidion!, wait for rp and kill 2 elites"},

        {Text="HS to Ironforge", UseItem="Hearthstone", Zone="Ironforge"},
        
        {Text="Accept A Little Slime Goes a Long Way (DONT OPEN THE CONTAINER YET)", At="A Little Slime Goes a Long Way", point={x=7579,y=2344,m=1455}},

        {Text="If Rise Obsidion, deliver at hall of explorers", Dt={q="Rise, Obsidion!", SkipIfNotHaveQuest=1}, point={x=7247,y=1559,m=1455}},
        {Text="If 50, train Hunter Skills", Class="Hunter", TrainSkill=1, point={x=7087,y=8356,m=1455}},
        {Text="If 50, train pet Skills", Class="Hunter", TrainSkill=1, point={x=7087,y=8356,m=1455}},

        {Text="Fly to Blasted Lands. If you failed timed delivery in SoS section at 41-42, fly to Darkshire instead, take it, then fly Blasted Lands",
            PinAdd="Abandon Release Them if still have & incomplete",
            point={x=5551,y=4774,m=1455}},
    }
}
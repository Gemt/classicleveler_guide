-- Author      : G3m7
-- Create Date : 5/11/2019 7:51:10 AM

CLGuide_Southshore = {
    Title="30-32 Southshore",
    Pinboard = {},
    Steps = {
        {Text="Deliver Malin's Request", Dt={q="Malin's Request"}, point={x=4667,y=4701,m=1417}},
        {Text="Buy Razor Arrows", Class="Hunter", BuyItem={Item="Razor Arrow", Count=2800}, point={x=4645,y=4760,m=1417}},
        {Text="Accept Northfold Manor", At="Northfold Manor", point={x=4583,y=4756,m=1417}},
        
        {Text="Complete Northfold Manor", Ct="Northfold Manor", point={x=3045,y=2751,m=1417}},
        
        {Text="Grind to Southshore, Accept Costly Menace (at barn)", At="Costly Menace", point={x=5242,y=5596,m=1424}},
        {Text="Accept Soothing Turtle Bisque (inn, cook)", At="Soothing Turtle Bisque", point={x=5188,y=5868,m=1424}},
        {Text="Accept Down the Coast (inn)", At="Down the Coast", point={x=5146,y=5839,m=1424}},

        -- TODO: Is it the right call to set HS here, or do we need it elsewhere? 
        -- TODO: set/keep the HS in menethil or IF instead
        -- TODO: As a group you may want to do Crushridge Bounty as well, so you can do the elite version at 38 section
        --{Text="Set HS at Innkeeper Anderson", SetHs="Innkeeper Anderson", point={x=5117,y=5892}},
        {Text="Accept Hints of a New Plague? (outside Inn)", At="Hints of a New Plague?", point={x=5034,y=5905,m=1424}},
        {Text="Accept Syndicate Assassins (in town hall) (skip quest outside town hall)", At="Syndicate Assassins", point={x=4814,y=5911,m=1424}},
        
        {Text="Complete Down the Coast (kill murlocs on SW coast)", Ct="Down the Coast", point={x=4530,y=6667,m=1424}},
        {Text="Deliver Down the Coast (Inn)", Dt={q="Down the Coast"}, point={x=5146,y=5839,m=1424}},

        {Text="Accept Farren's Proof", At="Farren's Proof", point={x=5146,y=5839,m=1424}},
        {Text="Complete Farren's Proof (murloc heads from the same murlocs)", Ct="Farren's Proof", point={x=4530,y=6667,m=1424}},
        {Text="Deliver Farren's Proof (Inn)", Dt={q="Farren's Proof"}, point={x=5146,y=5839,m=1424}},
        {Text="Accept Farren's Proof pt2", At="Farren's Proof", point={x=5146,y=5839,m=1424}},

        {Text="Deliver Farren's Proof pt2 (outside Inn)", Dt={q="Farren's Proof"}, point={x=4947,y=5873,m=1424}},
        {Text="Accept Farren's Proof pt3", At="Farren's Proof", point={x=4947,y=5873,m=1424}},

        {Text="Deliver Farren's Proof (Inn)", Dt={q="Farren's Proof"}, point={x=5146,y=5839,m=1424}},
        {Text="Accept Stormwind Ho!", At="Stormwind Ho!", point={x=5146,y=5839,m=1424}},

        {Text="Complete Stormwind Ho! on the SE shore (nagas) (maxrange sirens to avoid CC)", Ct="Stormwind Ho!", point={x=5702,y=6719,m=1424}},

        {Text="Get 10 Turtle meat by following the river North", Item={Name="Turtle Meat", Count=10},
            PinAdd="There may be Hulking Lions to kill Around Cyclonian area" },

        {Text="Run into Alterac Mountains - Accept Foreboding Plans (northernmost syndicate camp, maybe also southern)", At="Foreboding Plans", point={x=5832,y=6795,m=1416}, 
            PinAdd="Make sure you actually get the 2 quests. Need 2 bagspace"},
        {Text="Accept Encrypted Letter (northernmost syndicate camp, maybe also southern)", At="Encrypted Letter", point={x=5832,y=6795,m=1416},
            PinAdd="Can vendor at alterac valley place (second furthest SW hill)"},
        
        {Text="Complete Syndicate Assassins and Costly Menace on the 4 different \"hills\"", 
            Mct={"Syndicate Assassins", "Costly Menace"}},
        
        {Text="Run into Southshore, yeti cave, and complete A King's Tribute (Loot alterac granite from ground)", Ct="A King's Tribute", point={x=4634,y=3192,m=1424}},
        {Text="Deathwarp to Southshore", DeathWarp=1},
              
        --{Text="Discover FP if not yet done", Proximity=10, point={x=4933,y=5226}},
        -- for turtle meats
        -- and 3 for dustwallow quest
        {Text="Buy Soothing Spices x4 from Micha Yance (3 for dustwallow)", BuyItem={Npc="Micha Yance", Item="Soothing Spices", Count=4}, point={x=4893,y=5503,m=1424}},
        {Text="Buy Razor Arrows (second floor)", Class="Hunter", BuyItem={Npc="Sarah Raycroft", Item="Razor Arrow", Count=2800}, point={x=4914,y=5505,m=1424}},
        {Text="Deliver Southshore to Loremaster Dibbs (skip followup)", Dt={q="Southshore"}, point={x=5057,y=5710,m=1424}},
        {Text="Deliver Encrypted Letter (same dude)", Dt={q="Encrypted Letter"}, point={x=5057,y=5710,m=1424}},
        {Text="Accept Letter to Stormpike", At="Letter to Stormpike", point={x=5057,y=5710,m=1424}},
        {Text="Deliver Costly Menace (at barn) (check that belt is upgrade)", Dt={q="Costly Menace", Item="Shepherd's Girdle", Use=1}, point={x=5242,y=5596,m=1424}},
        {Text="Deliver Stormwind Ho! (Inn)", Dt={q="Stormwind Ho!"}, point={x=5146,y=5839,m=1424}},
        {Text="Accept Reassignment", At="Reassignment", point={x=5146,y=5839,m=1424}},
        {Text="Turn inn Soothing Turtle Bisque (inn, cook)", Dt={q="Soothing Turtle Bisque"}, point={x=5188,y=5868,m=1424}},
        
        {Text="Turn inn Foreboding Plans (town hall) (skip noble death)", Dt={q="Foreboding Plans"}, point={x=4814,y=5911}},
        {Text="Turn inn Syndicate Assassins (town hall)", Dt={q="Syndicate Assassins", Item="Crusader Belt", Vendor="Crusader Belt"}, point={x=4814,y=5911,m=1424}},


        {Text="Fly to Arathi Highlands", Taxi="Refuge Pointe, Arathi", point={x=4934,y=5226,m=1424}},
        {Text="Deliver Northfold Manor (Refuge Pointe)", Dt={q="Northfold Manor"}, point={x=4584,y=4756,m=1417}},
        {Text="Deliver Hints of a New Plague?", Dt={q="Hints of a New Plague?"}, point={x=6019,y=5381,m=1417}},
        {Text="Accept Hints of a New Plague?", At="Hints of a New Plague?", point={x=6019,y=5381,m=1417}},
        {Text="Look for the forsaken courier from south of farm and on road back to Refuge Pointe. Skip if not found",
            PinAdd="5min respawn after killing all guards, 30min respawn if not killing the guards"},

        {Text="Hearthstone to Wetlands", UseItem="Hearthstone", Zone="Wetlands"},
    }
}
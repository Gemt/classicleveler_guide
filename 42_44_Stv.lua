-- Author      : G3m7
-- Create Date : 5/11/2019 12:39:45 PM

CLGuide_Stv2 = {
    Title="42-43 STV",
    Pinboard = {"Be on lookout for someone to help kill elite on isle west of BB"},
    Steps = {
        -- booty bay:
        -- we already have skullsplitter tusks in qlog from first run most likely
        -- We should already have the raptor/panther quests
        -- todo: need to get Sample Elven Gem from bank
        {Text="Accept Skullsplitter Tusks (Inn, 3rd floor)", At="Skullsplitter Tusks", point={x=2700,y=7712,m=1434}}, 
        {Text="Deliver Sunken Treasure (at fleet master) - Skip Followup", Dt={q="Sunken Treasure"}, point={x=2717,y=7701,m=1434}},
        {Text="Deliver Ansirem's Key (Inn, middle floor floor)", Dt={q="Ansirem's Key"}, point={x=2728,y=7753,m=1434}},
        {Text="Accept \"Pretty Boy\" Duncan", At="\"Pretty Boy\" Duncan", point={x=2728,y=7753,m=1434}}, 

        {Text="Set HS in BB (1st floor)", SetHs="Innkeeper Skindle", point={x=2704,y=7731,m=1434}},

        {Text="Accept Venture Company Mining (Inn, 1st floor)", At="Venture Company Mining", point={x=2712,y=7721,m=1434}},
        {Text="Accept Zanzil's Secret (Inn, 1st floor)", At="Zanzil's Secret", point={x=2712,y=7721,m=1434}},

        {Text="Accept Akiris by the Bundle (by bank)", At="Akiris by the Bundle", point={x=2676,y=7638,m=1434}},
        {Text="Bank any cloth you may have", PutInBank={"Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth"}, point={x=2654,y=7657,m=1434}},

        {Text="Accept Scaring Shaky", At="Scaring Shaky", point={x=2778,y=7707,m=1434}},
        -- wait with stoley's debt until leaving BB. Takes bagspace and questlog space
        --{Text="Accept Stoley's Debt", At="Stoley's Debt", point={x=2778,y=7707}}, 

        {Text="Accept Stranglethorn Fever", At="Stranglethorn Fever", point={x=2761,y=7674,m=1434}},
        {Text="Accept Excelsior (leatherwork croc quest place)", At="Excelsior", point={x=2829,y=7759,m=1434}},
        --excelsior (leatherworking place)
        {Text="Accept The Bloodsail Buccaneers", At="The Bloodsail Buccaneers", point={x=2809,y=7622,m=1434}},
        {Text="Buy Arrows/check for bow if still need", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=2800}, point={x=2831,y=7457,m=1434}},
        {Text="Accept The Captain's Chest", At="The Captain's Chest", point={x=2670,y=7361,m=1434}},

        {Text="Complete \"Pretty Boy\" Duncan (Exit BB, then left)", Ct="\"Pretty Boy\" Duncan", point={x=2739,y=6942,m=1434}},
        {Text="Deliver The Bloodsail Buccaneers (note at the barrel)", Dt={q="The Bloodsail Buccaneers"}, point={x=2728,y=6952,m=1434}},
        {Text="Accept The Bloodsail Buccaneers", At="The Bloodsail Buccaneers", point={x=2728,y=6952,m=1434}},

        {Text="Complete Scaring Shaky (drops from gorillas)", Ct="Scaring Shaky", point={x=3175,y=6752,m=1434}},

        {Text="Deliver Scaring Shaky (\"Shaky\" Phillipe, either out on pier, or patrol?)", Dt={q="Scaring Shaky"}, point={x=2689,y=7359,m=1434}},
        {Text="Accept Return to MacKinley", At="Return to MacKinley"}, 
        {Text="Deliver The Bloodsail Buccaneers", Dt={q="The Bloodsail Buccaneers"}, point={x=2809,y=7622,m=1434}},
        {Text="Accept The Bloodsail Buccaneers", At="The Bloodsail Buccaneers", point={x=2809,y=7622,m=1434}},
        {Text="Deliver Return to MacKinley", Dt={q="Return to MacKinley"}, point={x=2778,y=7707,m=1434}},
        {Text="Accept Voodoo Dues", At="Voodoo Dues", point={x=2778,y=7707,m=1434}},
         
        {Text="Deliver \"Pretty Boy\" Duncan (second floor Inn)", Dt={q="\"Pretty Boy\" Duncan"}, point={x=2728,y=7753,m=1434}},
        {Text="Accept The Curse of the Tides", At="The Curse of the Tides", point={x=2728,y=7753,m=1434}},
        {Text="Accept Up to Snuff (third floor)", At="Up to Snuff", point={x=2692,y=7735,m=1434}},
        {Text="Deliver The Bloodsail Buccaneers (at fleet master seahorn, by baron)", Dt={q="The Bloodsail Buccaneers"}, point={x=2717,y=7701,m=1434}},
        {Text="Accept The Bloodsail Buccaneers", At="The Bloodsail Buccaneers", point={x=2717,y=7701,m=1434}},
        {Text="Accept Keep An Eye Out (close to blacksmithing place, upper tier on way out)", At="Keep An Eye Out", point={x=2860,y=7589,m=1434}},

      

        {Text="Follow road north, then to SW of arena, complete Raptor Mastery (next step is E-NE of arena, work that direction)", Ct="Raptor Mastery", point={x=2746,y=4907,m=1434}},
        {Text="Complete Venture Company Mining (E-NE of arena)", Ct="Venture Company Mining", point={x=4070,y=4331,m=1434}},
        -- todo: point
        {Text="Complete Troll Witchery and Skullsplitter Tusks", Mct={"Troll Witchery", "Skullsplitter Tusks"}, point={x=4638,y=3782,m=1434},
        PinAdd="Skullsplitter Fetish only drops from Witch Doctors and Mystics"}, 
        {Text="Run north, search for Bhag'thera for Panther Mastery", Ct="Panther Mastery", point={x=4956,y=2408,m=1434}},

        {Text="Complete The Curse of the Tides (click altar under water at vile reef, kill 40 nonelite goblin)", Ct="The Curse of the Tides", point={x=2496,y=2358,m=1434},
            PinAdd="Look for elite crrocs for Excelsior"}, 
        {Text="Complete Excelsior (crocs along shore around Grom'Gol)", Ct="Excelsior"},

        {Text="Deliver Raptor Mastery pt3", Dt={q="Raptor Mastery"}, point={x=3566,y=1081,m=1434}},
        {Text="Accept Raptor Mastery pt4", At="Raptor Mastery", point={x=3566,y=1081,m=1434}},
        {Text="Deliver Panther Mastery pt4", Dt={q="Panther Mastery"}, point={x=3556,y=1054,m=1434}},

        {Text="Deliver Troll Witchery", Dt={q="Troll Witchery"}, point={x=3783,y=356,m=1434}},
        --{Text="Accept Mai'Zoth", At="Mai'Zoth", point={x=3783,y=356,m=1434}},
        {Text="Accept Colonel Kurzen (at rebel camp)", At="Colonel Kurzen", point={x=3804,y=301,m=1434}},
        {Text="Restock arrows", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=2800}, point={x=3796,y=299,m=1434}},
        {Text="Complete Colonel Kurzen (furthest in the Kurzen cave)", Ct="Colonel Kurzen", point={x=4981,y=398,m=1434}},
        {Text="Deliver Colonel Kurzen (at rebel camp)", Dt={q="Colonel Kurzen"}, point={x=3804,y=301,m=1434}},
        
        -- todo: maybe hearthstone after completing kurzen in cave, and deliver it after delivering mai'zoth (if soloable)
        {Text="Hearthstone to BB", UseItem="Hearthstone", Proximity=20, point={x=2707,y=7736,m=1434}},
        
        {Text="Deliver Venture Company Mining (next to innkeeper)", Dt={q="Venture Company Mining", Item="Silver Spade"}, point={x=2712,y=7721,m=1434}},
        {Text="Deliver Skullsplitter Tusks (Third floor)", Dt={q="Skullsplitter Tusks"}, point={x=2700,y=7713,m=1434}},
        {Text="Deliver The Curse of the Tides (Baron outside)", Dt={q="The Curse of the Tides", Item="Robe of Crystal Waters", Use=1}, point={x=2723,y=7687,m=1434}},
        
        {Text="Drop off cloth in bank. Get complete, or nearly complete STV Page sets out", 
            PutInBank={"Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth", "Green Hills of Stranglethorn"},
            point={x=2655,y=7657,m=1434}},

        

        {Text="Deliver Excelsior (leatherwork croc quest place)", Dt={q="Excelsior"}, point={x=2829,y=7759,m=1434}}, 

        {Text="If wont be 44 by end of STV2, get Cortellos riddle at point now while working on Snuff (middle floor inside ship). If already 43++ (or 2man+) skip to next step)", Item={Name="Cortello's Riddle", Count=1}, point={x=3364,y=8837,m=1434}},
        {Text="Complete Snuff (drop), Orders/charts/kills & Dizzy's Eye", 
            Mct={"Up to Snuff", "Keep An Eye Out", "The Bloodsail Buccaneers"}, point={x=3293,y=7420,m=1434},
            PinAdd={"Up to Snuff, Keep An Eye Out & The Bloodsail Buccaneers. Orders/Charts can spawn in both camps on mainland", "Look for Half-Buried Bottle"}},
        -- todo: stranglethorn fever?

        -- todo: run eastern shore to zanzil's mixtures, or go by akiris by the bundle first? 
        {Text="Complete Akiris by the Bundle", Ct="Akiris by the Bundle", point={x=2462,y=6400,m=1434}},
        
        
        {Text="Kill & loot Jon-Jon the Crow for Voodoo Dues", Item={Name="Jon-Jon's Golden Spyglass", Count=1}, point={x=3492,y=5184,m=1434},
            PinAdd="Clear out camp for Zanzil's Mixtures"}, 
        {Text="Kill & loot Maury \"Club Foot\" Wilkins for Voodoo Dues", Item={Name="Maury's Clubbed Foot", Count=1}, point={x=3527,y=5113,m=1434}},
        {Text="Kill & loot Chucky \"Ten Thumbs\" for Voodoo Dues", Item={Name="Chucky's Huge Ring", Count=1}, point={x=4009,y=5819,m=1434}},
        {Text="Complete Zanzil's Secret", Ct="Zanzil's Secret"},
        
        --{Text="Complete Zanzil's Secret now unless terrible luck", Ct="Zanzil's Secret"},

        {Text="Complete Raptor Mastery (Talon of Tethis)", Ct="Raptor Mastery", point={x=2874,y=4474,m=1434}},
        
        {Text="Deliver Raptor Mastery", Dt={q="Raptor Mastery"}, point={x=3566,y=1081,m=1434}},
        {Text="Accept Big Game Hunter", At="Big Game Hunter", point={x=3566,y=1081,m=1434}},
        
        
        {Text="Complete Big Game Hunter", Ct="Big Game Hunter", point={x=3819,y=3556,m=1434}},
            --PinAdd="Look for group for BGH and/or Cracking Maury's Foot & Mai'Zoth"},

        {Text="Deliver Big Game Hunter", Dt={q="Big Game Hunter", Item="Master Hunter's Bow", Use=1}, point={x=3566,y=1081,m=1434}},
        --{Text="Deliver Mai'Zoth at rebel camp (if you did it, otherwise abandon)", Dt={q="Mai'Zoth"}, point={x=3783,y=356,m=1434}},

        {Text="Hearthstone to BB", UseItem="Hearthstone", Proximity=20, point={x=2707,y=7736,m=1434}},


        {Text="Deliver Zanzil's Secret if completed (next to innkeeper)", Dt={q="Zanzil's Secret"}, point={x=2712,y=7721,m=1434}}, 
        {Text="Deliver Up to Snuff (3rd floor)", Dt={q="Up to Snuff"}, point={x=2692,y=7735,m=1434}},
        {Text="Accept Tran'rek", At="Tran'rek", point={x=2694,y=7721,m=1434}}, 
        {Text="Deliver The Bloodsail Buccaneers (outside by Baron)", Dt={q="The Bloodsail Buccaneers"}, point={x=2717,y=7701,m=1434}}, 
        {Text="Accept The Bloodsail Buccaneers", At="The Bloodsail Buccaneers", point={x=2717,y=7701,m=1434}}, 
        {Text="Accept Goblin Sponsorship", At="Goblin Sponsorship", point={x=2722,y=7687,m=1434}},

        {Text="Deliver Akiris by the Bundle if you did it (by the bank)", Dt={q="Akiris by the Bundle", Item="Scorching Sash", Use=1}, point={x=2676,y=7638,m=1434}}, 
        {Text="Accept Akiris by the Bundle", At="Akiris by the Bundle", point={x=2676,y=7638,m=1434}}, 
        {Text="Drop off cloth, leftover pages and Carefully folded note, if found, in bank", 
            PutInBank={"Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth", "Green Hills of Stranglethorn", "Carefully Folded Note"},
            point={x=2655,y=7657,m=1434}},


        -- todo: this is the place where we have to figure out if we got enough pages to deliver the stv page quests
        {Text="Deliver Voodoo Dues", Dt={q="Voodoo Dues"}, point={x=2778,y=7707,m=1434}}, 
        --{Text="Accept Cracking Maury's Foot (wait for RP)", At="Cracking Maury's Foot", point={x=2778,y=7707,m=1434}}, 
        {Text="Accept Stoley's Debt", At="Stoley's Debt", point={x=2778,y=7707,m=1434}}, 
        --{Text="Deliver Cracking Maury's Foot (if you did it)", Dt={q="Cracking Maury's Foot"}, point={x=2778,y=7707,m=1434}}, 

        {Text="Deliver Keep An Eye Out (close to blacksmithing place, upper tier on way out)", Dt={q="Keep An Eye Out"}, point={x=2860,y=7589,m=1434},
            PinAdd="Probably use this cloak"}, 

        {Text="Kill 3 captains for Bloodsail Buccaneers and find Cortello's Riddle. Skip this if solo < 43-44", Ct="The Bloodsail Buccaneers", point={x=3342,y=8819,m=1434},
            PinAdd="Look for Cortello's Riddle in each of the 3 ships. If you're skipping but still didnt take cortellos riddle go take it now"},
        
        {Text="Kite-kill Gorlash back to: North if next step; BB if skipping", Ct="The Captain's Chest", point={x=3662,y=6940,m=1434}},

        {Text="Complete Stranglethorn Fever if 2+man", Ct="Stranglethorn Fever", point={x=3511,y=6068,m=1434},
            PinAdd="Need 10 Gorilla Fangs to summon"},
        
        {Text="Buy Arrows/check for bow if still need", Class="Hunter", BuyItem={Item="Jagged Arrow", Count=2800}, point={x=2831,y=7457,m=1434}},
        {Text="Deliver Stranglethorn Fever/abandon if skipped", Dt={q="Stranglethorn Fever", Item="Medicine Blanket", Use=1}, point={x=2760,y=76731,m=1434}},

        {Text="Accept Whiskey Slim's Lost Grog (oposite to Innkeeper)", At="Whiskey Slim's Lost Grog", point={x=2713,y=7745,m=1434}},
        {Text="Deliver The Bloodsail Buccaneers (Baron, 3rd floor Inn)", Dt={q="The Bloodsail Buccaneers", Item="Blackwater Tunic", Use=1}, point={x=2717,y=7701,m=1434}}, 

        
        
        -- SoS cortello's riddle & eastern shore
        {Text="Fly to Blasted Lands (Fly Darkshire for timed to nethergarde if failed earlier)", point={x=2753,y=7778,m=1434}},
        
        {Text="Run to SoS, Accept Driftwood", At="Driftwood", point={x=2675,y=5983,m=1435}},
        {Text="Accept & Turn in Cortello's Riddle under bridge west", UseItem="Cortello's Riddle", Dt={q="Cortello's Riddle"}, point={x=2287,y=4819,m=1435}},
        {Text="Accept Cortello's Riddle pt2", At="Cortello's Riddle", point={x=2287,y=4819,m=1435}},
        
        {Text="Complete ... and bugs, and Driftwood", Mct={"... and Bugs", "Driftwood"}, point={x=9477,y=4573,m=1435}},
        {Text="Grind until HS CD is < 2 min, then Deathwarp to Stonard", DeathWarp=1},
        {Text="Deliver Driftwood", Dt={q="Driftwood"}, point={x=2675,y=5983,m=1435}},
        {Text="Accept Deliver the Shipment", At="Deliver the Shipment", point={x=2675,y=5983,m=1435}},
        {Text="Run into Blasted Lands", Zone="Blasted Lands", point={x=3346,y=6687,m=1435}},
        {Text="Deliver Deliver the Shipment", Dt={q="Deliver the Shipment"}, point={x=6652,y=2138,m=1419}},
        
        {Text="Hearthstone to BB", UseItem="Hearthstone", Proximity=20, point={x=2707,y=7736,m=1434}},
        



        {Text="Bank all Cloth and Carefully Folded Note", PutInBank={"Silk Cloth", "Wool Cloth", "Mageweave Cloth", "Runecloth", "Carefully Folded Note", "Gorilla Fang"}, point={x=2655,y=7657,m=1434}},

        {Text="Deliver The Captain's Chest", Dt={q="The Captain's Chest"}, point={x=2669,y=7361,m=1434}},
        {Text="Take boat to Ratchet", Zone="The Barrens", point={x=2587,y=7311,m=1434}},
        
    }
}